Require Import List String.
Require OrderedType.

Declare Scope module_scope.

Module Type LanguageSig.

(** * Language Variables *)
(** modules, types and variables identifiers *)
Parameter modules types variables: Type.
Notation M := modules.
Notation T := types.
Notation V := variables.

(** Constants *)
Parameter constants : Type.
Notation C := constants.

(** Locations *)
Parameter loc : Type.
Notation ℒ := loc.

(** Decidable equality on locations and variables *)
Parameter loc_eq_dec : forall (l1 l2 : ℒ), {l1 = l2} + {l1 <> l2}.
Parameter mod_eq_dec : forall (ι1 ι2 : M), {ι1 = ι2} + {ι1 <> ι2}.
Parameter typ_eq_dec : forall (ι1 ι2 : T), {ι1 = ι2} + {ι1 <> ι2}.
Parameter val_eq_dec : forall (ι1 ι2 : V), {ι1 = ι2} + {ι1 <> ι2}.

(** Pretty printing functions *)
Parameter print_module : modules -> string.
Parameter print_type : types -> string.
Parameter print_variable : variables -> string.
Parameter print_constant : constants -> string.
Parameter print_loc : loc -> string.

(* Required for efficient finite maps *)
Parameter lt : ℒ -> ℒ -> Prop.
Parameter lt_trans : forall x y z : ℒ, lt x y -> lt y z -> lt x z.
Parameter lt_not_eq : forall x y : ℒ, lt x y -> ~ eq x y.
Parameter compare : forall x y : ℒ, OrderedType.Compare lt eq x y.

End LanguageSig.

(** * Module System Definition *)
Module Modules (Import L : LanguageSig).

Inductive var := IM : M -> var | IT : T -> var | IV : V -> var.
Notation ℐ := var.

Definition var_eq_dec : forall (ι1 ι2 : ℐ), {ι1 = ι2} + {ι1 <> ι2}.
Proof.
intros [M1 | T1 | V1] [M2 | T2 | V2];
try (solve[right; intro H; inversion H]);
[destruct (mod_eq_dec M1 M2) |
 destruct (typ_eq_dec T1 T2) |
 destruct (val_eq_dec V1 V2)];
solve[left; f_equal; trivial] ||
(right; intro H; inversion H; tauto).
Defined.

Definition mloc : Type := (M * ℒ).
Definition tloc : Type := (T * ℒ).
Definition vloc : Type := (V * ℒ).

(** ** Module Paths *)
Inductive m_path : Type :=
| M_id :> mloc -> m_path (* module identifier *)
| M_comp : m_path -> mloc -> m_path. (* access to a module component *)

Local Notation "p ⋅¹ x" := (M_comp p x) (at level 40) : module_scope.

(** ** Extended Module Paths *)
Inductive em_path : Type :=
| EM_id :> mloc -> em_path (* module identifier *)
| EM_comp : em_path -> mloc -> em_path (* access to a module component *)
| EM_app : em_path -> em_path -> em_path. (* functor application *)

Local Notation "q ⋅² x" := (EM_comp q x) (at level 40) : module_scope.

Local Notation "q '(' q2 ')'" := (EM_app q q2) (at level 40, right associativity) : module_scope.

(** ** Value expressions *)
Inductive v_exp : Type :=
| V_id :> vloc -> v_exp (* value identifier *)
| V_field : m_path -> vloc -> v_exp (* value field of a structure *)
| V_const :> C -> v_exp (* constant *)
| V_let : vloc -> v_exp -> v_exp -> v_exp (* let-binding *)
| V_fun : vloc -> v_exp-> v_exp (* lambda abstraction *)
| V_app : v_exp -> v_exp -> v_exp (* application *)
| V_open : m_path -> v_exp -> v_exp. (* module open *)

Local Notation "p ⋅ v" := (V_field p v) (at level 40) : module_scope.
Local Notation "'vlet' v '==' e 'in' e2" := (V_let v e e2) (at level 210) : module_scope.
Local Notation "'fn' v '-->' e" := (V_fun v e) (at level 40) : module_scope.

(** ** Module Types *)
Inductive m_type : Type :=
| MT_id : tloc -> m_type (* module type identifier *)
| MT_field : em_path -> tloc -> m_type (* module type field in a structure *)
| MT_sig : (list sig_comp) -> m_type (* signature *)
| MT_functor : mloc -> m_type -> m_type -> m_type (* functor type *)
| MT_constr : m_type -> mloc -> em_path -> m_type(* module constraint *)
| MT_dsubst : m_type -> mloc -> em_path -> m_type  (* destructive module substitution *)
| MT_extr : m_exp -> m_type (* module type extraction *)
(** _Signature Body_ = lists of _signature components_ *)
(** _Signature Components_  *)
with sig_comp : Type :=
| Sig_val : vloc -> sig_comp (* value specification *)
| Sig_m : mloc -> m_type -> sig_comp (* module specification *)
| Sig_alias : mloc -> m_path -> sig_comp (* module alias specification *)
| Sig_abs : tloc -> sig_comp (* abstract module type specification *)
| Sig_concr : tloc -> m_type -> sig_comp (* concrete module type specification *)
| Sig_incl : m_type -> sig_comp (* signature include *)
| Sig_open : m_path -> sig_comp
(** ** Module Expressions *)
with m_exp : Type :=
| M_path :> m_path -> m_exp (* module path *)
| M_struct : (list str_comp) -> m_exp (* structure *)
| M_functor : mloc -> m_type -> m_exp -> m_exp (* functor *)
| M_app : m_exp -> m_exp -> m_exp (* functor application *)
| M_tannot : m_exp -> m_type -> m_exp (* module type annotation *)
(** _Structure Body_ = lists of _structure components_ *)
(** _Structure Components_ *)
with str_comp : Type :=
| Str_let : vloc -> v_exp -> str_comp (* value definition *)
| Str_let_ : v_exp -> str_comp (* anonymous expression *)
| Str_mdef : mloc -> m_exp -> str_comp (* module definition *)
| Str_mtdef : tloc -> m_type -> str_comp (* module type definition *)
| Str_incl : m_exp -> str_comp (* module include *)
| Str_open : m_path -> str_comp. (* module open *)

(** Strong induction principle for modules and module types *)
Definition module_ind P' P0' P P0 :
 (forall t : tloc, P (MT_id t)) ->
 (forall (m : em_path) (t : tloc), P (MT_field m t)) ->
 (forall (m : mloc) (m0 : m_type),
  P m0 -> forall m1 : m_type, P m1 -> P (MT_functor m m0 m1)) ->
 (forall m : m_type,
  P m -> forall (m0 : mloc) (e : em_path), P (MT_constr m m0 e)) ->
 (forall m : m_type,
  P m -> forall (m0 : mloc) (e : em_path), P (MT_dsubst m m0 e)) ->
 (forall l : list sig_comp, P0 l -> P (MT_sig l)) ->
 (forall m, P' m -> P (MT_extr m)) ->
 (forall ls (v : vloc), P0 ls -> P0 (Sig_val v :: ls)) ->
 (forall ls (m : mloc) (m0 : m_type), P0 ls -> P m0 -> P0 (Sig_m m m0 :: ls)) ->
 (forall ls (m : mloc) (m0 : m_path), P0 ls -> P0 (Sig_alias m m0 :: ls)) ->
 (forall ls (t : tloc), P0 ls -> P0 (Sig_abs t :: ls)) ->
 (forall ls (t : tloc) (m : m_type), P0 ls -> P m -> P0 (Sig_concr t m :: ls)) ->
 (forall ls (m : m_type), P0 ls -> P m -> P0 (Sig_incl m :: ls)) ->
 (P0 nil) ->
 (forall ls p, P0 ls -> P0 (Sig_open p :: ls)) ->
 (forall (v : vloc) (v0 : v_exp) ls, P0' ls -> P0' (Str_let v v0 :: ls)) ->
 (forall (v0 : v_exp) ls, P0' ls -> P0' (Str_let_ v0 :: ls)) ->
 (forall (m : mloc) (m0 : m_exp) ls, P0' ls -> P' m0 -> P0' (Str_mdef m m0 :: ls)) ->
 (forall (t : tloc) (m : m_type) ls, P0' ls -> P m -> P0' (Str_mtdef t m :: ls)) ->
 (forall m ls, P' m -> P0' ls -> P0' (Str_incl m :: ls)) ->
 (forall m : m_path, P' m) ->
 (forall (m : mloc) (m0 : m_type) (m1 : m_exp), P m0 -> P' m1 -> P' (M_functor m m0 m1)) ->
 (forall m : m_exp, P' m -> forall m0 : m_exp, P' m0 -> P' (M_app m m0)) ->
 (forall m m0, P' m -> P m0 -> P' (M_tannot m m0)) ->
 (forall ls, P0' ls -> P' (M_struct ls)) ->
 (P0' nil) ->
 (forall ls p, P0' ls -> P0' (Str_open p :: ls)) ->
   (forall p : m_exp, P' p) * (forall ls, P0' ls) *
   (forall p : m_type, P p) * (forall ls, P0 ls).
Proof.
exact(
fun f1 f2 f3 f4 f5 f6 f6' f7 f8 f9 f10 f11 f12 f13 f14 g1 g1' g2 g3 g4 g5 g6 g7 g8 g9 g10 g11 =>
let m_ind := (fix m_ind p :=
let s_ind : forall ls', P0' ls' := (fix s_ind ls' :=
let t_ind := (fix t_ind p :=
let S_ind : forall ls, P0 ls := fix S_ind ls :=
  match ls with
  | nil => f13
  | Sig_val vl :: ls => f7 ls vl (S_ind ls)
  | Sig_m ml t :: ls => f8 ls ml t (S_ind ls) (t_ind t)
  | Sig_alias ml p :: ls => f9 ls ml p (S_ind ls)
  | Sig_abs tl :: ls => f10 ls tl (S_ind ls)
  | Sig_concr tl m :: ls => f11 ls tl m (S_ind ls) (t_ind m)
  | Sig_incl t :: ls => f12 ls t (S_ind ls) (t_ind t)
  | Sig_open p :: ls => f14 ls p (S_ind ls)
  end in
 (match p with
  | MT_id ml => f1 ml
  | MT_field p tl => f2 p tl
  | MT_functor ml t1 t2 => f3 ml t1 (t_ind t1) t2 (t_ind t2)
  | MT_constr t ml p => f4 t (t_ind t) ml p
  | MT_dsubst t ml p => f5 t (t_ind t) ml p
  | MT_sig l => f6 l (S_ind l)
  | MT_extr m => f6' m (m_ind m)
end)) in
  match ls' with
  | nil => g10
  | Str_let vl e :: ls' => g1 vl e ls' (s_ind ls')
  | Str_let_ e :: ls' => g1' e ls' (s_ind ls')
  | Str_mdef ml p :: ls' => g2 ml p ls' (s_ind ls') (m_ind p)
  | Str_mtdef tl m :: ls' => g3 tl m ls' (s_ind ls') (t_ind m)
  | Str_incl m :: ls' => g4 m ls' (m_ind m) (s_ind ls')
  | Str_open p :: ls' => g11 ls' p (s_ind ls')
  end) in
let t_ind := (fix t_ind p :=
let S_ind : forall ls, P0 ls := fix S_ind ls :=
  match ls with
  | nil => f13
  | Sig_val vl :: ls => f7 ls vl (S_ind ls)
  | Sig_m ml t :: ls => f8 ls ml t (S_ind ls) (t_ind t)
  | Sig_alias ml p :: ls => f9 ls ml p (S_ind ls)
  | Sig_abs tl :: ls => f10 ls tl (S_ind ls)
  | Sig_concr tl m :: ls => f11 ls tl m (S_ind ls) (t_ind m)
  | Sig_incl t :: ls => f12 ls t (S_ind ls) (t_ind t)
  | Sig_open p :: ls => f14 ls p (S_ind ls)
  end in
 (match p with
  | MT_id ml => f1 ml
  | MT_field p tl => f2 p tl
  | MT_functor ml t1 t2 => f3 ml t1 (t_ind t1) t2 (t_ind t2)
  | MT_constr t ml p => f4 t (t_ind t) ml p
  | MT_dsubst t ml p => f5 t (t_ind t) ml p
  | MT_sig l => f6 l (S_ind l)
  | MT_extr m => f6' m (m_ind m)
end)) in
 (match p with
  | M_path mp => g5 mp
  | M_functor ml t e => g6 ml t e (t_ind t) (m_ind e)
  | M_app e1 e2 => g7 e1 (m_ind e1) e2 (m_ind e2)
  | M_tannot m t => g8 m t (m_ind m) (t_ind t)
  | M_struct l => g9 l (s_ind l)
end)) in

let t_ind := (fix t_ind p :=
let S_ind : forall ls, P0 ls := (fix S_ind ls :=
  match ls with
  | nil => f13
  | Sig_val vl :: ls => f7 ls vl (S_ind ls)
  | Sig_m ml t :: ls => f8 ls ml t (S_ind ls) (t_ind t)
  | Sig_alias ml p :: ls => f9 ls ml p (S_ind ls)
  | Sig_abs tl :: ls => f10 ls tl (S_ind ls)
  | Sig_concr tl m :: ls => f11 ls tl m (S_ind ls) (t_ind m)
  | Sig_incl t :: ls => f12 ls t (S_ind ls) (t_ind t)
  | Sig_open p :: ls => f14 ls p (S_ind ls)
  end) in
  match p with
  | MT_id ml => f1 ml
  | MT_field p tl => f2 p tl
  | MT_functor ml t1 t2 => f3 ml t1 (t_ind t1) t2 (t_ind t2)
  | MT_constr t ml p => f4 t (t_ind t) ml p
  | MT_dsubst t ml p => f5 t (t_ind t) ml p
  | MT_sig l => f6 l (S_ind l)
  | MT_extr m => f6' m (m_ind m)
  end) in

let s_ind : forall ls', P0' ls' := (fix s_ind ls' :=
  match ls' with
  | nil => g10
  | Str_let vl e :: ls' => g1 vl e ls' (s_ind ls')
  | Str_let_ e :: ls' => g1' e ls' (s_ind ls')
  | Str_mdef ml p :: ls' => g2 ml p ls' (s_ind ls') (m_ind p)
  | Str_mtdef tl m :: ls' => g3 tl m ls' (s_ind ls') (t_ind m)
  | Str_incl m :: ls' => g4 m ls' (m_ind m) (s_ind ls')
  | Str_open p :: ls' => g11 ls' p (s_ind ls')
  end) in
let S_ind : forall ls, P0 ls := (fix S_ind ls :=
  match ls with
  | nil => f13
  | Sig_val vl :: ls => f7 ls vl (S_ind ls)
  | Sig_m ml t :: ls => f8 ls ml t (S_ind ls) (t_ind t)
  | Sig_alias ml p :: ls => f9 ls ml p (S_ind ls)
  | Sig_abs tl :: ls => f10 ls tl (S_ind ls)
  | Sig_concr tl m :: ls => f11 ls tl m (S_ind ls) (t_ind m)
  | Sig_incl t :: ls => f12 ls t (S_ind ls) (t_ind t)
  | Sig_open p :: ls => f14 ls p (S_ind ls)
  end) in

    (m_ind, s_ind, t_ind, S_ind)).
Defined.

Local Notation "'val' v ':_'" := (Sig_val v) (at level 200) : module_scope.
Local Notation "'struct' s 'end'" := (M_struct s) (at level 200) : module_scope.
Local Notation "'struct' d1 ; .. ; dn 'end'" := (M_struct (cons d1 .. (cons dn nil) ..)) (at level 200) : module_scope.
Local Notation "'functor' '(' x ':' m1 ')' '→' m2" := (M_functor x m1 m2) (at level 200, x at level 40, m2 at level 200, right associativity) : module_scope.
Local Infix ":;" := M_tannot (at level 40, no associativity) : module_scope.
Local Notation "'Sig' d1 ; .. ; dn 'end'" := (MT_sig (cons d1 .. (cons dn nil) ..)) (at level 200) : module_scope.
Local Notation "'module' x '=' m" := (Str_mdef x m) (at level 200, m at level 200, right associativity) : module_scope.
Local Notation "'moduletype' t '=' M" := (Str_mtdef t M) (at level 200, t at level 40, M at level 200, right associativity) : module_scope.
Local Notation "'include' m" := (Str_incl m) (at level 40) : module_scope.
Local Notation "'vlet' v '=' e" := (Str_let v e) (at level 200, v at level 40, e at level 40) : module_scope.
Local Notation "'module' x ':' m" := (Sig_m x m) (at level 200, x at level 40, m at level 40) : module_scope.
Local Notation "'modulealias' x '=' m" := (Sig_alias x m) (at level 200, m at level 200, right associativity) : module_scope.
Local Notation "p ⋅³ x" := (MT_field p x) (at level 40) : module_scope.

(** ** Programs *)
Inductive prog : Type :=
| P_exp :> v_exp -> prog
| P_mod : mloc -> m_exp -> prog -> prog.

Local Notation "'module' x '=' m ';;' P" := (P_mod x m P) (at level 200, x at level 40, m at level 200, left associativity) : module_scope.

(* --- *)

(** Locations *)
Fixpoint locs_m_path (m : m_path) : list ℒ :=
match m with
| M_id (m, l) => l :: nil
| M_comp p (m, l) => l :: locs_m_path p
end.

Fixpoint locs_em_path (m : em_path) : list ℒ :=
match m with
| EM_id (_, l) => l :: nil
| EM_comp p (_, l) => l :: locs_em_path p
| EM_app p1 p2 => locs_em_path p1 ++ locs_em_path p2
end.

Fixpoint locs_v (v : v_exp) : list ℒ :=
match v with
| V_id (_, l) => l :: nil
| V_field p (_, l) => l :: locs_m_path p
| V_const _ => nil
| V_let (_, l) v1 v2 => l :: (locs_v v1 ++ locs_v v2)
| V_fun (_, l) v => l :: locs_v v
| V_app v1 v2 => locs_v v1 ++ locs_v v2
| V_open p v => locs_v v
end.

Fixpoint locs_t t : list ℒ :=
match t with
| MT_id (_, l) => l :: nil
| MT_field p (_, l) => l :: locs_em_path p
| MT_sig l => flat_map locs_S l
| MT_extr m => locs_m m
| MT_functor (_, l) t1 t2 => l :: (locs_t t1 ++ locs_t t2)
| MT_constr t (_, l) p => l :: (locs_t t) ++ (locs_em_path p)
| MT_dsubst t (_, l) p => l :: (locs_t t) ++ (locs_em_path p)
end
with locs_S S : list ℒ :=
match S with
| Sig_val (_, l) => l :: nil
| Sig_m (_, l) t => l :: locs_t t
| Sig_alias (_, l) p => l :: locs_m_path p
| Sig_abs (_, l) => l :: nil
| Sig_concr (_, l) t => l :: locs_t t
| Sig_incl t => locs_t t
| Sig_open p => nil
end
with locs_m m : list ℒ :=
match m with
| M_path m => locs_m_path m
| M_struct l => flat_map locs_s l
| M_functor (_, l) t m => l :: locs_t t ++ locs_m m
| M_app m1 m2 => locs_m m1 ++ locs_m m2
| M_tannot m t => locs_m m ++ locs_t t
end
with locs_s s : list ℒ :=
match s with
| Str_let (_, l) v => l :: locs_v v
| Str_let_ v => locs_v v
| Str_mdef (_, l) m => l :: locs_m m
| Str_mtdef (_, l) t => l :: locs_t t
| Str_incl m => locs_m m
| Str_open p => nil
end.

Fixpoint locs p : list ℒ :=
match p with
| P_exp v => locs_v v
| P_mod (m, l) e p => l :: (locs_m e) ++ locs p
end.

(** Well-formed terms *)
Fixpoint wf_m_path (m : m_path) : Prop :=
match m with
| M_id (m, l) => True
| M_comp p (m, l) => ~ In l (locs_m_path p) /\ wf_m_path p
end.

(* begin hide *)
Definition disj {A} (l1 l2 : list A):= forall x, In x l1 -> In x l2 -> False.
(* end hide *)

Fixpoint wf_em_path (m : em_path) : Prop :=
match m with
| EM_id (_, l) => True
| EM_comp p (_, l) => ~ In l (locs_em_path p) /\ wf_em_path p
| EM_app p1 p2 => wf_em_path p1 /\ wf_em_path p2 /\ disj (locs_em_path p1) (locs_em_path p2)
end.

Fixpoint wf_v (v : v_exp) : Prop :=
match v with
| V_id (_, l) => True
| V_field p (_, l) => ~ In l (locs_m_path p) /\ wf_m_path p
| V_const _ => True
| V_let (_, l) v1 v2 => ~ In l (locs_v v1) /\
                        ~ In l (locs_v v2) /\
                        disj (locs_v v1) (locs_v v2) /\
                        (wf_v v1 /\ wf_v v2)
| V_fun (_, l) v => ~ In l (locs_v v) /\ wf_v v
| V_app v1 v2 => disj (locs_v v1) (locs_v v2) /\ wf_v v1 /\ wf_v v2
| V_open p v => wf_v v
end.

Fixpoint wf_t t : Prop :=
match t with
| MT_id (_, l) => True
| MT_field p (_, l) => ~ In l (locs_em_path p) /\ wf_em_path p
| MT_sig l =>
   (fix wf_list_S l := match l with
    | nil => True
    | h :: t => wf_S h /\ wf_list_S t /\
                disj (locs_S h) (flat_map locs_S t)
    end) l
| MT_extr m => wf_m m
| MT_functor (_, l) t1 t2 => ~ In l (locs_t t1) /\
                             ~ In l (locs_t t2) /\
                             disj (locs_t t1) (locs_t t2) /\
                             (wf_t t1 /\ wf_t t2)
| MT_constr t (_, l) p => ~ In l (locs_t t) /\
                          ~ In l (locs_em_path p) /\
                          disj (locs_t t) (locs_em_path p) /\
                          (wf_t t) /\ (wf_em_path p)
| MT_dsubst t (_, l) p => ~ In l (locs_t t) /\
                          ~ In l (locs_em_path p) /\
                          disj (locs_t t) (locs_em_path p) /\
                          (wf_t t) /\ (wf_em_path p)
end
with wf_S S : Prop :=
match S with
| Sig_val (_, l) => True
| Sig_m (_, l) t => ~ In l (locs_t t) /\ wf_t t
| Sig_alias (_, l) p => ~ In l (locs_m_path p) /\ wf_m_path p
| Sig_abs (_, l) => True
| Sig_concr (_, l) t => ~ In l (locs_t t) /\ wf_t t
| Sig_incl t => wf_t t
| Sig_open p => True
end
with wf_m m : Prop :=
match m with
| M_path m => wf_m_path m
| M_struct l =>
   (fix wf_list_s l := match l with
    | nil => True
    | h :: t => wf_s h /\ wf_list_s t /\
                disj (locs_s h) (flat_map locs_s t)
    end) l
| M_functor (_, l) t m => ~ In l (locs_t t) /\
                          ~ In l (locs_m m) /\
                          disj (locs_m m) (locs_t t) /\
                          wf_t t /\ wf_m m
| M_app m1 m2 => disj (locs_m m1) (locs_m m2) /\ wf_m m1 /\ wf_m m2
| M_tannot m t => disj (locs_m m) (locs_t t) /\ wf_m m /\ wf_t t
end
with wf_s s : Prop :=
match s with
| Str_let (_, l) v => ~ In l (locs_v v) /\ wf_v v
| Str_let_ v => wf_v v
| Str_mdef (_, l) m => ~ In l (locs_m m) /\ wf_m m
| Str_mtdef (_, l) t => ~ In l (locs_t t) /\ wf_t t
| Str_incl m => wf_m m
| Str_open p => True
end.

Definition wf_list_S :=(fix wf_list_S l :=
match l with
| nil => True
| h :: t => wf_S h /\ wf_list_S t /\
            disj (locs_S h) (flat_map locs_S t)
end).

Definition wf_list_s := (fix wf_list_s l := match l with
| nil => True
| h :: t => wf_s h /\ wf_list_s t /\
            disj (locs_s h) (flat_map locs_s t)
end).

Fixpoint wf p : Prop :=
match p with
| P_exp v => wf_v v
| P_mod (m, l) e p => ~ In l (locs_m e) /\ ~ In l (locs p) /\
                      (wf_m e) /\ wf p /\ disj (locs_m e) (locs p)
end.

(** References (_Definition 5_) *)

Fixpoint ref_v v : list ℒ :=
match v with
| V_id (va, l) => l :: nil
| V_let (va, l) v1 v2 => ref_v v1 ++  ref_v v2
| V_fun (va, l) v => ref_v v
| V_app v1 v2 => ref_v v1 ++ ref_v v2
| V_open p v => ref_v v
| _ => nil
end.

Fixpoint ref_m (p: m_exp) : list ℒ :=
match p with
| M_functor ml t m => ref_t t ++ ref_m m
| M_app m1 m2 => ref_m m1 ++ ref_m m2
| M_tannot m t => ref_m m ++ ref_t t
| M_struct l =>
  (fix ref_list_s l := match l with
    | h :: t => ref_s h ++ ref_list_s t
    | _ => nil
    end) l
| _ => nil
end
with ref_s s : list ℒ :=
match s with
| Str_let (va, l) v => ref_v v
| Str_let_ v => ref_v v
| Str_mdef (m, l) e => ref_m e
| Str_mtdef (t, l) m => ref_t m
| Str_incl m => ref_m m
| _ => nil
end
with ref_t t : list ℒ :=
match t with
| MT_id (_, l) => nil
| MT_field p (_, l) => nil
| MT_sig l => flat_map ref_S l
| MT_extr m => ref_m m
| MT_functor (_, l) t1 t2 => ref_t t1 ++ ref_t t2
| MT_constr t (_, l) p => ref_t t
| MT_dsubst t (_, l) p => ref_t t
end
with ref_S S : list ℒ :=
match S with
| Sig_val (_, l) => nil
| Sig_m (_, l) t => ref_t t
| Sig_alias (_, l) p => nil
| Sig_abs (_, l) => nil
| Sig_concr (_, l) t => ref_t t
| Sig_incl t => ref_t t
| Sig_open p => nil
end.

Fixpoint ref p : list ℒ :=
match p with
| P_exp v => ref_v v
| P_mod (m, l) e p => ref_m e ++ ref p
end.

(** Declarations (_Definition 3_) *)

Fixpoint decl_v v : list ℒ :=
match v with
| V_let (va, l) v1 v2 => l :: decl_v v1 ++  decl_v v2
| V_fun (va, l) v => l :: decl_v v
| V_app v1 v2 => decl_v v1 ++ decl_v v2
| V_open p v => decl_v v
| _ => nil
end.

Fixpoint decl_t t : list ℒ :=
match t with
| MT_id _ => nil
| MT_field _ _ => nil
| MT_sig l => flat_map decl_S l
| MT_extr m => decl_m m
| MT_functor _ t1 t2 => decl_t t1 ++ decl_t t2
| MT_constr t _ p => decl_t t
| MT_dsubst t _ p => decl_t t
end
with decl_S S : list ℒ :=
match S with
| Sig_val (_, l) => l :: nil
| Sig_m _ t => decl_t t
| Sig_alias _ p => nil
| Sig_abs _ => nil
| Sig_concr (_, l) t => decl_t t
| Sig_incl t => decl_t t
| Sig_open p => nil
end
with decl_m (p: m_exp) : list ℒ :=
match p with
| M_functor ml t m => decl_m m ++ decl_t t
| M_app m1 m2 => decl_m m1 ++ decl_m m2
| M_tannot m t => decl_m m ++ decl_t t
| M_struct l =>
  (fix decl_list_s l := match l with
    | h :: t => decl_s h ++ decl_list_s t
    | _ => nil
    end) l
| M_path _ => nil
end
with decl_s s : list ℒ :=
match s with
| Str_let (va, l) v => l :: decl_v v
| Str_let_ v => decl_v v
| Str_mdef (m, l) e => decl_m e
| Str_mtdef (m, l) t => decl_t t
| Str_incl m => decl_m m
| Str_open p => nil
end.

Fixpoint decl p : list ℒ :=
match p with
| P_exp v => decl_v v
| P_mod (m, l) e p => decl_m e ++ decl p
end.

(** _Value Locations outside of value expressions_ *)
(** This will be the domain of the extension kernel *)

Fixpoint val_locs_t t : list ℒ :=
match t with
| MT_id (_, l) => nil
| MT_field p (_, l) => nil
| MT_sig l => flat_map val_locs_S l
| MT_extr m => val_locs_m m
| MT_functor (_, l) t1 t2 => val_locs_t t1 ++ val_locs_t t2
| MT_constr t (_, l) p => val_locs_t t
| MT_dsubst t (_, l) p => val_locs_t t
end
with val_locs_S S : list ℒ :=
match S with
| Sig_val (_, l) => l :: nil
| Sig_m (_, l) t => val_locs_t t
| Sig_concr (_, l) t => val_locs_t t
| Sig_incl t => val_locs_t t
| _ => nil
end
with val_locs_m m : list ℒ :=
match m with
| M_path m => nil
| M_struct l => flat_map val_locs_s l
| M_functor (_, l) t m => val_locs_t t ++ val_locs_m m
| M_app m1 m2 => val_locs_m m1 ++ val_locs_m m2
| M_tannot m t => val_locs_m m ++ val_locs_t t
end
with val_locs_s s : list ℒ :=
match s with
| Str_let (_, l) v => l :: nil
| Str_let_ v => nil
| Str_mdef (_, l) m => val_locs_m m
| Str_mtdef (_, l) t => val_locs_t t
| Str_incl m => val_locs_m m
| Str_open p => nil
end.

Fixpoint val_locs p : list ℒ :=
match p with
| P_exp v => nil
| P_mod (m, l) e p => val_locs_m e ++ val_locs p
end.

Lemma val_locs_locs_m :
  (forall m, incl (val_locs_m m) (locs_m m)) /\
  (forall ls, incl (flat_map val_locs_s ls) (flat_map locs_s ls)) /\
  (forall t, incl (val_locs_t t) (locs_t t)) /\
  (forall ls, incl (flat_map val_locs_S ls) (flat_map locs_S ls)).
Proof.
refine ((fun x => match x with (a, b, c, d) => conj a (conj b (conj c d)) end) _).
apply module_ind; intros; simpl in *; try constructor; auto; simpl in *;
try match goal with
| a : vloc |- _ => destruct a
| a : mloc |- _ => destruct a
| a : tloc |- _ => destruct a
end;
auto with *; intros x Hin; simpl in *; try tauto;
try rewrite in_app_iff in *; destruct Hin as [Hin|Hin]; subst; try tauto.
auto with *.
Defined.

Local Definition val_locs_locs_m_m := proj1 val_locs_locs_m.
Global Hint Resolve val_locs_locs_m_m : core.

Lemma val_locs_locs : forall P, incl (val_locs P) (locs P).
Proof.
induction P; simpl in *;
try match goal with
| a : vloc |- _ => destruct a
| a : mloc |- _ => destruct a
| a : tloc |- _ => destruct a
end;
auto with *; intros x Hin; simpl in *; try tauto.
Qed.

(** Locations within value expression *)
Fixpoint ve_locs_t t : list ℒ :=
match t with
| MT_id (_, l) => nil
| MT_field p (_, l) => nil
| MT_sig l => flat_map ve_locs_S l
| MT_extr m => ve_locs_m m
| MT_functor (_, l) t1 t2 => ve_locs_t t1 ++ ve_locs_t t2
| MT_constr t (_, l) p => ve_locs_t t
| MT_dsubst t (_, l) p => ve_locs_t t
end
with ve_locs_S S : list ℒ :=
match S with
| Sig_m (_, l) t => ve_locs_t t
| Sig_concr (_, l) t => ve_locs_t t
| Sig_incl t => ve_locs_t t
| _ => nil
end
with ve_locs_m m : list ℒ :=
match m with
| M_path m => nil
| M_struct l => flat_map ve_locs_s l
| M_functor (_, l) t m => ve_locs_t t ++ ve_locs_m m
| M_app m1 m2 => ve_locs_m m1 ++ ve_locs_m m2
| M_tannot m t => ve_locs_m m ++ ve_locs_t t
end
with ve_locs_s s : list ℒ :=
match s with
| Str_let (_, l) v => locs_v v
| Str_let_ v => locs_v v
| Str_mdef (_, l) m => ve_locs_m m
| Str_mtdef (_, l) t => ve_locs_t t
| Str_incl m => ve_locs_m m
| Str_open p => nil
end.

Fixpoint ve_locs p : list ℒ :=
match p with
| P_exp v => locs_v v
| P_mod (m, l) e p => ve_locs_m e ++ ve_locs p
end.

Lemma ve_locs_locs_m :
  (forall m, incl (ve_locs_m m) (locs_m m)) /\
  (forall ls, incl (flat_map ve_locs_s ls) (flat_map locs_s ls)) /\
  (forall t, incl (ve_locs_t t) (locs_t t)) /\
  (forall lS, incl (flat_map ve_locs_S lS) (flat_map locs_S lS)).
Proof.
refine ((fun x => match x with (a, b, c, d) => conj a (conj b (conj c d)) end) _).
apply module_ind; intros; simpl in *; try constructor; auto; simpl in *;
try match goal with
| a : vloc |- _ => destruct a
| a : mloc |- _ => destruct a
| a : tloc |- _ => destruct a
end;
auto with *; intros x Hin; simpl in *; try tauto.
Qed.

Local Definition ve_locs_locs_m_m := proj1 ve_locs_locs_m.
Global Hint Resolve ve_locs_locs_m_m : core.

Lemma ve_locs_locs : forall P, incl (ve_locs P) (locs P).
Proof.
induction P; simpl in *;
try match goal with
| a : vloc |- _ => destruct a
| a : mloc |- _ => destruct a
| a : tloc |- _ => destruct a
end;
auto with *; intros x Hin; simpl in *; try tauto.
Qed.

(* begin hide *)

(* tactic for splitting located variables into location + variable *)
Ltac destr_locs := repeat (match goal with
| [x : vloc |- _ ] => let lv := fresh x "l" in destruct x as (x&lv)
| [x : tloc |- _ ] => let lv := fresh x "l" in destruct x as (x&lv)
| [x : mloc |- _ ] => let lv := fresh x "l" in destruct x as (x&lv)
end).

Lemma disj_nil_l {A : Type} (l : list A) : disj nil l.
Proof. intros x Hx. inversion Hx. Qed.
Global Hint Immediate disj_nil_l : core.

Lemma disj_ve_val_locs_m:
  (forall m, wf_m m -> disj (ve_locs_m m) (val_locs_m m)) /\
  (forall ls, wf_list_s ls -> disj (flat_map ve_locs_s ls) (flat_map val_locs_s ls)) /\
  (forall t, wf_t t -> disj (ve_locs_t t) (val_locs_t t)) /\
  (forall ls, wf_list_S ls -> disj (flat_map ve_locs_S ls) (flat_map val_locs_S ls)).
Proof.
refine ((fun x => match x with (a, b, c, d) => conj a (conj b (conj c d)) end) _). unfold disj.
apply module_ind; intros; simpl in *; try apply Forall_cons; auto; simpl in *;
destr_locs; trivial;
try rewrite in_app_iff in *;
try destruct H1 as [Hin1|Hin1]; try destruct H2 as [Hin2|Hin2];
try destruct H3 as [Hin1|Hin1]; try rewrite in_app_iff in *; auto;
try solve[intuition; eauto];
try solve[apply val_locs_locs_m in Hin1; apply ve_locs_locs_m in Hin2;eapply H1; eauto; auto with *];
try(solve[eapply H0; try (apply val_locs_locs_m; eauto);
    apply ve_locs_locs_m in Hin1; eauto; auto with *]);
try solve[apply val_locs_locs_m in Hin1; apply ve_locs_locs_m in Hin2;eapply H1; eauto; auto with *];
try solve[apply val_locs_locs_m in Hin2; apply ve_locs_locs_m in Hin1;eapply H0; eauto; auto with *].
- eapply H0; eauto. apply ve_locs_locs_m; auto with *.
- destruct Hin2 as [Hin2|Hin2]; subst; try tauto.
- apply val_locs_locs_m in Hin2. eapply H0; eauto; auto with *.
- destruct Hin2 as [Hin2|Hin2]; subst; try tauto.
  apply ve_locs_locs_m, H0 in Hin1; auto with *.
- apply val_locs_locs_m, H0 in H2; auto with *.
Qed.

Lemma disj_ve_val_locs P: wf P -> disj (ve_locs P) (val_locs P).
Proof.
intro Hwf. unfold disj.
induction P; simpl in *; destr_locs;
auto with *; intros x Hin1 Hin2; simpl in *; try tauto.
rewrite in_app_iff in *.
destruct Hin1 as [Hin1|Hin1]; destruct Hin2 as [Hin2|Hin2].
- eapply (proj1 disj_ve_val_locs_m); eauto. tauto.
- apply val_locs_locs in Hin2.
  apply ve_locs_locs_m in Hin1. eapply Hwf; eauto.
- apply val_locs_locs_m in Hin2.
  apply ve_locs_locs in Hin1. eapply Hwf; eauto.
- eapply IHP; eauto. tauto.
Qed.

Lemma ref_locs_v v : incl (ref_v v) (locs_v v).
Proof.
induction v; intros; simpl in *;
try match goal with | a : vloc |- _ => destruct a end;
auto with *; intros x Hin; simpl in *; tauto.
Qed.
Global Hint Resolve ref_locs_v : core.

Lemma ref_locs_m :
  (forall m, incl (ref_m m) (locs_m m)) /\
  (forall ls, Forall (fun s => incl (ref_s s) (locs_s s)) ls) /\
  (forall t, incl (ref_t t) (locs_t t)) /\
  (forall ls, Forall (fun s => incl (ref_S s) (locs_S s)) ls).
Proof.
refine ((fun x => match x with (a, b, c, d) => conj a (conj b (conj c d)) end) _).
apply module_ind; intros; simpl in *; try constructor; auto; simpl in *; destr_locs;
auto with *; intros x Hin; simpl in *; try tauto.
- induction l; [inversion Hin |].
  simpl in *. rewrite in_app_iff in *.
  inversion H; subst.
  destruct Hin; auto; tauto.
- induction ls; [inversion Hin |].
  simpl. rewrite in_app_iff in *.
  inversion H; subst.
  destruct Hin; auto; tauto.
Qed.
Global Hint Resolve ref_locs_m : core.

Lemma ref_locs : forall P, incl (ref P) (locs P).
Proof.
induction P; simpl in *; destr_locs;
auto with *; intros x Hin; simpl in *; try tauto.
rewrite in_app_iff in *.
intuition.
apply ref_locs_m in H; tauto.
Qed.

Lemma decl_locs_v v : incl (decl_v v) (locs_v v).
Proof.
induction v; intros; simpl in *; destr_locs;
auto with *; intros x Hin; simpl in *; tauto.
Qed.
Global Hint Resolve decl_locs_v : core.

Lemma decl_locs_m:
  (forall m, incl (decl_m m) (locs_m m)) /\
  (forall ls, Forall (fun s => incl (decl_s s) (locs_s s)) ls) /\
  (forall t, incl (decl_t t) (locs_t t)) /\
  (forall ls, Forall (fun S => incl (decl_S S) (locs_S S)) ls).
Proof.
refine ((fun x => match x with (a, b, c, d) => conj a (conj b (conj c d)) end) _).
apply module_ind; intros; simpl in *; try constructor; auto; simpl in *; destr_locs;
auto with *; intros x Hin; simpl in *; try tauto.
- induction l; [inversion Hin |].
  simpl in *. rewrite in_app_iff in *.
  inversion H; subst. destruct Hin; auto; tauto.
- induction ls; [inversion Hin |].
  simpl. rewrite in_app_iff in *.
  inversion H; subst. destruct Hin; auto; tauto.
Qed.

Local Definition decl_locs_m_m := proj1 decl_locs_m.
Global Hint Resolve decl_locs_m_m : core.

Lemma decl_locs : forall P, incl (decl P) (locs P).
Proof.
induction P; simpl in *; destr_locs; auto with *.
Qed.

(* end hide *)

(** _Value identifiers_ *)
Fixpoint value_identifier_v vx v : Prop :=
match v with
| V_id v => vx = v
| V_field _ v => vx = v
| V_const _ => False
| V_let v v1 v2 => vx = v \/ (value_identifier_v vx v1 \/ value_identifier_v vx v2)
| V_fun v v0 => vx = v \/ value_identifier_v vx v0
| V_app v1 v2 => value_identifier_v vx v1 \/ value_identifier_v vx v2
| V_open p e => value_identifier_v vx e
end.

Fixpoint value_identifier_t vx t : Prop :=
let value_identifier_S := fix value_identifier_S lS :=
match lS with
| Sig_val v :: lS => vx = v \/ value_identifier_S lS
| Sig_m _ t :: lS => value_identifier_t vx t \/ value_identifier_S lS
| Sig_concr _ t :: lS => value_identifier_t vx t \/ value_identifier_S lS
| Sig_incl t :: lS => value_identifier_t vx t \/ value_identifier_S lS
| _ :: lS => value_identifier_S lS
| nil => False
end in
match t with
| MT_sig l => value_identifier_S l
| MT_extr m => value_identifier_m vx m
| MT_functor _ t1 t2 => value_identifier_t vx t1 \/ value_identifier_t vx t2
| MT_constr t _ _ => value_identifier_t vx t
| MT_dsubst t _ _ => value_identifier_t vx t
| _ => False
end
with value_identifier_m vx m : Prop :=
let value_identifier_s := fix value_identifier_s ls :=
match ls with
| Str_let v v0 :: ls => vx = v \/ value_identifier_v vx v0 \/ value_identifier_s ls
| Str_let_ v0 :: ls => value_identifier_v vx v0 \/ value_identifier_s ls
| Str_mdef _ m :: ls => value_identifier_m vx m \/ value_identifier_s ls
| Str_mtdef _ t :: ls => value_identifier_t vx t \/ value_identifier_s ls
| Str_incl m :: ls => value_identifier_m vx m \/ value_identifier_s ls
| nil => False
| Str_open p :: ls => value_identifier_s ls
end in
match m with
| M_path _ => False
| M_struct l => value_identifier_s l
| M_functor _ t m => value_identifier_t vx t \/ value_identifier_m vx m
| M_app m1 m2 => value_identifier_m vx m1 \/ value_identifier_m vx m2
| M_tannot m t => value_identifier_m vx m \/ value_identifier_t vx t
end.

Definition value_identifier_S vx := fix value_identifier_S lS :=
match lS with
| Sig_val v :: lS => vx = v \/ value_identifier_S lS
| Sig_m _ t :: lS => value_identifier_t vx t \/ value_identifier_S lS
| Sig_concr _ t :: lS => value_identifier_t vx t \/ value_identifier_S lS
| Sig_incl t :: lS => value_identifier_t vx t \/ value_identifier_S lS
| _ :: lS => value_identifier_S lS
| nil => False
end.

Definition value_identifier_s vx := fix value_identifier_s ls :=
match ls with
| Str_let v v0 :: ls => vx = v \/ value_identifier_v vx v0 \/ value_identifier_s ls
| Str_let_ v0 :: ls => value_identifier_v vx v0 \/ value_identifier_s ls
| Str_mdef _ m :: ls => value_identifier_m vx m \/ value_identifier_s ls
| Str_mtdef _ t :: ls => value_identifier_t vx t \/ value_identifier_s ls
| Str_incl m :: ls => value_identifier_m vx m \/ value_identifier_s ls
| nil => False
| Str_open p :: ls => value_identifier_s ls
end.

Fixpoint value_identifier vx p : Prop :=
match p with
| P_exp v => value_identifier_v vx v
| P_mod _ e p => value_identifier_m vx e \/ value_identifier vx p
end.

Lemma value_identifier_locs_v l v0 v:
  value_identifier_v (v0, l) v -> In l (locs_v v).
Proof.
induction v; destr_locs; simpl in *; trivial.
- intro Heq; inversion Heq; subst; tauto.
- intro Heq; inversion Heq; subst; tauto.
- intros [Heq | [Heq|Heq] ]; simpl.
  + inversion Heq; subst; tauto.
  + right. apply in_app_iff. tauto.
  + right. apply in_app_iff. tauto.
- intros [Heq|Heq].
  + inversion Heq; subst; tauto.
  + tauto.
- intros [Heq|Heq]; rewrite in_app_iff; tauto.
Qed.
Global Hint Resolve value_identifier_locs_v : core.

Lemma value_identifier_locs_m:
 (forall m l v0, value_identifier_m (v0, l) m -> In l (locs_m m)) /\
 (forall ls l v0, value_identifier_s (v0, l) ls -> In l (flat_map locs_s ls)) /\
 (forall t l v0, value_identifier_t (v0, l) t -> In l (locs_t t)) /\
 (forall ls l v0, value_identifier_S (v0, l) ls -> In l (flat_map locs_S ls)).
Proof.
refine ((fun x => match x with (a, b, c, d) => conj a (conj b (conj c d)) end) _).
apply module_ind; intros; destr_locs; simpl in *; intuition; eauto with *;
inversion H1; subst; tauto.
Qed.

Local Definition value_identifier_locs_m_m := proj1 value_identifier_locs_m.
Global Hint Resolve value_identifier_locs_m_m : core.

Lemma value_identifier_locs P l v0: value_identifier (v0, l) P -> In l (locs P).
Proof. induction P; destr_locs; simpl; intuition; eauto with *. Qed.

(* begin hide *)
Definition option_eq_dec {A} :
  (forall x y : A, {x = y} + {x <> y}) -> forall (l1 l2 : option A), {l1 = l2} + {l1 <> l2}.
Proof.
intro Heq.
destruct l1 as [l1 | ];
destruct l2 as [l2 | ].
- destruct (Heq l1 l2).
 + left; subst; trivial.
 + right; intro H; inversion H; tauto.
- right; intro H; inversion H.
- right; intro H; inversion H.
- left; trivial.
Defined.

Definition var_eq_bool l1 l2 := if option_eq_dec var_eq_dec l1 l2 then true else false.

Lemma var_eq_bool_true l1 l2 : var_eq_bool l1 l2 = true <-> l1 = l2.
Proof.
unfold var_eq_bool.
split.
- intro H.
  case option_eq_dec in H; trivial.
  inversion H.
- intro H.
  subst.
  case option_eq_dec; tauto.
Qed.

Lemma var_eq_bool_false l1 l2 : var_eq_bool l1 l2 = false <-> l1 <> l2.
Proof.
unfold var_eq_bool.
split.
- intro H.
  case option_eq_dec in H; trivial.
  inversion H.
- intro H.
  subst.
  case option_eq_dec; tauto.
Qed.

Definition loc_eq_bool l1 l2 := if loc_eq_dec l1 l2 then true else false.
(* end hide *)

Module Notations.
  Notation "p ⋅¹ x" := (M_comp p x) (at level 40) : module_scope.
  Notation "q ⋅² x" := (EM_comp q x) (at level 40) : module_scope.
  Notation "q '(' q2 ')'" := (EM_app q q2) (at level 40, right associativity) : module_scope.
  Notation "p ⋅ v" := (V_field p v) (at level 40) : module_scope.
  Notation "'vlet' v '==' e 'in' e2" := (V_let v e e2) (at level 210) : module_scope.
  Notation "'fn' v '-->' e" := (V_fun v e) (at level 40) : module_scope.
  Notation "'val' v ':_'" := (Sig_val v) (at level 200) : module_scope.
  Notation "'struct' s 'end'" := (M_struct s) (at level 200) : module_scope.
  Notation "'struct' d1 ; .. ; dn 'end'" := (M_struct (cons d1 .. (cons dn nil) ..)) (at level 200) : module_scope.
  Notation "'functor' '(' x ':' m1 ')' '→' m2" := (M_functor x m1 m2) (at level 200, x at level 40, m2 at level 200, right associativity) : module_scope.
  Notation "'Sig' d1 ; .. ; dn 'end'" := (MT_sig (cons d1 .. (cons dn nil) ..)) (at level 200) : module_scope.
  Notation "'module' x '=' m" := (Str_mdef x m) (at level 200, m at level 200, right associativity) : module_scope.
  Notation "'moduletype' t '=' M" := (Str_mtdef t M) (at level 200, t at level 40, M at level 200, right associativity) : module_scope.
  Notation "'include' m" := (Str_incl m) (at level 40) : module_scope.
  Notation "'vlet' v '=' e" := (Str_let v e) (at level 200, v at level 40, e at level 40) : module_scope.
  Notation "'module' x ':' m" := (Sig_m x m) (at level 200, x at level 40, m at level 40) : module_scope.
  Notation "'modulealias' x '=' m" := (Sig_alias x m) (at level 200, m at level 200, right associativity) : module_scope.
  Notation "p ⋅³ x" := (MT_field p x) (at level 40) : module_scope.
  Notation "'module' x '=' m ';;' P" := (P_mod x m P) (at level 200, x at level 40, m at level 200, left associativity) : module_scope.
  Infix ":;" := M_tannot (at level 40, no associativity) : module_scope.
End Notations.

Coercion print_module: modules>->string.
Coercion print_type: types>->string.
Coercion print_variable: variables>->string.
Coercion print_constant: constants>->string.
Coercion print_loc: loc>->string.

(** Pretty printer *)

(* begin hide *)
Definition LF : string :="
".

Fixpoint indent n : string := match n with
| O => ""
| S n => "  " ++ indent n
end.

Fixpoint pp_m_path (m0 : m_path) : string :=
match m0 with
| M_id (m', _) => m'
| M_comp p (m', l) => pp_m_path p ++ "." ++ m'
end.

Fixpoint pp_em_path (m0 : em_path) : string :=
match m0 with
| EM_id (x0, l) => x0
| EM_comp p (x0, l) => pp_em_path p ++ "." ++ x0
| EM_app p1 p2 => pp_em_path p1 ++ "(" ++ pp_em_path p2 ++ ")"
end.

Fixpoint pp_v n (v0 : v_exp) : string :=
match v0 with
| V_id (v0, l) => v0
| V_field p (v0, l) => pp_m_path p ++ "." ++ v0
| V_const c0 => c0
| V_let (v0, l) v1 v2 => "let " ++ v0 ++ " := " ++ pp_v n v1 ++ " in" ++
                            LF ++ indent (S n) ++ pp_v (S n) v2
| V_fun (v0, l) v => "fn " ++ v0 ++ " -> " ++ pp_v n v
| V_app v1 v2 => pp_v n v1 ++ "(" ++ pp_v n v2 ++ ")"
| V_open p v => "let open " ++ pp_m_path p ++ " in " ++
                   LF ++ indent (S n) ++ pp_v (S n) v
end.

Fixpoint pp_t n t : string :=
match t with
| MT_id (t0, l) => t0
| MT_field p (t0, l) => pp_em_path p ++ "." ++ t0
| MT_sig l => LF ++ indent n ++ "sig " ++ fold_left append (map (pp_S (S n)) l) EmptyString ++
              LF ++ indent n ++ "end"
| MT_extr m => "module type of " ++ pp_m n m
| MT_functor (t0, l) t1 t2 => "functor " ++ t0 ++ " " ++ pp_t n t1 ++ " -> " ++ pp_t n t2
| MT_constr t (t0, l) p => pp_t n t ++ " with module " ++ t0 ++ " = " ++ pp_em_path p
| MT_dsubst t (t0, l) p => pp_t n t ++ " with module " ++ t0 ++ " := " ++ pp_em_path p
end
with pp_S n (S0 : sig_comp) {struct S0} : string :=
LF ++ indent n ++
match S0 with
| Sig_val (v0, l) => "val " ++ v0 ++ " := _"
| Sig_m (m0, l) t => "module " ++ m0 ++ " := " ++ pp_t n t
| Sig_alias (m0, l) p => "module " ++ m0 ++ " = " ++ pp_m_path p
| Sig_abs (t0, l) => "module type " ++ t0
| Sig_concr (t0, l) t => "module type " ++ t0 ++ " = " ++ pp_t n t
| Sig_incl t => "include " ++ pp_t n t
| Sig_open p => "open " ++ pp_m_path p
end
with pp_m n m0 : string :=
match m0 with
| M_path m0 => pp_m_path m0
| M_struct l => LF ++ indent n ++ "struct " ++
                fold_left append (map (pp_s (S n)) l) EmptyString ++
                LF ++ indent n ++ "end"
| M_functor (x0, l) t0 m0 => "functor (" ++ x0 ++ " : " ++ pp_t n t0 ++ ") -> " ++ pp_m n m0
| M_app m1 m2 => pp_m n m1 ++ "(" ++ pp_m n m2 ++ ")"
| M_tannot m0 t0 => pp_m n m0 ++ " : " ++  pp_t n t0
end
with pp_s n s : string :=
LF ++ indent n ++
match s with
| Str_let (v0, l) v => "let " ++ v0 ++ " := " ++ pp_v n v
| Str_let_ v => "let _ := " ++ pp_v n v
| Str_mdef (x0, l) m0 => "module " ++ x0 ++ " = " ++ pp_m n m0
| Str_mtdef (t0, l) t => "module type " ++ t0 ++ " = " ++ pp_t n t
| Str_incl m0 => "include " ++ pp_m n m0
| Str_open p => "open " ++ pp_m_path p
end.
(* end hide *)

Fixpoint pp p : string :=
match p with
| P_exp v => pp_v 0 v ++ LF
| P_mod (m0, l) e p => "module " ++ m0 ++ " = " ++ pp_m 1 e ++ ";;" ++ LF ++ LF ++ pp p
end.

End Modules.

Module Type ModulesType (L : LanguageSig).
  Include Modules L.
End ModulesType.
