_: makefile.coq example

makefile.coq: _CoqProject
	coq_makefile $(shell cat _CoqProject) *.v > $@ 2> /dev/null

-include makefile.coq

clean::
	rm makefile.coq
	rm -f *.ml *.mli
	rm -fR examples/extraction
	make -C examples clean

examples: Examples.vo
	mkdir -p examples/extraction
	mv *.ml *.mli examples/extraction 2> /dev/null || true
	make -C examples

release:
	tar zcvf abstract_renaming.tar.gz Default_renaming.v Examples.v Modules.v Semantics.v Renaming_operations.v Makefile _CoqProject

.PHONY: _
