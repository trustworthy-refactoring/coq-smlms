Require Import AbstractRenaming.Semantics AbstractRenaming.Modules AbstractRenaming.Renaming_operations List Coq.Program.Basics.
Set Implicit Arguments.

Module Definitions (Export L : LanguageSig) (Import SE : SemanticsType L).

(** * Definitions *)
Section Definitions.
(** Semantics from which the locations to rename will be computed *)
Variable Σf: sem.
(** New name *)
Variable vx : V.
(** Location where the renaming takes place *)
Variable lx : ℒ.

(** The locations which are related to lx in Σf *)
Notation L := (L Σf lx).
Notation L_dec := (L_dec Σf lx).

(** ** Renaming function *)

Fixpoint rename_v (v : v_exp) : v_exp :=
match v with
| V_id (v, l) => V_id (if L_dec l then vx else v, l)
| V_field p (v, l) => V_field p (if L_dec l then vx else v, l)
| V_const c => V_const c
| V_let (v, l) v1 v2 => V_let (if L_dec l then vx else v, l) (rename_v v1) (rename_v v2)
| V_fun (v, l) v0 => V_fun (if L_dec l then vx else v, l) (rename_v v0)
| V_app v1 v2 => V_app (rename_v v1) (rename_v v2)
| V_open m e => V_open m (rename_v e)
end.

Fixpoint rename_t t : m_type :=
match t with
| MT_sig l => MT_sig (map rename_S l)
| MT_functor ml t1 t2 => MT_functor ml (rename_t t1) (rename_t t2)
| MT_constr t ml p => MT_constr (rename_t t) ml p
| MT_dsubst t ml p => MT_dsubst (rename_t t) ml p
| MT_extr m => MT_extr (rename_m m)
| t => t
end
with rename_S S : sig_comp :=
match S with
| Sig_val (v, l) => if L_dec l then Sig_val (vx, l) else Sig_val (v, l)
| Sig_m ml t => Sig_m ml (rename_t t)
| Sig_concr tl t => Sig_concr tl (rename_t t)
| Sig_incl t => Sig_incl(rename_t t)
| s => s
end
with rename_m m : m_exp :=
match m with
| M_path m => M_path m
| M_struct l => M_struct (map rename_s l)
| M_functor ml t m => M_functor ml (rename_t t) (rename_m m)
| M_app m1 m2 => M_app (rename_m m1) (rename_m m2)
| M_tannot m t => M_tannot (rename_m m) (rename_t t)
end
with rename_s s : str_comp :=
match s with
| Str_let (v, l) v0 => if L_dec l then Str_let (vx, l) (rename_v v0)
                             else Str_let (v, l) (rename_v v0)
| Str_let_ v0 => Str_let_ (rename_v v0)
| Str_mdef ml m => Str_mdef ml (rename_m m)
| Str_mtdef tl t => Str_mtdef tl (rename_t t)
| Str_incl m => Str_incl(rename_m m)
| Str_open p => Str_open p
end.

Fixpoint rename p : prog :=
match p with
| P_exp v => P_exp (rename_v v)
| P_mod ml e p => P_mod ml (rename_m e) (rename p)
end.

(** The renaming function doesn't modify locations *)
(* begin hide *)
Lemma locs_rename_v: forall v, locs_v (rename_v v) = locs_v v.
Proof.
induction v; destr_locs; simpl; trivial.
- now rewrite IHv1, IHv2.
- now rewrite IHv.
- now rewrite IHv1, IHv2.
Qed.
Hint Rewrite locs_rename_v : core.

Lemma locs_rename_m:
  (forall m, locs_m (rename_m m) = locs_m m) /\
  (forall ls, flat_map locs_s (map rename_s ls) = flat_map locs_s ls) /\
  (forall t, locs_t (rename_t t) = locs_t t) /\
  (forall ls, flat_map locs_S (map rename_S ls) = flat_map locs_S ls).
Proof.
refine ((fun x => match x with (a, b, c, d) => conj a (conj b (conj c d)) end) _).
apply module_ind; intros; simpl in *; destr_locs; auto;
try case L_dec; simpl; intros; try rewrite locs_rename_v; try congruence.
Qed.
(* end hide *)

Lemma locs_rename p: locs (rename p) = locs p.
Proof.
induction p; simpl.
- now rewrite locs_rename_v.
- destr_locs. now rewrite (proj1 locs_rename_m), IHp.
Qed.

(* begin hide *)
Lemma ref_rename_v: forall v, ref_v (rename_v v) = ref_v v.
Proof.
induction v; destr_locs; simpl; trivial; now rewrite IHv1, IHv2.
Qed.

Lemma ref_rename_m:
  (forall m, ref_m (rename_m m) = ref_m m) /\
  (forall ls, flat_map ref_s (map rename_s ls) = flat_map ref_s ls) /\
  (forall t, ref_t (rename_t t) = ref_t t) /\
  (forall ls, flat_map ref_S (map rename_S ls) = flat_map ref_S ls).
Proof.
refine ((fun x => match x with (a, b, c, d) => conj a (conj b (conj c d)) end) _).
apply module_ind; intros; simpl in *; destr_locs; auto;
try case L_dec; simpl; intros; try rewrite ref_rename_v; try congruence.
Qed.

Hint Rewrite (proj1 locs_rename_m) : core.
Hint Rewrite (proj1 (proj2 (locs_rename_m))) : core.
Hint Rewrite (proj1 (proj2 (proj2 (locs_rename_m)))) : core.
Hint Rewrite (proj2 (proj2 (proj2 (locs_rename_m)))) : core.
Hint Rewrite locs_rename : core.

(* end hide *)

(** The renaming function preserves well-formed programs *)
(* begin hide *)
Lemma rename_wf_v v : wf_v v -> wf_v (rename_v v).
Proof.
induction v; destr_locs; simpl; trivial; repeat split;
repeat rewrite locs_rename_v; try tauto.
Qed.
Hint Resolve rename_wf_v : core.

Lemma rename_wf_m:
  (forall m, wf_m m -> wf_m (rename_m m)) /\
  (forall ls, wf_list_s ls -> wf_list_s (map rename_s ls)) /\
  (forall t, wf_t t -> wf_t (rename_t t)) /\
  (forall ls, wf_list_S ls -> wf_list_S (map rename_S ls)).
Proof.
refine ((fun x => match x with (a, b, c, d) => conj a (conj b (conj c d)) end) _).
apply module_ind; simpl; intros; destr_locs; intuition;
try case L_dec; simpl; intros; repeat rewrite rename_wf_v;
autorewrite with core in *; intuition.
Qed.

Local Definition rename_wf_m_m := proj1 rename_wf_m.
Hint Resolve rename_wf_m_m : core.
(* end hide *)

Lemma rename_wf p: wf p -> wf (rename p).
Proof.
intro H. induction p; simpl in *; destr_locs; auto;
autorewrite with core in *; intuition.
Qed.
Hint Resolve rename_wf : core.

(** ** Renamed semantics *)
Definition Σr Σ:=
{| br := br Σ;
   ek := E Σ;
   sr := LocMaps.mapi (fun l i => if L_dec l then IV vx else i) (sr Σ)
 |}.

(* begin hide *)
Lemma ρ_Σr' a b Σ l:
  ρ {| br := a;
       ek := b;
       sr := LocMaps.mapi (fun l i => if L_dec l then IV vx else i) (sr Σ)
     |} l =
  match ρ Σ l with
  | None => None
  | Some i => if L_dec l then Some (IV vx) else Some i
  end.
Proof.
unfold sr'. simpl.
rewrite LocMapsFacts.mapi_o; [|intros; subst; trivial].
case LocMaps.find; intros; subst; trivial.
case L_dec; now simpl.
Qed.

Lemma ρ_Σr Σ l:
  ρ (Σr Σ) l =
  match ρ Σ l with
  | None => None
  | Some i => if L_dec l then Some (IV vx) else Some i
  end.
Proof.
unfold sr'. simpl.
rewrite LocMapsFacts.mapi_o; [|intros; subst; trivial].
case LocMaps.find; intros; subst; trivial.
case L_dec; now simpl.
Qed.
(* end hide *)

(** Related locations are reified to the same value *)
Lemma proper_closure Σ l l': proper Σ -> Ê Σ l l' -> l <> l' ->
  exists v : V, ρ Σ l = Some (IV v) /\ ρ Σ l' = Some (IV v).
Proof.
intros Hp HE. induction HE; [tauto|].
intro Hneq. case (loc_eq_dec y z).
- intro; subst z.
  destruct H as [H|H]; apply Hp in H; destruct H as (_ & _ & v & Hv1 & Hv2); eauto.
- intro Hneq2.
  destruct H as [H|H]; apply Hp in H; destruct H as (_ & _ & v & Hv1 & Hv2);
  apply IHHE in Hneq2; destruct Hneq2 as (v' & Hv1' & Hv2'); eauto;
  rewrite Hv2 in Hv1' || rewrite Hv1 in Hv1'; inversion Hv1'; subst; eauto.
Qed.

Variable Hp : proper Σf.
(** We assume the location lx to be a declaration *)
Variable Hcod: cod Σf lx.

(** The original name (vy) can be computed from lx *)
Definition cod_dec:
  {vy | ρ Σf lx = Some (IV vy) /\ ↣ Σf lx = None}.
Proof.
unfold cod in Hcod.
destruct (ρ Σf lx) as [ [ty|my|vy]|];
try solve[apply False_rect; destruct Hcod as (v & Hv & Hv'); discriminate].
destruct (↣ Σf lx) as [o|].
- apply False_rect; destruct Hcod as (v & Hv & Hv'); discriminate.
- eauto.
Defined.

Definition vy := proj1_sig cod_dec.
Lemma vy_prop: ρ Σf lx = Some (IV vy) /\ ↣ Σf lx = None.
Proof. unfold vy. destruct cod_dec. trivial. Qed.


(** All locations to be renamed reify to vy *)
Lemma L_values l:
  L l -> ρ Σf l = Some (IV vy).
Proof.
intros [H|(l' & Hl' & Hl)].
- rewrite <- (proj1 vy_prop).
  case(loc_eq_dec lx l).
 + intro; subst; trivial.
 + intro Hneq. apply proper_closure in H; trivial.
  destruct H as (v & Hv1 & Hv2). now rewrite <- Hv1 in Hv2.
- rewrite <- (proj1 vy_prop).
  case(loc_eq_dec lx l').
  + intro; subst l'. apply Hp in Hl. rewrite Hl. eauto.
  + intro. apply proper_closure in Hl'; trivial.
    destruct Hl' as (l'' & Hl'' & Hl'). apply Hp in Hl.
    rewrite Hl''. now rewrite Hl, Hl'.
Qed.

Variable Γ1 : env.
Variable Henv: wb_env Γ1 Σf.

(** It is decidable whether the environment contains a renamed variable *)
Definition dec_range:
  {ly | envV Γ1 vy = Some ly /\ Ê Σf lx ly} +
  {forall vy ly, envV Γ1 vy = Some ly -> ~ L ly}.
Proof.
case_eq (envV Γ1 vy).
- intros ly Hly. destruct (L_dec ly) as [HL|HL].
  + left. exists ly. split; trivial. destruct HL as [HE|(l'&HL'&Hl')]; trivial.
    apply Henv in Hly. destruct Hly as (_&Hly).
    rewrite Hly in Hl'; discriminate Hl'.
  + right. intros vy' ly' Hvy' HL'.
    assert(HL'' := HL').
    eapply L_values in HL'; eauto.
    assert(H' := Hvy'). apply Henv in Hvy'.
     rewrite HL' in Hvy'. destruct Hvy' as (Hvy' & _); inversion Hvy'; subst.
    rewrite H' in Hly; inversion Hly; subst. tauto.
- intro Hy. right. intros vy' ly Hvy' HL'.
  assert(H' := Hvy').
  eapply L_values in HL'; eauto. apply Henv in Hvy'.
  rewrite (proj1 Hvy') in HL'; inversion HL'; subst. clear HL'.
  rewrite Hy in H'; discriminate H'.
Defined.

(** ** Renamed Environment *)
Definition Γr : env:=
match dec_range with
(** _swap the original and new variables locations if renaming has already started *)
| inleft (exist _ ly _) => Γ1 [[vx ↦v envV Γ1 vy]] [[vy ↦v envV Γ1 vx]]
(** _the environment is left unchanged otherwise *)
| _ => Γ1
end.

End Definitions.
End Definitions.


Module Correctness (Import L : LanguageSig) (Import SE : SemanticsType L).
Module Export Md := Definitions L SE.
Module Export SEq := Semantics.Equivalence L SE.


(** * Correctness *)
Section Correctness.

(* begin hide *)
Variable Σf: sem.
Variable vx : V.
Variable lx : ℒ.

Variable Hcodf: cod Σf lx.
Variable Hpf : proper Σf.
Variable Hvx: forall l, ~ ρ Σf l = Some (IV vx).
Arguments Hvx : clear implicits.

Notation Γr := (Γr vx Hpf Hcodf).
Notation vy := (vy Hcodf).
Notation vy_prop := (vy_prop Hcodf).
Notation L_values := (L_values Hpf Hcodf).
Notation Σr := (Σr Σf vx lx).
Notation L_dec := (L_dec Σf lx).
Notation L := (L Σf lx).

Hint Rewrite (fun s => proj1 (locs_rename_m s vx lx)) : core.
Hint Rewrite (fun s => proj1 (proj2 (locs_rename_m s vx lx))) : core.
Hint Rewrite (fun s => proj1 (proj2 (proj2 (locs_rename_m s vx lx)))) : core.
Hint Rewrite (fun s => proj2 (proj2 (proj2 (locs_rename_m s vx lx)))) : core.
Hint Rewrite locs_rename : core.

Lemma vxy : vx <> vy.
Proof. destruct vy_prop as (Hvy&_). intro. rewrite <- H in Hvy. eapply Hvx, Hvy. Qed.
Hint Resolve vxy : core.
(* end hide *)

(** The renamed environment is semantically equivalent to the initial one *)
Lemma Σr_eq Σ1: Σ1 ⊆ Σf -> Σr Σ1 ≡ Σ1.
Proof.
intro Hincl. unfold Σr; repeat split; simpl.
intro l. rewrite ρ_Σr'.
case_eq (ρ Σ1 l); trivial.
intros [v | m | t] Heq; case (L_dec l); trivial;
intro HL; apply L_values in HL;
apply Hincl in Heq; rewrite (proj1 Heq) in HL; discriminate HL.
Qed.

(* begin hide *)
Hint Resolve LocMapsFacts.mapi_o : core.
Hint Unfold sr' br' : core.
(* end hide *)

Lemma wb_d_Σr Σ1 d:
  (Σ1 ⊆ Σf) ->
    wb_d Σ1 d -> wb_d (Σr Σ1) d.
Proof.
destruct Hcodf as (v0 & Hv0 & _).
intros Hincl. unfold Σr; simpl.
induction d using desc_ind3; intros Hwb.
- inversion Hwb as [lv' ls' H1 H2 H3 H4|]; subst.
  constructor.
  + intros l Hl. apply H1 in Hl. destruct Hl as (v & Hv & Hb).
    unfold cod; simpl. rewrite ρ_Σr', Hv. case L_dec; eauto.
  + intros l d Hld. assert(Hld' := Hld).
    apply H2 in Hld'. apply H in Hld; [|tauto].
    split; trivial. destruct Hld' as [_ Hld'].
    fold (Σr Σ1). rewrite ρ_Σr.
    destruct Hld' as [(m & Hld')|(t & Hld')]; rewrite Hld';
    case L_dec; intro H0; eauto; unfold L in Hpf; simpl; eauto;
    apply Hincl in Hld'; rewrite L_values in Hld'; eauto;
    destruct Hld'; discriminate.
  + intros l1 l2 Hl1 Hl2.
    destruct (H1 _ Hl1) as (v1 & Hv1&_); destruct (H1 _ Hl2) as (v2 & Hv2& _).
    fold (Σr Σ1). do 2 rewrite ρ_Σr. rewrite Hv1, Hv2.
    intro Heq. apply H3; trivial.
    rewrite Hv1, Hv2. case (L_dec l1) as [H1'|H1'].
    * eapply L_values in H1'; eauto.
      apply Hincl in Hv1; destruct Hv1 as (Hv1&_).
      rewrite Hv1 in H1'; inversion H1'; subst.
      case (L_dec l2) as [H2'|H2'].
      -- eapply L_values in H2'; eauto.
         apply Hincl in Hv2; destruct Hv2 as (Hv2&_); rewrite Hv2 in H2'.
         inversion H2'; subst; auto.
      -- inversion Heq; subst v2. apply False_rect, (Hvx l2), Hincl. tauto.
    * case (L_dec l2) as [H2'|H2'].
      -- inversion Heq; subst v1. apply False_rect, (Hvx l1), Hincl. tauto.
      -- inversion Heq; now subst.
  + intros [l1 d1] [l2 d2] Hl1 Hl2. simpl.
    destruct (H2 _ _ Hl1) as (Hwb1 & Hv1); destruct (H2 _ _ Hl2) as (Hwb2 & Hv2).
    fold (Σr Σ1). do 2 rewrite ρ_Σr.
    case (L_dec l1) as [H1'|H1'].
    * eapply L_values in H1'; eauto.
      destruct Hv1 as [(m & HH)|(t & HH)];
      apply Hincl in HH; rewrite (proj1 HH) in H1'; discriminate H1'.
    * case (L_dec l2) as [H2'|H2'].
      -- eapply L_values in H2'; eauto.
         destruct Hv2 as [(m & HH)|(t & HH)];
         apply Hincl in HH; rewrite (proj1 HH) in H2'; discriminate H2'.
      -- clear H1' H2'.
         destruct Hv1 as [(m1 & HH1)|(t1 & HH1)];
         destruct Hv2 as [(m2 & HH2)|(t2 & HH2)];
         rewrite HH1, HH2; intro Heq; inversion Heq; subst;
         apply H4; simpl; auto; now rewrite HH1, HH2.
- inversion Hwb as [| l' D1' D2' H1 H2]; subst l' D1' D2'. constructor; tauto.
Qed.
Hint Resolve wb_d_Σr : core.

(* begin hide *)
Lemma L_unique Γ vy' ly': wb_env Γ Σf ->
  envV Γ vy' = Some ly' -> L ly' -> vy = vy'.
Proof.
  intros Henv Hy' HLy'.
  apply L_values in HLy'; auto.
  assert(Hy0' := Hy'). apply Henv in Hy0'.
  rewrite (proj1 Hy0') in HLy'. now inversion HLy'.
Qed.

Lemma L_unique' Γ vy' ly ly': wb_env Γ Σf ->
  envV Γ vy' = Some ly' -> L ly' -> envV Γ vy = Some ly -> L ly -> ly = ly'.
Proof.
  intros Henv Hy' HLy' Hy HLy.
  erewrite (L_unique vy') in Hy; eauto.
  rewrite Hy' in Hy; now inversion Hy.
Qed.

Lemma wb_env_Γr Γ1 Σ1 (Henv: wb_env Γ1 Σ1) (Henv': wb_env Γ1 Σf) (Hincl: Σ1 ⊆ Σf):
    wb_env (Γr Henv') (Σr Σ1).
Proof.
destruct Henv as (Hwb1 & Hwb2 & Hwb3 & Hwb4).
unfold Γr. destruct dec_range as [(lz & Hz & HEz)| Hf].
- repeat apply conj; simpl.
  + intros v v'. case (val_eq_dec vy v); intro Hneq; subst.
    * rewrite envV_env_up_v_eq.
      case (val_eq_dec vy v'); intro Hneq; subst; trivial.
      rewrite envV_env_up_v_neq; trivial.
      case (val_eq_dec vx v'); intro Hneq'; subst.
      -- rewrite envV_env_up_v_eq; trivial.
         intros. eapply Hwb1; auto. rewrite Hz; discriminate.
      -- rewrite envV_env_up_v_neq; trivial.
         intros. contradict Hneq'; eapply Hwb1; auto.
    * rewrite (envV_env_up_v_neq); trivial.
      case (val_eq_dec vx v); intro Hneq'; subst; trivial.
      -- rewrite envV_env_up_v_eq; trivial.
         case (val_eq_dec vy v'); intro Hneq'; subst; trivial.
         ++ rewrite envV_env_up_v_eq; auto.
         ++ rewrite envV_env_up_v_neq; trivial.
            case (val_eq_dec v v'); intro Hneq''; subst; trivial.
            rewrite envV_env_up_v_neq; trivial.
            intros; contradict Hneq'. apply Hwb1; auto.
      -- rewrite envV_env_up_v_neq; trivial.
         case (val_eq_dec vy v'); intro Hneq''; subst; trivial.
         ++ rewrite envV_env_up_v_eq; trivial.
            intros; contradict Hneq'. symmetry. apply Hwb1; auto.
         ++ rewrite envV_env_up_v_neq; trivial.
            case (val_eq_dec vx v'); intro Hneq'''; subst; trivial.
            ** rewrite envV_env_up_v_eq; trivial.
               intros; contradict Hneq. symmetry. apply Hwb1; auto.
            ** rewrite envV_env_up_v_neq; auto.
  + intros v l. case (val_eq_dec vy v); intro Hneq; subst.
    * rewrite envV_env_up_v_eq; trivial. intro Heq.
      apply Hwb2 in Heq. eapply False_rect, Hvx, Hincl, (proj1 Heq).
    * rewrite envV_env_up_v_neq; trivial.
      rewrite ρ_Σr.
      case (val_eq_dec vx v); intro; subst.
      -- rewrite envV_env_up_v_eq; trivial. intro Heq.
         rewrite Heq in Hz; inversion Hz; subst lz.
         apply Hwb2 in Heq; split; try tauto.
         rewrite (proj1 Heq).
         case L_dec; trivial; intro HL. contradict HL. now left.
      -- rewrite envV_env_up_v_neq; trivial. intro Heq.
         assert(Heq' := Heq).
         apply Hwb2 in Heq; split; try tauto. rewrite (proj1 Heq).
         case L_dec; trivial; intro HL.
         apply L_unique in Heq'; try tauto.
  + intro. do 2 rewrite envV_up_v_m. apply wb_d_Σr; auto.
  + intro. do 2 rewrite envV_up_v_t. apply wb_d_Σr; auto.
- simpl; repeat apply conj; intros.
  + apply Hwb1; trivial.
  + destruct (Hwb2 _ _ H) as (H' & Hn). unfold Σr; simpl.
    fold (Σr Σ1). rewrite ρ_Σr.
    repeat (rewrite LocMapsFacts.mapi_o; [|intros; subst; trivial]); simpl.
    rewrite H'. case L_dec as [Hl|]; split; trivial. contradict Hf. eauto.
  + apply wb_d_Σr; auto.
  + apply wb_d_Σr; auto.
Qed.
Hint Resolve wb_env_Γr : core.

Lemma Σr_up_ρ_nL Σ l i:
 ~ (L l) ->
  sem_equal ((Σr Σ) [[l ↦ i]]) (Σr (Σ [[l ↦ i]])).
Proof.
intro HL. unfold Σr, sem_up_sr, sem_equal. simpl. split; auto.
apply LocMapsFacts.Equal_mapsto_iff. intros k e. split; intro H.
- rewrite LocMapsFacts.mapi_mapsto_iff; [|intros; subst; trivial].
  rewrite LocMapsFacts.add_mapsto_iff in H. intuition; subst.
  + case L_dec; try tauto. intros _. eexists; split; eauto.
    rewrite LocMapsFacts.add_mapsto_iff; auto.
  + rewrite LocMapsFacts.mapi_mapsto_iff in H1; [|intros; subst; trivial].
    destruct H1 as (a&Ha1&Ha2). eexists; split; eauto. auto.
    apply LocMapsFacts.add_mapsto_iff; auto.
- rewrite LocMapsFacts.add_mapsto_iff.
  rewrite LocMapsFacts.mapi_mapsto_iff in H; [|intros; subst; trivial].
  destruct H as (a&Ha1&Ha2). subst.
  rewrite LocMapsFacts.add_mapsto_iff in Ha2. intuition; subst.
  + case L_dec; tauto.
  + right. rewrite LocMapsFacts.mapi_mapsto_iff; [|intros; subst; trivial].
    eexists; eauto.
Qed.

Lemma Σr_up_ρ_L Σ l v:
  (L l) ->
  sem_equal ((Σr Σ) [[l ↦ IV vx]]) (Σr (Σ [[l ↦ IV v]])).
Proof.
intros HL. unfold Σr, sem_up_sr. simpl.
split; simpl; auto.
apply LocMapsFacts.Equal_mapsto_iff. intros k e. split; intro H.
- rewrite LocMapsFacts.mapi_mapsto_iff; [|intros; subst; trivial].
  rewrite LocMapsFacts.add_mapsto_iff in H. intuition; subst.
  + case L_dec; try tauto. intros _. eexists; split; eauto.
    rewrite LocMapsFacts.add_mapsto_iff; auto.
  + rewrite LocMapsFacts.mapi_mapsto_iff in H1; [|intros; subst; trivial].
    destruct H1 as (a&Ha1&Ha2). eexists; split; eauto. auto.
    apply LocMapsFacts.add_mapsto_iff; auto.
- rewrite LocMapsFacts.add_mapsto_iff.
  rewrite LocMapsFacts.mapi_mapsto_iff in H; [|intros; subst; trivial].
  destruct H as (a&Ha1&Ha2). subst.
  rewrite LocMapsFacts.add_mapsto_iff in Ha2. intuition; subst.
  + case L_dec; tauto.
  + right. rewrite LocMapsFacts.mapi_mapsto_iff; [|intros; subst; trivial].
    eexists; eauto.
Qed.

Lemma sem_up_br_br Σ l v l': ↣ (Σ [[ (l, v)↦l l']]) l = Some l'.
Proof. now apply LocMapsFacts.add_eq_o. Qed.
Hint Resolve sem_up_br_br : core.

Lemma sem_up_br_ρ Σ l v l': ρ (Σ [[ (l, v)↦l l']]) l = Some (IV v).
Proof. now apply LocMapsFacts.add_eq_o. Qed.
Hint Resolve sem_up_br_ρ : core.

Lemma renamed_value_None Γ: wb_env Γ Σf -> envV Γ vx = None.
Proof.
intro Henvf. case_eq (envV Γ vx); trivial.
intros lx' H. apply Henvf in H; destruct H as (H&_); apply Hvx in H. tauto.
Qed.
Hint Resolve renamed_value_None : core.

Lemma env_up_v_disj_none Γ l v: Γ ⊓ l -> (Γ [[v ↦v None]]) ⊓ l.
Proof.
intros H l' Hl' v'. case (val_eq_dec v' v).
- intro; subst. rewrite envV_env_up_v_eq. discriminate.
- intro. rewrite envV_env_up_v_neq; auto.
Qed.

Lemma Γr_locs Γ1 H ll: Γ1 ⊓ ll -> Γr (Γ1 := Γ1) H ⊓ ll.
Proof.
unfold Γr. intro Hlocs. destruct dec_range as [(ly & Hy & HLy)|]; trivial.
rewrite Hy, renamed_value_None; auto.
apply env_up_v_disj_none, env_up_v_disj; trivial.
intro. apply Hlocs in Hy; tauto.
Qed.
Hint Resolve Γr_locs : core.

Lemma Σr_locs Σ1 ll:
  Σ1 ⊔ ll ->
  let Σ2 := Σr Σ1 in
   Σ2 ⊔ ll.
Proof.
intro Hlocs. intro Σ2. subst Σ2. unfold Σr. simpl; split.
- intros l Hl. unfold sr'. simpl.
  rewrite LocMapsFacts.mapi_o; intros; subst; trivial.
  assert(H0 := proj1 (Hlocs) l Hl). unfold sr' in H0. rewrite H0. trivial.
- intros l Hl. unfold br'. simpl.
  assert(H0 := proj2 (Hlocs) l Hl). now unfold br' in H0.
Qed.
Hint Resolve Σr_locs : core.

Lemma E_cod l: Ê Σf lx l -> cod Σf l.
Proof.
intro H. induction H; trivial. destruct H as[H|H].
- apply IHclos_refl_sym_trans_1n.
  apply Hpf in H. destruct H as (_ & Hb & v & _ & Hy).
  exists v. tauto.
- apply IHclos_refl_sym_trans_1n.
  apply Hpf in H. destruct H as (Hb & _ & v & Hy & _ ).
  exists v. tauto.
Qed.

Lemma Γr_eq_mt Γ1 H: Γr (Γ1 := Γ1) H =mt Γ1.
Proof.
unfold Γr. destruct Γ1 as [ [ Γv Γm] Γt ]. case dec_range; simpl.
- intros (s&_). now split.
- now split.
Qed.
Hint Resolve Γr_eq_mt : core.

Lemma Γr_env Σ Γ l v (Henvf: wb_env Γ Σf):
 ((Σ [[ (l, v)↦l envV Γ v]]) ⊆ Σf) ->
  envV Γ v = if L_dec l
             then envV (Γr Henvf) vx
             else envV (Γr Henvf) v.
Proof.
intros Hi. case L_dec; intro HL; unfold Γr.
- case dec_range.
  + intros (ly & Hy & HLy).
    assert(Hv:= sem_up_br_ρ Σ l v (envV Γ v)). apply Hi in Hv. destruct Hv as (Hv&_).
    apply L_values in HL; trivial.
    rewrite Hv in HL; inversion HL; subst.
    rewrite envV_env_up_v_neq, envV_env_up_v_eq; auto.
  + intros Hn.
    rewrite renamed_value_None; trivial.
    assert(Hb:= sem_up_br_br Σ l v (envV Γ v)). apply Hi in Hb.
    case_eq(envV Γ v); trivial.
    intros l0 Hl0. rewrite Hl0 in Hb. apply Hn in Hl0.
    destruct HL as [HE| HL].
    * apply E_cod in HE. destruct HE as (_ & _ & HE). rewrite HE in Hb; discriminate.
    * destruct HL as (l1 & Hl1 & Heq1).
      rewrite Hb in Heq1; inversion Heq1; subst.
      contradict Hl0; now left.
- case dec_range; trivial.
  intros (ly & Hy & HLy). simpl.
  assert(Hb:= sem_up_br_br Σ l v (envV Γ v)). apply Hi in Hb.
  case (val_eq_dec v vx).
  + intro; subst.
    assert(Hv:= sem_up_br_ρ Σ l vx (envV Γ vx)). apply Hi in Hv.
    destruct Hv as (Hv&_). apply Hvx in Hv. tauto.
  + intro Hneq. case (val_eq_dec vy v); intro; subst.
    * contradict HL. right. exists ly. rewrite <- Hy, Hb. split; trivial.
    * rewrite envV_env_up_v_neq; trivial.
      rewrite envV_env_up_v_neq; auto.
Qed.

Lemma Σr_up_br Σ Γ l l' v (Henvf: wb_env Γ Σf):
   ((Σ [[ (l, v)↦l l']]) ⊆ Σf) ->
    sem_equal(Σr (Σ [[ (l, v)↦l l']]))
             (if L_dec l
              then (Σr Σ) [[ (l, vx)↦l l']]
              else (Σr Σ) [[ (l, v)↦l l']]).
Proof.
case L_dec.
* intros HL Hincl. unfold Σr, sem_up_sr, sem_equal. simpl. split; auto.
  apply LocMapsFacts.Equal_mapsto_iff. intros k e. split; intro H.
  - rewrite LocMapsFacts.add_mapsto_iff.
    rewrite LocMapsFacts.mapi_mapsto_iff in H; [|intros; subst; trivial].
    destruct H as (a&Ha1&Ha2). subst.
    rewrite LocMapsFacts.add_mapsto_iff in Ha2. intuition; subst.
    + case L_dec; tauto.
    + right. rewrite LocMapsFacts.mapi_mapsto_iff; [|intros; subst; trivial].
      eexists; eauto.
  - rewrite LocMapsFacts.mapi_mapsto_iff; [|intros; subst; trivial].
    rewrite LocMapsFacts.add_mapsto_iff in H. intuition; subst.
    + case L_dec; try tauto. intros _. eexists; split; eauto.
      rewrite LocMapsFacts.add_mapsto_iff; auto.
    + rewrite LocMapsFacts.mapi_mapsto_iff in H1; [|intros; subst; trivial].
      destruct H1 as (a&Ha1&Ha2). eexists; split; eauto. auto.
      apply LocMapsFacts.add_mapsto_iff; auto.
* intros HL Hincl. unfold Σr, sem_up_sr, sem_equal. simpl. split; auto.
  apply LocMapsFacts.Equal_mapsto_iff. intros k e. split; intro H.
  - rewrite LocMapsFacts.add_mapsto_iff.
    rewrite LocMapsFacts.mapi_mapsto_iff in H; [|intros; subst; trivial].
    destruct H as (a&Ha1&Ha2). subst.
    rewrite LocMapsFacts.add_mapsto_iff in Ha2. intuition; subst.
    + case L_dec; tauto.
    + right. rewrite LocMapsFacts.mapi_mapsto_iff; [|intros; subst; trivial].
      eexists; eauto.
  - rewrite LocMapsFacts.mapi_mapsto_iff; [|intros; subst; trivial].
    rewrite LocMapsFacts.add_mapsto_iff in H. intuition; subst.
    + case L_dec; try tauto. intros _. eexists; split; eauto.
      rewrite LocMapsFacts.add_mapsto_iff; auto.
    + rewrite LocMapsFacts.mapi_mapsto_iff in H1; [|intros; subst; trivial].
      destruct H1 as (a&Ha1&Ha2). eexists; split; eauto. auto.
      apply LocMapsFacts.add_mapsto_iff; auto.
Qed.

Lemma Γr_envM Γ1 H : envM (@Γr Γ1 H) = envM Γ1.
Proof.
unfold Γr; case dec_range; trivial.
intros (ly & Hy & HLy). rewrite envV_up_v_m. trivial.
Qed.

Lemma Γr_envT Γ1 H : envT (@Γr Γ1 H) = envT Γ1.
Proof.
unfold Γr; case dec_range; trivial.
intros (ly & Hy & HLy). rewrite envV_up_v_t. trivial.
Qed.

Lemma Γr_up_m Γ1 (Henvf : wb_env Γ1 _) m Δ H':
  Γr Henvf [[m ↦m Δ]] = Γr (Γ1:=Γ1 [[m ↦m Δ]]) H'.
Proof.
unfold Γr. destruct Γ1 as [ [ Γv Γm] Γt ].
destruct (dec_range Hpf Hcodf Henvf) as [(ly & Hy & HLy)|Hf];
destruct (dec_range Hpf Hcodf H') as [(ly' & Hy' & HLy')|Hf'];
simpl in *; trivial.
- apply False_rect, (Hf' vy ly Hy). now left.
- apply False_rect, (Hf vy ly' Hy'). now left.
Qed.

Lemma Γr_up_t Γ1 (Henvf : wb_env Γ1 _) t Δ H':
  Γr Henvf [[t ↦t Δ]] = Γr (Γ1:=Γ1 [[t ↦t Δ]]) H'.
Proof.
unfold Γr. destruct Γ1 as [ [ Γv Γm] Γt ].
destruct (dec_range Hpf Hcodf Henvf) as [(ly & Hy & HLy)|Hf];
destruct (dec_range Hpf Hcodf H') as [(ly' & Hy' & HLy')|Hf'];
simpl in *; trivial.
- apply False_rect, (Hf' vy ly Hy). now left.
- apply False_rect, (Hf vy ly' Hy'). now left.
Qed.

Lemma env_up_v_neq:
  forall (Γ : env) (v : V) (ly : ℒ), envV (Γ [[v ↦v Some ly]]) v = Some ly.
Proof. intros. destruct Γ as [ [ Γv Γm] Γt ]. simpl. case val_eq_dec; tauto. Qed.

Lemma L_not_m x l: ρ Σf l = Some (IM x) -> ~ L l.
Proof.
intros Hm HL. apply L_values in HL; eauto.
rewrite HL in Hm. discriminate.
Qed.

Lemma Σr_m Σ1 l x:
 (Σ1 ⊆ Σf) ->
    ρ Σ1 l = Some (IM x) -> ρ (Σr Σ1) l = Some (IM x).
Proof.
intros Hi Hl. unfold sr' in *. simpl.
rewrite LocMapsFacts.mapi_o; intros; subst; trivial.
rewrite Hl; simpl. case L_dec; trivial. intro HL.
contradict HL; eapply L_not_m, Hi, Hl.
Qed.
Hint Resolve Σr_m : core.

Lemma L_not_t x l: ρ Σf l = Some (IT x) -> ~ L l.
Proof.
intros Hm HL. apply L_values in HL; eauto.
rewrite HL in Hm. discriminate.
Qed.

Lemma Σr_t Σ1 l x:
 (Σ1 ⊆ Σf) ->
    ρ Σ1 l = Some (IT x) -> ρ (Σr Σ1) l = Some (IT x).
Proof.
intros Hi Hl. unfold sr' in *. simpl.
rewrite LocMapsFacts.mapi_o; intros; subst; trivial.
rewrite Hl. case L_dec; intro HL; trivial.
contradict HL; eapply L_not_t, Hi, Hl.
Qed.
Hint Resolve Σr_t : core.

Lemma Σr_v Σ1 l:
 (Σ1 ⊆ Σf) -> ρ (Σr Σ1) l <> Some (IV vx) ->
    ρ (Σr Σ1) l = ρ Σ1 l.
Proof.
intros Hi. unfold sr' in *. simpl.
rewrite LocMapsFacts.mapi_o; intros; subst; trivial.
revert H; case (LocMaps.find l (sr Σ1)); trivial.
intros j. unfold sr' in *. simpl.
case L_dec; intro HL; tauto.
Qed.

Lemma Σr_v_L Σ1 l:
 (Σ1 ⊆ Σf) -> ρ (Σr Σ1) l <>  ρ Σ1 l -> L l.
Proof.
intros Hi. unfold sr' in *. simpl.
rewrite LocMapsFacts.mapi_o; intros; subst; trivial.
revert H; case (LocMaps.find l (sr Σ1)); simpl; try tauto.
intros j. case L_dec; intros; tauto.
Qed.

Lemma Σr_up_m Σ l m: (Σ [[ l ↦ IM m]]) ⊆ Σf ->
  sem_equal (Σr (Σ [[ l ↦ IM m]])) ((Σr Σ) [[ l ↦ IM m]]).
Proof.
intros Hi. unfold Σr, sem_up_sr. simpl.
f_equal. split; simpl; auto.
apply LocMapsFacts.Equal_mapsto_iff. intros k e. split; intro H.
- rewrite LocMapsFacts.add_mapsto_iff.
  rewrite LocMapsFacts.mapi_mapsto_iff in H; [|intros; subst; trivial].
  destruct H as (a&Ha1&Ha2). subst.
  rewrite LocMapsFacts.add_mapsto_iff in Ha2. intuition; subst.
  + case L_dec; try tauto. intro HL. contradict HL.
    eapply L_not_m. apply Hi. eauto.
  + right. rewrite LocMapsFacts.mapi_mapsto_iff; [|intros; subst; trivial].
    eexists; eauto.
- rewrite LocMapsFacts.mapi_mapsto_iff; intros; subst; trivial.
  rewrite LocMapsFacts.add_mapsto_iff in H.
  destruct H as [(Heq1&Heq2) | (Hneq&H)]; subst.
  + eexists. case L_dec; eauto.
    * intro H; contradict H. eapply L_not_m. apply Hi. eauto.
    * intuition.
  + rewrite LocMapsFacts.mapi_mapsto_iff in H; intros; subst; trivial.
    destruct H as (a&Ha1&Ha2). subst; eexists; eauto.
    case L_dec; eauto.
    * intros HL. rewrite LocMapsFacts.add_mapsto_iff. eauto.
    * intuition.
Qed.

Lemma Σr_up_t Σ l m: (Σ [[ l ↦ IT m]]) ⊆ Σf ->
  sem_equal (Σr (Σ [[ l ↦ IT m]])) ((Σr Σ) [[ l ↦ IT m]]).
Proof.
intro Hi. unfold Σr, sem_up_sr. simpl. f_equal.
f_equal. split; simpl; auto.
apply LocMapsFacts.Equal_mapsto_iff. intros k e. split; intro H.
- rewrite LocMapsFacts.add_mapsto_iff.
  rewrite LocMapsFacts.mapi_mapsto_iff in H; [|intros; subst; trivial].
  destruct H as (a&Ha1&Ha2). subst.
  rewrite LocMapsFacts.add_mapsto_iff in Ha2. intuition; subst.
  + case L_dec; try tauto. intro HL. contradict HL.
    eapply L_not_t. apply Hi. eauto.
  + right. rewrite LocMapsFacts.mapi_mapsto_iff; [|intros; subst; trivial].
    eexists; eauto.
- rewrite LocMapsFacts.mapi_mapsto_iff; intros; subst; trivial.
  rewrite LocMapsFacts.add_mapsto_iff in H.
  destruct H as [(Heq1&Heq2) | (Hneq&H)]; subst.
  + eexists. case L_dec; eauto.
    * intro H; contradict H. eapply L_not_t. apply Hi. eauto.
    * intuition.
  + rewrite LocMapsFacts.mapi_mapsto_iff in H; intros; subst; trivial.
    destruct H as (a&Ha1&Ha2). subst; eexists; eauto.
    case L_dec; eauto.
    * intros HL. rewrite LocMapsFacts.add_mapsto_iff. eauto.
    * intuition.
Qed.

Lemma Σr_v_L' Σ1 l:  (Σ1 ⊆ Σf) -> ρ (Σr Σ1) l = Some (IV vx) -> L l.
Proof.
intro Hi. unfold sr'.
simpl; rewrite LocMapsFacts.mapi_o; [|intros; subst; trivial].
case_eq (LocMaps.find l (sr Σ1)).
- intros j Hj. case L_dec; trivial.
  intros HL Heq. inversion Heq; subst.
  eapply False_rect, Hvx, Hi, Hj.
- intros _ Hf; discriminate Hf.
Qed.

Lemma d_nL lv ls : wb_d Σf (Structural lv ls) ->
  (forall l d, In (l, d) ls -> ~ L l).
Proof.
intro H; inversion H as[lv' ls' _ Hw _ _ |]; subst.
intros l d Hl. apply Hw in Hl. destruct Hl as [_ [(m & Hm) | (t & Ht)] ].
- eapply L_not_m; eauto.
- eapply L_not_t; eauto.
Qed.

Lemma Σr_join Σ1 Δ1 Δ2:
  (Σ1[[Δ1 ⊛ Δ2]] ⊆ Σf) ->
  wb_d Σ1 Δ1 -> wb_d Σ1 Δ2 ->
    Δ1 ⊗ ρ (Σr Σ1) # Δ2 = Δ1 ⊗ ρ Σ1 # Δ2.
Proof.
intros Hi'.
assert(Hi'': forall l1 l2, In (l1, l2) (Δ1 ⊗ ρ Σ1 # Δ2) -> In (l1, l2) (E Σf))
by (intros l1 l2 H12; apply Hi'; simpl; auto with *).
assert(Hi: Σ1 ⊆ Σf) by eauto.
clear Hi'.
revert Δ2 Hi''. induction Δ1 using desc_ind3; intros Δ2 Hi' H1 H2; unfold Σr, join; simpl.
- inversion H1 as [lv' ls' Hw1 Hw2 _ _ |]; subst lv' ls'.
  destruct Δ2 as [lv1 ls2| l2 D1 D2]; unfold sem_join; trivial.
  inversion H2 as [lv' ls' Hw1' Hw2' _ _ |]; subst lv' ls'. f_equal.
  (* values *)
  + clear H1 Hw2 Hw2' H2. revert lv1 Hi' Hw1'.
    fold (Σr Σ1).
    induction lv as [|l1 lv]; intros lv1 Hi' Hw1'; simpl; trivial; f_equal.
    2: (rewrite IHlv; auto with *; clear IHlv;
        simpl; intros la lb Hab; apply Hi'; simpl;
        rewrite app_assoc_reverse, in_app_iff; tauto).
    induction lv1 as [|l2 lv2]; simpl; trivial.
    rewrite IHlv2; auto with *; clear IHlv2.
    {
      f_equal.
      clear IHlv.
      destruct (Hw1 l1) as (v1 & Hv1 & Hb1); auto with *; rewrite Hv1.
      destruct (Hw1' l2) as (v2 & Hv2 & Hb2); auto with *; rewrite Hv2.
      assert(vx <> v1) by (intro; subst; now apply (Hvx l1), Hi).
      assert(vx <> v2) by (intro; subst; now apply (Hvx l2), Hi).
      assert(Hv1' := Hv1). assert (Hv2' := Hv2).
      apply Hi in Hv1. destruct Hv1 as (Hv1&Hb1'); apply Hb1' in Hb1; clear Hb1'.
      apply Hi in Hv2. destruct Hv2 as (Hv2&Hb2'); apply Hb2' in Hb2; clear Hb2'.
      rewrite ρ_Σr, ρ_Σr, Hv1', Hv2'.
      case L_dec; intro HL1; case L_dec; intro HL2;
      case option_eq_dec; intro Heq; try tauto; try inversion Heq; subst; try tauto;
      case option_eq_dec; trivial; intro Heq2; try inversion Heq2; subst; try tauto.
      -- apply L_values in HL1; apply L_values in HL2; trivial.
          rewrite HL1, <- HL2, Hv2 in Hv1. inversion Hv1; subst; tauto.
      -- destruct HL1 as [HE|(l0&HL0&Hb0)]; [|rewrite Hb1 in Hb0; discriminate].
         simpl. clear Heq Heq2.
         contradict HL2. left. apply Operators_Properties.clos_rst1n_trans with l1; trivial.
         econstructor 2; trivial. left. apply Hi'.
         simpl. repeat rewrite in_app_iff. left. left. left.
         rewrite Hv1', Hv2'. case option_eq_dec; auto with *; tauto.
      -- destruct HL2 as [HE|(l0&HL0&Hb0)]; [|rewrite Hb2 in Hb0; discriminate].
         simpl. clear Heq Heq2.
         contradict HL1. left. apply Operators_Properties.clos_rst1n_trans with l2; trivial.
         econstructor 2; trivial. right. apply Hi'.
         simpl. repeat rewrite in_app_iff. left. left. left.
         rewrite Hv1', Hv2'. case option_eq_dec; auto with *; try tauto.
    }
    simpl; intros la lb; repeat rewrite in_app_iff.
    intros [ [Hab|Hab]|Hab]; apply Hi'; simpl.
    * repeat rewrite in_app_iff. tauto.
    * repeat rewrite in_app_iff. clear IHlv. left. right.
      rewrite in_flat_map in Hab. destruct Hab as (l0&Hin&Hab).
      apply in_flat_map. exists l0; split; trivial.
      rewrite in_app_iff. tauto.
    * repeat rewrite in_app_iff. tauto.
  (* submodules *)
  + clear Hw1 Hw1' H1 H2.
  induction ls as [|(l1 & d1) ls]; simpl; trivial. rewrite IHls; eauto with *.
    2: (simpl; intros la lb; rewrite in_app_iff; intros [Hab|Hab]; apply Hi'; simpl;
        rewrite in_app_iff; auto with *; tauto).
    clear IHls.  f_equal.
    induction ls2 as [|(l2 & d2) ls']; simpl; trivial.
    assert(Hi'': forall l0 l3 : ℒ,
        In (l0, l3) (Structural lv ((l1, d1) :: ls) ⊗ ρ Σ1 # Structural lv1 ls') ->
          In (l0, l3) (E Σf)).
    {
      clear IHls'. simpl. intros l1' l2'. repeat rewrite in_app_iff.
      intros [HL|[HL|HR] ]; apply Hi'; simpl;
      repeat rewrite in_app_iff; try tauto. right. right.
      generalize HR; clear.
      induction ls as [|(l0 & d0) ls]; simpl in *; repeat rewrite in_app_iff; trivial.
      intros [H|H]; tauto.
    }
    rewrite IHls'; eauto with *.
    f_equal. clear IHls'. fold join (Σr Σ1).
    destruct (Hw2 l1 d1) as (Hwb1 & H1); auto with *.
    destruct (Hw2' l2 d2) as (Hwb2 & H2); auto with *.
    assert(H1' : exists x, ρ Σ1 l1 = Some x /\ ~ L l1) by
      (destruct H1 as [(x1 & H1)|(x1 & H1)]; rewrite H1;
       eexists; split; eauto; [eapply L_not_m|eapply L_not_t]; apply Hi; eauto).
    assert(H2' : exists x, ρ Σ1 l2 = Some x /\ ~ L l2) by
      (destruct H2 as [(x2 & H2)|(x2 & H2)]; rewrite H2;
       eexists; split; eauto; [eapply L_not_m|eapply L_not_t]; apply Hi; eauto).
    clear H1 H2.
    destruct H1' as (x1 & H1 & HL1). destruct H2' as (x2 & H2 & HL2).
    rewrite ρ_Σr, ρ_Σr, H1, H2.
    case L_dec; try tauto; case L_dec; try tauto. intros _ _.
    case option_eq_dec; trivial. intro Heq. inversion Heq;  subst; clear Heq.
    eapply H; eauto; [left; trivial|]. intros l1' l2' H12'. apply Hi'.
    simpl. repeat rewrite in_app_iff. right; left; left.
    rewrite H1, H2. case option_eq_dec; trivial; tauto.
- destruct Δ2 as [lv' ls'|l' d1' d2']; simpl; trivial.
  fold join. inversion H1; inversion H2.
  rewrite IHΔ1_1, IHΔ1_2; trivial; subst;
  intros; apply Hi'; simpl; auto with *.
Qed.
Hint Rewrite Σr_join : core.

Lemma Σr_sem_join Σ1 Δ1 Δ2:
  (Σ1[[Δ1 ⊛ Δ2]] ⊆ Σf) -> wb_d Σ1 Δ1 -> wb_d Σ1 Δ2 ->
    Σr (Σ1 [[Δ1 ⊛ Δ2]]) = (Σr Σ1) [[Δ1 ⊛ Δ2]].
Proof. intros. unfold Σr, sem_join. simpl; erewrite <- Σr_join; auto. Qed.

Lemma Σr_filtering Σ1 Δ m: (Σ1 ⊆ Σf) -> (ρ (Σr Σ1) \ Δ) m = (ρ Σ1 \ Δ) m.
Proof.
intro Hi. destruct Δ as [lv ls|]; trivial. simpl. f_equal.
induction ls as [|(l&d) ls]; trivial. simpl.
rewrite ρ_Σr. case option_eq_dec.
- case_eq (ρ Σ1 l);[|discriminate].
  intros j Hj. case L_dec;[discriminate|].
  case option_eq_dec; trivial. tauto.
- case_eq (ρ Σ1 l).
  + intros j Hj. case L_dec.
    * intros Hl _. case option_eq_dec.
      -- intro Heq; inversion Heq; subst. clear Heq.
         apply Hi in Hj. eapply L_not_m in Hl;[tauto|intuition; eauto].
      -- intro. now f_equal.
    * case option_eq_dec; [tauto|]. intros. now f_equal.
  + case option_eq_dec; try tauto. intros. now f_equal.
Qed.

Lemma wb_d_cons_lv Σ1 a lv ls:
  wb_d Σ1 (Structural (a :: lv) ls) ->
    wb_d Σ1 (Structural lv ls).
Proof.
intros H. inversion H as [lv' ls' H1 H2 H3 H4|]; subst lv' ls'.
constructor; auto with *.
Qed.
Hint Resolve wb_d_cons_lv : core.

Lemma wb_d_cons_ls Σ1 l d lv ls:
  wb_d Σ1 (Structural lv ((l,d) :: ls)) -> wb_d Σ1 (Structural lv ls).
Proof.
intros H. inversion H as [lv' ls' H1 H2 H3 H4|]; subst lv' ls'.
constructor; auto with *.
Qed.
Hint Resolve wb_d_cons_ls : core.

Lemma wb_d_hd_ls Σ1 l d lv ls: wb_d Σ1 (Structural lv ((l,d) :: ls)) -> wb_d Σ1 d.
Proof.
intros H. inversion H as [lv' ls' H1 H2 H3 H4|]; subst lv' ls'.
eapply H2; auto with *.
Qed.
Hint Resolve wb_d_hd_ls : core.

Lemma In_join_cons_lv_l r l1 l2 lv ls lv' ls' a:
  In (l1, l2) (Structural lv ls ⊗ r # Structural lv' ls') ->
    In (l1, l2) (Structural (a :: lv) ls ⊗ r# Structural lv' ls').
Proof. simpl. repeat rewrite in_app_iff. tauto. Qed.
Hint Resolve In_join_cons_lv_l : core.

Lemma In_join_cons_lv_r r l1 l2 d lv' ls' a:
  In (l1, l2) (d ⊗ r # Structural lv' ls') ->
    In (l1, l2) (d ⊗ r# Structural (a :: lv') ls').
Proof.
destruct d; simpl; repeat rewrite in_app_iff; intuition; left;
apply in_flat_map; apply in_flat_map in H0; destruct H0 as (x&Hinx&Hx);
exists x;split; trivial; apply in_app_iff; now right.
Qed.
Hint Resolve In_join_cons_lv_r : core.

Lemma In_join_cons_ls_l r l1 l2 lv ls d a:
  In (l1, l2) (Structural lv ls ⊗ r # d) ->
    In (l1, l2) (Structural lv (a :: ls) ⊗ r# d).
Proof. destruct d; simpl; repeat rewrite in_app_iff; tauto. Qed.
Hint Resolve In_join_cons_ls_l : core.

Lemma In_join_cons_ls_r r l1 l2 d lv' ls' a:
  In (l1, l2) (d ⊗ r # Structural lv' ls') ->
    In (l1, l2) (d ⊗ r# Structural lv' (a :: ls')).
Proof.
destruct d; simpl; repeat rewrite in_app_iff; intuition; right;
apply in_flat_map; apply in_flat_map in H0; destruct H0 as (x&Hinx&Hx);
exists x;split; trivial; apply in_app_iff; now right.
Qed.
Hint Resolve In_join_cons_ls_r : core.

Lemma app_flat_map {A B} (f: A-> list B) g l x:
  In x (flat_map (fun y => f y ++ g y) l) <->
  (In x (flat_map f l) \/ In x (flat_map g l)).
Proof. induction l as [|a l]; simpl; try tauto. repeat rewrite in_app_iff; intuition. Qed.

Lemma wb_d_ls_not_None Σ1 lv ls l d:
  wb_d Σ1 (Structural lv ls) -> In (l,d) ls -> ~ ρ Σ1 l = None.
Proof.
 intros Hwb Hin; inversion Hwb as [lv' ls' _ H _ _ |]; subst.
 destruct (H l d Hin) as [_ [(x&Hx) |(x&Hx)] ]; rewrite Hx; discriminate.
Qed.

Lemma wb_d_ls_not_V Σ1 lv ls l d v:
  wb_d Σ1 (Structural lv ls) -> In (l,d) ls -> ~ ρ Σ1 l = Some (IV v).
Proof.
 intros Hwb Hin; inversion Hwb as [lv' ls' _ H _ _ |]; subst.
 destruct (H l d Hin) as [_ [(x&Hx) |(x&Hx)] ]; rewrite Hx; discriminate.
Qed.

Add Parametric Relation: ℒ (Ê Σf)
  reflexivity proved by (Relation_Operators.rst1n_refl _ ((fun l l' : ℒ => In (l, l') (E Σf))))
  symmetry proved by (Operators_Properties.clos_rst1n_sym  _ ((fun l l' : ℒ => In (l, l') (E Σf))))
  transitivity proved by (Operators_Properties.clos_rst1n_trans  _ ((fun l l' : ℒ => In (l, l') (E Σf))))
    as Ef.

Lemma E_incl l1 l2 : In (l1, l2) (E Σf) -> Ê Σf l1 l2.
Proof. intro. econstructor; eauto. Qed.
Hint Resolve E_incl : core.

Lemma join_equiv Σ Δ1 Δ2: Σ [[Δ1 ⊛ Δ2]] ⊆ Σf ->
  forall l1 l2, In (l1, l2) (Δ1 ⊗ ρ Σ# Δ2) -> L l1 <-> L l2.
Proof.
intros Hi l1 l2 Hj.
assert (H12: In (l1, l2) (E Σf)) by (apply Hi; simpl; auto with *).
 split; intro HL.
- destruct HL as [HE|(y&_&Hb)].
  + left; etransitivity; eauto; now apply E_incl.
  + apply Hpf in H12. rewrite Hb in H12; intuition; discriminate.
- destruct HL as [HE|(y&_&Hb)].
  + left; etransitivity; eauto; symmetry. now apply E_incl.
  + apply Hpf in H12. rewrite Hb in H12; intuition; discriminate.
Qed.

(* The following three lemmas are quite redundant *)
Lemma Σr_sup Σ1 Δ1 Δ2: Σ1 ⊆ Σf ->
  (Σ1[[Δ1 ⊛ Δ2]] ⊆ Σf) -> wb_d Σ1 Δ1 -> wb_d Σ1 Δ2 ->
    Δ1 ⊕ρ (Σr Σ1)# Δ2 = Δ1 ⊕ρ Σ1# Δ2.
Proof. intros Hi Hi'' Hw1 Hw2. unfold Σr.
assert (HiL: (forall l1 l2, In (l1, l2) (Δ1 ⊗ ρ Σ1# Δ2) -> L l1 <-> L l2))
 by (now apply join_equiv).
 fold (Σr Σ1).
clear Hi''. destruct Δ1 as [lv ls|]; destruct Δ2 as [lv' ls'|]; try tauto.
unfold desc_sup. simpl. f_equal.
- f_equal. induction lv; trivial; simpl.
  case Exists_dec; case Exists_dec; trivial.
  + rewrite IHlv; auto. eapply wb_d_cons_lv. eauto.
  + intros Hf1 Hf2. contradict Hf1.
    apply Exists_exists in Hf2. destruct Hf2 as (x&Hin&Hx).
    apply Exists_exists. exists x. split; trivial.
    do 2 rewrite ρ_Σr in Hx.
    case_eq(ρ Σ1 a).
    * intros va Ha.
      rewrite Ha in Hx. case_eq(ρ Σ1 x).
     -- intros x' Hx'. rewrite Hx' in Hx.
        case L_dec as [HL'|HL'] in Hx; case L_dec as [HL|HL] in Hx; trivial.
        ++ apply Hi in Ha. apply Hi in Hx'.
           apply L_values in HL. apply L_values in HL'.
           destruct Ha as (Ha&_). destruct Hx' as (Hx'&_).
           rewrite HL in Ha. rewrite HL' in Hx'.
           inversion Ha; subst. inversion Hx'; subst. trivial.
        ++ rewrite <- Hx in Ha. apply Hi in Ha. apply False_rect, (Hvx a). tauto.
        ++ rewrite Hx in Hx'. apply Hi in Hx'. apply False_rect, (Hvx x). tauto.
     -- intro Hx'; rewrite Hx' in Hx. case L_dec in Hx; discriminate.
    * intro Hx'; rewrite Hx' in Hx. revert Hx. case (ρ Σ1 x); trivial.
     intro. case L_dec; discriminate.
  + intros Hf2 Hf1. contradict Hf1. apply Exists_exists in Hf2. destruct Hf2 as (x&Hin&Hx).
    apply Exists_exists. exists x. split; trivial. simpl.
    rewrite ρ_Σr, ρ_Σr, Hx. case_eq (ρ Σ1 x); trivial. intros j Hj.
    case L_dec; intro HLa; case L_dec; intro HLx; trivial.
    -- contradict HLx. rewrite HiL; eauto.
       simpl. repeat rewrite in_app_iff. left. left.
       apply in_flat_map.
       exists x; split; trivial. rewrite Hx. case option_eq_dec; auto with *; tauto.
    -- contradict HLa. rewrite <- HiL; eauto.
       simpl. repeat rewrite in_app_iff. left. left. apply in_flat_map.
       exists x; split; trivial. rewrite Hx. case option_eq_dec; auto with *; tauto.
    -- case_eq (ρ Σ1 a); trivial. intros j Hj Hx'; rewrite Hj, Hx' in Hx.
       discriminate.
  + rewrite IHlv; trivial. eapply wb_d_cons_lv. eauto.
    intros. apply HiL. auto.
- f_equal. induction ls as [|(l'&d')];trivial. simpl.
  case Exists_dec; intro HE; case Exists_dec; intro HE';
  [rewrite IHls; auto; eapply wb_d_cons_ls; eauto| | |
   rewrite IHls; auto; eapply wb_d_cons_ls; eauto]; clear IHls.
  + contradict HE'. apply Exists_exists in HE. apply Exists_exists.
    destruct HE as ((l&d)&Hin&Hld). exists (l,d). split; trivial.
    revert Hld. simpl. do 2 rewrite ρ_Σr.
    case_eq (ρ Σ1 l'); case_eq (ρ Σ1 l); trivial; case L_dec;
    try discriminate; case L_dec; trivial.
    * intros HL HL' j Hj j' Hj' _.
      apply L_values in HL. apply L_values in HL'.
      apply Hi in Hj. apply Hi in Hj'.
      now rewrite <- (proj1 Hj), HL', <- (proj1 Hj'), HL.
    * intros _ _ j _ j' Hj' HF. apply Hi in Hj'.
      eapply False_rect, Hvx. now rewrite (proj1 Hj'), <- HF.
    * intros _ _ j Hj j' _ HF. apply Hi in Hj.
      eapply False_rect, Hvx. now rewrite (proj1 Hj), <- HF.
  + contradict HE. apply Exists_exists in HE'. apply Exists_exists.
    destruct HE' as ((l&d)&Hin&Hld). exists (l,d). split; trivial.
    do 2 rewrite ρ_Σr.
    revert Hld. simpl. case_eq (ρ Σ1 l'); case_eq (ρ Σ1 l); trivial; case L_dec;
    try discriminate; case L_dec; trivial.
    * intros HL' HL j' Hj' j Hj Heq. inversion Heq; subst; clear Heq.
      eapply wb_d_ls_not_V with (v:=vy) in Hw2; eauto with *.
      apply L_values in HL. rewrite Hj' in Hw2. apply Hi in Hj'.
      rewrite <- (proj1 Hj') in Hw2. tauto.
    * intros HL HL' j Hj j' Hj' Heq. inversion Heq; subst; clear Heq.
      eapply wb_d_ls_not_V with (v:=vy) in Hw1; auto with *.
      apply L_values in HL. rewrite Hj' in Hw1. apply Hi in Hj'.
      rewrite <- (proj1 Hj') in Hw1. tauto.
Qed.

Lemma Σr_sup_lv Σ1 lv ls lv' ls': Σ1 ⊆ Σf ->
  (Σ1[[Structural lv nil ⊛ Structural lv' nil]] ⊆ Σf) ->
  wb_d Σ1 (Structural lv ls)-> wb_d Σ1 (Structural lv' ls') ->
    (Structural lv ls) ⊕ρ (Σr Σ1)# (Structural lv' ls') =
    (Structural lv ls) ⊕ρ Σ1# (Structural lv' ls').
Proof. intros Hi Hi'' Hw1 Hw2. unfold Σr.
assert (HiL: (forall l1 l2, In (l1, l2) (Structural lv nil ⊗ ρ Σ1# Structural lv' nil) -> L l1 <-> L l2))
 by (now apply join_equiv).
clear Hi''.
unfold desc_sup. simpl. f_equal.
- f_equal. induction lv; trivial; simpl. fold (Σr Σ1) in *.
  case Exists_dec; case Exists_dec; trivial.
  + rewrite IHlv; auto. eapply wb_d_cons_lv. eauto.
  + intros Hf1 Hf2. contradict Hf1.
    apply Exists_exists in Hf2. destruct Hf2 as (x&Hin&Hx).
    apply Exists_exists. exists x. split; trivial.
    case_eq(ρ Σ1 a).
    * intros va Ha. rewrite ρ_Σr, ρ_Σr, Ha in Hx. case_eq(ρ Σ1 x).
     -- intros x' Hx'. rewrite Hx' in Hx.
        case L_dec as [HL'|HL'] in Hx; case L_dec as [HL|HL] in Hx; trivial.
        ++ apply Hi in Ha. apply Hi in Hx'.
           apply L_values in HL. apply L_values in HL'.
           destruct Ha as (Ha&_). destruct Hx' as (Hx'&_).
           rewrite HL in Ha. rewrite HL' in Hx'.
           inversion Ha; subst. inversion Hx'; subst. trivial.
        ++ rewrite <- Hx in Ha. apply Hi in Ha. apply False_rect, (Hvx a). tauto.
        ++ rewrite Hx in Hx'. apply Hi in Hx'. apply False_rect, (Hvx x). tauto.
     -- intro Hx'; rewrite Hx' in Hx. case L_dec in Hx; discriminate.
    * intro Hx'; rewrite ρ_Σr, ρ_Σr, Hx' in Hx. revert Hx. case (ρ Σ1 x); trivial.
     intro. case L_dec; discriminate.
  + intros Hf2 Hf1. contradict Hf1. apply Exists_exists in Hf2. destruct Hf2 as (x&Hin&Hx).
    apply Exists_exists. exists x. split; trivial. simpl.
    rewrite ρ_Σr, ρ_Σr, Hx. case_eq (ρ Σ1 x); trivial. intros j Hj.
    case L_dec; intro HLa; case L_dec; intro HLx; trivial.
    -- contradict HLx. rewrite HiL; eauto.
       simpl. repeat rewrite in_app_iff. left. left.
       apply in_flat_map.
       exists x; split; trivial. rewrite Hx. case option_eq_dec; auto with *; tauto.
    -- contradict HLa. rewrite <- HiL; eauto.
       simpl. repeat rewrite in_app_iff. left. left. apply in_flat_map.
       exists x; split; trivial. rewrite Hx. case option_eq_dec; auto with *; tauto.
    -- case_eq (ρ Σ1 a); trivial. intros j Hj Hx'; rewrite Hj, Hx' in Hx.
       discriminate.
  + rewrite IHlv; trivial. eapply wb_d_cons_lv. eauto.
    intros. apply HiL. auto.
- f_equal. induction ls as [|(l'&d')];trivial. simpl.
  case Exists_dec; intro HE; case Exists_dec; intro HE';
  [rewrite IHls; auto; eapply wb_d_cons_ls; eauto| | |
   rewrite IHls; auto; eapply wb_d_cons_ls; eauto]; clear IHls.
  + contradict HE'. apply Exists_exists in HE. apply Exists_exists.
    destruct HE as ((l&d)&Hin&Hld). exists (l,d). split; trivial.
    revert Hld. simpl. fold (Σr Σ1). do 2 rewrite ρ_Σr.
    case_eq (ρ Σ1 l'); case_eq (ρ Σ1 l); trivial; case L_dec;
    try discriminate; case L_dec; trivial.
    * intros HL HL' j Hj j' Hj' _.
      apply L_values in HL. apply L_values in HL'.
      apply Hi in Hj. apply Hi in Hj'.
      now rewrite <- (proj1 Hj), HL', <- (proj1 Hj'), HL.
    * intros _ _ j _ j' Hj' HF. apply Hi in Hj'.
      eapply False_rect, Hvx. now rewrite (proj1 Hj'), <- HF.
    * intros _ _ j Hj j' _ HF. apply Hi in Hj.
      eapply False_rect, Hvx. now rewrite (proj1 Hj), <- HF.
  + contradict HE. apply Exists_exists in HE'. apply Exists_exists.
    destruct HE' as ((l&d)&Hin&Hld). exists (l,d). split; trivial.
    fold (Σr Σ1). do 2 rewrite ρ_Σr.
    revert Hld. simpl. case_eq (ρ Σ1 l'); case_eq (ρ Σ1 l); trivial; case L_dec;
    try discriminate; case L_dec; trivial.
    * intros HL' HL j' Hj' j Hj Heq. inversion Heq; subst; clear Heq.
      eapply wb_d_ls_not_V with (v:=vy) in Hw2; eauto with *.
      apply L_values in HL. rewrite Hj' in Hw2. apply Hi in Hj'.
      rewrite <- (proj1 Hj') in Hw2. tauto.
    * intros HL HL' j Hj j' Hj' Heq. inversion Heq; subst; clear Heq.
      eapply wb_d_ls_not_V with (v:=vy) in Hw1; auto with *.
      apply L_values in HL. rewrite Hj' in Hw1. apply Hi in Hj'.
      rewrite <- (proj1 Hj') in Hw1. tauto.
Qed.

Lemma Σr_sup_ls Σ1 l0 Δ1 Δ2: Σ1 ⊆ Σf -> ρ Σf l0 <> Some (IV vy) -> wb_d Σ1 Δ2 ->
    Structural nil ((l0, Δ1):: nil) ⊕ρ (Σr Σ1)# Δ2 = Structural nil ((l0, Δ1):: nil)  ⊕ρ Σ1# Δ2.
Proof.
intros Hi Hw1 Hw2.
unfold desc_sup. destruct Δ2 as [lv' ls'|ŀ' Δ2_1 Δ2_2]; simpl; trivial.
rewrite app_nil_r. f_equal. f_equal.
case Exists_dec; case Exists_dec; trivial; intros HE' HE.
- contradict HE'. apply Exists_exists in HE. apply Exists_exists.
  destruct HE as ((l&d)&Hin&Hld). exists (l,d). split; trivial.
  revert Hld. simpl. do 2 rewrite ρ_Σr.
  case_eq (ρ Σ1 l0); case_eq (ρ Σ1 l); trivial; case L_dec;
  try discriminate; case L_dec; trivial.
  + intros HL HL' j Hj j' Hj' _.
    apply L_values in HL. apply L_values in HL'.
    apply Hi in Hj. apply Hi in Hj'.
    now rewrite <- (proj1 Hj), HL', <- (proj1 Hj'), HL.
  + intros _ _ j _ j' Hj' HF. apply Hi in Hj'.
    eapply False_rect, Hvx. now rewrite (proj1 Hj'), <- HF.
  + intros _ _ j Hj j' _ HF. apply Hi in Hj.
    eapply False_rect, Hvx. now rewrite (proj1 Hj), <- HF.
- contradict HE. apply Exists_exists in HE'. apply Exists_exists.
  destruct HE' as ((l&d)&Hin&Hld). exists (l,d). split; trivial.
  revert Hld. simpl. do 2 rewrite ρ_Σr.
  case_eq (ρ Σ1 l0); case_eq (ρ Σ1 l); trivial; case L_dec;
  try discriminate; case L_dec; trivial.
  * intros HL' HL j' Hj' j Hj Heq. inversion Heq; subst; clear Heq.
    eapply wb_d_ls_not_V with (v:=vy) in Hw2; eauto with *.
    apply L_values in HL. rewrite Hj' in Hw2. apply Hi in Hj'.
    rewrite <- (proj1 Hj') in Hw2. tauto.
  * intros HL HL' j Hj j' Hj' Heq. inversion Heq; subst; clear Heq.
    apply L_values in HL. tauto.
Qed.

Lemma Σr_d_mod Σ1 Δ1 Δ2: (forall l1 l2, In (l1, l2) (Δ1 ⊗ ρ Σ1# Δ2) -> L l1 <-> L l2) ->
  Σ1 ⊆ Σf -> wb_d Σ1 Δ1 -> wb_d Σ1 Δ2 ->
    Δ1 ⊳ ρ (Σr Σ1) # Δ2 = Δ1 ⊳ ρ Σ1 # Δ2.
Proof.
intros HiL Hi.
revert Δ2 HiL. induction Δ1 using desc_ind3;
destruct Δ2 as [lv' ls'|ŀ' Δ2_1 Δ2_2]; intros HiL Hw1 Hw2; trivial.
- unfold desc_mod. f_equal; f_equal.
  + clear H. induction lv; trivial. unfold filter.
    case Exists_dec; intro Hf1; case Exists_dec; intro Hf2.
    * f_equal; apply IHlv; eauto.
    * contradict Hf2. apply Exists_exists in Hf1. destruct Hf1 as (x&Hin&Hx).
      apply Exists_exists. exists x. split; trivial.
      simpl in Hx. case_eq(ρ Σ1 a).
      -- intros va Ha. rewrite ρ_Σr, ρ_Σr, Ha in Hx. case_eq(ρ Σ1 x).
         ++ intros x' Hx'. rewrite Hx' in Hx.
            case L_dec as [HL|HL] in Hx; case L_dec as [HL'|HL'] in Hx; trivial.
            ** apply Hi in Ha. apply Hi in Hx'.
               apply L_values in HL. apply L_values in HL'.
               destruct Ha as (Ha&_). destruct Hx' as (Hx'&_).
               rewrite HL in Ha. rewrite HL' in Hx'.
               inversion Ha; subst. inversion Hx'; subst. trivial.
            ** rewrite <- Hx in Hx'. apply Hi in Hx'. apply False_rect, (Hvx x). tauto.
            ** rewrite Hx in Ha. apply Hi in Ha. apply False_rect, (Hvx a). tauto.
         ++ intro Hx'; rewrite Hx' in Hx. case L_dec in Hx; discriminate.
      -- intro Hx'; rewrite ρ_Σr, ρ_Σr, Hx' in Hx.
         revert Hx. case (ρ Σ1 x); trivial.
         intro. case L_dec; discriminate.
    * contradict Hf1. apply Exists_exists in Hf2. destruct Hf2 as (x&Hin&Hx).
      apply Exists_exists. exists x. split; trivial. simpl.
      rewrite ρ_Σr, ρ_Σr, Hx. case_eq (ρ Σ1 x); trivial. intros j Hj.
      case L_dec; intro HLa; case L_dec; intro HLx; trivial.
      -- contradict HLx. rewrite <- HiL; eauto.
         simpl. repeat rewrite in_app_iff. left. left. apply in_flat_map.
         exists x; split; trivial. rewrite Hx. case option_eq_dec; auto with *; tauto.
      -- contradict HLa. rewrite HiL; eauto.
         simpl. repeat rewrite in_app_iff. left. left. apply in_flat_map.
         exists x; split; trivial. rewrite Hx. case option_eq_dec; auto with *; tauto.
    * apply IHlv; eauto.
  + clear H. induction lv'; trivial. unfold filter.
    case Exists_dec; intro Hf1; case Exists_dec; intro Hf2.
    * apply IHlv'; eauto.
    * contradict Hf2. apply Exists_exists in Hf1. destruct Hf1 as (x&Hin&Hx).
      apply Exists_exists. exists x. split; trivial.
      simpl in Hx. case_eq(ρ Σ1 a).
      -- intros va Ha. rewrite ρ_Σr, ρ_Σr, Ha in Hx. case_eq(ρ Σ1 x).
         ++ intros x' Hx'. rewrite Hx' in Hx.
            case L_dec as [HL|HL] in Hx; case L_dec as [HL'|HL'] in Hx; trivial.
            ** apply Hi in Ha. apply Hi in Hx'.
               apply L_values in HL. apply L_values in HL'.
               destruct Ha as (Ha&_). destruct Hx' as (Hx'&_).
               rewrite HL in Ha. rewrite HL' in Hx'.
               inversion Ha; subst. inversion Hx'; subst. trivial.
            ** rewrite <- Hx in Hx'. apply Hi in Hx'. apply False_rect, (Hvx x). tauto.
            ** rewrite Hx in Ha. apply Hi in Ha. apply False_rect, (Hvx a). tauto.
         ++ intro Hx'; rewrite Hx' in Hx. case L_dec in Hx; discriminate.
      -- intro Hx'; rewrite ρ_Σr, ρ_Σr, Hx' in Hx. revert Hx. case (ρ Σ1 x); trivial.
         intro. case L_dec; discriminate.
    * contradict Hf1. apply Exists_exists in Hf2. destruct Hf2 as (x&Hin&Hx).
      apply Exists_exists. exists x. split; trivial. simpl.
      rewrite ρ_Σr, ρ_Σr, Hx. case_eq (ρ Σ1 x); trivial. intros j Hj.
      case L_dec; intro HLa; case L_dec; intro HLx; trivial.
      -- contradict HLx. rewrite HiL; eauto.
         simpl. repeat rewrite in_app_iff. left.
         apply app_flat_map. left. apply in_flat_map.
         exists x; split; trivial. rewrite Hx. case option_eq_dec; auto with *; tauto.
      -- contradict HLa. rewrite <- HiL; eauto.
         simpl. repeat rewrite in_app_iff. left. apply app_flat_map. left. apply in_flat_map.
         exists x; split; trivial. rewrite Hx. case option_eq_dec; auto with *; tauto.
    * f_equal. apply IHlv'; eauto.
  + fold desc_mod. induction ls as [|(l&d)];trivial. simpl. f_equal;
    [|apply IHls; auto; intros; eauto; eapply H; eauto; right; eauto]. clear IHls.
    induction ls' as [|(l'&d')]; trivial. simpl. rewrite IHls' by eauto. clear IHls'.
    f_equal. case_eq (ρ Σ1 l).
    * intros j Hj. case_eq (ρ Σ1 l').
     -- intros j' Hj'.
        assert(Hj0:= Hj). apply Hi in Hj. destruct Hj as (Hj&_).
        assert(Hj0':= Hj'). apply Hi in Hj'. destruct Hj' as (Hj'&_).
        do 2 rewrite ρ_Σr.
        case L_dec; intro HL.
       ++ apply L_values in HL. eapply wb_d_ls_not_V with (v:=vy) in Hw1; auto with *.
          contradict Hw1. now rewrite Hj0, <- Hj.
       ++ case L_dec; intro HL'.
        ** apply L_values in HL'. eapply wb_d_ls_not_V with (v:=vy) in Hw2; auto with *.
           contradict Hw2. now rewrite Hj0', <- Hj'.
        ** rewrite Hj0, Hj0'.
           case option_eq_dec; trivial. intro Heq; inversion Heq; subst.
           f_equal. erewrite H; eauto; auto with *.
           intros. apply HiL. simpl. repeat rewrite in_app_iff. right; left.
           rewrite Hj0', Hj0. case option_eq_dec; tauto.
      -- intro Hn'. eapply False_rect, wb_d_ls_not_None; eauto. auto with *.
    * intro Hn. clear Hw2. eapply False_rect, wb_d_ls_not_None; eauto. auto with *.
  + induction ls' as [|(l'&d') ls'];trivial. unfold filter.
    case Exists_dec; intro HE; case Exists_dec; intro HE';
    [f_equal; eauto| | |f_equal;eauto]; clear IHls'.
    * contradict HE'. apply Exists_exists in HE. apply Exists_exists.
      destruct HE as ((l&d)&Hin&Hld). exists (l,d). split; trivial.
      revert Hld. simpl. do 2 rewrite ρ_Σr.
      case_eq (ρ Σ1 l'); case_eq (ρ Σ1 l); trivial; case L_dec;
      try discriminate; case L_dec; trivial.
     -- intros HL HL' j Hj j' Hj' _.
        apply L_values in HL. apply L_values in HL'.
        apply Hi in Hj. apply Hi in Hj'.
        now rewrite <- (proj1 Hj), HL, <- (proj1 Hj'), HL'.
     -- intros _ _ j Hj j' _ HF. apply Hi in Hj.
        eapply False_rect, Hvx. now rewrite (proj1 Hj), <- HF.
     -- intros _ _ j _ j' Hj' HF. apply Hi in Hj'.
        eapply False_rect, Hvx. now rewrite (proj1 Hj'), <- HF.
    * contradict HE. apply Exists_exists in HE'. apply Exists_exists.
      destruct HE' as ((l&d)&Hin&Hld). exists (l,d). split; trivial.
      revert Hld. simpl. do 2 rewrite ρ_Σr.
      case_eq (ρ Σ1 l'); case_eq (ρ Σ1 l); trivial; case L_dec;
      try discriminate; case L_dec; trivial.
     -- intros HL HL' j Hj j' Hj' Heq. inversion Heq; subst; clear Heq.
        eapply wb_d_ls_not_V with (v:=vy) in Hw2; auto with *.
        apply L_values in HL'. rewrite Hj' in Hw2. apply Hi in Hj'.
        rewrite (proj1 Hj') in  HL'. tauto.
     -- intros HL' HL j' Hj' j Hj Heq. inversion Heq; subst; clear Heq.
        eapply wb_d_ls_not_V with (v:=vy) in Hw1; eauto.
        apply L_values in HL'. rewrite Hj' in Hw1. apply Hi in Hj'.
        rewrite (proj1 Hj') in  HL'. tauto.
- inversion Hw2; subst. inversion Hw1; subst.
  unfold desc_mod. f_equal; fold desc_mod.
  + apply IHΔ1_1; trivial. intros. apply HiL. simpl. auto with *.
  + apply IHΔ1_2; trivial. intros. apply HiL. simpl. auto with *.
Qed.

Lemma Σr_s_mod Σ1 Δ1 Δ2 m lm:
  Σ1 ⊆ Σf -> (ρ Σ1 lm = Some (IM m)) ->
  ((Σ1 [[Δ1 ⊛ Structural nil ((lm, Δ2) :: nil)]]) ⊆ Σf) ->
  wb_d Σ1 Δ1 -> wb_d Σ1 Δ2 ->
    Δ1 ⊲ ρ (Σr Σ1) # (m, Δ2) = Δ1 ⊲ ρ Σ1 # (m, Δ2).
Proof.
intros Hi Hlm Hi' Hw1 Hw2.
assert(HL0: forall l1 l2 : ℒ, In (l1, l2) (Δ1 ⊗ ρ Σ1 # Structural nil ((lm, Δ2) :: nil)) -> L l1 <-> L l2) by (now apply join_equiv).
clear Hi'. destruct Δ1 as [lv ls |l' Δ Δ']; trivial.
unfold s_mod. f_equal. induction ls as [|(l&d) ls]; trivial.
simpl. rewrite ρ_Σr. f_equal.
- case_eq (ρ Σ1 l).
  + intros j Hj. case L_dec; intro HL.
    * case option_eq_dec;intro;[discriminate|].
      case option_eq_dec; trivial. intro H; inversion H; subst.
      apply L_values in HL. apply Hi in Hj. rewrite HL in Hj. intuition. discriminate.
    * case option_eq_dec; trivial. intro H; inversion H; subst.
      rewrite Σr_d_mod; auto; [|inversion Hw1; eauto].
      intros l1 l2 H0. apply HL0. simpl. repeat rewrite in_app_iff.
      right. left. rewrite Hj, Hlm. case option_eq_dec; tauto.
  + intro Hn. case option_eq_dec; trivial. intro; discriminate.
- apply IHls; auto. eapply wb_d_cons_ls; eassumption.
Qed.

Lemma Σr_up_br_L Σ1 l v l': (Σ1 [[ (l, v)↦l Some l']]) ⊆ Σf -> L l <-> L l'.
Proof.
intros Hi. assert(Hn': ↣ Σf l = Some (Some l')) by now apply Hi.
split; intro HL.
- destruct HL as [HE|(l''&HL&H'')].
  + apply E_cod in HE. destruct HE as (_&_&Hn).
    rewrite Hn' in Hn; discriminate Hn.
  + rewrite Hn' in H''; inversion H''; subst.
    left; trivial.
- destruct HL as [HE|(l''&HL&H'')].
  + right. eauto.
  + apply (proj1 Hpf) in Hn'. rewrite Hn' in H''; discriminate H''.
Qed.

Proposition P9_m_path : forall p Δ Σ1 Γ1 (Henvf : wb_env Γ1 Σf),
 (Σ1 ⊆ Σf) ->
  Σ1 / Γ1 ⊢mp p ⋮ (Δ, Σ1) ->
 let Σ2 := Σr Σ1 in
 let Γ2 := Γr Henvf in
   Σ2 / Γ2 ⊢mp p ⋮ (Δ, Σ2).
Proof.
induction p; intros Δ' Σ1 Γ1 Henvf Hi H' Σ2 Γ2; subst Σ2 Γ2;
inversion H'; subst; clear H'.
- erewrite <- (Γr_envM Henvf). constructor.
- destruct m as [m l']; simpl.
  econstructor; eauto.
Qed.
Hint Resolve P9_m_path : core.

Proposition P9_em_path : forall p Δ Σ1 Σ1' Γ1 (Henvf : wb_env Γ1 Σf),
 (wb_env Γ1 Σ1) ->
 (Σ1' ⊆ Σf) ->
  Σ1 / Γ1 ⊢emp p ⋮ (Δ, Σ1') ->
 let Σ2 := Σr Σ1 in
 let Γ2 := Γr Henvf in
 let Σ2' := Σr Σ1' in
   Σ2 / Γ2 ⊢emp p ⋮ (Δ, Σ2').
Proof.
induction p; intros Δ' Σ1 Σ1' Γ1 Henvf Henv0 Hi H' Σ2 Γ2 Σ2'; subst Σ2 Γ2 Σ2';
inversion H'; subst; clear H'.
- erewrite <- (Γr_envM Henvf). constructor.
- destruct m as [m l']; simpl.
  econstructor; eauto.
- assert(Hi0 : Σ1 ⊆ Σ'') by (apply deterministic_semantics_em_path in X; tauto).
  assert(Hi'' : Σ' ⊆ Σf) by (etransitivity; try apply Hi; trivial).
  assert(Hi': Σ'' ⊆ Σf /\ wb_d Σ' Δ1 /\ wb_d Σ' Δ1').
  {
    apply deterministic_semantics_em_path in X; trivial.
    destruct X as (_&Henv'&Hwb&_). inversion Hwb; subst.
    apply deterministic_semantics_em_path in X0; trivial.
    destruct X0 as (_&_&Hwb'&_&_&Hincl'&_). inversion Hwb; subst.
    split; [etransitivity; eauto|split; eauto].
  }
  destruct Hi' as (Hi'&Hwb1&Hwb2).
  apply IHp1 with (Henvf:= Henvf) in X; trivial.
  eapply IHp2 with (Henvf:= Henvf) in X0; trivial; [|eapply wb_env_incl; eauto].
  erewrite Σr_sem_join; trivial.
  econstructor; eauto.
Qed.
Hint Resolve P9_em_path : core.

Lemma dom_rel_closure l: Ê Σf lx l -> lx <> l -> dom_rel (E Σf) lx /\ dom_rel (E Σf) l.
Proof.
intros H Hneq. split.
- destruct H; [tauto|]. destruct H; eexists; eauto.
- apply Operators_Properties.clos_rst1n_sym in H.
  destruct H; [tauto|]. destruct H; eexists; eauto.
Qed.

Lemma env_up_v_id Γ v : Γ [[v ↦v envV Γ v]] = Γ.
Proof.
destruct Γ as [ [ Γv Γm] Γt ]. simpl. f_equal. f_equal.
apply FunctionalExtensionality.functional_extensionality.
intros v'. case val_eq_dec; trivial. intro Heq; inversion Heq; now subst.
Qed.

Lemma L_lx: L lx.
Proof. left. apply Relation_Operators.rst1n_refl. Qed.
Hint Resolve L_lx : core.

Lemma Γr_env_up_v Γ1 l Henvf': L l -> Γr (Γ1:=Γ1 [[vy ↦v Some l]]) Henvf' = Γ1 [[vx ↦v Some l]] [[vy ↦v envV Γ1 vx ]].
Proof.
intros HL. unfold Γr. case dec_range.
- intros (ly&_). rewrite env_up_v_comm, env_up_v_idem; trivial.
  rewrite envV_env_up_v_neq, envV_env_up_v_eq; auto.
  now rewrite env_up_v_comm by auto.
- intros Hf. eapply False_rect, (Hf vy l); trivial.
Qed.

Lemma Γr_env_up_v_nL Γ1 v l Henvf':
  ~ L l -> (forall (vy : V) (ly : ℒ), envV Γ1 vy = Some ly -> ~ L ly) ->
    Γr (Γ1:=Γ1 [[v ↦v Some l]]) Henvf' = Γ1 [[v ↦v Some l]].
Proof.
intros HnL HnL'. unfold Γr. case dec_range; trivial.
intros (ly&Hly&HEy).
assert(v <> vy).
{
  intro; subst. rewrite envV_env_up_v_eq in Hly. inversion Hly; subst.
  contradict HnL. now left.
}
rewrite envV_env_up_v_neq in Hly; auto. eapply False_rect, HnL'; eauto. now left.
Qed.

Lemma Γr_env_up_v_nL' Γ1 v l Henvf' ly (Hly : envV Γ1 vy = Some ly /\ Ê Σf lx ly):
  ~ L l -> v <> vx -> v <> vy ->
    Γr (Γ1:=Γ1 [[v ↦v Some l]]) Henvf' =
    ((Γ1 [[vx ↦v envV Γ1 vy]]) [[vy ↦v envV Γ1 vx]]) [[v ↦v Some l]].
Proof.
intros HnL Hneq Hneq'. unfold Γr. case dec_range.
- intros (ly'&Hly'&HEy').
  repeat rewrite envV_env_up_v_neq; auto.
  do 2 (rewrite env_up_v_comm with (y := v); auto).
- intros Hn. apply False_rect, (Hn vy ly).
  + rewrite envV_env_up_v_neq; auto. tauto.
  + now left.
Qed.

Lemma Γr_env_up_v_nL'' Γ1 l Henvf':
  ~ L l ->
    Γr (Γ1:=Γ1 [[vy ↦v Some l]]) Henvf' = Γ1 [[vy ↦v Some l]].
Proof.
intros HnL. unfold Γr. case dec_range; trivial.
intros (ly'&Hly'&HEy'). rewrite envV_env_up_v_eq in Hly'. inversion Hly'; subst.
contradict HnL; now left.
Qed.

Lemma Γr_disj Γ1 L H: Γ1 ⊓ L -> Γr (Γ1:=Γ1) H ⊓ L.
Proof.
intros Hdisj l Hl v. unfold Γr. case dec_range; intros; auto.
destruct s. simpl.
case(val_eq_dec vy v); intro; subst.
+ rewrite envV_env_up_v_eq. auto.
+ rewrite envV_env_up_v_neq; auto.
  case(val_eq_dec vx v); intro; subst.
  * rewrite envV_env_up_v_eq. auto.
  * rewrite envV_env_up_v_neq; auto.
Qed.
Hint Resolve Γr_disj : core.

Lemma Σr_disj Σ1 L: Σ1 ⊔ L -> (Σr Σ1) ⊔ L.
Proof.
intros H. simpl; split; try apply H.
intros l Hl. apply (proj1 H) in Hl. now rewrite ρ_Σr, Hl.
Qed.
Hint Resolve Σr_disj : core.

Lemma envV_vx_nL Γ1 l Henvf': ~ L l ->
  envV (Γr (Γ1:=Γ1 [[vy ↦v Some l]]) Henvf') vx = None.
Proof.
intro HnL. unfold Γr. case dec_range; auto.
intros (ly&Hly&Hy'). rewrite envV_env_up_v_eq in Hly. inversion Hly; subst.
contradict HnL; now left.
Qed.

Lemma p_sem_env_up_v_eq_mt Γ1 Σ1 d v l:
  p_sem_env (ρ Σ1) d (Γ1[[v ↦v l]]) =mt p_sem_env (ρ Σ1) d Γ1.
Proof. destruct Γ1 as [ [ Γv Γm] Γt ]. destruct d; split; trivial. Qed.

Lemma Σr_eq_mt_p Γ1 Σ1 d : (Σ1 ⊆ Σf) -> wb_d Σ1 d ->
  p_sem_env (ρ Σ1) d Γ1 =mt p_sem_env (ρ (Σr Σ1)) d Γ1.
Proof.
intros Hi Hwb. destruct d as [lv|ls]; unfold p_sem_env; auto with *.
inversion Hwb as [lv' ls' H1 H2 H3 H4|]; subst lv' ls'.
split; unfold envM, envT;
apply FunctionalExtensionality.functional_extensionality; intro x.
- case_eq (find (var_eq_bool (Some (IM x)) ∘ ρ Σ1 ∘ fst) l).
  + intros (l'&d') H. apply find_some in H. destruct H as (Hin&H).
    apply var_eq_bool_true in H. simpl in H.
    case_eq (find (var_eq_bool (Some (IM x)) ∘ ρ (Σr Σ1) ∘ fst) l).
    * intros (l''&d'') H'.
      apply find_some in H'. destruct H' as (Hin'&H').
      apply var_eq_bool_true in H'. simpl in H'.
      case_eq (ρ Σ1 l'').
      -- intros x' Hx'. rewrite ρ_Σr, Hx' in H'.
         case L_dec in H'; try discriminate.
         inversion H'; subst; clear H'.
         assert(Heq: (l',d') = (l'',d''))
         by (eapply H4; simpl; eauto; now rewrite Hx', <- H).
         now inversion Heq.
      -- intro HF. apply H2 in Hin'. destruct Hin' as [_ [ [s Hs]|[s Hs] ] ];
         rewrite Hs in HF; discriminate.
    * intro Hn. eapply find_none in Hn; eauto.
      apply var_eq_bool_false in Hn. simpl in Hn. rewrite ρ_Σr, <- H in Hn.
      case L_dec in Hn; try tauto.
      eapply False_rect, L_not_m; eauto. apply Hi. eauto.
  + intro Hn. case_eq (find (var_eq_bool (Some (IM x)) ∘ ρ (Σr Σ1) ∘ fst) l); trivial.
    intros (l''&d'') H'.
    apply find_some in H'. destruct H' as (Hin'&H').
    apply var_eq_bool_true in H'. simpl in H'.
    case_eq (ρ Σ1 l'').
    * intros x' Hx'. rewrite ρ_Σr, Hx' in H'.
      case L_dec in H'; try discriminate.
      inversion H'; subst; clear H'.
      eapply find_none in Hn; eauto.
      apply var_eq_bool_false in Hn. simpl in Hn. rewrite <- Hx' in Hn. tauto.
    * intro HF. rewrite ρ_Σr, HF in H'. discriminate.
- case_eq (find (var_eq_bool (Some (IT x)) ∘ ρ Σ1 ∘ fst) l).
  + intros (l'&d') H. apply find_some in H. destruct H as (Hin&H).
    apply var_eq_bool_true in H. simpl in H.
    case_eq (find (var_eq_bool (Some (IT x)) ∘ ρ (Σr Σ1) ∘ fst) l).
    * intros (l''&d'') H'.
      apply find_some in H'. destruct H' as (Hin'&H').
      apply var_eq_bool_true in H'. simpl in H'.
      case_eq (ρ Σ1 l'').
      -- intros x' Hx'. rewrite ρ_Σr, Hx' in H'.
         case L_dec in H'; try discriminate.
         inversion H'; subst; clear H'.
         assert(Heq: (l',d') = (l'',d''))
         by (eapply H4; simpl; eauto; now rewrite Hx', <- H).
         now inversion Heq.
      -- intro HF. apply H2 in Hin'. destruct Hin' as [_ [ [s Hs]|[s Hs] ] ];
         rewrite Hs in HF; discriminate.
    * intro Hn. eapply find_none in Hn; eauto.
      apply var_eq_bool_false in Hn. simpl in Hn. rewrite ρ_Σr, <- H in Hn.
      case L_dec in Hn; try tauto.
      eapply False_rect, L_not_t; eauto. apply Hi. eauto.
  + intro Hn. case_eq (find (var_eq_bool (Some (IT x)) ∘ ρ (Σr Σ1) ∘ fst) l); trivial.
    intros (l''&d'') H'.
    apply find_some in H'. destruct H' as (Hin'&H').
    apply var_eq_bool_true in H'. simpl in H'.
    case_eq (ρ Σ1 l'').
    * intros x' Hx'. rewrite ρ_Σr, Hx' in H'.
      case L_dec in H'; try discriminate.
      inversion H'; subst; clear H'.
      eapply find_none in Hn; eauto.
      apply var_eq_bool_false in Hn. simpl in Hn. rewrite <- Hx' in Hn. tauto.
    * intro HF. rewrite ρ_Σr, HF in H'. discriminate.
Qed.

Lemma Γr_eq_mt_p Γ1 Σ1 d H H': (Σ1 ⊆ Σf) -> wb_d Σ1 d ->
  p_sem_env (ρ(Σr Σ1)) d (Γr (Γ1 := Γ1) H) =mt
  Γr (Γ1 := p_sem_env (ρ Σ1) d Γ1) H'.
Proof.
intros Hi Hwb. unfold Γr. case dec_range.
- intros (s&_). case dec_range.
  + intros (s'&_). do 2 (etransitivity; auto).
    symmetry. do 2 (etransitivity; auto). now apply Σr_eq_mt_p.
  + intros _. do 2 (etransitivity; auto). symmetry. now apply Σr_eq_mt_p.
- intros _. case dec_range.
  + intros (s'&_). symmetry. do 2 (etransitivity; auto). now apply Σr_eq_mt_p.
  + intros _. symmetry. now apply Σr_eq_mt_p.
Qed.

Lemma Γr_eq Γ1 l H: ~ L l -> envV Γ1 vy = Some l -> Γr (Γ1 := Γ1) H = Γ1.
Proof.
intros HL Hl. unfold Γr.
case dec_range; trivial.
intros (s&Hs&HL'). contradict HL.
left. rewrite Hs in Hl. inversion Hl; now subst.
Qed.

Lemma env_vx Γ1: wb_env Γ1 Σf -> envV Γ1 vx = None.
Proof.
intro Hw. case_eq(envV Γ1 vx); trivial.
intros l Hl. apply Hw in Hl. destruct Hl. eapply False_rect, Hvx; eauto.
Qed.
Hint Resolve env_vx : core.

Lemma Γr_vy_L Γ1 l H: L l -> envV Γ1 vy = Some l -> envV (Γr (Γ1:=Γ1) H) vy = None.
Proof.
intros HL Hl. unfold Γr.
case dec_range.
- intros (s&Hs&HL'). rewrite envV_env_up_v_eq. auto.
- intro Hn. contradict (Hn vy l Hl HL).
Qed.

Lemma not_sem_free_up Σ1 l0 v0 (Γ : env) (σ : list ℒ) (v : V):
  ρ Σ1 l0 = None ->
   not_sem_free (Σ1[[l0 ↦ v0]]) Γ σ v -> not_sem_free Σ1 Γ σ v.
Proof.
intros Hn H l1 Hin Hl1. simpl in Hl1.
destruct (H l1 Hin) as (l'&H'&He').
- rewrite sr_up. case loc_eq_dec; trivial.
  intro; subst; rewrite Hn in Hl1; discriminate.
- exists l'. split; trivial.
Qed.

Lemma not_sem_free_none Σ Γ Γ' σ v:
  envV Γ v = None ->
  not_sem_free Σ Γ' σ v ->
  not_sem_free Σ Γ σ v.
Proof.
intros Hn H l0 Hin Hl0.
destruct (H l0 Hin Hl0) as (l'&Hl'&He').
eexists; split; eauto. rewrite Hn. discriminate.
Qed.

(* renaming cannot create unbound variables *)
Lemma rename_bound Σ1 σ v: (Σ1 ⊆ Σf) -> v <> vx ->
    bound Σ1 σ v -> bound (Σr Σ1) σ v.
Proof.
intros Hi Hd Hb l Hin Hl.
rewrite Σr_v in Hl; trivial.
- unfold Σr, br'. simpl. eauto.
- rewrite Hl. intro H. inversion H. now apply vxy.
Qed.

Lemma Σr_vx_vy Σ l: Σ ⊆ Σf ->
  ρ (Σr Σ) l = Some (IV vx) -> ρ Σ l = Some (IV vy).
Proof.
intros Hi. simpl. rewrite ρ_Σr. case_eq (ρ Σ l); try discriminate.
case L_dec; intro HL.
- intros j Hj _.
  apply L_values in HL. rewrite <- HL. symmetry. now apply Hi.
- intros j Hj Hj'. rewrite Hj' in Hj. eapply False_rect, Hvx, Hi, Hj.
Qed.

Lemma Σr_vy_vy Σ l: Σ ⊆ Σf ->
  ρ (Σr Σ) l = Some (IV vy) -> ρ Σ l = Some (IV vy).
Proof.
intros Hi. simpl. rewrite ρ_Σr. case_eq (ρ Σ l); try discriminate.
case L_dec; intro HL; trivial.
intros j Hj _. apply L_values in HL. rewrite <- HL. symmetry. now apply Hi.
Qed.

Lemma Σr_vy_nL Σ l: Σ ⊆ Σf -> ~ L l ->
  ρ Σ l = Some (IV vy) -> ρ (Σr Σ) l = Some (IV vy).
Proof.
intros Hi HL. simpl. rewrite ρ_Σr. case_eq (ρ Σ l); try discriminate.
case L_dec; intro HL'; tauto.
Qed.
Hint Resolve Σr_vy_nL : core.

Lemma rename_bound_xy Σ1 σ: (Σ1 ⊆ Σf) ->
    bound Σ1 σ vy -> bound (Σr Σ1) σ vx.
Proof.
intros Hi Hb l Hin Hl.
apply Σr_vx_vy in Hl; trivial.
simpl. apply Hb; trivial.
Qed.

Lemma notfree_rename_v : forall v1 Σ1 Γ1 Σ1' l (Henvf : wb_env Γ1 Σf),
 (wb_env Γ1 Σ1) ->
 (Σ1' ⊆ Σf) ->
  Σ1 / Γ1 ⊢v v1 ⇝ Σ1' ->
  wf_v v1 ->
  Γ1 ⊓ locs_v v1 ->
  Σ1 ⊔ locs_v v1 ->
 let Σ2 := Σr Σ1 in
 let v2 := rename_v Σf vx lx v1 in
 let Γ2 := Γr Henvf in
 let Σ2' := Σr Σ1' in
   (envV Γ1 vy = Some l -> L l -> not_sem_free Σ2' Γ2 (ref_v v2) vy).
Proof.
simpl. intros v1 Σ1 Γ1 Σ1' l Henvf Henv0 Hinclf Hj Hwf Hlocs1 Hlocs2 Hb Hl.
apply not_sem_free_sem_bound.
- eapply Γr_vy_L; eauto.
- apply rename_bound; auto.
  rewrite ref_rename_v. eapply deterministic_semantics_v; eauto.
Qed.

Lemma eq_mt_eq Γ Γ':
  (forall v, envV Γ' v = envV Γ v) ->
  Γ =mt Γ' -> Γ = Γ'.
Proof.
intros Heq1 [Heq2 Heq3].
destruct Γ' as [ [ Γv' Γm'] Γt' ].
destruct Γ as [ [ Γv Γm] Γt ]. simpl in *.
f_equal; trivial. f_equal; trivial.
apply FunctionalExtensionality.functional_extensionality. auto.
Qed.

Lemma not_find_none Σ lv v:
  ~In (Some (IV v)) (map (ρ Σ) lv) ->
  find (var_eq_bool (Some (IV v)) ∘ ρ Σ) lv = None.
Proof.
intros Hn. case_eq(find (var_eq_bool (Some (IV v)) ∘ ρ Σ) lv); trivial.
intros l Hl. apply find_some in Hl. destruct Hl as (Hin&Hl).
apply var_eq_bool_true in Hl.
contradict Hn. apply in_map_iff.
eexists; split; eauto.
Qed.

Lemma find_vx_none Σ lv: Σ ⊆ Σf ->
  find (var_eq_bool (Some (IV vx)) ∘ ρ Σ) lv = None.
Proof.
intro Hi. case_eq(find (var_eq_bool (Some (IV vx)) ∘ ρ Σ) lv); trivial.
intros l Hl. apply find_some in Hl. destruct Hl as (_&Hl).
apply var_eq_bool_true in Hl.
eapply False_rect, Hvx, Hi. eauto.
Qed.

Lemma Σr_find_vx_none Σ lv: Σ ⊆ Σf ->
  ~In (Some (IV vy)) (map (ρ Σ) lv) ->
  find (var_eq_bool (Some (IV vx)) ∘ ρ (Σr Σ)) lv = None.
Proof.
intros Hi Hn. case_eq(find (var_eq_bool (Some (IV vx)) ∘ ρ Σ) lv).
- intros l Hl. apply find_some in Hl. destruct Hl as (_&Hl).
  apply var_eq_bool_true in Hl.
  eapply False_rect, Hvx, Hi. eauto.
- intro Hn'. case_eq (find (var_eq_bool (Some (IV vx)) ∘ ρ (Σr Σ)) lv); trivial.
  intros l Hl. contradict Hn.
  apply find_some in Hl. destruct Hl as (Hin&Hl).
  apply var_eq_bool_true in Hl. symmetry in Hl.
  apply in_map_iff. exists l. split; trivial.
  apply Σr_vx_vy; trivial.
Qed.

Lemma Σr_find_vy_none Σ lv: Σ ⊆ Σf ->
  ~In (Some (IV vy)) (map (ρ Σ) lv) ->
  find (var_eq_bool (Some (IV vy)) ∘ ρ (Σr Σ)) lv = None.
Proof.
intros Hi Hn. case_eq(find (var_eq_bool (Some (IV vx)) ∘ ρ Σ) lv).
- intros l Hl. apply find_some in Hl. destruct Hl as (_&Hl).
  apply var_eq_bool_true in Hl.
  eapply False_rect, Hvx, Hi. eauto.
- intro Hn'. case_eq (find (var_eq_bool (Some (IV vy)) ∘ ρ (Σr Σ)) lv); trivial.
  intros l Hl. contradict Hn.
  apply find_some in Hl. destruct Hl as (Hin&Hl).
  apply var_eq_bool_true in Hl. symmetry in Hl.
  apply Σr_vy_vy in Hl; trivial.
  apply in_map_iff. exists l. split; trivial.
Qed.

Lemma Σr_find_vy_L_none Σ l lv ls: Σ ⊆ Σf ->
  let d := Structural lv ls in
  wb_d Σ d ->
  In l lv -> ρ Σ l = Some (IV vy) -> L l ->
  find (var_eq_bool (Some (IV vy)) ∘ ρ (Σr Σ)) lv = None.
Proof.
intros Hi d Hwb Hin Hl HL. subst d.
case_eq (find (var_eq_bool (Some (IV vy)) ∘ ρ (Σr Σ)) lv); trivial.
intros l' Hl'. apply find_some in Hl'. destruct Hl' as (Hin'&Hl').
apply var_eq_bool_true in Hl'.
simpl in Hl'. case_eq(ρ Σ l').
- intros v Hv. rewrite ρ_Σr, Hv in Hl'.
  case L_dec in Hl'.
  + inversion Hl'. now eapply False_rect, vxy.
  + rewrite <- Hl' in Hv.
    assert(l' = l).
    {
      inversion Hwb as [lv' ls' H1 H2 H3 H4|]; subst lv' ls'.
      eapply H3; eauto. now rewrite Hl, Hv.
    } subst. tauto.
- intro Hn. rewrite ρ_Σr, Hn in Hl'. discriminate.
Qed.

Lemma Σr_find_other Σ lv ls v: Σ ⊆ Σf -> v <> vx -> v <> vy ->
  let d := Structural lv ls in
  wb_d Σ d ->
  find (var_eq_bool (Some (IV v)) ∘ ρ (Σr Σ)) lv =
  find (var_eq_bool (Some (IV v)) ∘ ρ Σ) lv.
Proof.
intros Hi Hneq1 Hneq2 d Hwb. subst d.
case_eq(find (var_eq_bool (Some (IV v)) ∘ ρ Σ) lv).
- intros l Hl. apply find_some in Hl. destruct Hl as (Hin&Hl).
  apply var_eq_bool_true in Hl.
  case_eq (find (var_eq_bool (Some (IV v)) ∘ ρ (Σr Σ)) lv).
  + intros l' Hl'.
    apply find_some in Hl'. destruct Hl' as(Hin'&Hl').
    apply var_eq_bool_true in Hl'. simpl in Hl'.
    case_eq(ρ Σ l').
    * intros j Hj. rewrite ρ_Σr, Hj in Hl'.
      case L_dec in Hl'.
      -- inversion Hl'; subst; tauto.
      -- rewrite <- Hl' in Hj.
         f_equal. inversion Hwb as [lv' ls' H1 H2 H3 H4|]; subst lv' ls'.
         eapply H3; eauto. now rewrite Hj, <- Hl.
    * intro Hnone. rewrite ρ_Σr, Hnone in Hl'. discriminate.
  + intro Hn. eapply find_none in Hn; eauto.
    apply var_eq_bool_false in Hn. contradict Hn.
    simpl. rewrite ρ_Σr, <- Hl. case L_dec; trivial. intro HL.
    apply L_values in HL. symmetry in Hl.
    apply Hi in Hl. rewrite (proj1 Hl) in HL. inversion HL. tauto.
- intro Hn'. case_eq (find (var_eq_bool (Some (IV v)) ∘ ρ (Σr Σ)) lv); trivial.
  intros l Hl.
  apply find_some in Hl. destruct Hl as (Hin&Hl).
  apply var_eq_bool_true in Hl. symmetry in Hl.
  eapply find_none in Hn'; eauto.
  apply var_eq_bool_false in Hn'. contradict Hn'.
  simpl in Hl. case_eq (ρ Σ l).
  + intros j Hj; rewrite ρ_Σr, Hj in Hl.
    case L_dec in Hl; auto. inversion Hl. subst; tauto.
  + intro Heq. rewrite ρ_Σr, Heq in Hl. discriminate.
Qed.

Lemma Γr_p_out lv ls Σ Γ H H': Σ ⊆ Σf ->
  ~In (Some (IV vy)) (map (ρ Σ) lv) ->
  let d := Structural lv ls in
  wb_d Σ d ->
    p_sem_env (ρ (Σr Σ)) d (Γr (Γ1 := Γ) H) =
    Γr (Γ1 := p_sem_env (ρ Σ) d Γ) H'.
Proof.
intros Hi Hout d Hwb. subst d.
apply eq_mt_eq;[|now apply Γr_eq_mt_p].
destruct Γ as [ [ Γv Γm] Γt ].
intro x.
unfold Γr. case dec_range.
- intros (s&Hs&HLs). case dec_range.
  + intros (s'&Hs'&HLs'). unfold p_sem_env, env_up_v, envV.
    rewrite find_vx_none by auto.
    case var_eq_dec.
    * intro Heq; inversion Heq; subst.
      now rewrite Σr_find_vy_none.
    * intros Hneq. rewrite not_find_none; trivial.
    case var_eq_dec.
      -- intro Heq; inversion Heq; subst x. clear Heq.
          rewrite Σr_find_vx_none; trivial.
      -- intro Hneq'. erewrite Σr_find_other; eauto; intro; subst; tauto.
  + intro Hn. unfold p_sem_env, env_up_v, envV. simpl in Hn, Hs.
    rewrite not_find_none in Hs; trivial.
      apply False_rect, (Hn vy s Hs). now left.
- intro Hn. simpl in Hn.
  case dec_range.
  + intros (s'&Hs'&HLs').
    eapply False_rect, (Hn vy s');[| now left].
    rewrite not_find_none; trivial.
  + intro Hn'. unfold p_sem_env, env_up_v, envV.
    case (val_eq_dec x vx).
    * intro; subst x. rewrite find_vx_none; trivial.
      rewrite Σr_find_vx_none; trivial.
    * intro Hneq. case(val_eq_dec x vy).
     -- intro; subst x. rewrite Σr_find_vy_none; trivial.
        rewrite not_find_none; trivial.
     -- intro. erewrite Σr_find_other; eauto.
Qed.

Lemma wb_d_find_det lv ls Σ v l:
  let d := Structural lv ls in
  wb_d Σ d ->
  In l lv -> ρ Σ l = Some (IV v) ->
    find (var_eq_bool (Some (IV v)) ∘ ρ Σ) lv = Some l.
Proof.
intros d Hwb Hin Hl. subst d.
case_eq(find (var_eq_bool (Some (IV v)) ∘ ρ Σ) lv).
- intros j Hj. apply find_some in Hj.
  destruct Hj as (Hin'&Hl').
  apply var_eq_bool_true in Hl'.
  f_equal. inversion Hwb as [lv' ls' H1 H2 H3 H4|]; subst lv' ls'.
  eapply H3; eauto. now rewrite <- Hl', Hl.
- intro Hn; eapply find_none in Hn; eauto.
  apply var_eq_bool_false in Hn.
  rewrite Hl in Hn; tauto.
Qed.

Lemma Σr_vy_vx Σ l: ρ Σ l = Some (IV vy) -> L l ->
  ρ (Σr Σ) l = Some (IV vx).
Proof. intros Hy HL. simpl. rewrite ρ_Σr, Hy. case L_dec; tauto. Qed.
Hint Resolve Σr_vy_vx : core.

Lemma Σr_find_vx_L lv ls Σ l: Σ ⊆ Σf ->
  In l lv -> ρ Σ l = Some (IV vy) -> L l ->
  let d := Structural lv ls in
  wb_d Σ d ->
    find (var_eq_bool (Some (IV vx)) ∘ ρ (Σr Σ)) lv = Some l.
Proof.
intros Hi Hin Hl HL d Hwb. subst d.
eapply wb_d_find_det; auto.
apply wb_d_Σr; eauto.
Qed.

Lemma Γr_p_in_L lv ls Σ Γ H H' l ly: Σ ⊆ Σf ->
  envV Γ vy = Some ly -> L ly ->
  In l lv -> ρ Σ l = Some (IV vy) -> L l ->
  let d := Structural lv ls in
  wb_d Σ d ->
    p_sem_env (ρ (Σr Σ)) d (Γr (Γ1 := Γ) H) =
    Γr (Γ1 := p_sem_env (ρ Σ) d Γ) H'.
Proof.
intros Hi Hvy Hly Hin Hl HL d Hwb. subst d.
apply eq_mt_eq;[|apply Γr_eq_mt_p; auto].
destruct Γ as [ [ Γv Γm] Γt ].
intro x.
unfold Γr. case dec_range.
- intros (s&Hs&HLs).
  case dec_range;[|intro Hn; contradict (Hn vy ly Hvy Hly)].
  intros (s'&Hs'&HLs'). unfold p_sem_env, env_up_v, envV.
  erewrite wb_d_find_det with (v:= vy); eauto.
  rewrite find_vx_none by auto.
  case var_eq_dec.
  + intro Heq; inversion Heq; subst. clear Heq.
    erewrite Σr_find_vy_L_none; eauto.
  + intros Hneq. case var_eq_dec.
    * intro Heq; inversion Heq; subst x. clear Heq.
      erewrite Σr_find_vx_L; eauto.
    * intro Hneq'. erewrite Σr_find_other; eauto; intro; subst; tauto.
- intro Hn'. case dec_range;[|intro Hn; contradict (Hn vy ly Hvy Hly)].
  intros (s&_). simpl in Hn'. assert(HF:= Hn' vy l).
  erewrite wb_d_find_det in HF; eauto.
  eapply False_rect, HF; auto.
Qed.

Lemma Γr_p_in_L' lv ls Σ Γ H H' l: Σ ⊆ Σf ->
  (forall ly, envV Γ vy = Some ly -> ~ L ly) ->
  In l lv -> ρ Σ l = Some (IV vy) -> L l ->
  let d := Structural lv ls in
  wb_d Σ d ->
    p_sem_env (ρ (Σr Σ)) d (Γr (Γ1 := Γ) H) =
    Γr (Γ1 := p_sem_env (ρ Σ) d Γ) H' [[vy ↦v envV Γ vy]].
Proof.
intros Hi Hn Hin Hl HL d Hwb. subst d.
apply eq_mt_eq;[|etransitivity; [|symmetry; apply eq_mt_up_v_weak];apply Γr_eq_mt_p; auto].
destruct Γ as [ [ Γv Γm] Γt ].
intro x.
unfold Γr. case dec_range.
- intros (s&Hs&HLs).
  case dec_range.
  + intros (s'&Hs'&HEs'); eapply False_rect, (Hn s' Hs'); now left.
  + intros _. unfold p_sem_env, env_up_v, envV.
    case var_eq_dec.
    * intro Heq; inversion Heq; subst x. clear Heq.
      erewrite Σr_find_vy_L_none; eauto.
    * intro Hneq.
      case var_eq_dec.
      -- intro Heq; inversion Heq; subst x. clear Heq.
          erewrite wb_d_find_det; eauto.
          erewrite Σr_find_vx_L; eauto.
      -- intro Hneq'. erewrite Σr_find_other; eauto; intro; subst; tauto.
- intro Hn'. simpl in Hn'. assert(HF:= Hn' vy l).
  erewrite wb_d_find_det in HF; eauto.
  eapply False_rect, HF; auto.
Qed.

Lemma envV_p_sem_env_in l (Γ1 : env) (Σ1 : sem) (lv : list ℒ) (ls : list (ℒ * desc)) (v : V):
   let d := Structural lv ls in
   wb_d Σ1 d ->
   In l lv -> ρ Σ1 l = Some (IV v) ->
   envV (p_sem_env (ρ Σ1) d Γ1) v = Some l.
Proof.
simpl. intros Hwb Hin Hl.
case_eq (find (var_eq_bool (Some (IV v)) ∘ ρ Σ1) lv).
- intros l' Hl'.
  apply find_some in Hl'. destruct Hl' as (Hin'&Hl').
  apply var_eq_bool_true in Hl'.
  f_equal. inversion Hwb as [lv' ls' H1 H2 H3 H4|]; subst lv' ls'.
  eapply H3; eauto. now rewrite <- Hl', Hl.
- intro Hn. eapply find_none in Hn; eauto.
  apply var_eq_bool_false in Hn. rewrite Hl in Hn; tauto.
Qed.

Lemma Σr_p_sem_env Σ1 Γ l lv ls: Σ1 ⊆ Σf ->
  let d := Structural lv ls in
  wb_d Σ1 d -> In l lv -> ρ Σ1 l = Some (IV vy) -> ~ L l ->
  p_sem_env (ρ (Σr Σ1)) d Γ = p_sem_env (ρ Σ1) d Γ.
Proof.
intros Hi d Hwb Hinl Hl HL. unfold d.
assert(Hvy: ρ (Σr Σ1) l = Some (IV vy)) by auto.
inversion Hwb as [lv' ls' H1 H2 H3 H4|]; subst lv' ls'.
apply eq_mt_eq;[|symmetry; now apply Σr_eq_mt_p].
intro x. unfold p_sem_env, envV.
case_eq (find (var_eq_bool (Some (IV x)) ∘ ρ Σ1) lv).
- intros l' H. apply find_some in H. destruct H as (Hin&H).
  apply var_eq_bool_true in H. symmetry in H.
  case_eq (find (var_eq_bool (Some (IV x)) ∘ ρ (Σr Σ1)) lv).
  + intros l'' H'.
    apply find_some in H'. destruct H' as (Hin'&H').
    apply var_eq_bool_true in H'. rewrite ρ_Σr in H'.
    case_eq (ρ Σ1 l'').
    * intros x' Hx'. rewrite Hx' in H'.
      case L_dec in H'.
      -- inversion H'. subst x. eapply False_rect, Hvx, Hi, H.
      -- inversion H'; subst; clear H'.
         f_equal. eapply H3; eauto. now rewrite Hx', <- H.
    * intro HF. apply H1 in Hin'. destruct Hin' as (v''&Hl''&Hbr'').
      rewrite Hl'' in HF; discriminate.
  + intro Hn. eapply find_none in Hn; eauto.
    apply var_eq_bool_false in Hn. simpl in Hn. rewrite ρ_Σr, <- H in Hn.
    case L_dec in Hn.
    * replace l' with l in *;[tauto|].
      replace x with vy in *.
      -- eapply H3; eauto. now rewrite H.
      -- apply L_values in l0. apply Hi in H. rewrite (proj1 H) in l0. now inversion l0.
    * rewrite H in Hn. tauto.
- intro Hn. case_eq (find (var_eq_bool (Some (IV x)) ∘ ρ (Σr Σ1)) lv); trivial.
  intros l'' H'.
  apply find_some in H'. destruct H' as (Hin'&H').
  apply var_eq_bool_true in H'. simpl in H'.
  case_eq (ρ Σ1 l'').
  + intros x' Hx'. rewrite ρ_Σr, Hx' in H'.
    case L_dec in H'.
    * inversion H'; subst; clear H'.
      replace l'' with l in *;[tauto|].
      replace x' with (IV vy) in *.
      -- eapply H3; eauto. now rewrite Hx'.
      -- apply L_values in l0. apply Hi in Hx'. rewrite (proj1 Hx') in l0. now inversion l0.
    * eapply find_none in Hn; eauto.
      apply var_eq_bool_false in Hn. rewrite Hx' in Hn. tauto.
  + intro HF. rewrite ρ_Σr, HF in H'. discriminate.
Qed.

Lemma sig_sem P s' s'':
  {s : sem & P s ∧ (sem_equal s'' s)} ->
  sem_equal s'' s' ->
  {s : sem & P s ∧ (sem_equal s' s)}.
Proof. intros (s&Hs&Heq1) Heq2. exists s. split; trivial. transitivity s''; eauto. Qed.

Lemma not_sem_free_sem_equal s s' e l v:
  sem_equal s s' -> not_sem_free s e l v -> not_sem_free s' e l v.
Proof.
unfold not_sem_free, sr'. intros (Heq1&_&Heq2) Hnsf l0 Hl0 Heq'.
rewrite <- Heq1 in Heq'. apply Hnsf in Heq'; auto.
destruct Heq'. eexists; split; eauto.
- unfold br'. rewrite <- Heq2. apply H.
- apply H.
Qed.

Lemma bound_sem_equal s s' l v: sem_equal s s' -> bound s l v -> bound s' l v.
Proof.
unfold bound, sr'. intros (Heq1&_&Heq2) Hb l0 Hl0 Heq'.
rewrite <- Heq1 in Heq'. apply Hb in Heq'; auto.
destruct Heq'. eexists; eauto.
unfold br'. rewrite <- Heq2. apply H.
Qed.

Definition in_map_inv: forall (A B : Type) (f : A -> B),
 (forall x y : B, {x = y} + {x <> y}) ->
 forall l y, In y (map f l) -> {x : A | f x = y /\ In x l}.
Proof.
intros A B f Hdec. induction l as [|a l]; intros y Hy; simpl in *; try tauto.
destruct (Hdec (f a) y).
- subst y. eexists; eauto.
- destruct (IHl y) as (x&Hx); [tauto|].
  eexists x; split; tauto.
Defined.

Proposition P9_v : forall v1 Σ1 Γ1 Σ1' (Henvf : wb_env Γ1 Σf),
 (wb_env Γ1 Σ1) ->
 (Σ1' ⊆ Σf) ->
  Σ1 / Γ1 ⊢v v1 ⇝ Σ1' ->
  proper Σ1 ->
  wf_v v1 ->
  Γ1 ⊓ locs_v v1 ->
  Σ1 ⊔ locs_v v1 ->
 (forall l, In l (locs_v v1) -> ~ dom_rel (E Σf) l) ->
 let Σ2 := Σr Σ1 in
 let v2 := rename_v Σf vx lx v1 in
 let Γ2 := Γr Henvf in
 {Σ2' & (Σ2 / Γ2 ⊢v v2 ⇝ Σ2') ∧ (sem_equal (Σr Σ1') Σ2')}.
Proof.
induction v1; intros Σ1 Γ1 Σ1' Henvf Henv0 Hinclf Hj Hp Hwf Hlocs1 Hlocs2 HL0; destr_locs;
apply destr_v in Hj; trivial; simpl in Hj; subst; simpl in *; trivial.
- destruct Hj as (Henv & Hj&Heq); subst.
  eapply sig_sem; [| erewrite (Σr_up_br Henvf); auto].
  erewrite (Γr_env Henvf); eauto. case L_dec; intro HL;
  (eexists; split; [constructor|eauto]).
- destruct Hj as (lv & ls & Hj &Hcase).
  destruct Hcase as  [(l' & Hin & Hsome & Heq & Henv'&Hi')| (Hnone & Heq)].
  + subst. eapply (P9_m_path Henvf) in Hj; eauto.
    eapply sig_sem; [| erewrite (Σr_up_br Henvf); auto].
    case L_dec; intro HL; (eexists; split; [econstructor|]; eauto);
    rewrite ρ_Σr; simpl; rewrite Hsome;
    apply Σr_up_br_L in Hinclf; trivial; case L_dec; tauto.
  + subst.
    assert(Hincl : Σ1 ⊆ (Σ1 [[ (vl, v)↦l None]]))
    by (apply sem_incl_up_br; apply Hlocs2; tauto).
    eapply (P9_m_path Henvf) in Hj; [|etransitivity; try apply Hinclf; trivial].
    eapply sig_sem; [| erewrite (Σr_up_br Henvf); auto].
    case L_dec; intro HL.
    * eexists; split; [econstructor 3|]; eauto. intros l' H'.
      simpl. apply Hnone in H'.
      assert(ρ Σf vl = Some (IV v)) by now apply Hinclf. rewrite ρ_Σr.
      case_eq (ρ Σ1 l');[|intros _ Hf; discriminate Hf]. intros j0 Hj0.
      assert(H'': ρ Σf l' = Some j0).
      {
        rewrite Hj0 in H'.
        apply Hincl in Hj0. destruct Hj0 as (Hj0&_).
        apply Hinclf in Hj0. destruct Hj0 as (Hj0&_).
        rewrite <- Hj0 in H'. tauto.
      }
      case L_dec.
      -- intro HL'. apply L_values in HL'; trivial. apply L_values in HL; trivial.
         rewrite Hj0, <- H'', HL', <- HL in H'. tauto.
      -- intros _. now rewrite <- H''.
    * eexists; split; [econstructor 3|]; eauto. intros l' H'.
      unfold Σr. simpl. apply Hnone in H'.
      assert(ρ Σf vl = Some (IV v)) by now apply Hinclf.
      rewrite ρ_Σr'.
      case_eq (ρ Σ1 l');[|intros _ Hf; discriminate Hf]. intros j0 Hj0.
      assert(H'': ρ Σf l' = Some j0).
      {
        rewrite Hj0 in H'.
        apply Hincl in Hj0. destruct Hj0 as (Hj0&_).
        apply Hinclf in Hj0. destruct Hj0 as (Hj0&_).
        rewrite <- Hj0 in H'. tauto.
      }
      case L_dec.
      -- intros HL' Heq; inversion Heq; subst v. clear Heq. now apply (Hvx vl).
      -- intros _. now rewrite <- Hj0.
- eexists; split; [constructor|]; eauto.
(* vlet *)
- destruct Hj as (Σ''&Hv1&Hv2&Hwf1&Hlocs1'&Hlocs2'&Hwf2&Hdisj).
  assert(Hj1 := Hv1). assert(Hj2 := Hv2).
  apply deterministic_semantics_v in Hj1; eauto.
  destruct Hj1 as (_&Henv''&Hlocs1''&Hbr''&Hdecl1&Hincl1&_&Hp').
  assert(Hl1 : ρ Σ'' vl = None)
  by (rewrite Hlocs1''; [apply Hlocs2; auto with *|tauto]).
  assert(Hl2 : ↣ Σ'' vl = None) by (rewrite Hbr'';[apply Hlocs2|];tauto).
  assert(Henv' : wb_env (Γ1 [[v ↦v Some vl]]) (Σ'' [[vl ↦ IV v]]))
  by (apply wb_env_up_v; trivial).
  assert(Hlocs0: (Σ'' [[vl ↦ IV v]]) ⊔ locs_v v1_2).
  {
    disjtac.
    - rewrite sem_up_sr_id, Hlocs1''; eauto.
      + apply Hlocs2; auto with *.
      + intro HF; apply Hwf in HF. tauto.
      + intro HF; subst. tauto.
    - rewrite br_up, Hbr''; [apply Hlocs2; auto with *|].
      intro HF; apply Hwf in HF; tauto.
  }
  apply deterministic_semantics_v in Hj2; eauto.
  assert(Hincl'': Σ'' ⊆ Σf).
  {
    transitivity Σ1';trivial.
    transitivity (Σ'' [[vl ↦ IV v]]); [|tauto].
    now apply sem_incl_up_ρ.
  }
  apply (IHv1_1 _ _ _ Henvf) in Hv1; auto; clear IHv1_1; [|eauto with *].
  destruct Hj2 as (_&Henvf1'&Hlocs'&Hlocs''&_&Hinclf'&_&_&_&Hmon0).
  pose(Henvf' := wb_env_incl Henvf1' Hinclf).
  assert(Hj2 := Hv2). apply IHv1_2 with (Henvf:=Henvf') in Hv2;
  auto with *; [|apply proper_up; auto; tauto]; clear IHv1_2. clear Hp'.
  destruct Hv1 as (s'&Hv1&Heqs').
  assert(Hv1' := Hv1).
  apply deterministic_semantics_v in Hv1;
  [|now apply rename_wf_v|eapply wb_env_Γr; eauto| rewrite locs_rename_v; now apply Γr_disj
   |rewrite locs_rename_v; now apply Σr_disj].
   destruct Hv2 as (s''&Hv2&Heqs'').
  assert(Hv2' := Hv2). clear Hv1.
  case L_dec; intro HL.
  + assert(Heq: vl = lx /\ v = vy).
    {
      assert (HL' := HL). apply L_values in HL.
      assert(Hl: ρ ((Σ'' [[vl ↦ IV v]])) vl = Some (IV v)) by trivial.
      eapply sem_incl_trans in Hl; eauto. destruct Hl as (Hl&Hbrl). rewrite Hl in HL.
      inversion HL; subst. split; trivial.
      case (loc_eq_dec vl lx); trivial; intro Hneq.
      destruct HL' as [HE'|(l''&Hl''&Hbrl'')].
      - eapply False_rect, HL0, dom_rel_closure; auto with *.
      - rewrite Hbrl in Hbrl''; trivial. discriminate.
    }
    inversion Heq; subst.
    assert(Hw'':= Hv2').
    apply weakening_v with (y := vy) (ly := envV Γ1 vy) in Hv2';
    [|now apply rename_wf_v|eapply wb_env_Γr; eauto|rewrite locs_rename_v; now apply Γr_disj
     |rewrite locs_rename_v; now apply Σr_disj |
      eapply not_sem_free_sem_equal, notfree_rename_v; eauto].
    unfold Γr in *. case_eq (dec_range Hpf Hcodf Henvf).
    * intros (ly&Hly&HEy) Heq0. apply False_rect, (HL0 lx); try tauto.
      apply (dom_rel_closure HEy). intro; subst. eapply Hlocs1; eauto.
    * intros n Heq0. rewrite Heq0 in *; clear Heq0.
      unfold Γr in *. case_eq (dec_range Hpf Hcodf Henvf').
      -- intros (ly&Hly) Heq0. rewrite Heq0 in *; clear Heq0.
         apply sem_equal_compat_v with (Σ0 := (s' [[lx ↦ IV vx]])) in Hv2';
         [|rewrite <- Σr_up_ρ_L]; eauto.
         destruct Hv2' as (s'''&Hv2'&Heq''').
         eexists s'''. split;[ econstructor; [exact Hv1'|]|]; [|etransitivity; eauto].
         rewrite envV_env_up_v_eq, envV_env_up_v_neq in Hv2'; auto.
         rewrite env_up_v_idem, env_up_v_comm, env_up_v_idem in Hv2'; auto.
         rewrite env_up_v_id in Hv2'. apply Hv2'.
      -- intros n0 Heq0. rewrite Heq0 in *. clear Heq0.
         eapply False_rect, n0, HL. apply envV_env_up_v_eq.
    + unfold Γr; unfold Γr in Hv1'. case_eq (dec_range Hpf Hcodf Henvf).
      * intros (ly&Hvy&Hly) Heq0. rewrite Heq0 in *; clear Heq0.
        case(val_eq_dec v vy).
        -- intro; subst v. erewrite Γr_env_up_v_nL'' in Hv2; eauto.
           apply weakening_v with (y := vx) (ly := envV Γ1 vy) in Hv2';
           [|now apply rename_wf_v|eapply wb_env_Γr; eauto
            |rewrite locs_rename_v; now apply Γr_disj
            |rewrite locs_rename_v; now apply Σr_disj
            | erewrite Γr_env_up_v_nL''; trivial; eapply not_sem_free_sem_bound;
              eauto; eapply bound_sem_equal; eauto;
              apply rename_bound_xy; trivial;
              rewrite ref_rename_v; now eapply Hmon0].
           unfold Γr in Hv2'. case dec_range in Hv2'.
           ++ destruct s as (ly'&Hly'&HEy').
              rewrite envV_env_up_v_eq in Hly'. inversion Hly'; subst.
              contradict HL; now left.
           ++ eapply sem_equal_compat_v with (Σ0 := s' [[vl ↦ IV vy]]) in Hv2';
              [|rewrite <- Σr_up_ρ_nL; auto].
              destruct Hv2' as (s'''&Hs'''&Heq''').
              eexists s'''; split; [econstructor|]; [eauto| |etransitivity; eauto].
              rewrite env_up_v_idem. rewrite env_up_v_comm; trivial; apply Hs'''.
        -- intro Hneq. erewrite Γr_env_up_v_nL' in Hv2; eauto.
         ++ eapply sem_equal_compat_v with (Σ0 := s' [[vl ↦ IV v]]) in Hv2;
            [|rewrite <- Σr_up_ρ_nL; auto].
            destruct Hv2 as (s'''&Hv2&Heq''').
            exists s'''; split; [|etransitivity; eauto]. econstructor; eauto.
         ++ intro; subst v; apply False_rect, (Hvx vl).
            assert(HH: ρ (Σ'' [[vl ↦ IV vx]]) vl = Some (IV vx)) by trivial.
            apply Hinclf' in HH. destruct HH as (HH&_).
            apply Hinclf in HH. now destruct HH as (HH&_).
    * intros n0 H0. rewrite H0 in *; clear H0.
      rewrite Γr_env_up_v_nL in Hv2; eauto.
      eapply sem_equal_compat_v with (Σ0 := s' [[vl ↦ IV v]]) in Hv2;
      [|rewrite <- Σr_up_ρ_nL; auto].
      destruct Hv2 as (s'''&Hv2&Heq''').
      exists s'''; split; [|etransitivity; eauto]. econstructor; eauto.
- destruct Hj as (Hj1&HH).
  assert(Hj1' := Hj1).
  assert(Henv: wb_env (Γ1 [[v ↦v Some vl]]) (Σ1 [[vl ↦ IV v]])) by
  (apply wb_env_up_v; try (apply Hlocs2; tauto);eapply wb_env_incl; [exact Henv0|trivial]).
  apply deterministic_semantics_v in Hj1'; auto with *; try tauto.
  assert(Henvf': wb_env (Γ1 [[v ↦v Some vl]]) Σf)
  by (eapply wb_env_incl; [exact Henv|transitivity Σ1'; tauto]).
  assert(proper (Σ1 [[vl ↦ IV v]])) by (apply proper_up; trivial; apply Hlocs2; tauto).
  assert(Hincl': (Σ1 [[vl ↦ IV v]]) ⊆ Σ1') by tauto.
  assert(Hj2' := Hj1).
  apply IHv1 with (Henvf := Henvf') in Hj1; auto with *; try tauto;clear IHv1.
  destruct Hj1' as (_&_&_&_&_&_&_&_&_&Hmon0).
  destruct Hj1 as (s'&Hj1&Heqs').
  assert(Hj1' := Hj1). apply deterministic_semantics_v in Hj1';
  [|now apply rename_wf_v|eapply wb_env_Γr; eauto| rewrite locs_rename_v; apply Γr_disj; tauto
   |rewrite locs_rename_v; apply Σr_disj; tauto].
  case L_dec; intro HL.
  + assert(Heq: vl = lx /\ v = vy).
    {
      assert (HL' := HL). apply L_values in HL.
      assert(Hl: ρ ((Σ1 [[vl ↦ IV v]])) vl = Some (IV v)) by trivial.
      eapply sem_incl_trans in Hl; eauto. destruct Hl as (Hl&Hbrl). rewrite Hl in HL.
      inversion HL; subst. split; trivial.
      case (loc_eq_dec vl lx); trivial; intro Hneq.
      destruct HL' as [HE'|(l''&Hl''&Hbrl'')].
      - eapply False_rect, HL0, dom_rel_closure; auto with *.
      - rewrite Hbrl in Hbrl''; simpl; trivial; try discriminate. apply Hlocs2; tauto.
    } inversion Heq; subst vl v. clear Heq.
    unfold Γr. case dec_range.
    * intros (ly&Hly&HEy). apply False_rect, (HL0 lx); try tauto.
      apply (dom_rel_closure HEy). intro; subst. eapply Hlocs1; eauto.
    * intro Hn.
      assert(Hv2': not_sem_free (Σr Σ1') (Γr (Γ1:=Γ1 [[vy ↦v Some lx]]) Henvf')
         (ref_v (rename_v Σf vx lx v1)) vy) by
        (eapply notfree_rename_v; eauto; tauto).
      eapply not_sem_free_sem_equal in Hv2'; eauto.
      eapply weakening_v in Hv2';
      [|now apply rename_wf_v|eapply wb_env_Γr; eauto| rewrite locs_rename_v; apply Γr_disj; tauto
       |rewrite locs_rename_v; apply Σr_disj; tauto|eauto ].
      eapply sem_equal_compat_v in Hv2'; [|rewrite <- Σr_up_ρ_L]; eauto.
      destruct Hv2' as (s'''&Hv2'&Heq''').
      exists s'''; split; [eauto; econstructor| etransitivity; eauto].
      unfold Γr in Hv2'. case dec_range in Hv2'.
      -- destruct s as (ly&_).
         rewrite envV_env_up_v_eq, envV_env_up_v_neq in Hv2'; auto.
         rewrite env_up_v_idem, env_up_v_comm,env_up_v_idem in Hv2'; auto.
         rewrite env_up_v_id in Hv2'. apply Hv2'.
      -- eapply False_rect, n, HL. apply envV_env_up_v_eq.
  + unfold Γr. case dec_range.
    * intros (ly&Hvy&Hly). case(val_eq_dec v vy); intro Hneq; subst.
      -- eapply weakening_v with (y := vx) (ly := envV Γ1 vy) in Hj1;
         [|now apply rename_wf_v|eapply wb_env_Γr; eauto
          |rewrite locs_rename_v; apply Γr_disj; tauto
          |rewrite locs_rename_v; apply Σr_disj; tauto
          | erewrite Γr_env_up_v_nL'' by trivial;
            apply not_sem_free_sem_bound; eauto; eapply bound_sem_equal; eauto;
            apply rename_bound_xy; trivial; rewrite ref_rename_v; now eapply Hmon0].
         erewrite Γr_env_up_v_nL'' in Hj1 by trivial.
         eapply sem_equal_compat_v in Hj1; [|rewrite <- Σr_up_ρ_nL]; eauto.
         destruct Hj1 as (s'''&Hs'''&Heq''').
         eexists s'''; split; [econstructor|etransitivity; eauto].
         rewrite env_up_v_idem. rewrite env_up_v_comm.
         apply Hs'''. apply vxy.
      -- erewrite Γr_env_up_v_nL' in Hj1; eauto.
       ++ eapply sem_equal_compat_v in Hj1; [|rewrite <- Σr_up_ρ_nL; auto].
          destruct Hj1 as (s'''&Hj1&Heq''').
          exists s'''; split; [|etransitivity; eauto]. econstructor; eauto.
       ++ intro; subst v; apply False_rect, (Hvx vl).
          assert(HH': ρ (Σ1 [[vl ↦ IV vx]]) vl = Some (IV vx)) by trivial.
          apply Hincl' in HH'. destruct HH' as (HH'&_).
          apply Hinclf in HH'. now destruct HH' as (HH'&_).
    * intro Hn. rewrite Γr_env_up_v_nL in Hj1; auto.
      eapply sem_equal_compat_v in Hj1; [|rewrite <- Σr_up_ρ_nL; auto].
      destruct Hj1 as (s'''&Hj1&Heq''').
      exists s'''; split; [|etransitivity; eauto]. econstructor; eauto.
- destruct Hj as (Σ''&Hj1&Hj2&HH).
  assert(Hj1' := Hj1).
  apply deterministic_semantics_v in Hj1'; auto with *; try tauto.
  assert(Σ'' ⊔ locs_v v1_2).
  {
    destruct Hj1' as (Hdet''&Henv''''&Hlocs''&Hbr''&Hcod''&Hincl''&Hpr'').
    split; intros l0 Hl0.
    - rewrite Hlocs''; [| intro; apply Hwf in Hl0; tauto]. apply Hlocs2; auto with *.
    - rewrite Hbr''; [| intro; apply Hwf in Hl0; tauto]. apply Hlocs2; auto with *.
  }
  assert(Σ'' ⊆ Σf) by
    (transitivity Σ1';trivial; apply deterministic_semantics_v in Hj2; auto with *; tauto).
  eapply IHv1_1 with (Henvf := Henvf) in Hj1; auto with *; try tauto. clear IHv1_1.
  eapply IHv1_2 with (Henvf := Henvf) in Hj2; auto with *; try tauto; clear IHv1_2.
  destruct Hj1 as (s1'&Hj1&HH1). destruct Hj2 as (s2'&Hj2&HH2).
  eapply sem_equal_compat_v with (Σ0 := s1') in Hj2; eauto.
  destruct Hj2 as (s2''&Hj2&HH2').
  eexists s2''; split; [econstructor; [apply Hj1|apply Hj2]| etransitivity; eauto].
- destruct Hj as (lv&ls&Hj1&Hj2&HH).
  assert(Hj1' := Hj1). apply deterministic_semantics_m_path in Hj1; auto.
  assert (Hj2' := Hj2). apply deterministic_semantics_v in Hj2; auto;
  [|apply wb_p_sem_env; tauto| now apply p_sem_env_disj].
  assert(Henv1 : Σ1 ⊆ Σf) by (intuition; eauto).
  assert(Henv: wb_env (p_sem_env (ρ Σ1) (Structural lv ls) Γ1) Σ1)
  by (apply wb_p_sem_env; tauto).
  assert(Henvf': wb_env (p_sem_env (ρ Σ1) (Structural lv ls) Γ1) Σf)
  by (eapply wb_env_incl; [exact Henv|trivial]). assert(Hj2''' := Hj2').
  apply IHv1 with (Henvf:=Henvf') in Hj2';eauto; try tauto; auto with *. clear IHv1.
  assert(Hj2'' := Hj2'). destruct Hj2' as (s'''&Hj2'&Heq''').
   apply deterministic_semantics_v in Hj2';
  [|now apply rename_wf_v|now apply wb_env_Γr|
    rewrite locs_rename_v; apply Γr_disj; tauto|
    rewrite locs_rename_v; apply Σr_disj; tauto].
  assert(wb_d Σ1 (Structural lv ls)) by tauto.
  case(In_dec (option_eq_dec var_eq_dec) (Some (IV vy)) (map (ρ Σ1) lv)); intro HinD.
  (* there is a matching l in d *)
  + assert (HinD0 := HinD). apply in_map_inv in HinD; [|exact (option_eq_dec var_eq_dec)].
    destruct HinD as (l&Hl&Hin).
    case (L_dec l); intro HL.
    * destruct Hj2'' as (s'&Hs'&Heq').
      case (dec_range Hpf Hcodf Henvf).
      -- intros (s&Hs&HEs). eexists s'; split; [| etransitivity; eauto].
         econstructor;[apply P9_m_path; [transitivity Σ1'; tauto|exact Hj1']|].
         rewrite Γr_p_in_L with (l:= l) (ly := s) (H' := Henvf'); auto; try tauto; eauto. now left.
      -- intro Hn.
         assert(Hv2': not_sem_free s' (Γr (Γ1:=p_sem_env (ρ Σ1) (Structural lv ls) Γ1) Henvf')
                (ref_v (rename_v Σf vx lx v1)) vy).
         {
           eapply not_sem_free_sem_equal; eauto.
           eapply notfree_rename_v with (Σ1 := Σ1); eauto; try tauto.
           now apply envV_p_sem_env_in.
         }
         eapply weakening_v with (ly := envV Γ1 vy) in Hv2'; try apply Hs';
         [|now apply rename_wf_v|eapply wb_env_Γr; eauto
          |rewrite locs_rename_v; now apply Γr_disj
          |rewrite locs_rename_v; now apply Σr_disj].
         eexists s'; split; [| transitivity (Σr Σ1'); eauto].
         econstructor;[apply P9_m_path; [transitivity Σ1'; tauto|exact Hj1']|].
         rewrite Γr_p_in_L' with (l:= l) (H' := Henvf'); auto; try tauto.
         intro; apply Hn.
    (* it is not renamed *)
    * apply P9_m_path with (Henvf := Henvf) in Hj1'; auto. revert Hj1'.
       unfold Γr. case dec_range.
      (* there is a renamed location in the environment *)
      -- intros (s&Hs&_) Hj1'. erewrite Γr_eq in Hj2''; eauto; [|apply envV_p_sem_env_in; auto].
         destruct Hj2'' as (s''&Hj2''&Heq'').
         assert(Hv2': not_sem_free (Σr Σ1') (Γr (Γ1:=p_sem_env (ρ Σ1) (Structural lv ls) Γ1) Henvf')
              (ref_v (rename_v Σf vx lx v1)) vx).
         {
           apply not_sem_free_sem_bound.
           - erewrite Γr_eq; eauto. apply envV_p_sem_env_in; auto.
           - apply rename_bound_xy; [etransitivity; eauto|].
             rewrite ref_rename_v.
             eapply p_sem_up_mon in Hs; eauto. destruct Hs.
             eapply Hj2; eauto.
         }
        eapply not_sem_free_sem_equal with (s' := s'') in Hv2'; eauto.
        eapply weakening_v with (ly := envV Γ1 vy) in Hv2'; try apply Hv2';
        [|now apply rename_wf_v|eapply wb_env_Γr; eauto
         |rewrite locs_rename_v; now apply Γr_disj
         |rewrite locs_rename_v; now apply Σr_disj
         |erewrite Γr_eq; [|eauto|apply envV_p_sem_env_in; auto]; trivial].
        erewrite Γr_eq in Hv2'; [|eauto|apply envV_p_sem_env_in; auto].
        erewrite <- p_sem_env_up_v_out in Hv2'; eauto.
        erewrite <- p_sem_env_up_v_in in Hv2'; [|apply in_map_iff; eauto].
        ** eexists s''; split; [econstructor|]; eauto.
           erewrite Σr_p_sem_env with (l := l); eauto.
        ** rewrite in_map_iff. intros (l0&HF&_). eapply False_rect, Hvx, Henv1. eauto.
      -- intros Hn Hj1'. erewrite Γr_eq in Hj2''; eauto.
        ++ destruct Hj2'' as (s'&Hs'&Heq').
           exists s'; split; [econstructor|]; eauto.
            rewrite Σr_p_sem_env with (l := l); auto.
        ++ apply envV_p_sem_env_in; auto.
  + destruct Hj2'' as (s'&Hs'&Heq').
    exists s'; split; [econstructor|]; eauto.
    erewrite Γr_p_out; eauto.
Qed.

Lemma not_sem_free_rename_s : forall s1 Σ1 Γ1 Δ1' Σ1' (Henvf : wb_env Γ1 Σf),
 (wb_env Γ1 Σ1) ->
 (Σ1' ⊆ Σf) ->
  Σ1 / Γ1 ⊢s s1 ⋮ (Δ1', Σ1') ->
  wf_list_s s1 ->
  Γ1 ⊓ flat_map locs_s s1 ->
  Σ1 ⊔ flat_map locs_s s1 ->
 let Σ2 := Σr Σ1 in
 let s2 := map (rename_s Σf vx lx) s1 in
 let Γ2 := Γr Henvf in
 let Σ2' := Σr Σ1' in
   (forall l, envV Γ1 vy = Some l -> L l -> not_sem_free Σ2' Γ2 (flat_map ref_s s2) vy).
Proof.
simpl. intros s1 Σ1 Γ1 Σ1' Henvf Henv0 Hinclf Hj Hp Hwf Hlocs1 Hlocs2 l Hb HL.
apply not_sem_free_sem_bound.
- eapply Γr_vy_L; eauto.
- apply rename_bound; auto.
  rewrite (proj1 (proj2 (ref_rename_m Σf vx lx))). eapply deterministic_semantics_m; eauto.
Qed.

Lemma not_sem_free_rename_S : forall s1 Σ1 Γ1 Δ1' Σ1' (Henvf : wb_env Γ1 Σf),
 (wb_env Γ1 Σ1) ->
 (Σ1' ⊆ Σf) ->
  Σ1 / Γ1 ⊢S s1 ⋮ (Δ1', Σ1') ->
  wf_list_S s1 ->
  Γ1 ⊓ flat_map locs_S s1 ->
  Σ1 ⊔ flat_map locs_S s1 ->
 let Σ2 := Σr Σ1 in
 let s2 := map (rename_S Σf vx lx) s1 in
 let Γ2 := Γr Henvf in
 let Σ2' := Σr Σ1' in
   (forall l, envV Γ1 vy = Some l -> L l -> not_sem_free Σ2' Γ2 (flat_map ref_S s2) vy).
Proof.
simpl. intros s1 Σ1 Γ1 Σ1' Henvf Henv0 Hinclf Hj Hp Hwf Hlocs1 Hlocs2 l Hb HL.
apply not_sem_free_sem_bound.
- eapply Γr_vy_L; eauto.
- apply rename_bound; auto.
  rewrite (proj2 (proj2 (proj2 (ref_rename_m Σf vx lx)))). eapply deterministic_semantics_m; eauto.
Qed.

Proposition P9_m:
(forall m1 Σ1 Γ1 Σ1' Δ1' (Henvf : wb_env Γ1 Σf),
 (wb_env Γ1 Σ1) ->
 (Σ1' ⊆ Σf) ->
  Σ1 / Γ1 ⊢ m1 ⋮ (Δ1', Σ1') ->
  proper Σ1 ->
  wf_m m1 ->
  Γ1 ⊓ locs_m m1 ->
  Σ1 ⊔ locs_m m1 ->
 (forall l, In l (ve_locs_m m1) -> ~ dom_rel (E Σf) l) ->
 let Σ2 := Σr Σ1 in
 let m2 := rename_m Σf vx lx m1 in
 let Γ2 := Γr Henvf in
  {Σ2' & Σ2 / Γ2 ⊢ m2 ⋮ (Δ1', Σ2') ∧ sem_equal Σ2' (Σr Σ1')}) *

(forall s1 Σ1 Γ1 Σ1' Δ1' (Henvf : wb_env Γ1 Σf),
 (wb_env Γ1 Σ1) ->
 (Σ1' ⊆ Σf) ->
  Σ1 / Γ1 ⊢s s1 ⋮ (Δ1', Σ1') ->
  proper Σ1 ->
  wf_list_s s1 ->
  Γ1 ⊓ flat_map locs_s s1 ->
  Σ1 ⊔ flat_map locs_s s1 ->
 (forall l, In l (flat_map ve_locs_s s1) -> ~ dom_rel (E Σf) l) ->
 let Σ2 := Σr Σ1 in
 let s2 := map (rename_s Σf vx lx) s1 in
 let Γ2 := Γr Henvf in
  {Σ2' &  Σ2 / Γ2 ⊢s s2 ⋮ (Δ1', Σ2') ∧ sem_equal Σ2' (Σr Σ1')}) *

(forall t1 Σ1 Γ1 Σ1' Δ1' (Henvf : wb_env Γ1 Σf),
 (wb_env Γ1 Σ1) ->
 (Σ1' ⊆ Σf) ->
  Σ1 / Γ1 ⊢t t1 ⋮ (Δ1', Σ1') ->
  proper Σ1 ->
  wf_t t1 ->
  Γ1 ⊓ locs_t t1 ->
  Σ1 ⊔ locs_t t1 ->
 (forall l, In l (ve_locs_t t1) -> ~ dom_rel (E Σf) l) ->
 let Σ2 := Σr Σ1 in
 let t2 := rename_t Σf vx lx t1 in
 let Γ2 := Γr Henvf in
   {Σ2' & (Σ2 / Γ2 ⊢t t2 ⋮ (Δ1', Σ2')) ∧ (sem_equal (Σr Σ1') Σ2')}) *

(forall S1 Σ1 Γ1 Σ1' Δ1' (Henvf : wb_env Γ1 Σf),
 (wb_env Γ1 Σ1) ->
 (Σ1' ⊆ Σf) ->
  Σ1 / Γ1 ⊢S S1 ⋮ (Δ1', Σ1') ->
  proper Σ1 ->
  wf_list_S S1 ->
  Γ1 ⊓ flat_map locs_S S1 ->
  Σ1 ⊔ flat_map locs_S S1 ->
 (forall l, In l (flat_map ve_locs_S S1) -> ~ dom_rel (E Σf) l) ->
 let Σ2 := Σr Σ1 in
 let S2 := map (rename_S Σf vx lx) S1 in
 let Γ2 := Γr Henvf in
  {Σ2' & Σ2 / Γ2 ⊢S S2 ⋮ (Δ1', Σ2')  ∧ (sem_equal (Σr Σ1') Σ2')}).
Proof.
apply module_ind.
- intros t1 Σ1 Γ1 Σ1' Δ1' Henvf Henv0 Hinclf Hj Hp Hwf Hlocs1 Hlocs2 Hve.
  inversion Hj; subst. erewrite <- Γr_envT. eexists; split; eauto; constructor.
- intros p (t&l) Σ1 Γ1 Σ1' Δ1' Henvf Henv0 Hinclf Hj Hp Hwf Hlocs1 Hlocs2 Hve.
  inversion Hj; subst. eexists; split; eauto; econstructor; eauto.
(* functor *)
- intros [m l] t1 Hind1 t2 Hind2 Σ1 Γ1 Σ1' Δ1' Henvf Henv0 Hinclf Hj Hp Hwf Hlocs1 Hlocs2 Hve.
  inversion Hj; subst. simpl in *.
  rename X into Hj1. rename X0 into Hj2. assert(Hj1' := Hj1). assert(Hj2' := Hj2).
  assert(Σ1 ⊔ locs_t t1) by (split; intros; apply Hlocs2; auto with *).
  simpl in *. apply deterministic_semantics_m in Hj1'; auto with *; [|apply Hwf; auto with *].
  destruct Hj1' as (Hdet'&Henv'&Hwb'&Hlocs'&Hbr'&Hcod'&Hincl'&Hvi'&Hpr'&HE'&HD').
  apply deterministic_semantics_m in Hj2'; auto with *; try tauto.
  + assert(Hincl''': Σ'' ⊆ Σf).
    {
      transitivity Σ1';trivial. transitivity (Σ'' [[l ↦ IM m]]); [|tauto].
      apply sem_incl_up_ρ. rewrite Hlocs'; try tauto. apply Hlocs2; tauto.
    }
    apply Hind1 with (Henvf:=Henvf) in Hj1; auto with *; try tauto.
    destruct Hj2' as (_&Henv''&_&_&_&_&Hincl''&_).
    pose(Henvf' := wb_env_incl Henv'' Hinclf).
    apply Hind2 with (Henvf:=Henvf') in Hj2; auto with *; try tauto.
    * destruct Hj1 as (s&Hj1&Heq). destruct Hj2 as (s'&Hj2&HH2).
      eapply sem_equal_compat_m with (Σ0 := s [[l ↦ IM m]]) in Hj2; [|rewrite Σr_up_m; eauto].
      destruct Hj2 as (s''&Hj2&HH2').
      exists s''; split; [|etransitivity; eauto].
      econstructor; [exact Hj1|]. simpl.
      erewrite Γr_up_m. exact Hj2.
    * eapply wb_env_incl; [| apply sem_incl_up_ρ]; [apply wb_env_up_m; auto|].
      rewrite Hlocs'; [|tauto]. auto. rewrite (proj1 Hlocs2); tauto.
    * apply proper_up; try tauto; try rewrite Hlocs'; try rewrite Hbr';
      try (apply Hlocs2); tauto.
    * apply env_up_m_disj; auto with *.
    * apply sem_up_disj; [ tauto |].
      split; intros l0 Hl0; try rewrite Hlocs'.
      -- rewrite (proj1 Hlocs2); auto with *.
      -- intro Hf; apply Hwf in Hf. tauto.
      -- rewrite Hbr'. apply Hlocs2; auto with *.
         intro HF; apply Hwf in HF; tauto.
  + eapply wb_env_incl; [| apply sem_incl_up_ρ]; [apply wb_env_up_m; auto|].
    rewrite Hlocs'; [|tauto]. auto. rewrite (proj1 Hlocs2); tauto.
  + apply env_up_m_disj; auto with *.
  + apply sem_up_disj; [ tauto |split; intros l0 Hl0; try rewrite Hlocs'].
    * rewrite (proj1 Hlocs2); auto with *.
    * intro Hf; apply Hwf in Hf. tauto.
    * rewrite Hbr'. apply Hlocs2; auto with *. intro HF; apply Hwf in HF; tauto.
- intros t1 Hind (m&l) q Σ1 Γ1 Σ1' Δ1' Henvf Henv0 Hinclf Hj Hp Hwf Hlocs1 Hlocs2 Hve.
  inversion Hj; subst. simpl in *.
  rename X into Hj1. rename X0 into Hj2. assert(Hj1' := Hj1). assert(Hj2' := Hj2).
  assert(Σ1 ⊔ locs_t t1) by (split; intros; apply Hlocs2; auto with *).
  apply deterministic_semantics_m in Hj1'; auto with *; [|apply Hwf; auto with *].
  destruct Hj1' as (Hdet'&Henv'&Hwb'&Hlocs'&Hbr'&Hcod'&Hincl'&Hvi'&Hpr'&HE'&HD').
  assert(wb_env Γ1 (Σ'' [[l ↦ IM m]])) by
    (eapply wb_env_incl;[eauto|]; apply sem_incl_up_ρ;
     rewrite Hlocs'; [|tauto]; auto; rewrite (proj1 Hlocs2); tauto).
  apply deterministic_semantics_em_path in Hj2'; auto with *; try tauto.
  assert(Hincl0: Σ'' ⊆ (Σ'' [[l ↦ IM m]]))
  by (apply sem_incl_up_ρ; rewrite Hlocs'; [|tauto]; auto; rewrite (proj1 Hlocs2); tauto).
  assert(Hincl1: (Σ'' [[l ↦ IM m]]) ⊆ Σf) by (transitivity Σ';[tauto|eauto]).
  assert(Σ'' ⊆ Σf) by eauto.
  apply Hind with (Henvf:=Henvf) in Hj1; try tauto; auto with *. clear Hind.
  apply P9_em_path with (Henvf:=Henvf) in Hj2;eauto.
  destruct Hj1 as (s1&Hj1&HH1).
  eapply sem_equal_compat_em_path with (Σ0 := (s1 [[l ↦ IM m]])) in Hj2;
  [|rewrite Σr_up_m; eauto].
  destruct Hj2 as (s2''&Hj2&HH2').
  assert(Hincl: (Σ'' [[l ↦ IM m]]) ⊆ Σ') by tauto.
  assert(Hwb'': wb_d Σ' Δ') by tauto. clear Hj2'.
  erewrite <- Σr_s_mod; eauto.
  + rewrite Σr_sem_join; auto with *.
    * erewrite s_mod_sem_equal; eauto. eexists; split;[econstructor|]; eauto.
    * eapply wb_desc_incl; [|exact Hwb']; eauto.
    * apply wb_singleton_ls; trivial. left. exists m. now apply Hincl.
  + now apply Hincl.
- intros t1 Hind [m l] q Σ1 Γ1 Σ1' Δ1' Henvf Henv0 Hinclf Hj Hp Hwf Hlocs1 Hlocs2 Hve.
  inversion Hj; subst. simpl in *.
  rename X into Hj1. rename X0 into Hj2. assert(Hj1' := Hj1). assert(Hj2' := Hj2).
  assert(Σ1 ⊔ locs_t t1) by (split; intros; apply Hlocs2; auto with *).
  apply deterministic_semantics_m in Hj1'; auto with *; [|apply Hwf; auto with *].
  destruct Hj1' as (Hdet'&Henv'&Hwb'&Hlocs'&Hbr'&Hcod'&Hincl'&Hvi'&Hpr'&HE'&HD').
  assert(wb_env Γ1 (Σ'' [[l ↦ IM m]])) by
    (eapply wb_env_incl;[eauto|]; apply sem_incl_up_ρ;
     rewrite Hlocs'; [|tauto]; auto; rewrite (proj1 Hlocs2); tauto).
  apply deterministic_semantics_em_path in Hj2'; auto with *; try tauto.
  assert(Hincl0: Σ'' ⊆ (Σ'' [[l ↦ IM m]]))
  by (apply sem_incl_up_ρ; rewrite Hlocs'; [|tauto]; auto; rewrite (proj1 Hlocs2); tauto).
  assert(Hincl1: (Σ'' [[l ↦ IM m]]) ⊆ Σf) by (transitivity Σ';[tauto|eauto]).
  assert(Σ'' ⊆ Σf) by eauto.
  apply Hind with (Henvf:=Henvf) in Hj1; try tauto; auto with *. clear Hind.
  apply P9_em_path with (Henvf:=Henvf) in Hj2;eauto.
  destruct Hj1 as (s1&Hj1&HH1).
  eapply sem_equal_compat_em_path with (Σ0 := (s1 [[l ↦ IM m]])) in Hj2;
  [|rewrite Σr_up_m; eauto].
  destruct Hj2 as (s2''&Hj2&HH2').
  assert(Hincl: (Σ'' [[l ↦ IM m]]) ⊆ Σ') by tauto.
  assert(Hwb'': wb_d Σ' Δ') by tauto. clear Hj2'.
  rewrite Σr_sem_join; auto with *;
  [|eapply wb_desc_incl; [|exact Hwb']; eauto
   |apply wb_singleton_ls; trivial; left; exists m; now apply Hincl].
  erewrite <- Σr_filtering, desc_filtering_sem_equal; eauto.
  eexists; split; [econstructor|];eauto.
- intros l Hind Σ1 Γ1 Σ1' Δ1' Henvf Henv0 Hinclf Hj Hp Hwf Hlocs1 Hlocs2 Hve.
  inversion Hj; subst. simpl.
  apply Hind with (Henvf := Henvf) in X; auto with *.
  destruct X as (s&Hs&Heq). eexists; split; [econstructor|];eauto.
- intros m Hind Σ1 Γ1 Σ1' Δ1' Henvf Henv0 Hinclf Hj Hp Hwf Hlocs1 Hlocs2 Hve.
  inversion Hj; subst. simpl in *.
  apply Hind with (Henvf := Henvf) in X; auto with *.
  destruct X as (s&Hs&Heq). eexists; split; [econstructor|];eauto.
- intros ls [v l] Hind Σ1 Γ1 Σ1' Δ1' Henvf Henv0 Hinclf Hj Hp Hwf Hlocs1 Hlocs2 Hve.
  inversion Hj; subst. simpl in *.
  rename X into Hj1. assert(Hj' := Hj1).
  assert(Henv: wb_env (Γ1 [[v ↦v Some l]]) (Σ1 [[l ↦ IV v]])) by
  (apply wb_env_up_v; try (apply Hlocs2; tauto);eapply wb_env_incl; [exact Henv0|trivial]).
  assert((Γ1 [[v ↦v Some l]]) ⊓ flat_map locs_S ls)
   by (apply env_up_v_disj; auto; intro HF; apply Hwf in HF; auto with *).
  assert((Σ1 [[l ↦ IV v]]) ⊔ flat_map locs_S ls) by
    (apply sem_up_v_disj;[intro Hl0; apply Hwf in Hl0; auto with *|disjtac]).
  apply deterministic_semantics_m in Hj'; auto with *; try tauto.
  destruct Hj' as (Hdet'&Henv'&Hwb'&Hlocs'&Hbr'&Hcod'&Hincl'&Hvi'&Hpr'&HE' &HD'&Hw'&Hmon0).
  assert(Henvf': wb_env (Γ1 [[v ↦v Some l]]) Σf) by
    (eapply wb_env_incl; [exact Henv|intuition; eauto]).
  assert(proper (Σ1 [[l ↦ IV v]])) by (apply proper_up; trivial; apply Hlocs2; tauto).
  assert(Hincl: Σ' ⊆ Σf) by eauto.
  assert(Hj' := Hj1).
  apply Hind with (Henvf := Henvf') in Hj1; auto with *; try tauto;clear Hind.
  assert(wb_d Σ' (Structural (l :: nil) nil)) by
  (apply wb_singleton_lv; eapply sem_incl_cod; [exact Hincl'|];
   eapply sem_up_sr_cod, Hlocs2; tauto).
  rewrite Σr_sem_join; trivial.
  erewrite <- Σr_sup; eauto.
  destruct Hj1 as (s1&Hj1&Heq1). assert(Hj1' := Hj1).
  apply deterministic_semantics_m in Hj1; autorewrite with core in *;
  [|eapply wb_env_Γr; eauto| now apply rename_wf_m|
    apply Γr_disj; auto | apply Σr_disj; auto].
  destruct Hj1 as (_&_&_&_&_&_&_&_&_ &_&_&Hw0&_).
  assert(Hinclf'': Σ1 [[l ↦ IV v]] ⊆ Σf) by eauto.
  unfold rename_S at 1. case L_dec; intro HL.
  + assert(Heq: v = vy). {
      assert (HL' := HL). apply L_values in HL.
      assert(Hl: ρ ((Σ1 [[l ↦ IV v]])) l = Some (IV v)) by trivial.
      apply Hinclf'' in Hl. rewrite (proj1 Hl) in HL; now inversion HL.
    } subst v.
    erewrite Γr_env_up_v in Hj1' by auto.
    eapply (snd (sem_equal_compat_m)) with (Σ0 := Σr Σ1 [[l ↦ IV vx]]) in Hj1';
      [|rewrite <- Σr_up_ρ_L by trivial; eauto].
    revert Hj1'; unfold Γr; case dec_range.
    * intros (ly&Hly&HEy) Hj1'.
      destruct Hj1' as (s2'&Hj2&Heq2').
      assert(sem_equal (Σr Σ') s2') by (transitivity s1; auto).
      erewrite desc_sup_sem_equal with (s' := s2') by trivial.
      eexists (sem_join s2' _ _); split; [econstructor|]; eauto.
      rewrite env_up_v_comm, env_up_v_idem; auto.
    * intros Hn Hj1'. (* requires weakening *)
      assert(Hv2': not_sem_free (Σr Σ') (Γr (Γ1:=Γ1 [[vy ↦v Some l]]) Henvf')
       (flat_map ref_S (map (rename_S Σf vx lx) ls)) vy)
      by (eapply not_sem_free_rename_S; eauto; try tauto).
      eapply not_sem_free_sem_equal with (s' := s1) in Hv2'; eauto.
      eapply Hw0 with (l := envV Γ1 vy) in Hv2'.
      unfold Γr in Hv2'. case dec_range in Hv2'.
      -- destruct s as (ly&_).
         rewrite envV_env_up_v_eq, envV_env_up_v_neq in Hv2'; auto.
         rewrite env_up_v_idem, env_up_v_comm,env_up_v_idem in Hv2'; auto.
         eapply (snd sem_equal_compat_m) with (Σ0 :=  (Σr Σ1)  [[l ↦ IV vx]]) in Hv2';
         [|rewrite <- Σr_up_ρ_L]; eauto.
         rewrite env_up_v_id in Hv2'; auto.
         destruct Hv2' as (s'&Hv2'&Heq').
         erewrite desc_sup_sem_equal with (s' := s'); [|transitivity s1; eauto].
         eexists; split; [econstructor|]; eauto.
         apply sem_equal_join_compat. transitivity s1; eauto.
      -- eapply False_rect, n, HL. apply envV_env_up_v_eq.
  + eapply (snd sem_equal_compat_m) with (Σ0 := Σr Σ1 [[l ↦ IV v]]) in Hj1';
    [|rewrite <- Σr_up_ρ_nL; auto].
    fold rename_S; revert Hj1'; unfold Γr at 2. case dec_range.
    * intros (ly&Hvy&Hly) Hj1'. case(val_eq_dec v vy).
      -- intro; subst v.
         assert(Hv2': not_sem_free (Σr Σ') (Γr (Γ1:=Γ1 [[vy ↦v Some l]]) Henvf')
           (flat_map ref_S (map (rename_S Σf vx lx) ls)) vx). {
            apply not_sem_free_sem_bound.
            - rewrite Γr_env_up_v_nL''; auto.
            - apply rename_bound_xy;[eauto|].
              rewrite (proj2 (proj2 (proj2 (ref_rename_m Σf vx lx)))). now eapply Hmon0.
         }
         eapply not_sem_free_sem_equal with (s' := s1) in Hv2'; eauto.
         apply Hw0 with (l:= envV Γ1 vy) in Hv2'.
         erewrite Γr_eq in Hv2'; eauto.
         rewrite env_up_v_comm in Hv2'; auto.
         eapply sem_equal_compat_m with (Σ0 := (Σr Σ1 [[l ↦ IV vy]])) in Hv2'; eauto;
         [|rewrite <- Σr_up_ρ_nL; auto].
         destruct Hv2' as (s2'&Hv2'&Heq2').
         erewrite desc_sup_sem_equal with (s' := s2'); [|transitivity s1; eauto].
         eexists (sem_join s2' _ _); split; [econstructor|]; eauto.
         rewrite env_up_v_idem; trivial.
         apply sem_equal_join_compat. transitivity s1; eauto.
       -- intros Hneq. erewrite Γr_env_up_v_nL' in Hj1'; eauto.
         ++ destruct Hj1' as (s'&Hj2&Heq''').
            eexists (sem_join s' _ _); split;
            [|apply sem_equal_join_compat; transitivity s1; eauto].
            erewrite desc_sup_sem_equal with (s' := s'); [|transitivity s1; eauto].
            econstructor; eauto.
         ++ intro; subst v; apply False_rect, (Hvx l), Hinclf''. auto.
  * intros. rewrite Γr_env_up_v_nL in Hj1'; auto.
    destruct Hj1' as (s'&Hj2&Heq').
    erewrite desc_sup_sem_equal with (s' := s'); [|transitivity s1; eauto].
    eexists (sem_join s' _ _); split; [econstructor|]; eauto.
    apply sem_equal_join_compat; transitivity s1; eauto.
- intros ls [m l] t Hind1 Hind2 Σ1 Γ1 Σ1' Δ1' Henvf Henv0 Hinclf Hj Hp Hwf Hlocs1 Hlocs2 Hve.
  inversion Hj; subst. simpl in *.
  rename X into Hj1. rename X0 into Hj2. assert(Hj1' := Hj1). assert(Hj2' := Hj2).
  assert(Σ1 ⊔ locs_t t) by (split; intros; apply Hlocs2; auto with *).
  apply deterministic_semantics_m in Hj1'; auto with *; [|apply Hwf; auto with *].
  destruct Hj1' as (Hdet'&Henv'&Hwb'&Hlocs'&Hbr'&Hcod'&Hincl'&Hvi'&Hpr'&HE'&HD').
  assert(wb_env Γ1 (Σ'' [[l ↦ IM m]])) by
    (eapply wb_env_incl;[eauto|]; apply sem_incl_up_ρ;
     rewrite Hlocs'; [|tauto]; auto; rewrite (proj1 Hlocs2); tauto).
  assert(Hincl0: Σ1 ⊆ (Σ1 [[l ↦ IM m]]))
  by (apply sem_incl_up_ρ;apply Hlocs2; tauto).
  assert(Henv: wb_env (Γ1 [[m ↦m Δ]]) (Σ'' [[l ↦ IM m]]))
  by (eapply wb_env_incl; [| apply sem_incl_up_ρ]; [apply wb_env_up_m; auto|];
      rewrite Hlocs';[|tauto]; apply Hlocs2; tauto).
  assert((Γ1 [[m ↦m Δ]]) ⊓ flat_map locs_S ls)
   by (apply env_up_m_disj; auto with *).
  assert((Σ'' [[l ↦ IM m]]) ⊔ flat_map locs_S ls).
  { unfold disj in Hwf. apply sem_up_disj.
    - intro HF; apply Hwf in HF; auto with *.
    - disjtac; rewrite Hlocs' || rewrite Hbr'; try apply Hlocs2; auto with *;
      simpl in Hwf; intro Hf; apply Hwf in H2; tauto.
  }
  apply deterministic_semantics_m in Hj2'; auto with *; try tauto.
  destruct Hj2' as (Hdet''&Henv''&Hwb''&Hlocs''&Hbr''&Hincl''&Hvi''&Hpr''&HE''&HD'').
  assert(Hincl1: (Σ'' [[l ↦ IM m]]) ⊆ Σf) by (transitivity Σ1';[tauto|eauto]).
  assert(Hincl: (Σ'' ⊆ (Σ'' [[l ↦ IM m]])))
  by(apply sem_incl_up_ρ; rewrite Hlocs';[apply Hlocs2|]; simpl in *; tauto).
   assert(Σ'' ⊆ Σf) by (transitivity (Σ'' [[l ↦ IM m]]); intuition; eauto).
  apply Hind2 with (Henvf:=Henvf) in Hj1; try tauto; auto with *. clear Hind2.
  assert(Henvf': wb_env (Γ1 [[m ↦m Δ]]) Σf) by
    (eapply wb_env_incl; [exact Henv|intuition; eauto]).
  assert(proper (Σ'' [[l ↦ IM m]]))
  by (apply proper_up; try tauto; try rewrite Hlocs'; try rewrite Hbr'; try (apply Hlocs2); tauto).
  apply Hind1 with (Henvf:=Henvf') in Hj2;eauto; try tauto; auto with *.
  assert(Hl: ρ Σf l = Some(IM m)) by (now apply Hincl1).
  rewrite <- Σr_sup_ls; auto;[|rewrite Hl; discriminate].
  destruct Hj1 as (s&Hj1&Heq1). destruct Hj2 as (s2&Hj2&Heq2).
  eapply sem_equal_compat_m with (Σ0 := (s [[l ↦ IM m]])) in Hj2; [|rewrite Σr_up_m; eauto].
  destruct Hj2 as (s2'&Hj2&Heq2').
  erewrite desc_sup_sem_equal; [|etransitivity; eauto].
  eexists s2'; split; [econstructor|etransitivity]; eauto.
  erewrite Γr_up_m;eauto.
- intros ls [m l] q Hind Σ1 Γ1 Σ1' Δ1' Henvf Henv0 Hinclf Hj Hp Hwf Hlocs1 Hlocs2 Hve.
  inversion Hj; subst. simpl in *.
  rename X into Hj1. rename X0 into Hj2. assert(Hj1' := Hj1). assert(Hj2' := Hj2).
  assert(Σ1 ⊔ locs_m_path q) by (split; intros; apply Hlocs2; auto with *).
  apply deterministic_semantics_m_path in Hj1'; auto with *.
  destruct Hj1' as (Hdet'&Heq&Hwb'&_&_). (* subst Σ''. *)
  assert(wb_env Γ1 (Σ1 [[l ↦ IM m]]))
  by (eapply wb_env_incl; [| apply sem_incl_up_ρ]; trivial; apply Hlocs2;auto).
  assert(Hincl0: Σ1 ⊆ (Σ1 [[l ↦ IM m]]))
  by (apply sem_incl_up_ρ;apply Hlocs2; tauto).
  assert(Henv: wb_env (Γ1 [[m ↦m Δ]]) (Σ1 [[l ↦ IM m]]))
  by (eapply wb_env_incl; [| apply sem_incl_up_ρ]; [apply wb_env_up_m|]; simpl; try apply Hlocs2;auto).
  assert((Γ1 [[m ↦m Δ]]) ⊓ flat_map locs_S ls)
   by (apply env_up_m_disj; auto with *).
  assert((Σ1 [[l ↦ IM m]]) ⊔ flat_map locs_S ls)
  by (apply sem_up_disj; auto with *; intuition; eauto with *).
  apply deterministic_semantics_m in Hj2'; auto with *; try tauto.
  destruct Hj2' as (Hdet''&Henv''&Hwb''&Hlocs''&Hbr''&Hincl''&Hvi''&Hpr''&HE''&HD'').
  assert(Hincl1: (Σ1 [[l ↦ IM m]]) ⊆ Σf) by eauto.
  assert(Henvf': wb_env (Γ1 [[m ↦m Δ]]) Σf) by
    (eapply wb_env_incl; [exact Henv|intuition; eauto]).
  assert(proper (Σ1 [[l ↦ IM m]]))
  by (apply proper_up; try tauto; try rewrite Hlocs'; try rewrite Hbr'; try (apply Hlocs2); tauto).
  apply Hind with (Henvf:=Henvf') in Hj2; try tauto; auto with *. clear Hind.
  assert(Hl: ρ Σf l = Some(IM m)) by (now apply Hincl1).
  rewrite <- Σr_sup_ls; auto;[|rewrite Hl; discriminate].
  destruct Hj2 as (s2&Hj2&Heq2).
  eapply sem_equal_compat_m with (Σ0 :=Σr Σ1 [[l ↦ IM m]]) in Hj2; [|rewrite Σr_up_m; eauto].
  destruct Hj2 as (s2'&Hj2&Heq2').
  erewrite desc_sup_sem_equal; [|etransitivity; eauto].
  eexists; split; [econstructor|etransitivity]; eauto.
  erewrite Γr_up_m;eauto.
- intros ls [t l] Hind Σ1 Γ1 Σ1' Δ1' Henvf Henv0 Hinclf Hj Hp Hwf Hlocs1 Hlocs2 Hve.
  inversion Hj; subst. simpl in *.
  rename X into Hj1. assert(Hj' := Hj1).
  assert(Henv: wb_env (Γ1 [[t ↦t ∅]]) (Σ1 [[l ↦ IT t]]))
  by (apply wb_env_up_t; try (apply Hlocs2; tauto);trivial;
      eapply wb_env_incl; [exact Henv0|apply sem_incl_up_ρ, Hlocs2; tauto]).
  assert((Γ1 [[t ↦t ∅]]) ⊓ flat_map locs_S ls) by
  (apply env_up_t_disj; auto; rewrite in_map_iff;
    intros (x&Heq&HF); inversion Heq; subst; apply Hwf in HF; auto with *).
  assert((Σ1 [[l ↦ IT t]]) ⊔ flat_map locs_S ls) by
    (apply sem_up_v_disj;[intro Hl0; apply Hwf in Hl0; auto with *|disjtac]).
  apply deterministic_semantics_m in Hj'; auto with *; try tauto.
  destruct Hj' as (Hdet'&Henv'&Hwb'&Hlocs'&Hbr'&Hcod'&Hincl'&Hvi'&Hpr'&HE' &HD').
  assert(Henvf': wb_env (Γ1 [[t ↦t ∅]]) Σf) by
    (eapply wb_env_incl; [exact Henv|intuition; eauto]).
  assert(proper (Σ1 [[l ↦ IT t]])) by (apply proper_up; trivial; apply Hlocs2; tauto).
  apply Hind with (Henvf := Henvf') in Hj1; auto with *; try tauto;clear Hind.
  assert(Hincl1: (Σ1 [[l ↦ IT t]]) ⊆ Σf) by (transitivity Σ1'; intuition; eauto).
  assert(Hl: ρ Σf l = Some(IT t)) by (now apply Hincl1).
  assert(wb_d Σ1' (Structural nil ((l, ∅) :: nil)))
  by (apply wb_singleton_ls; trivial; right; exists t; now apply Hincl').
  destruct Hj1 as (s&Hj1&Heq1).
  eapply sem_equal_compat_m with (Σ0 :=Σr Σ1 [[l ↦ IT t]]) in Hj1; [|rewrite Σr_up_t; eauto].
  destruct Hj1 as (s'&Hj1&Heq1').
  erewrite <- Σr_sup_ls; auto;[|rewrite Hl; discriminate].
  erewrite desc_sup_sem_equal; [|etransitivity; eauto].
  eexists s'. split; [constructor; erewrite Γr_up_t; eauto|etransitivity; eauto].
- intros ls [t l] m Hind1 Hind2 Σ1 Γ1 Σ1' Δ1' Henvf Henv0 Hinclf Hj Hp Hwf Hlocs1 Hlocs2 Hve.
  inversion Hj; subst. simpl in *.
  rename X into Hj1. rename X0 into Hj2. assert(Hj1' := Hj1). assert(Hj2' := Hj2).
  assert(Σ1 ⊔ locs_t m) by (split; intros; apply Hlocs2; auto with *).
  apply deterministic_semantics_m in Hj1'; auto with *; [|apply Hwf; auto with *].
  destruct Hj1' as (Hdet'&Henv'&Hwb'&Hlocs'&Hbr'&Hcod'&Hincl'&Hvi'&Hpr'&HE'&HD').
  assert(wb_env Γ1 (Σ'' [[l ↦ IT t]])) by
    (eapply wb_env_incl;[eauto|]; apply sem_incl_up_ρ;
     rewrite Hlocs'; [|tauto]; auto; rewrite (proj1 Hlocs2); tauto).
  assert(Hincl0: Σ1 ⊆ (Σ1 [[l ↦ IT t]]))
  by (apply sem_incl_up_ρ;apply Hlocs2; tauto).
  assert(Henv: wb_env (Γ1 [[t ↦t Δ]]) (Σ'' [[l ↦ IT t]]))
  by (eapply wb_env_incl; [| apply sem_incl_up_ρ]; [apply wb_env_up_t; auto|];
      rewrite Hlocs';[|tauto]; apply Hlocs2; tauto).
  assert((Γ1 [[t ↦t Δ]]) ⊓ flat_map locs_S ls)
   by (apply env_up_t_disj; auto with *).
  assert((Σ'' [[l ↦ IT t]]) ⊔ flat_map locs_S ls).
  { unfold disj in Hwf. apply sem_up_disj.
    - intro HF; apply Hwf in HF; auto with *.
    - disjtac; rewrite Hlocs' || rewrite Hbr'; try apply Hlocs2; auto with *;
      simpl in Hwf; intro Hf; apply Hwf in H2; tauto.
  }
  apply deterministic_semantics_m in Hj2'; auto with *; try tauto.
  destruct Hj2' as (Hdet''&Henv''&Hwb''&Hlocs''&Hbr''&Hincl''&Hvi''&Hpr''&HE''&HD'').
  assert(Hincl1: (Σ'' [[l ↦ IT t]]) ⊆ Σf) by (transitivity Σ1';[tauto|eauto]).
  assert(Hincl: (Σ'' ⊆ (Σ'' [[l ↦ IT t]])))
  by(apply sem_incl_up_ρ; rewrite Hlocs';[apply Hlocs2|]; simpl in *; tauto).
   assert(Σ'' ⊆ Σf) by (transitivity (Σ'' [[l ↦ IT t]]); intuition; eauto).
  apply Hind2 with (Henvf:=Henvf) in Hj1; try tauto; auto with *. clear Hind2.
  assert(Henvf': wb_env (Γ1 [[t ↦t Δ]]) Σf) by
    (eapply wb_env_incl; [exact Henv|intuition; eauto]).
  assert(proper (Σ'' [[l ↦ IT t]]))
  by (apply proper_up; try tauto; try rewrite Hlocs'; try rewrite Hbr'; try (apply Hlocs2); tauto).
  apply Hind1 with (Henvf:=Henvf') in Hj2;eauto; try tauto; auto with *.
  assert(Hl: ρ Σf l = Some(IT t)) by (now apply Hincl1).
  rewrite <- Σr_sup_ls; auto;[|rewrite Hl; discriminate].
  destruct Hj1 as (s&Hj1&Heq1). destruct Hj2 as (s2&Hj2&Heq2).
  eapply sem_equal_compat_m with (Σ0 := (s [[l ↦ IT t]])) in Hj2; [|rewrite Σr_up_t; eauto].
  destruct Hj2 as (s2'&Hj2&Heq2').
  erewrite desc_sup_sem_equal; [|etransitivity; eauto].
  eexists s2'; split; [econstructor; eauto; erewrite Γr_up_t; eauto|etransitivity; eauto].
(* include *)
- intros ls t Hind1 Hind2 Σ1 Γ1 Σ1' Δ' Henvf Henv0 Hi Hj Hp Hwf Hlocs1 Hlocs2 Hve.
  inversion Hj; subst d DUd' d'; subst. simpl in *.
  rename X into Hj1. rename X0 into Hj2. assert(Hj1' := Hj1). assert(Hj2' := Hj2).
  assert(Σ1 ⊔ locs_t t) by (split; intros; apply Hlocs2; auto with *).
  apply deterministic_semantics_m in Hj1'; auto with *; [|apply Hwf; auto with *].
  destruct Hj1' as (Hdet1&Henv1&Hwb11&Hlocs'&Hbr'&H1'&Hincl'&Hpr').
  assert(Henv: wb_env Γ' Σ'') by now (apply wb_p_sem_env).
  assert(Γ' ⊓ flat_map locs_S ls)
   by (apply p_sem_env_disj; auto with *;
         split;intros l0 Hl0; rewrite Hlocs'|| rewrite Hbr'; try apply Hlocs2; auto with *;
         intro HF; apply Hwf in Hl0; auto with *).
  assert(Σ'' ⊔ flat_map locs_S ls)
  by (split;intros l0 Hl0; rewrite Hlocs'|| rewrite Hbr'; try apply Hlocs2; auto with *;
      intro HF; apply Hwf in Hl0; auto with *).
  apply deterministic_semantics_m in Hj2'; auto with *; try tauto.
  assert(Hj0 := Hj2).
  destruct Hj2' as (Hdet''&Henv''&Hwb''&Hlocs''&Hbr''&Hincl''&Hvi''&Hpr''&HE''&HD''&_&_&Hb'').
  assert(Hincl1: Σ'' ⊆ Σf) by (transitivity Σ'; eauto).
  apply Hind2 with (Henvf:=Henvf) in Hj1; try tauto; auto with *. clear Hind2.
  assert(Henvf': wb_env Γ' Σf) by
    (eapply wb_env_incl; [exact Henv|intuition; eauto]).
  apply Hind1 with (Henvf:=Henvf') in Hj2;eauto; try tauto; auto with *. clear Hind1.
  assert(Hwb1: wb_d Σ' (Structural lv ls0)) by (eapply wb_desc_incl; [|exact Hwb11]; trivial).
  rewrite <- Σr_sup_lv; auto;[|eauto].
  rewrite Σr_sem_join; auto.
  2,3: eapply wb_desc_lv; eauto.
  assert(Hj2' := Hj2). destruct Hj2 as (s2&Hj2&Heq2).
  destruct Hj1 as (s1&Hj1&Heq1).
  apply deterministic_semantics_m in Hj2;
  [|eapply wb_env_Γr; eauto| now apply rename_wf_m|
    apply Γr_disj, p_sem_env_disj; autorewrite with core; auto with * |
    apply Σr_disj; autorewrite with core; auto].
  destruct Hj2 as (_&_&_&_&_&_&_&_&_ &_&_&Hw0&Hb0).
  clear Hpr' Hve Hpr'' Hlocs' Hlocs'' Hbr' Hbr''.
  case(In_dec (option_eq_dec var_eq_dec) (Some (IV vy)) (map (ρ Σ'') lv)); intro HinD.
  (* there is a matching l in d *)
  + assert (HinD0 := HinD). apply in_map_inv in HinD; [|exact (option_eq_dec var_eq_dec)].
    destruct HinD as (l&Hl&Hin). case (L_dec l); intro HL.
    * destruct Hj2' as (s'&Hj2'&Heq').
      revert Hj1; case (dec_range Hpf Hcodf Henvf).
      -- intros (s&Hs&HEs) Hj1.
         subst Γ'. erewrite <- Γr_p_in_L with (l := l) in Hj2'; eauto; [|now left].
         eapply (snd sem_equal_compat_m) with (Σ0 := s1) in Hj2'; eauto.
         destruct Hj2' as (s''&Hj2'&Heq'').
         erewrite desc_sup_sem_equal with (s' := s'');[| transitivity s'; eauto].
         erewrite p_sem_env_sem_equal with (s' := s1) in Hj2'; eauto.
         eexists (sem_join s'' _ _); split; [econstructor|]; eauto.
         apply sem_equal_join_compat; transitivity s'; eauto.
      -- intros Hn Hj1'.
         assert(Hv2': not_sem_free (Σr Σ') (Γr (Γ1:=Γ') Henvf')
                (flat_map ref_S (map (rename_S Σf vx lx) ls)) vy).
         {
           eapply not_sem_free_rename_S; eauto; try tauto.
           apply envV_p_sem_env_in; auto.
         }
         eapply not_sem_free_sem_equal with (s' := s2) in Hv2'; eauto.
         eapply Hw0 in Hv2'.
         subst Γ'. erewrite <- Γr_p_in_L' in Hv2'; eauto.
         eapply sem_equal_compat_m with (Σ0 := s1) in Hv2'; eauto.
         destruct Hv2' as (s2'&Hj2&Heq2').
         erewrite desc_sup_sem_equal.
         erewrite p_sem_env_sem_equal in Hj2.
         eexists (sem_join _ _ _); split; [econstructor|]; eauto.
         apply sem_equal_join_compat; transitivity s2; eauto.
         etransitivity; eauto.
         transitivity s2; eauto.
    (* it is not renamed *)
    * revert Hj1. unfold Γr. case dec_range.
      (* there is a renamed location in the environment *)
      -- intros (s&Hs&_) Hj1. erewrite Γr_eq in Hj2'; eauto.
        ++ destruct Hj2' as (s''&Hj2''&Heq'').
           assert(Hv2': not_sem_free (Σr Σ') (Γr (Γ1:=Γ') Henvf')
              (flat_map ref_S (map (rename_S Σf vx lx) ls)) vx).
            {
              apply not_sem_free_sem_bound.
              - erewrite Γr_eq; eauto. apply envV_p_sem_env_in; auto.
              - apply rename_bound_xy; [etransitivity; eauto|].
                rewrite (proj2 (proj2 (proj2 (ref_rename_m Σf vx lx)))).
                eapply p_sem_up_mon in Hs; eauto. destruct Hs.
                eapply Hb''; eauto.
            }
            eapply not_sem_free_sem_equal with (s' := s2) in Hv2'; eauto.
            apply Hw0 with (l:= envV Γ1 vy) (v := vx) in Hv2'.
            erewrite Γr_eq in Hv2'; [|eauto|apply envV_p_sem_env_in; auto].
            subst Γ'.
            erewrite <- p_sem_env_up_v_out in Hv2'; eauto.
            ** erewrite <- p_sem_env_up_v_in in Hv2'; [|apply in_map_iff; eauto].
               erewrite <- Σr_p_sem_env in Hv2'; eauto.
               eapply sem_equal_compat_m with (Σ0 := s1) in Hv2'; eauto.
              destruct Hv2' as (s'&Hv2'&Heq').
               erewrite p_sem_env_sem_equal with (s' := s1) in Hv2'; [|etransitivity; eauto].
               erewrite desc_sup_sem_equal.
               eexists (sem_join s' _ _); split; [econstructor|]; eauto.
               apply sem_equal_join_compat; transitivity s2; eauto.
               transitivity s2; eauto.
            ** rewrite in_map_iff. intros (l0&HF&_).
               eapply False_rect, Hvx, Hincl1. eauto.
        ++ eapply envV_p_sem_env_in; eauto.
      -- intros Hn Hj1. erewrite Γr_eq in Hj2'; eauto.
        ++ destruct Hj2' as (s''&Hj2'&Heq'').
           eapply sem_equal_compat_m with (Σ0 := s1) in Hj2'; eauto.
           destruct Hj2' as (s'&Hj2'&Heq').
           erewrite desc_sup_sem_equal with (s' := s'); [|transitivity s''; eauto].
           subst Γ'. erewrite <- Σr_p_sem_env in Hj2'; eauto.
           erewrite p_sem_env_sem_equal with (s' := s1) in Hj2'; [|etransitivity; eauto].
           eexists (sem_join s' _ _); split; [econstructor|]; eauto.
           apply sem_equal_join_compat; transitivity s''; eauto.
        ++ apply envV_p_sem_env_in; auto.
  + destruct Hj2' as (s'&Hj2'&Heq').
    subst Γ'. erewrite <- Γr_p_out in Hj2'; eauto.
    eapply sem_equal_compat_m with (Σ0 := s1) in Hj2'; eauto.
    destruct Hj2' as (s''&Hj2'&Heq'').
    erewrite desc_sup_sem_equal with (s' := s''); [|transitivity s'; eauto].
    eexists (sem_join s'' _ _); split; [econstructor|]; eauto.
    -- erewrite <- p_sem_env_sem_equal in Hj2'; eauto.
    -- apply sem_equal_join_compat; transitivity s'; eauto.
- intros Σ1 Γ1 Σ1' Δ1' Henvf Henv0 Hinclf Hj Hp Hwf Hlocs1 Hlocs2 Hve.
  inversion Hj; subst. eauto.
- intros ls p Hind1 Σ1 Γ1 Σ1' Δ' Henvf Henv0 Hi Hj Hp Hwf Hlocs1 Hlocs2 Hve.
  inversion Hj; subst d DUd' d'; subst. simpl in *.
  rename X into Hj1. rename X0 into Hj2. assert(Hj1' := Hj1). assert(Hj2' := Hj2).
(*   assert(Σ1 ⊔ locs_m_path p) by (split; intros; apply Hlocs2; auto with * ). *)
  apply deterministic_semantics_m_path in Hj1'; auto with *.
  destruct Hj1' as (Hdet1&_&Hwb11&Hmt&Hlocs).
  assert(Henv: wb_env Γ' Σ1) by now (apply wb_p_sem_env).
  assert(Γ' ⊓ flat_map locs_S ls)
   by (apply p_sem_env_disj; auto with *;
         split;intros l0 Hl0; rewrite Hlocs'|| rewrite Hbr'; try apply Hlocs2; auto with *;
         intro HF; apply Hwf in Hl0; auto with *).
  apply deterministic_semantics_m in Hj2'; auto with *; try tauto.
  assert(Hj0 := Hj2).
  destruct Hj2' as (Hdet''&Henv''&Hwb''&Hlocs''&Hbr''&Hincl''&Hvi''&Hpr''&HE''&HD''&_&_&Hb'').
  assert(Hincl0 : Σ1 ⊆ Σf) by eauto.
  apply P9_m_path with (Henvf:=Henvf) in Hj1; try tauto; auto with *.
  assert(Henvf': wb_env Γ' Σf) by
    (eapply wb_env_incl; [exact Henv|intuition; eauto]).
  apply Hind1 with (Henvf:=Henvf') in Hj2;eauto; try tauto; auto with *. clear Hind1.
  assert(Hwb1: wb_d Σ' (Structural lv ls0)) by (eapply wb_desc_incl; [|exact Hwb11]; trivial).
  rewrite <- Σr_sup_lv; auto;[|eauto].
  rewrite Σr_sem_join; auto.
  2,3: eapply wb_desc_lv; eauto.
  assert(Hj2' := Hj2). destruct Hj2 as (s2&Hj2&Heq2).
(*   destruct Hj1 as (s1&Hj1&Heq1). *)
  apply deterministic_semantics_m in Hj2;
  [|eapply wb_env_Γr; eauto| now apply rename_wf_m|
    apply Γr_disj, p_sem_env_disj; autorewrite with core; auto with * |
    apply Σr_disj; autorewrite with core; auto].
  destruct Hj2 as (_&_&_&_&_&_&_&_&_ &_&_&Hw0&Hb0).
  clear Hve Hpr'' Hlocs Hlocs'' Hbr''.
  case(In_dec (option_eq_dec var_eq_dec) (Some (IV vy)) (map (ρ Σ1) lv)); intro HinD.
  (* there is a matching l in d *)
  + assert (HinD0 := HinD). apply in_map_inv in HinD; [|exact (option_eq_dec var_eq_dec)].
    destruct HinD as (l&Hl&Hin). case (L_dec l); intro HL.
    * destruct Hj2' as (s'&Hj2'&Heq').
      revert Hj1; case (dec_range Hpf Hcodf Henvf).
      -- intros (s&Hs&HEs) Hj1.
         subst Γ'. erewrite <- Γr_p_in_L with (l := l) (H := Henvf) in Hj2'; eauto; [|now left].
         eapply (snd sem_equal_compat_m) in Hj2'; eauto.
         destruct Hj2' as (s''&Hj2'&Heq'').
         erewrite desc_sup_sem_equal with (s' := s'');[| transitivity s'; eauto].
         eexists (sem_join s'' _ _); split; [econstructor|]; eauto.
         apply sem_equal_join_compat; transitivity s'; eauto.
      -- intros Hn Hj1'.
         assert(Hv2': not_sem_free (Σr Σ') (Γr (Γ1:=Γ') Henvf')
                (flat_map ref_S (map (rename_S Σf vx lx) ls)) vy).
         {
           eapply not_sem_free_rename_S; eauto; try tauto.
           apply envV_p_sem_env_in; auto.
         }
         eapply not_sem_free_sem_equal with (s' := s2) in Hv2'; eauto.
         eapply Hw0 in Hv2'.
         subst Γ'. erewrite <- Γr_p_in_L' in Hv2'; eauto.
         erewrite desc_sup_sem_equal.
         eexists (sem_join _ _ _); split; [econstructor|]; eauto.
         transitivity s2; eauto.
    (* it is not renamed *)
    * revert Hj1. unfold Γr. case dec_range.
      (* there is a renamed location in the environment *)
      -- intros (s&Hs&_) Hj1. erewrite Γr_eq in Hj2'; eauto.
        ++ destruct Hj2' as (s''&Hj2''&Heq'').
           assert(Hv2': not_sem_free (Σr Σ') (Γr (Γ1:=Γ') Henvf')
              (flat_map ref_S (map (rename_S Σf vx lx) ls)) vx).
            {
              apply not_sem_free_sem_bound.
              - erewrite Γr_eq; eauto. apply envV_p_sem_env_in; auto.
              - apply rename_bound_xy; [etransitivity; eauto|].
                rewrite (proj2 (proj2 (proj2 (ref_rename_m Σf vx lx)))).
                eapply p_sem_up_mon in Hs; eauto. destruct Hs.
                eapply Hb''; eauto.
            }
            eapply not_sem_free_sem_equal with (s' := s2) in Hv2'; eauto.
            apply Hw0 with (l:= envV Γ1 vy) (v := vx) in Hv2'.
            erewrite Γr_eq in Hv2'; [|eauto|apply envV_p_sem_env_in; auto].
            subst Γ'.
            erewrite <- p_sem_env_up_v_out in Hv2'; eauto.
            ** erewrite <- p_sem_env_up_v_in in Hv2'; [|apply in_map_iff; eauto].
               erewrite <- Σr_p_sem_env in Hv2'; eauto.
               erewrite desc_sup_sem_equal with (s' := s2) by trivial.
               eexists (sem_join s2 _ _); split; [econstructor|]; eauto.
            ** rewrite in_map_iff. intros (l0&HF&_).
               eapply False_rect, Hvx, Hincl0. eauto.
        ++ eapply envV_p_sem_env_in; eauto.
      -- intros Hn Hj1. erewrite Γr_eq in Hj2'; eauto.
        ++ destruct Hj2' as (s''&Hj2'&Heq'').
           erewrite desc_sup_sem_equal with (s' := s''); [|transitivity s''; eauto].
           subst Γ'. erewrite <- Σr_p_sem_env in Hj2'; eauto.
           eexists (sem_join s'' _ _); split; [econstructor|]; eauto.
        ++ apply envV_p_sem_env_in; auto.
  + destruct Hj2' as (s'&Hj2'&Heq').
    subst Γ'. erewrite <- Γr_p_out in Hj2'; eauto.
    erewrite desc_sup_sem_equal with (s' := s'); [|transitivity s'; eauto].
    eexists (sem_join _ _ _); split; [econstructor|]; eauto.
(* let *)
- intros [v l] e ls Hind Σ1 Γ1 Σ1' Δ1' Henvf Henv0 Hinclf Hj Hp Hwf Hlocs1 Hlocs2 Hve.
  inversion Hj; subst. simpl in *.
  rename X into Hj1. rename X0 into Hj2. assert(Hj1' := Hj1). assert(Hj2' := Hj2).
  apply deterministic_semantics_v in Hj1'; auto with *; try tauto;[|disjtac].
  destruct Hj1' as (_&Henv''&Hlocs1''&Hbr''&Hdecl1&Hincl1&_&Hp').
  assert(Hl1 : ρ Σ'' l = None)
  by (rewrite Hlocs1''; [apply Hlocs2; auto with *|tauto]).
  assert(Hl2 : ↣ Σ'' l = None) by (rewrite Hbr'';[apply Hlocs2|];tauto).
  assert(Henv' : wb_env (Γ1 [[v ↦v Some l]]) (Σ'' [[l ↦ IV v]]))
  by (apply wb_env_up_v; trivial).
  assert(Hlocs0: (Σ'' [[l ↦ IV v]]) ⊔ flat_map locs_s ls).
  {
    disjtac.
    - rewrite sem_up_sr_id, Hlocs1''; eauto.
      + apply Hlocs2; auto with *.
      + intro HF; apply Hwf in H; auto with *.
      + intro HF; subst. apply Hwf in H; auto with *.
    - rewrite br_up, Hbr''; [apply Hlocs2; auto with *|].
      intro HF; apply Hwf in H; auto with *.
  }
  assert((Γ1 [[v ↦v Some l]]) ⊓ flat_map locs_s ls)
  by (apply env_up_v_disj; auto with *; intro HF; apply Hwf in HF; auto with *).
  apply deterministic_semantics_m in Hj2'; eauto;[|tauto].
  destruct Hj2' as (_&Henvf1'&Hlocs'&Hlocs''&_&_&Hinclf'&_&_&_&_&_&Hmon0).
  assert(wb_d Σ' (Structural (l :: nil) nil))
  by (apply wb_singleton_lv; eapply sem_incl_cod; [exact Hinclf'|auto]).
  rewrite Σr_sem_join by auto.
  rewrite <- Σr_sup by eauto.
  apply P9_v with (Henvf:=Henvf) in Hj1; auto with *; try tauto;
  [|eauto|disjtac; apply Hlocs2; auto with *].
  assert(Henvf' : wb_env (Γ1 [[v ↦v Some l]]) Σf) by (eapply wb_env_incl; eauto).
  assert(Hls:= Hj2).
  eapply Hind with (Henvf:= Henvf') in Hj2; auto with *; try tauto;
  [|eauto|apply proper_up; tauto].
  assert(Hinclf'': Σ'' [[l ↦ IV v]] ⊆ Σf) by eauto.
  destruct Hj1 as (s1&Hj1&Heq1). assert(Hj1' := Hj1).
  apply deterministic_semantics_v in Hj1;
  [|now apply rename_wf_v|eapply wb_env_Γr; eauto
   |rewrite locs_rename_v; apply Γr_disj;auto with *
   |rewrite locs_rename_v; apply Σr_disj; disjtac; apply Hlocs2; auto with *].
  destruct Hj2 as (s2&Hj2&Heq2). assert(Hj2' := Hj2).
  apply deterministic_semantics_m in Hj2';
  [|eapply wb_env_Γr; eauto|now apply rename_wf_m
   |autorewrite with core; now apply Γr_disj
  | autorewrite with core; now apply Σr_disj].
  clear Hj1. destruct Hj2' as (_&_&_&_&_&_&_&_&_&_&_&Hweak&_).
  unfold rename_s. case L_dec; intro HL.
  + assert(Heq: v = vy). {
      assert (HL' := HL). apply L_values in HL.
      assert(Hl: ρ ((Σ'' [[l ↦ IV v]])) l = Some (IV v)) by trivial.
      apply Hinclf'' in Hl. rewrite (proj1 Hl) in HL; now inversion HL.
    } subst v.
    erewrite Γr_env_up_v in Hj2 by auto. fold rename_s.
    eapply sem_equal_compat_m with (Σ0 := s1 [[l ↦ IV vx]]) in Hj2;
    [|rewrite <- Σr_up_ρ_L]; eauto.
    revert Hj1'; unfold Γr; case dec_range.
    * intros (ly&Hly&HEy) Hj1'.
      destruct Hj2 as (s2'&Hj2&Heq2').
      erewrite desc_sup_sem_equal with (s' := s2'); [|transitivity s2; eauto].
      eexists (sem_join s2' _ _); split; [econstructor|]; eauto.
      rewrite env_up_v_comm, env_up_v_idem; auto.
      apply sem_equal_join_compat; transitivity s2; eauto.
    * intros Hn Hj1'. (* requires weakening *)
      assert(Hv2': not_sem_free (Σr Σ') (Γr (Γ1:=Γ1 [[vy ↦v Some l]]) Henvf')
       (flat_map ref_s (map (rename_s Σf vx lx) ls)) vy)
      by (eapply not_sem_free_rename_s; eauto; try tauto).
      eapply not_sem_free_sem_equal with (s' := s2) in Hv2'; eauto.
      eapply Hweak with (l := envV Γ1 vy) in Hv2'.
      unfold Γr in Hv2'. case dec_range in Hv2'.
      -- destruct s as (ly&_).
         rewrite envV_env_up_v_eq, envV_env_up_v_neq in Hv2'; auto.
         rewrite env_up_v_idem, env_up_v_comm,env_up_v_idem in Hv2'; auto.
         eapply sem_equal_compat_m with (Σ0 := s1 [[l ↦ IV vx]]) in Hv2';
         [|rewrite <- Σr_up_ρ_L]; eauto.
         rewrite env_up_v_id in Hv2'; auto.
         destruct Hv2' as (s'&Hv2'&Heq').
         erewrite desc_sup_sem_equal with (s' := s'); [|transitivity s2; eauto].
         eexists; split; [econstructor|]; eauto.
         apply sem_equal_join_compat. transitivity s2; eauto.
      -- eapply False_rect, n, HL. apply envV_env_up_v_eq.
    + eapply sem_equal_compat_m with (Σ0 := s1 [[l ↦ IV v]]) in Hj2;
      [|rewrite <- Σr_up_ρ_nL; auto].
      fold rename_s; revert Hj1'; unfold Γr. case dec_range.
      * intros (ly&Hvy&Hly) Hj1'. case(val_eq_dec v vy).
        -- intro; subst v. erewrite Γr_env_up_v_nL'' in Hj2; eauto.
           assert(Hv2': not_sem_free (Σr Σ') (Γr (Γ1:=Γ1 [[vy ↦v Some l]]) Henvf')
             (flat_map ref_s (map (rename_s Σf vx lx) ls)) vx). {
              apply not_sem_free_sem_bound.
              - rewrite Γr_env_up_v_nL''; auto.
              - apply rename_bound_xy;[eauto|].
                rewrite (proj1(proj2 (ref_rename_m Σf vx lx))). now eapply Hmon0.
           }
           eapply not_sem_free_sem_equal with (s' := s2) in Hv2'; eauto.
           apply Hweak with (l:= envV Γ1 vy) in Hv2'.
           erewrite Γr_eq in Hv2'; eauto.
           rewrite env_up_v_comm in Hv2'; auto.
           eapply sem_equal_compat_m with (Σ0 := (s1 [[l ↦ IV vy]])) in Hv2'; eauto;
           [|rewrite <- Σr_up_ρ_nL; auto].
           destruct Hv2' as (s2'&Hv2'&Heq2').
           erewrite desc_sup_sem_equal with (s' := s2'); [|transitivity s2; eauto].
           eexists (sem_join s2' _ _); split; [econstructor|]; eauto.
           rewrite env_up_v_idem; trivial.
           apply sem_equal_join_compat. transitivity s2; eauto.
         -- intros Hneq. erewrite Γr_env_up_v_nL' in Hj2; eauto.
           ++ destruct Hj2 as (s'&Hj2&Heq''').
              eexists (sem_join s' _ _); split;
              [|apply sem_equal_join_compat; transitivity s2; eauto].
              erewrite desc_sup_sem_equal with (s' := s'); [|transitivity s2; eauto].
              econstructor; eauto.
           ++ intro; subst v; apply False_rect, (Hvx l), Hinclf''. auto.
    * intros. rewrite Γr_env_up_v_nL in Hj2; auto.
      destruct Hj2 as (s'&Hj2&Heq').
      erewrite desc_sup_sem_equal with (s' := s'); [|transitivity s2; eauto].
      eexists (sem_join s' _ _); split; [econstructor|]; eauto.
      apply sem_equal_join_compat; transitivity s2; eauto.
- intros e ls Hind Σ1 Γ1 Σ1' Δ1' Henvf Henv0 Hinclf Hj Hp Hwf Hlocs1 Hlocs2 Hve.
  inversion Hj; subst. simpl in *.
  rename X into Hj1. rename X0 into Hj2. assert(Hj1' := Hj1). assert(Hj2' := Hj2).
  apply deterministic_semantics_v in Hj1'; auto with *; try tauto;[|disjtac].
  destruct Hj1' as (_&Henv''&Hlocs1''&Hbr''&Hdecl1&Hincl1&_&Hp').
  assert(Γ1 ⊓ flat_map locs_s ls)
  by (intros; apply Hlocs1, in_app_iff; now right).
  assert(Hlocs0: Σ'' ⊔ flat_map locs_s ls).
  {
    split; intros l0 Hl0; simpl.
    - rewrite Hlocs1''; [apply Hlocs2; auto with *|].
      intro HF; apply Hwf in Hl0; auto with *; tauto.
    - rewrite Hbr''; [apply Hlocs2; auto with *|].
      intro HF; apply Hwf in Hl0; auto with *; tauto.
  }
  apply deterministic_semantics_m in Hj2'; eauto;[| tauto].
  destruct Hj2' as (_&Henvf1'&Hlocs'&Hlocs''&_&_&Hinclf'&_&_&_&_&_&Hmon0).
  apply P9_v with (Henvf:=Henvf) in Hj1; auto with *; try tauto;
  [|eauto|disjtac; apply Hlocs2; auto with *].
  assert(Hls:= Hj2).
  eapply Hind with (Henvf:= Henvf) in Hj2; auto with *; try tauto.
  destruct Hj1 as (s&Hj1&Heq).
  destruct Hj2 as (s'&Hj2&Heq').
  eapply sem_equal_compat_m in Hj2; eauto.
  destruct Hj2 as (s''&Hj2'&Heq'').
  eexists s''; split; [econstructor|]; eauto.
  transitivity s'; eauto.
- intros [m l] e ls Hind1 Hind2 Σ1 Γ1 Σ1' Δ1' Henvf Henv0 Hinclf Hj Hp Hwf Hlocs1 Hlocs2 Hve.
  inversion Hj; subst. simpl in *.
  rename X into Hj1. rename X0 into Hj2. assert(Hj1' := Hj1). assert(Hj2' := Hj2).
  assert(Σ1 ⊔ locs_m e) by (split; intros; apply Hlocs2; auto with *).
  apply deterministic_semantics_m in Hj1'; auto with *; [|apply Hwf; auto with *].
  destruct Hj1' as (Hdet'&Henv'&Hwb'&Hlocs'&Hbr'&Hcod'&Hincl'&Hvi'&Hpr'&HE'&HD').
  assert(wb_env Γ1 (Σ'' [[l ↦ IM m]])) by
    (eapply wb_env_incl;[eauto|]; apply sem_incl_up_ρ;
     rewrite Hlocs'; [|tauto]; auto; rewrite (proj1 Hlocs2); tauto).
  assert(Hincl0: Σ1 ⊆ (Σ1 [[l ↦ IM m]]))
  by (apply sem_incl_up_ρ;apply Hlocs2; tauto).
  assert(Henv: wb_env (Γ1 [[m ↦m Δ]]) (Σ'' [[l ↦ IM m]]))
  by (eapply wb_env_incl; [| apply sem_incl_up_ρ]; [apply wb_env_up_m; auto|];
      rewrite Hlocs';[|tauto]; apply Hlocs2; tauto).
  assert((Γ1 [[m ↦m Δ]]) ⊓ flat_map locs_s ls)
   by (apply env_up_m_disj; auto with *).
  assert((Σ'' [[l ↦ IM m]]) ⊔ flat_map locs_s ls).
  { unfold disj in Hwf. apply sem_up_disj.
    - intro HF; apply Hwf in HF; auto with *.
    - disjtac; rewrite Hlocs' || rewrite Hbr'; try apply Hlocs2; auto with *;
      simpl in Hwf; intro Hf; apply Hwf in H2; tauto.
  }
  apply deterministic_semantics_m in Hj2'; auto with *; try tauto.
  destruct Hj2' as (Hdet''&Henv''&Hwb''&Hlocs''&Hbr''&Hincl''&Hvi''&Hpr''&HE''&HD'').
  assert(Hincl1: (Σ'' [[l ↦ IM m]]) ⊆ Σf) by (transitivity Σ1';[tauto|eauto]).
  assert(Hincl: (Σ'' ⊆ (Σ'' [[l ↦ IM m]])))
  by(apply sem_incl_up_ρ; rewrite Hlocs';[apply Hlocs2|]; simpl in *; tauto).
   assert(Σ'' ⊆ Σf) by (transitivity (Σ'' [[l ↦ IM m]]); intuition; eauto).
  apply Hind2 with (Henvf:=Henvf) in Hj1; try tauto; auto with *. clear Hind2.
  assert(Henvf': wb_env (Γ1 [[m ↦m Δ]]) Σf) by
    (eapply wb_env_incl; [exact Henv|intuition; eauto]).
  assert(proper (Σ'' [[l ↦ IM m]]))
  by (apply proper_up; try tauto; try rewrite Hlocs'; try rewrite Hbr'; try (apply Hlocs2); tauto).
  apply Hind1 with (Henvf:=Henvf') in Hj2;eauto; try tauto; auto with *.
  assert(Hl: ρ Σf l = Some(IM m)) by (now apply Hincl1).
  rewrite <- Σr_sup_ls; auto;[|rewrite Hl; discriminate].
  destruct Hj1 as (s&Hj1&Heq).
  destruct Hj2 as (s'&Hj2&Heq').
  eapply sem_equal_compat_m with (Σ0 := s [[l ↦ IM m]]) in Hj2; [|rewrite Σr_up_m; eauto].
  destruct Hj2 as (s''&Hj2'&Heq'').
  erewrite desc_sup_sem_equal with (s' := s''); [|transitivity s'; eauto].
  eexists s''; split; [econstructor|]; eauto.
  erewrite Γr_up_m; eauto.
  transitivity s'; eauto.
(* module type *)
- intros [t0 l] t ls Hind1 Hind2 Σ1 Γ1 Σ1' Δ1' Henvf Henv0 Hinclf Hj Hp Hwf Hlocs1 Hlocs2 Hve.
  inversion Hj; subst. simpl in *.
  rename X into Hj1. rename X0 into Hj2. assert(Hj1' := Hj1). assert(Hj2' := Hj2).
  assert(Σ1 ⊔ locs_t t) by (split; intros; apply Hlocs2; auto with *).
  apply deterministic_semantics_m in Hj1'; auto with *; [|apply Hwf; auto with *].
  destruct Hj1' as (Hdet'&Henv'&Hwb'&Hlocs'&Hbr'&Hcod'&Hincl'&Hvi'&Hpr'&HE'&HD').
  assert(wb_env Γ1 (Σ'' [[l ↦ IT t0]])) by
    (eapply wb_env_incl;[eauto|]; apply sem_incl_up_ρ;
     rewrite Hlocs'; [|tauto]; auto; rewrite (proj1 Hlocs2); tauto).
  assert(Hincl0: Σ1 ⊆ (Σ1 [[l ↦ IT t0]]))
  by (apply sem_incl_up_ρ;apply Hlocs2; tauto).
  assert(Henv: wb_env (Γ1 [[t0 ↦t Δ]]) (Σ'' [[l ↦ IT t0]]))
  by (eapply wb_env_incl; [| apply sem_incl_up_ρ]; [apply wb_env_up_t; auto|];
      rewrite Hlocs';[|tauto]; apply Hlocs2; tauto).
  assert((Γ1 [[t0 ↦t Δ]]) ⊓ flat_map locs_s ls)
   by (apply env_up_t_disj; auto with *).
  assert((Σ'' [[l ↦ IT t0]]) ⊔ flat_map locs_s ls).
  { unfold disj in Hwf. apply sem_up_disj.
    - intro HF; apply Hwf in HF; auto with *.
    - disjtac; rewrite Hlocs' || rewrite Hbr'; try apply Hlocs2; auto with *;
      simpl in Hwf; intro Hf; apply Hwf in H2; tauto.
  }
  apply deterministic_semantics_m in Hj2'; auto with *; try tauto.
  destruct Hj2' as (Hdet''&Henv''&Hwb''&Hlocs''&Hbr''&Hincl''&Hvi''&Hpr''&HE''&HD'').
  assert(Hincl1: (Σ'' [[l ↦ IT t0]]) ⊆ Σf) by (transitivity Σ1';[tauto|eauto]).
  assert(Hincl: (Σ'' ⊆ (Σ'' [[l ↦ IT t0]])))
  by(apply sem_incl_up_ρ; rewrite Hlocs';[apply Hlocs2|]; simpl in *; tauto).
   assert(Σ'' ⊆ Σf) by (transitivity (Σ'' [[l ↦ IT t0]]); intuition; eauto).
  apply Hind2 with (Henvf:=Henvf) in Hj1; try tauto; auto with *. clear Hind2.
  assert(Henvf': wb_env (Γ1 [[t0 ↦t Δ]]) Σf) by
    (eapply wb_env_incl; [exact Henv|intuition; eauto]).
  assert(proper (Σ'' [[l ↦ IT t0]]))
  by (apply proper_up; try tauto; try rewrite Hlocs'; try rewrite Hbr'; try (apply Hlocs2); tauto).
  apply Hind1 with (Henvf:=Henvf') in Hj2;eauto; try tauto; auto with *.
  assert(Hl: ρ Σf l = Some(IT t0)) by (now apply Hincl1).
  rewrite <- Σr_sup_ls; auto;[|rewrite Hl; discriminate].
  destruct Hj1 as (s&Hj1&Heq).
  destruct Hj2 as (s'&Hj2&Heq').
  eapply sem_equal_compat_m with (Σ0 := s [[l ↦ IT t0]]) in Hj2; [|rewrite Σr_up_t; eauto].
  destruct Hj2 as (s''&Hj2'&Heq'').
  erewrite desc_sup_sem_equal with (s' := s''); [|transitivity s'; eauto].
  eexists; split. econstructor; eauto.
  erewrite Γr_up_t;eauto.
  transitivity s'; eauto.
(* include *)
- intros m ls Hind1 Hind2 Σ1 Γ1 Σ1' Δ' Henvf Henv0 Hi Hj Hp Hwf Hlocs1 Hlocs2 Hve.
  inversion Hj; subst d DUd' d'; subst. simpl in *.
  rename X into Hj1. rename X0 into Hj2. assert(Hj1' := Hj1). assert(Hj2' := Hj2).
  assert(Σ1 ⊔ locs_m m) by (split; intros; apply Hlocs2; auto with *).
  apply deterministic_semantics_m in Hj1'; auto with *; [|apply Hwf; auto with *].
  destruct Hj1' as (Hdet1&Henv1&Hwb11&Hlocs'&Hbr'&H1'&Hincl'&Hpr').
  assert(Henv: wb_env Γ' Σ'') by now (apply wb_p_sem_env).
  assert(Γ' ⊓ flat_map locs_s ls)
   by (apply p_sem_env_disj; auto with *;
         split;intros l0 Hl0; rewrite Hlocs'|| rewrite Hbr'; try apply Hlocs2; auto with *;
         intro HF; apply Hwf in Hl0; auto with *).
  assert(Σ'' ⊔ flat_map locs_s ls)
  by (split;intros l0 Hl0; rewrite Hlocs'|| rewrite Hbr'; try apply Hlocs2; auto with *;
      intro HF; apply Hwf in Hl0; auto with *).
  apply deterministic_semantics_m in Hj2'; auto with *; try tauto.
  assert(Hj0 := Hj2).
  destruct Hj2' as (Hdet''&Henv''&Hwb''&Hlocs''&Hbr''&Hincl''&Hvi''&Hpr''&HE''&HD''&_&_&Hb'').
  assert(Hincl1: Σ'' ⊆ Σf) by (transitivity Σ'; eauto).
  apply Hind1 with (Henvf:=Henvf) in Hj1; try tauto; auto with *. clear Hind1.
  assert(Henvf': wb_env Γ' Σf) by
    (eapply wb_env_incl; [exact Henv|intuition; eauto]).
  apply Hind2 with (Henvf:=Henvf') in Hj2;eauto; try tauto; auto with *. clear Hind2.
  assert(Hwb1: wb_d Σ' (Structural lv ls0)) by (eapply wb_desc_incl; [|exact Hwb11]; trivial).
  rewrite <- Σr_sup_lv; auto;[|eauto].
  rewrite Σr_sem_join; auto.
  2,3: eapply wb_desc_lv; eauto.
  assert(Hj2' := Hj2). destruct Hj2 as (s2&Hj2&Heq2).
  destruct Hj1 as (s1&Hj1&Heq1).
  apply deterministic_semantics_m in Hj2;
  [|eapply wb_env_Γr; eauto| now apply rename_wf_m|
    apply Γr_disj, p_sem_env_disj; autorewrite with core; auto with * |
    apply Σr_disj; autorewrite with core; auto].
  destruct Hj2 as (_&_&_&_&_&_&_&_&_ &_&_&Hw0&Hb0).
  clear Hpr' Hve Hpr'' Hlocs' Hlocs'' Hbr' Hbr''.
  case(In_dec (option_eq_dec var_eq_dec) (Some (IV vy)) (map (ρ Σ'') lv)); intro HinD.
  (* there is a matching l in d *)
  + assert (HinD0 := HinD). apply in_map_inv in HinD; [|exact (option_eq_dec var_eq_dec)].
    destruct HinD as (l&Hl&Hin). case (L_dec l); intro HL.
    * destruct Hj2' as (s'&Hj2'&Heq').
      revert Hj1; case (dec_range Hpf Hcodf Henvf).
      -- intros (s&Hs&HEs) Hj1.
         subst Γ'. erewrite <- Γr_p_in_L with (l := l) in Hj2'; eauto; [|now left].
         eapply sem_equal_compat_m with (Σ0 := s1) in Hj2'; eauto.
         destruct Hj2' as (s''&Hj2'&Heq'').
         erewrite desc_sup_sem_equal with (s' := s'');[| transitivity s'; eauto].
         erewrite p_sem_env_sem_equal with (s' := s1) in Hj2'; eauto.
         eexists (sem_join s'' _ _); split; [econstructor|]; eauto.
         apply sem_equal_join_compat; transitivity s'; eauto.
      -- intros Hn Hj1'.
         assert(Hv2': not_sem_free (Σr Σ') (Γr (Γ1:=Γ') Henvf')
                (flat_map ref_s (map (rename_s Σf vx lx) ls)) vy).
         {
           eapply not_sem_free_rename_s; eauto; try tauto.
           apply envV_p_sem_env_in; auto.
         }
         eapply not_sem_free_sem_equal with (s' := s2) in Hv2'; eauto.
         eapply Hw0 in Hv2'.
         subst Γ'. erewrite <- Γr_p_in_L' in Hv2'; eauto.
         eapply sem_equal_compat_m with (Σ0 := s1) in Hv2'; eauto.
         destruct Hv2' as (s2'&Hj2&Heq2').
         erewrite desc_sup_sem_equal.
         erewrite p_sem_env_sem_equal in Hj2.
         eexists (sem_join _ _ _); split; [econstructor|]; eauto.
         apply sem_equal_join_compat; transitivity s2; eauto.
         etransitivity; eauto.
         transitivity s2; eauto.
    (* it is not renamed *)
    * revert Hj1. unfold Γr. case dec_range.
      (* there is a renamed location in the environment *)
      -- intros (s&Hs&_) Hj1. erewrite Γr_eq in Hj2'; eauto.
        ++ destruct Hj2' as (s''&Hj2''&Heq'').
           assert(Hv2': not_sem_free (Σr Σ') (Γr (Γ1:=Γ') Henvf')
              (flat_map ref_s (map (rename_s Σf vx lx) ls)) vx).
            {
              apply not_sem_free_sem_bound.
              - erewrite Γr_eq; eauto. apply envV_p_sem_env_in; auto.
              - apply rename_bound_xy; [etransitivity; eauto|].
                rewrite (proj1(proj2 (ref_rename_m Σf vx lx))).
                eapply p_sem_up_mon in Hs; eauto. destruct Hs.
                eapply Hb''; eauto.
            }
            eapply not_sem_free_sem_equal with (s' := s2) in Hv2'; eauto.
            apply Hw0 with (l:= envV Γ1 vy) (v := vx) in Hv2'.
            erewrite Γr_eq in Hv2'; [|eauto|apply envV_p_sem_env_in; auto].
            subst Γ'.
            erewrite <- p_sem_env_up_v_out in Hv2'; eauto.
            ** erewrite <- p_sem_env_up_v_in in Hv2'; [|apply in_map_iff; eauto].
               erewrite <- Σr_p_sem_env in Hv2'; eauto.
               eapply sem_equal_compat_m with (Σ0 := s1) in Hv2'; eauto.
              destruct Hv2' as (s'&Hv2'&Heq').
               erewrite p_sem_env_sem_equal with (s' := s1) in Hv2'; [|etransitivity; eauto].
               erewrite desc_sup_sem_equal.
               eexists (sem_join s' _ _); split; [econstructor|]; eauto.
               apply sem_equal_join_compat; transitivity s2; eauto.
               transitivity s2; eauto.
            ** rewrite in_map_iff. intros (l0&HF&_).
               eapply False_rect, Hvx, Hincl1. eauto.
        ++ eapply envV_p_sem_env_in; eauto.
      -- intros Hn Hj1. erewrite Γr_eq in Hj2'; eauto.
        ++ destruct Hj2' as (s''&Hj2'&Heq'').
           eapply sem_equal_compat_m with (Σ0 := s1) in Hj2'; eauto.
           destruct Hj2' as (s'&Hj2'&Heq').
           erewrite desc_sup_sem_equal with (s' := s'); [|transitivity s''; eauto].
           subst Γ'. erewrite <- Σr_p_sem_env in Hj2'; eauto.
           erewrite <- p_sem_env_sem_equal in Hj2'; eauto.
           eexists (sem_join s' _ _); split; [econstructor|]; eauto.
           apply sem_equal_join_compat; transitivity s''; eauto.
        ++ apply envV_p_sem_env_in; auto.
  + destruct Hj2' as (s'&Hj2'&Heq').
    subst Γ'. erewrite <- Γr_p_out in Hj2'; eauto.
    eapply sem_equal_compat_m with (Σ0 := s1) in Hj2'; eauto.
    destruct Hj2' as (s''&Hj2'&Heq'').
    erewrite desc_sup_sem_equal with (s' := s''); [|transitivity s'; eauto].
    eexists (sem_join s'' _ _); split; [econstructor|]; eauto.
    -- erewrite <- p_sem_env_sem_equal in Hj2'; eauto.
    -- apply sem_equal_join_compat; transitivity s'; eauto.
- intros m Σ1 Γ1 Σ1' Δ1' Henvf Henv0 Hinclf Hj Hp Hwf Hlocs1 Hlocs2 Hve.
  simpl. inversion Hj. subst. apply P9_m_path with (Henvf:= Henvf) in X; trivial.
  eexists; split; [econstructor|]; eauto.
- intros [m l] t1 e Hind1 Hind2 Σ1 Γ1 Σ1' Δ1' Henvf Henv0 Hinclf Hj Hp Hwf Hlocs1 Hlocs2 Hve.
  inversion Hj; subst. simpl in *.
  rename X into Hj1. rename X0 into Hj2. assert(Hj1' := Hj1). assert(Hj2' := Hj2).
  assert(Σ1 ⊔ locs_t t1) by (split; intros; apply Hlocs2; auto with *).
  simpl in *. apply deterministic_semantics_m in Hj1'; auto with *; [|apply Hwf; auto with *].
  destruct Hj1' as (Hdet'&Henv'&Hwb'&Hlocs'&Hbr'&Hcod'&Hincl'&Hvi'&Hpr'&HE'&HD').
  apply deterministic_semantics_m in Hj2'; auto with *; try tauto.
  + assert(Hinclf': (Σ'' [[l ↦ IM m]]) ⊆ Σf)
    by (transitivity Σ1'; tauto).
    assert(Hincl''': Σ'' ⊆ Σf).
    {
      transitivity Σ1';trivial. transitivity (Σ'' [[l ↦ IM m]]); [|tauto].
      apply sem_incl_up_ρ. rewrite Hlocs'; try tauto. apply Hlocs2; tauto.
    }
    apply Hind1 with (Henvf:=Henvf) in Hj1; auto with *; try tauto. clear Hind1.
    destruct Hj2' as (_&Henv''&_&_&_&_&Hincl''&_).
    pose(Henvf' := wb_env_incl Henv'' Hinclf).
    apply Hind2 with (Henvf:=Henvf') in Hj2; auto with *; try tauto.
    * destruct Hj1 as (s&Hj1&Heq1). destruct Hj2 as (s2&Hj2&Heq2).
      eapply sem_equal_compat_m with (Σ0 := s [[l ↦ IM m]]) in Hj2; [|rewrite Σr_up_m; eauto].
      destruct Hj2 as (s2'&Hj2&Heq2').
      eexists s2'; split; [econstructor|transitivity s2; eauto]; eauto.
      erewrite Γr_up_m. exact Hj2.
    * eapply wb_env_incl; [| apply sem_incl_up_ρ]; [apply wb_env_up_m; auto|].
      rewrite Hlocs'; [|tauto]. auto. rewrite (proj1 Hlocs2); tauto.
    * apply proper_up; try tauto; try rewrite Hlocs'; try rewrite Hbr';
      try (apply Hlocs2); tauto.
    * apply env_up_m_disj; auto with *.
    * apply sem_up_disj; [ tauto |].
      split; intros l0 Hl0; try rewrite Hlocs'.
      -- rewrite (proj1 Hlocs2); auto with *.
      -- intro Hf; apply Hwf in Hf; tauto.
      -- rewrite Hbr'. apply Hlocs2; auto with *.
         intro HF; apply Hwf in HF; tauto.
  + eapply wb_env_incl; [| apply sem_incl_up_ρ]; [apply wb_env_up_m; auto|].
    rewrite Hlocs'; [|tauto]. auto. rewrite (proj1 Hlocs2); tauto.
  + apply env_up_m_disj; auto with *.
  + apply sem_up_disj; [ tauto |split; intros l0 Hl0; try rewrite Hlocs'].
    * rewrite (proj1 Hlocs2); auto with *.
    * intro Hf; apply Hwf in Hf; tauto.
    * rewrite Hbr'. apply Hlocs2; auto with *. intro HF; apply Hwf in HF; tauto.
- intros m1 Hind1 m2 Hind2 Σ1 Γ1 Σ1' Δ1' Henvf Henv0 Hinclf Hj Hp Hwf Hlocs1 Hlocs2 Hve.
  simpl in *. inversion Hj; subst.
  rename X into Hj1. rename X0 into Hj2. assert(Hj1' := Hj1). assert(Hj2' := Hj2).
  apply deterministic_semantics_m in Hj1'; auto with *; [|tauto|disjtac].
  destruct Hj1' as (Hdet'&Henv'&Hwb'&Hlocs'&Hbr'&Hcod'&Hincl'&Hvi'&Hpr'&HE'&HD').
  apply deterministic_semantics_m in Hj2'; auto with *; try tauto;[|disjtac];
  try ((rewrite Hlocs' || rewrite Hbr');[apply Hlocs2; auto with *|intro HF; apply Hwf in HF; tauto]).
  assert(Σ'' ⊆ Σ') by tauto.
  apply Hind1 with (Henvf:= Henvf) in Hj1; auto with *; try tauto; eauto.
  + apply Hind2 with (Henvf:= Henvf) in Hj2; auto with *; try tauto; eauto.
    * rewrite Σr_sem_join; try tauto; [|inversion Hwb'; subst; eauto].
      destruct Hj1 as (s&Hj1&Heq1). destruct Hj2 as (s2&Hj2&Heq2).
      eapply sem_equal_compat_m with (Σ0 := s) in Hj2; eauto.
      destruct Hj2 as (s2'&Hj2&Heq2').
      eexists (sem_join s2' _ _); split; [econstructor|]; eauto.
      apply sem_equal_join_compat. transitivity s2; eauto.
    * disjtac; ((rewrite Hlocs' || rewrite Hbr');[apply Hlocs2; auto with *|intro HF; apply Hwf in HF; tauto]).
  + disjtac.
- intros m t Hind1 Hind2 Σ1 Γ1 Σ1' Δ1' Henvf Henv0 Hinclf Hj Hp Hwf Hlocs1 Hlocs2 Hve.
  simpl in *. inversion Hj; subst.
  rename X into Hj1. rename X0 into Hj2. assert(Hj1' := Hj1). assert(Hj2' := Hj2).
  apply deterministic_semantics_m in Hj1'; auto with *; [|tauto|disjtac].
  destruct Hj1' as (Hdet'&Henv'&Hwb'&Hlocs'&Hbr'&Hcod'&Hincl'&Hvi'&Hpr'&HE'&HD').
  apply deterministic_semantics_m in Hj2'; auto with *; try tauto;[|disjtac];
  try ((rewrite Hlocs' || rewrite Hbr');[apply Hlocs2; auto with *|intro HF; apply Hwf in HF; tauto]).
  assert(Σ'' ⊆ Σ') by tauto.
  apply Hind1 with (Henvf:= Henvf) in Hj1; auto with *; try tauto; eauto. clear Hind1.
  + apply Hind2 with (Henvf:= Henvf) in Hj2; auto with *; try tauto; eauto. clear Hind2.
   * rewrite Σr_sem_join; try tauto;[|eauto].
     rewrite <- Σr_d_mod; try tauto.
     -- destruct Hj2 as (s2&Hj2&Heq2). destruct Hj1 as (s1&Hj1&Heq1).
        eapply sem_equal_compat_m with (Σ0 := s1) in Hj2; eauto.
        destruct Hj2 as (s2'&Hj2&Heq2').
        erewrite desc_mod_sem_equal with (s' := s2'); [|transitivity s2; eauto].
        eexists (sem_join s2' _ _); split; [ econstructor|]; eauto.
        apply sem_equal_join_compat; transitivity s2; eauto.
     -- now apply join_equiv.
     -- eauto.
     -- eauto.
   * disjtac; ((rewrite Hlocs' || rewrite Hbr');[apply Hlocs2; auto with *|intro HF; apply Hwf in HF; tauto]).
  + disjtac.
- intros ls Hind Σ1 Γ1 Σ1' Δ1' Henvf Henv0 Hinclf Hj Hp Hwf Hlocs1 Hlocs2 Hve.
  simpl in *. inversion Hj; subst.
  edestruct Hind as (s2&Hs2&Heq2); eauto. eexists; split; [econstructor|]; eauto.
- intros Σ1 Γ1 Σ1' Δ1' Henvf Henv0 Hinclf Hj Hp Hwf Hlocs1 Hlocs2 Hve.
  simpl in *. inversion Hj; subst. eauto.
- intros ls m Hind1 Σ1 Γ1 Σ1' Δ' Henvf Henv0 Hi Hj Hp Hwf Hlocs1 Hlocs2 Hve.
  inversion Hj; subst d DUd' d'; subst. simpl in *.
  rename X into Hj1. rename X0 into Hj2. assert(Hj1' := Hj1). assert(Hj2' := Hj2).
  apply deterministic_semantics_m_path in Hj1'; auto.
  destruct Hj1' as (Hdet1&_&Hwb11&Hlocs'&_).
  assert(Henv: wb_env Γ' Σ1) by now (apply wb_p_sem_env).
  assert(Γ' ⊓ flat_map locs_s ls)
   by (apply p_sem_env_disj; auto with *;
         split;intros l0 Hl0; rewrite Hlocs'|| rewrite Hbr'; try apply Hlocs2; auto with *;
         intro HF; apply Hwf in Hl0; auto with *).
  apply deterministic_semantics_m in Hj2'; auto with *; try tauto.
  assert(Hj0 := Hj2).
  destruct Hj2' as (Hdet''&Henv''&Hwb''&Hlocs''&Hbr''&Hincl''&Hvi''&Hpr''&HE''&HD''&_&_&Hb'').
  assert(Hincl1: Σ1 ⊆ Σf) by (transitivity Σ'; eauto).
  assert(Henvf': wb_env Γ' Σf) by
    (eapply wb_env_incl; [exact Henv|intuition; eauto]).
  apply Hind1 with (Henvf:=Henvf') in Hj2;eauto; try tauto; auto with *. clear Hind1.
  assert(Hwb1: wb_d Σ' (Structural lv ls0)) by (eapply wb_desc_incl; [|exact Hwb11]; trivial).
  rewrite <- Σr_sup_lv; auto;[|eauto].
  rewrite Σr_sem_join; auto.
  2,3: eapply wb_desc_lv; eauto.
  eapply P9_m_path with (Henvf:= Henvf) in Hj1; eauto.
  assert(Hj2' := Hj2). destruct Hj2 as (s2&Hj2&Heq2).
  apply deterministic_semantics_m in Hj2;
  [|eapply wb_env_Γr; eauto| now apply rename_wf_m|
    autorewrite with core; now apply Γr_disj
   | autorewrite with core; now apply Σr_disj].
  destruct Hj2 as (_&_&_&_&_&_&_&_&_ &_&_&Hw0&Hb0).
  clear Hve Hpr'' Hlocs' Hlocs'' Hbr''.
  case(In_dec (option_eq_dec var_eq_dec) (Some (IV vy)) (map (ρ Σ1) lv)); intro HinD.
  (* there is a matching l in d *)
  + assert (HinD0 := HinD). apply in_map_inv in HinD; [|exact (option_eq_dec var_eq_dec)].
    destruct HinD as (l&Hl&Hin). case (L_dec l); intro HL.
    * destruct Hj2' as (s'&Hj2'&Heq').
      revert Hj1; case (dec_range Hpf Hcodf Henvf).
      -- intros (s&Hs&HEs) Hj1.
         subst Γ'. erewrite <- Γr_p_in_L with (l := l) in Hj2'; eauto; [|now left].
         erewrite desc_sup_sem_equal with (s' := s');[| transitivity s'; eauto].
         eexists; split; [econstructor|]; eauto.
      -- intros Hn Hj1'.
         assert(Hv2': not_sem_free (Σr Σ') (Γr (Γ1:=Γ') Henvf')
                (flat_map ref_s (map (rename_s Σf vx lx) ls)) vy).
         {
           eapply not_sem_free_rename_s; eauto; try tauto.
           apply envV_p_sem_env_in; auto.
         }
         eapply not_sem_free_sem_equal with (s' := s2) in Hv2'; eauto.
         eapply Hw0 in Hv2'.
         subst Γ'. erewrite <- Γr_p_in_L' in Hv2'; eauto.
         erewrite desc_sup_sem_equal.
         eexists (sem_join s2 _ _); split; [econstructor|]; eauto.
         etransitivity; eauto.
    * revert Hj1. unfold Γr. case dec_range.
      -- intros (s&Hs&_) Hj1. erewrite Γr_eq in Hj2'; eauto.
        ++ destruct Hj2' as (s''&Hj2''&Heq'').
           assert(Hv2': not_sem_free (Σr Σ') (Γr (Γ1:=Γ') Henvf')
              (flat_map ref_s (map (rename_s Σf vx lx) ls)) vx).
            {
              apply not_sem_free_sem_bound.
              - erewrite Γr_eq; eauto. apply envV_p_sem_env_in; auto.
              - apply rename_bound_xy; [etransitivity; eauto|].
                rewrite (proj1 (proj2 (ref_rename_m Σf vx lx))).
                eapply p_sem_up_mon in Hs; eauto. destruct Hs.
                eapply Hb''; eauto.
            }
            eapply not_sem_free_sem_equal with (s' := s2) in Hv2'; eauto.
            apply Hw0 with (l:= envV Γ1 vy) (v := vx) in Hv2'.
            erewrite Γr_eq in Hv2'; [|eauto|apply envV_p_sem_env_in; auto].
            subst Γ'.
            erewrite <- p_sem_env_up_v_out in Hv2'; eauto.
            ** erewrite <- p_sem_env_up_v_in in Hv2'; [|apply in_map_iff; eauto].
               erewrite <- Σr_p_sem_env in Hv2'; eauto.
               erewrite desc_sup_sem_equal; [eexists; split; [econstructor|]|]; eauto.
            ** rewrite in_map_iff. intros (l0&HF&_).
               eapply False_rect, Hvx, Hincl1. eauto.
        ++ eapply envV_p_sem_env_in; eauto.
      -- intros Hn Hj1. erewrite Γr_eq in Hj2'; eauto.
        ++ destruct Hj2' as (s''&Hj2'&Heq'').
           erewrite desc_sup_sem_equal with (s' := s''); [|eauto].
           subst Γ'. erewrite <- Σr_p_sem_env in Hj2'; eauto.
           eexists; split; [econstructor|]; eauto.
        ++ apply envV_p_sem_env_in; auto.
  + destruct Hj2' as (s'&Hj2'&Heq').
    subst Γ'. erewrite <- Γr_p_out in Hj2'; eauto.
    erewrite desc_sup_sem_equal with (s' := s'); eauto.
    eexists; split; [econstructor|]; eauto.
Qed.
(* end hide *)

(** Renaming of vy (at location lx) with vx is valid *)
(* strengthening: Σf = Σ1' *)
Proposition P9 Σ1 Γ1 P (Σ1': 〚P〛Σ1;Γ1) (Henvf : wb_env Γ1 Σf):
 (Σ1' ⊆ Σf) ->
 proper Σ1 ->
 (forall l, In l (ve_locs P) -> ~ dom_rel (E Σf) l) ->
 let Σ2 := Σr Σ1 in
 let P' := rename Σf vx lx P in
 let Γ2 := Γr Henvf in
  {Σ2' & (Σ2 / Γ2 ⊢ P' ⇝ Σ2') ∧ sem_equal Σ2' (Σr Σ1')}.
Proof.
destruct Σ1' as (Σ1' & HP & Henv & Hwf & Hlocs1 & Hlocs2). simpl in *.
revert P Σ1 Σ1' Γ1 Henv Hwf Hlocs1 Hlocs2 HP Henvf.
simple refine (prog_ind2 _ _ _).
- intros Σ Σ' Γ v Hv Hwb Hwf HΓlocs HΣlocs Henvf Hi Hp HE0; simpl in *.
  destruct (deterministic_semantics_v Hwf Hwb HΓlocs HΣlocs Hv)
    as (Hdet'&Hwb'&Hlocs'&Hbr'&Hcod'&Hi'&Hvi'&Hpr'&HE'&_).
  eapply P9_v  with (Henvf:= Henvf) in Hv; auto. destruct Hv as (s&Hs&Heq). eauto.
- intros Σ Σ' Σ'' Γ x l m P Δ Hwb Hwf HΓlocs HΣlocs Hlocs1' Hlocs2' Henv' Hm HP Hind Henvf Hincl Hp HE0.
  simpl in *.
  destruct (fst(fst(fst deterministic_semantics_m)) _ _ _ _ _ Hm) as
      (_&Hwb''&HΔ''&Hlocs''&Hbr''&Hcod''&Hi''&Hvi''&Hpr''&HE''&HD'');
  [trivial|tauto|auto with *|disjtac; apply HΣlocs; auto with *|].
  assert(Hdet2 := HP). apply deterministic_semantics in Hdet2; try tauto.
  destruct Hdet2 as (_&Hwb'&Hlocs'&Hbr'&Hcod'&Hincl'&Hvi'&Hpr'&HE').
  assert(Hincl0 : Σ'' ⊆ Σf).
  {
    etransitivity; [|apply Hincl].
    etransitivity; [|apply Hincl'].
    apply sem_incl_up_ρ. rewrite Hlocs''.
    - apply HΣlocs. tauto.
    - tauto.
  }
  eapply P9_m in Hm; eauto;
  [|tauto|auto with *|disjtac;apply HΣlocs; auto with *|intros; apply HE0; auto with *].
  assert(Hl': ρ Σf l = Some (IM x)) by now apply Hincl, Hincl'.
  assert(Henvf': wb_env (Γ [[x ↦m Δ]]) Σf) by eauto.
  destruct Hm as (s&Hm&Heqm).
  destruct Hind with (Henvf := Henvf') as (s'&H'&Heq'); auto;
  [|intros; apply HE0; auto with *|].
  + apply proper_up.
    * rewrite Hlocs''; [|tauto]. apply HΣlocs; tauto.
    * rewrite Hbr''; [|tauto]. apply HΣlocs; tauto.
    * tauto.
  + eapply sem_equal_compat with (Σ0 := s [[l ↦ IM x]]) in H';
    [|rewrite <- Σr_up_ρ_nL; [|eapply L_not_m] ]; eauto.
    destruct H' as (s''&H'&Heq'').
      erewrite <- Γr_up_m with (H' := Henvf') in H'.
    eexists; split; [econstructor|]; eauto.
    transitivity s'; eauto.
Qed.

(* begin hide *)
Hint Resolve LocMapsFacts.empty_o : core.

Lemma Σr_Σ₀ : sem_equal (Σr Σ₀) Σ₀.
Proof.
apply Σ₀_eq. unfold Σr. repeat split; trivial.
unfold sr'. simpl. intro l. rewrite LocMapsFacts.mapi_o; [|intros; subst; trivial].
now rewrite LocMapsFacts.empty_o.
Qed.

Lemma Γr_Γ₀ : Γr (wb_env_Γ₀ _) = Γ₀.
Proof. unfold Γ₀, Γr; trivial. Qed.

Lemma dom_rel₀ l: ~ dom_rel (E Σ₀) l.
Proof. intro H; inversion H. tauto. Qed.
(* end hide *)

End Correctness.

(** Abstracting location from value identifier definition *)
Definition value_identifier' v P := exists l, value_identifier (v, l) P.

(* begin hide *)
Lemma L3 P (Σf': 〚P〛Σ₀;Γ₀) vx lx:
  let Σf := get_sem Σf' in
    In lx (decl P) ->
    ~ value_identifier' vx P ->
   let Σ2 := Σr Σf vx lx Σ₀ in
   let P' := rename Σf vx lx P in
    {Σ2' & (Σ2 / Γ₀ ⊢ P' ⇝ Σ2') ∧ sem_equal Σ2' (Σr Σf vx lx Σf')}.
Proof.
intros Σf Hdecl Hid P'.
assert(Hcod: cod Σf' lx) by
  (destruct Σf' as (Σ & H1 & Hc1); simpl in *;
   eapply deterministic_semantics; eauto; intuition).
assert(Hvx: forall l : ℒ, ρ Σf l <> Some (IV vx)).
{
  intros l Hl. apply Hid. exists l.
  destruct Σf' as (Σf' & H1 & Hc1); simpl in *; subst;
  eapply deterministic_semantics; eauto. intuition.
}
assert(Hp: proper Σf') by (eapply sem_proper). simpl.
destruct (P9 Hcod Hp Hvx Σf' (wb_env_Γ₀ Σf)) as (s&H'&Heq'); eauto.
intros l Hl Hl'.
destruct Σf' as (Σf' & H1 & Hc1); simpl in *.
eapply deterministic_semantics in H1; eauto; [|intuition].
apply H1 in Hl';[|apply dom_rel₀].
destruct Hl' as [Hl'|Hl'].
- eapply disj_ve_val_locs; eauto. intuition.
- destruct Hl' as [(x&Hl')|(x&Hl')]; inversion Hl'.
Qed.
(* end hide *)


(** Main correctness result (_Proposition 8_) *)
Proposition P5 P (Σf: 〚P〛Σ₀;Γ₀) vx lx:
    In lx (decl P) ->
    ~ value_identifier' vx P ->
       valid P (rename Σf vx lx P).
Proof.
intros Hdecl Hid.
destruct (L3 Σf lx Hdecl Hid) as (s&H'&Heq').
apply valid_sem_eq. exists Σf.
apply sem_equal_compat with (Σ0 := Σ₀) in H'; eauto; [|apply Σr_Σ₀].
destruct H' as (s'&H'&Heq'').
eexists (existT _ s' _); eauto.
simpl.
transitivity s; eauto. transitivity (Σr Σf vx lx Σf);[|apply sem_equal_eq; auto].
symmetry. apply Σr_eq; auto.
destruct Σf as (Σf & H1 & Hc1); simpl in *;
eapply deterministic_semantics; eauto; intuition.
Unshelve. simpl.
destruct Σf as (Σf & H1 & Hc). simpl in *.
intuition.
- now apply rename_wf.
- discriminate.
- unfold Σ₀, sr'. simpl. apply LocMapsFacts.empty_o.
Qed.

(** The minimal renaming is indeed a renaming *)
Module Import RO := Renaming L SE.ML.
(* begin hide *)
Lemma is_renaming_rename_v s vx lx v: is_renaming_v v (rename_v s vx lx v).
Proof.
induction v; try destruct v; try destruct v1; simpl in *; intuition.
Qed.
Local Hint Resolve is_renaming_rename_v : core.

Lemma is_renaming_rename_m s vx lx:
  (forall m, is_renaming_m m (rename_m s vx lx m)) *
  (forall ls, is_renaming_list_s ls (map (rename_s s vx lx) ls)) *
  (forall t, is_renaming_t t (rename_t s vx lx t)) *
  (forall ls, is_renaming_list_S ls (map (rename_S s vx lx) ls)).
Proof.
apply module_ind; intros; simpl in *; intuition; try constructor;
try destruct v; simpl; intuition; unfold rename_S, rename_s;
case L_dec; intuition.
Qed.
(* end hide *)

Lemma is_renaming_rename s vx lx P: is_renaming P (rename s vx lx P).
Proof.
induction P;  simpl in *; intuition.
now apply is_renaming_rename_m.
Qed.

End Correctness.
