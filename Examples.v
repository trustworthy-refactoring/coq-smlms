Require AbstractRenaming.Modules AbstractRenaming.Default_renaming.
Require Peano_dec BinaryString OrderedType.
Require Import List String ZArith.
Import ListNotations.

(** Define the parameters of a basic language *)
Module Import TestL <: Modules.LanguageSig.
  Definition modules := string.
  Definition types := string.
  Definition variables := string.
  Definition constants := string.
  Definition loc := Z.
  Definition loc_eq_dec := Z.eq_dec.
  Definition mod_eq_dec := string_dec.
  Definition typ_eq_dec := string_dec.
  Definition val_eq_dec := string_dec.
  Notation M := modules.
  Notation T := types.
  Notation V := variables.
  Notation C := constants.
  Notation ℒ := loc.

  Definition print_module x : string := x.
  Definition print_type x : string := x.
  Definition print_variable x : string := x.
  Definition print_loc x : string := BinaryString.of_Z x.
  Definition print_constant x : string := x.

  Definition lt := Z.lt.
  Definition lt_trans := Z.lt_trans.

  Lemma lt_not_eq : forall x y : ℒ, lt x y -> ~ eq x y.
  Proof.
    intros x y Hlt Heq. subst. inversion Hlt as [Hlt'].
    rewrite Z.compare_refl in Hlt'. discriminate.
  Qed.
  Definition compare := OrderedTypeEx.Z_as_OT.compare.
End TestL.

Module Import SM := Semantics.Semantics TestL.
Module Import SE := Semantics.Equivalence TestL SM.
Import Notations.

Open Scope module_scope.
Open Scope Z_scope.

(** * Utilities *)

(** Utility function generating unique locations *)
(* begin hide *)
Fixpoint gen_locs_m_path (m0 : m_path) (n: ℒ) : (m_path * ℒ) :=
match m0 with
| M_id (m', _) => (M_id (m', n), Z.succ n)
| M_comp p (m', l) => let (p', n') := gen_locs_m_path p (Z.succ n) in (M_comp p' (m', n), n')
end.

Fixpoint gen_locs_em_path (m0 : em_path) n :=
match m0 with
| EM_id (x0, l) => (EM_id (x0, n), Z.succ n)
| EM_comp p (x0, l) => let (p', n') := gen_locs_em_path p (Z.succ n) in
                         (EM_comp p' (x0, n), Z.succ n')
| EM_app p1 p2 => let (p1', n1) := gen_locs_em_path p1 n in
                  let (p2', n2) := gen_locs_em_path p1 n1 in
                    (EM_app p1' p2', n2)
end.

Fixpoint gen_locs_v (v0 : v_exp) n :=
match v0 with
| V_id (v0, l) => (V_id (v0, n), Z.succ n)
| V_field p (v0, l) => let (p', n') := gen_locs_m_path p (Z.succ n) in
                         (V_field p' (v0, n'), Z.succ n')
| V_const c0 => (V_const c0, n)
| V_let (v0, l) v1 v2 => let (v1', n1) := gen_locs_v v1 (Z.succ n) in
                         let (v2', n2) := gen_locs_v v2 n1 in
                           (V_let (v0, n) v1' v2', n2)
| V_fun (v0, l) v => let (v', n') := gen_locs_v v (Z.succ n) in
                        (V_fun (v0, n) v', n')
| V_app v1 v2 => let (v1', n1) := gen_locs_v v1 n in
                 let (v2', n2) := gen_locs_v v2 n1 in
                   (V_app v1' v2', n2)
| V_open m v => let (v', n1) := gen_locs_v v n in (V_open m v', n1)
end.

Fixpoint gen_locs_t t n:=
match t with
| MT_id (t0, l) => (MT_id (t0, n), Z.succ n)
| MT_field p (t0, l) => let (p', n') := gen_locs_em_path p (Z.succ n) in
                          (MT_field p' (t0, n), n')
| MT_sig l => let (l', n') :=
    (fix gen_locs_lS l : (ℒ -> (list sig_comp * ℒ)) := fun n => match l with
                         | s :: l => let (l', n') := gen_locs_lS l n in
                                     let (s', n'') := gen_locs_S s n' in
                                       (s' :: l', n'')
                         | nil => (nil, n)
                         end) l n in (MT_sig l', n')
| MT_functor (t0, l) t1 t2 => let (v1', n1) := gen_locs_t t1 (Z.succ n) in
                              let (v2', n2) := gen_locs_t t2 n1 in
                                (MT_functor (t0, n) v1' v2', n2)
| MT_constr t (t0, l) p => let (t', n1) := gen_locs_t t (Z.succ n) in
                           let (p', n2) := gen_locs_em_path p n1 in
                             (MT_constr t' (t0, n) p', n2)
| MT_dsubst t (t0, l) p => let (t', n1) := gen_locs_t t (Z.succ n) in
                           let (p', n2) := gen_locs_em_path p n1 in
                             (MT_dsubst t' (t0, n) p', n2)
| MT_extr m => let (m', n1) := gen_locs_m m n in (MT_extr m', n1)
end
with gen_locs_S (S0 : sig_comp) n {struct S0} : (sig_comp * ℒ) :=
match S0 with
| Sig_val (v0, l) => (Sig_val (v0, n), Z.succ n)
| Sig_m (m0, l) t => let (t', n'') := gen_locs_t t (Z.succ n) in
                             (Sig_m (m0, n) t', n'')
| Sig_alias (m0, l) p => let (p', n'') := gen_locs_m_path p (Z.succ n) in
                                 (Sig_alias (m0, n) p', n'')
| Sig_abs (t0, l) => (Sig_abs (t0, n), Z.succ n)
| Sig_concr (t0, l) t => let (t', n'') := gen_locs_t t (Z.succ n) in
                                 (Sig_concr (t0, n) t', n'')
| Sig_incl t => let (t', n') := gen_locs_t t n in (Sig_incl t', n')
| Sig_open p => let (p', n') := gen_locs_m_path p n in (Sig_open p', n')
end
with gen_locs_m m0 n : (m_exp * ℒ) :=
match m0 with
| M_path m0 => let (m', n') := gen_locs_m_path m0 n in (M_path m', n')
| M_struct l => let (l', n') :=
    (fix gen_locs_ls l : (ℒ -> (list str_comp * ℒ)) := fun n => match l with
                         | s :: l => let (l', n') := gen_locs_ls l n in
                                     let (s', n'') := gen_locs_s s n' in
                                       (s' :: l', n'')
                         | nil => (nil, n)
                         end) l n in (M_struct l', n')
| M_functor (x0, l) t0 m0 => let (t', n1) := gen_locs_t t0 (Z.succ n) in
                              let (m', n2) := gen_locs_m m0 n1 in
                                (M_functor (x0, n) t' m', n2)
| M_app m1 m2 => let (v1', n1) := gen_locs_m m1 n in
                 let (v2', n2) := gen_locs_m m2 n1 in
                   (M_app v1' v2', n2)
| M_tannot m0 t0 => let (m', n1) := gen_locs_m m0 n in
                 let (t', n2) := gen_locs_t t0 n1 in
                   (M_tannot m' t', n2)
end
with gen_locs_s s n : (str_comp * ℒ) :=
match s with
| Str_let (v0, l) v => let (v', n') := gen_locs_v v (Z.succ n) in
                           (Str_let (v0, n) v', n')
| Str_let_ v => let (v', n') := gen_locs_v v n in
                  (Str_let_ v', n')
| Str_mdef (x0, l) m0 => let (m', n') := gen_locs_m m0 (Z.succ n) in
                           (Str_mdef (x0, n) m', n')
| Str_mtdef (t0, l) t => let (t', n') := gen_locs_t t (Z.succ n) in
                           (Str_mtdef (t0, n) t', n')
| Str_incl m0 => let (m', n') := gen_locs_m m0 n in (Str_incl m', n')
| Str_open p => let (p', n') := gen_locs_m_path p n in (Str_open p', n')
end.

(* end hide *)
Fixpoint gen_locs p n :=
match p with
| P_exp v => let (v', n') := gen_locs_v v n in P_exp v'
| P_mod (m0, l) e p => let (e', n') := gen_locs_m e (Z.succ n) in
                         P_mod (m0, n) e' (gen_locs p n')
end.

(* begin hide *)
Definition default_Mloc (x : M) : mloc := (x, 0).
Coercion default_Mloc : M>->mloc.
Definition default_Tloc (x : T) : tloc := (x, 0).
Coercion default_Tloc : T>->tloc.
Definition default_Vloc (x : V) : vloc := (x, 0).
Coercion default_Vloc : V>->vloc.

Coercion M_id := M_id : mloc -> m_path.
Coercion MT_id := MT_id : tloc -> m_type.

Ltac make_ex t := (let t' := eval vm_compute in (gen_locs t 0) in (exact t')).

Open Scope string_scope.

Definition n1 : V:= "n1".
Definition n2 : V:= "n2".
Definition n : M:= "n".
Definition add : C:= "add".
Definition mult : C:= "mult".
Definition Z : C:= "Z".
Definition F : M := "F".

Definition m : M := "m".
Definition x : V:= "x".
Definition y : V:= "y".
Definition i : V:= "i".
Definition s : V:= "s".
Definition P : M := "P".
Definition Pair : M := "Pair".
Definition Int : M := "Int".
Definition String : M := "String".
Definition X : M := "X".
Definition Y : M := "Y".
Definition Stringable : T := "Stringable".
Definition to_string : V := "to_string".
Definition int_to_string : V := "int_to_string".
Definition print_endline : V := "print_endline".
Definition concat : C := "^".
Definition pair : C := ",".
Definition sp : C := " ".
Definition C0 : C := "0".
Definition C1 : C := "1".
Definition neq : C := "!=".

Notation "x '^' y" := (V_app (V_app (V_const concat) x) y).
Notation "'(' x ',,' y ')'" := (V_app (V_app (V_const pair) x) y).
Infix "$" := M_app (at level 40).
(* end hide *)

Open Scope module_scope.

(** * Main example (_Figure 2_) *)
Definition example2 : prog := ltac:(make_ex(
module m = struct
  moduletype Stringable = Sig
    val to_string :_
  end
end;;

module Pair =
  functor (X : m ⋅³ Stringable) →
  functor (Y : m ⋅³ Stringable) →
  struct
    vlet to_string = fn x --> fn y -->
      (V_app (X⋅to_string) x) ^ sp ^ (V_app (Y⋅to_string) y)
  end;;

module Int = struct
  vlet to_string = int_to_string
end;;

module String = struct
  vlet to_string = fn s --> s
end;;

module P = (Pair $ Int) $ ((Pair $ String) $ Int);;

V_app print_endline (V_app (P⋅to_string) (C0,, (neq,, C1)))
)).

Definition example1 : prog := ltac:(make_ex(
module m =
struct
  vlet x = n1;
  vlet x = n2
end ;;

module n = (m :; Sig val x :_ end);;

V_app (V_app add (m⋅x)) (n⋅x)
)).

Definition example3 := ltac:(make_ex(
module Pair = functor (m : MT_sig nil) →
  M_struct nil;;
module Int = M_struct nil;;
module P = Pair $ Int;;
C0
)).

(** Derives a program's semantics *)
Definition derive_0 P: {Σ' : sem & Σ₀ / Γ₀ ⊢ P ⇝ Σ'} + {_ | _} := derive P Σ₀ Γ₀.

(** Extracts the extension kernel of a program *)
Definition get_E P : list (ℒ * ℒ):=
match derive_0 P with
| inl _ Σ => E (projT1 Σ)
| _ => nil
end.

Definition e1 := get_E example1.
Definition e2 := get_E example2.
Definition e3 := get_E example3.

Definition Σf: sem :=
  match derive_0 example2 with
  | inl _ (existT _ Σ _) => Σ
  | _ => Σ₀
  end.

Module Import TestR := Default_renaming.Definitions TestL SM.

(** Rename into "bar" at location 22 in example 2 *)
Definition vx : V := "bar".
Definition lx : ℒ := 22.

Definition e2' := rename Σf vx lx example2.

(** Computes all renamed locations *)
Definition renamed_locs P:=
match derive_0 P with
| inl _ (existT _ Σ _) => filter (fun l => if L_dec Σf 22 l then true else false) (locs P)
| _ => nil
end.

Definition compare_sem s1 s2 := SE.sem_eq_dec s1 s2.

(* extract String as char lists and Z as int*)
Require Import ExtrOcamlString ExtrOcamlZInt.

(* Do not let Coq's libraries conflict with Ocaml's *)
Extraction Blacklist List String Nat.

(** Extract most renaming-related functions as a library *)
(* begin show *)
Separate Extraction concat e2 e2' renamed_locs compare_sem.
(* end show *)
