Require Import AbstractRenaming.Modules List.

Set Implicit Arguments.

Module Renaming (Import L : LanguageSig) (Import ML : ModulesType L).

(** * Renamings (_Definition 1_) *)
Fixpoint is_renaming_v v v' : Prop :=
match v, v' with
| V_id (_, l), V_id (_, l') => l = l'
| V_field m (_, l), V_field m' (_, l') => m = m' /\ l = l'
| V_const c, V_const c' => c =c'
| V_let (_, l) v1 v2, V_let (_, l') v1' v2' =>
    l = l' /\ is_renaming_v v1 v1' /\ is_renaming_v v2 v2'
| V_fun (_, l) v, V_fun (_, l') v' =>
    l = l' /\ is_renaming_v v v'
| V_app v1 v2, V_app v1' v2' =>
    is_renaming_v v1 v1' /\ is_renaming_v v2 v2'
| V_open p1 v1, V_open p2 v2 => p1 = p2 /\ is_renaming_v v1 v2
| _, _ => False
end.

Fixpoint is_renaming_t (p p' : m_type) : Prop :=
match p, p' with
| MT_id ml, MT_id ml' => ml = ml'
| MT_field p tl, MT_field p' tl' => tl = tl' /\ p = p'
| MT_functor ml t1 t2, MT_functor ml' t1' t2' =>
    ml = ml' /\ is_renaming_t t1 t1' /\ is_renaming_t t2 t2'
| MT_constr t ml p, MT_constr t' ml' p' =>
    is_renaming_t t t' /\ ml = ml' /\ p = p'
| MT_dsubst t ml p, MT_dsubst t' ml' p' =>
    is_renaming_t t t' /\ ml = ml' /\ p = p'
| MT_sig l, MT_sig l' =>
  (fix is_renaming_list_S (l : list _) (l' : list _) := match l, l' with
    | h :: t, h'::t' => is_renaming_S h h' /\ is_renaming_list_S t t'
    | nil, nil => True
    | _, _ => False
    end) l l'
| MT_extr m, MT_extr m' => is_renaming_m m m'
| _, _ => False
end
with is_renaming_S (p p' : sig_comp) : Prop :=
match p, p' with
| Sig_val (_, l), Sig_val (_, l') => l = l'
| Sig_m ml t, Sig_m ml' t' => ml = ml' /\ is_renaming_t t t'
| Sig_alias ml p, Sig_alias ml' p' => ml = ml' /\ p = p'
| Sig_abs tl, Sig_abs tl' => tl = tl'
| Sig_concr tl m, Sig_concr tl' m' =>
    tl = tl' /\ is_renaming_t m m'
| Sig_incl t, Sig_incl t' => is_renaming_t t t'
| Sig_open p, Sig_open p' => p = p'
| _, _ => False
end
with is_renaming_m (p p' : m_exp) {struct p} : Prop :=
match p, p' with
| M_path m, M_path m' => m = m'
| M_functor ml t m, M_functor ml' t' m' =>
    ml = ml' /\ is_renaming_t t t' /\ is_renaming_m m m'
| M_app m1 m2, M_app m1' m2' => is_renaming_m m1 m1' /\ is_renaming_m m2 m2'
| M_tannot m t, M_tannot m' t' => is_renaming_t t t' /\ is_renaming_m m m'
| M_struct l, M_struct l' =>
  (fix is_renaming_list_s l l' := match l, l' with
    | h :: t, h'::t' => is_renaming_s h h' /\ is_renaming_list_s t t'
    | nil, nil => True
    | _, _ => False
    end) l l'
| _, _ => False
end
with is_renaming_s s s' : Prop :=
match s, s' with
| Str_let (_, l) v, Str_let (_, l') v' => l = l' /\ is_renaming_v v v'
| Str_let_ v, Str_let_ v' => is_renaming_v v v'
| Str_mdef ml m, Str_mdef ml' m' => ml = ml' /\ is_renaming_m m m'
| Str_mtdef tl t, Str_mtdef tl' t' => tl = tl' /\ is_renaming_t t t'
| Str_incl m, Str_incl m' => is_renaming_m m m'
| Str_open p, Str_open p' => p = p'
| _, _ => False
end.

(* begin hide *)
Definition is_renaming_list_S :=
(fix is_renaming_list_S (l : list _) (l' : list _) := match l, l' with
    | h :: t, h'::t' => is_renaming_S h h' /\ is_renaming_list_S t t'
    | nil, nil => True
    | _, _ => False
    end).

Definition is_renaming_list_s :=
(fix is_renaming_list_s l l' := match l, l' with
    | h :: t, h'::t' => is_renaming_s h h' /\ is_renaming_list_s t t'
    | nil, nil => True
    | _, _ => False
    end).
(* end hide *)

Fixpoint is_renaming p p' : Prop :=
match p, p' with
| P_exp v, P_exp v' => is_renaming_v v v'
| P_mod ml e p, P_mod ml' e' p' =>
    ml = ml' /\ is_renaming_m e e' /\ is_renaming p p'
| _, _ => False
end.

(* begin hide *)
Ltac destr_pairs:=
  repeat match goal with| [H: (?a, ?b) = (?c, ?d) |- _] => inversion H; subst; clear H end.
(* end hide *)

(** * is_renaming is an equivalence relation (Proposition 4) *)

(* begin hide *)
Lemma is_renaming_v_refl v: is_renaming_v v v.
Proof. induction v; destr_locs; simpl; intuition. Qed.
Global Hint Resolve is_renaming_v_refl : core.

Lemma is_renaming_m_refl:
  (forall m, is_renaming_m m m) *
  (forall ls, is_renaming_list_s ls ls) *
  (forall t, is_renaming_t t t) *
  (forall ls, is_renaming_list_S ls ls).
Proof. apply module_ind; intros; try solve[simpl in *; destr_locs; intuition]. Qed.


Local Definition is_renaming_m_refl_m := fst (fst (fst is_renaming_m_refl)).
Local Definition is_renaming_m_refl_t := snd (fst is_renaming_m_refl).

Global Hint Resolve is_renaming_m_refl_m : core.
Global Hint Resolve is_renaming_m_refl_t : core.

(* end hide *)

Lemma is_renaming_refl P: is_renaming P P.
Proof. induction P; simpl; auto. Qed.

(* begin hide *)
Lemma is_renaming_v_sym v v': is_renaming_v v v' -> is_renaming_v v' v.
Proof.
revert v'; induction v; intro v'; destruct v'; simpl; try destruct v as (v&lv);
try destruct v1 as (v1 &lv1); try destruct v0 as (v0&lv0); simpl in *; intuition.
Qed.
Global Hint Resolve is_renaming_v_sym : core.

Lemma is_renaming_m_sym:
  (forall m m', is_renaming_m m m' -> is_renaming_m m' m) *
  (forall ls ls', is_renaming_list_s ls ls' -> is_renaming_list_s ls' ls) *
  (forall t t', is_renaming_t t t' -> is_renaming_t t' t) *
  (forall ls ls', is_renaming_list_S ls ls' -> is_renaming_list_S ls' ls).
Proof.
apply module_ind; intros; simpl in *;
try destruct m'; try destruct t'; try destruct ls'; simpl in *;
try inversion H0; try inversion H1;
destr_locs; intuition; destr_pairs;
subst; try constructor; simpl in *; intuition;
try (destruct s; destr_locs; subst; simpl; intuition);
try solve[inversion H].
- fold is_renaming_list_S in *. auto.
- fold is_renaming_list_s in *. auto.
Qed.

Local Definition is_renaming_m_sym_m := fst (fst (fst is_renaming_m_sym)).
Global Hint Resolve is_renaming_m_sym_m : core.
(* end hide *)

Lemma is_renaming_sym P P': is_renaming P P' -> is_renaming P' P.
Proof.
revert P'; induction P; intro P'; simpl; destruct P';
intuition; simpl; auto.
Qed.

(* begin hide *)
Lemma is_renaming_trans_v v v' v'':
  is_renaming_v v v' -> is_renaming_v v' v'' -> is_renaming_v v v''.
Proof.
revert v' v''; induction v; intros v' v''; intros;
destruct v'; simpl in *; destr_locs; destruct v''; destr_locs; intuition; subst;
trivial; eauto.
Qed.
Global Hint Resolve is_renaming_trans_v : core.

Lemma is_renaming_trans_m:
  (forall m m' m'', is_renaming_m m m' -> is_renaming_m m' m'' -> is_renaming_m m m'') *
  (forall ls ls' ls'', is_renaming_list_s ls ls' -> is_renaming_list_s ls' ls'' -> is_renaming_list_s ls ls'') *
  (forall t t' t'', is_renaming_t t t' -> is_renaming_t t' t'' -> is_renaming_t t t'') *
  (forall ls ls' ls'', is_renaming_list_S ls ls' -> is_renaming_list_S ls' ls'' -> is_renaming_list_S ls ls'').
Proof.
apply module_ind; intros; try destruct t'; try destruct m'; try destruct m'';
try destruct ls'; try destruct s; simpl in *; destr_locs;
try destruct t''; try destruct ls''; try destruct s; destr_locs; intuition; subst;
destr_pairs; subst; fold is_renaming_list_S in *; fold is_renaming_list_s in *;
trivial; eauto.
Qed.

Local Definition is_renaming_trans_m_m := fst (fst (fst is_renaming_trans_m)).
Global Hint Resolve is_renaming_trans_m_m : core.
(* end hide *)

Lemma is_renaming_trans P P' P'':
  is_renaming P P' -> is_renaming P' P'' -> is_renaming P P''.
Proof.
revert P' P''; induction P; intros P' P''; intros;
destruct P'; simpl in *; destr_locs; destruct P''; destr_locs; intuition;
destr_pairs; subst; trivial; eauto.
Qed.

(** Renamings preserve declarations **)
(* begin hide *)
Lemma renaming_decl_v v v': is_renaming_v v v' -> decl_v v = decl_v v'.
Proof.
revert v'.
induction v; intros v' Hr; simpl;
try destruct v as [v l];
try destruct v1 as [v1 l1];
destruct v'; try inversion Hr; simpl in *; trivial.
- destruct v. intuition; subst.
  erewrite IHv1; eauto. erewrite IHv2; eauto.
- destruct v1. intuition; subst. erewrite IHv; eauto.
- erewrite IHv1; eauto. erewrite IHv2; eauto.
- erewrite IHv; eauto.
Qed.
Global Hint Resolve renaming_decl_v : core.

Lemma renaming_decl_m:
  (forall m m', is_renaming_m m m' -> decl_m m = decl_m m') *
  (forall ls ls', Forall2 is_renaming_s ls ls' ->
    flat_map decl_s ls = flat_map decl_s ls') *
  (forall t t', is_renaming_t t t' -> decl_t t = decl_t t') *
  (forall ls ls', Forall2 is_renaming_S ls ls' ->
    flat_map decl_S ls = flat_map decl_S ls').
Proof.
apply module_ind; simpl; intros; try destruct t'; try destruct m';
simpl; intuition; subst;
destr_locs; simpl in *; try destruct ls'; intuition;
try match goal with |[H: Forall2 _ _ _|- _] => inversion H end;
simpl in *; try destruct s; destr_locs; subst; simpl in *; intuition;
try solve[f_equal; auto].
- apply H. clear H. revert l0 H0; induction l; intros [|]; intuition.
- f_equal; auto; f_equal; auto.
- apply H. clear H. revert l H0; induction ls; intros [|]; intuition.
Qed.

Local Definition renaming_decl_m_m := fst (fst (fst renaming_decl_m)).
Global Hint Resolve renaming_decl_m_m : core.
(* end hide *)

Lemma renaming_decl P P': is_renaming P P' -> decl P = decl P'.
Proof.
revert P'; induction P; intros P' HP; destruct P'; simpl in HP; try tauto.
- now apply renaming_decl_v.
- destruct m as (m&l). destruct HP as (Heq&HP1&HP2). subst; simpl. f_equal.
  + now apply renaming_decl_m.
  + now apply IHP.
Qed.
Global Hint Resolve renaming_decl : core.

(** Renamings preserve locations **)
(* begin hide *)
Lemma renaming_locs_v v v': is_renaming_v v v' -> locs_v v = locs_v v'.
Proof.
revert v'.
induction v; intros v' Hr; simpl in *; destr_locs; trivial;
destruct v'; try inversion Hr; destr_locs; simpl in *; intuition; subst; trivial;
repeat (f_equal; auto).
Qed.
Global Hint Resolve renaming_locs_v : core.

Lemma renaming_locs_m:
  (forall m m', is_renaming_m m m' -> locs_m m = locs_m m') *
  (forall ls ls', is_renaming_list_s ls ls' ->
  flat_map locs_s ls = flat_map locs_s ls') *
  (forall t t', is_renaming_t t t' -> locs_t t = locs_t t') *
  (forall ls ls', is_renaming_list_S ls ls' ->
  flat_map locs_S ls = flat_map locs_S ls').
Proof.
apply module_ind; intros; destr_locs; simpl in *;
try destruct m';
try destruct t'; try destruct ls';
destr_locs; intuition; destr_pairs; subst; simpl in *; auto;
try destruct s; destr_locs; intuition; destr_pairs; subst; simpl in *; try tauto;
try solve [repeat (f_equal; auto)]; auto.
Qed.

Local Definition renaming_locs_m_m := fst (fst (fst renaming_locs_m)).
Global Hint Resolve renaming_locs_m_m : core.
(* end hide *)

Lemma renaming_locs P P': is_renaming P P' -> locs P = locs P'.
Proof.
revert P'; induction P; intros P' HP; destruct P'; simpl in HP; try tauto.
- now apply renaming_locs_v.
- destruct m as (m&l). destruct HP as (Heq&HP1&HP2). subst; simpl.
  f_equal. f_equal; auto.
Qed.
Global Hint Resolve renaming_locs : core.

(** * Footprints (_Definition 2_) *)

Fixpoint φv v v' : list ℒ :=
match v, v' with
| V_id (va, l), V_id (vb, l') => if val_eq_dec va vb then nil else l :: nil
| V_field m (va, l), V_field m' (vb, l') => if val_eq_dec va vb then nil else l :: nil
| V_let (va, l) v1 v2, V_let (vb, l') v1' v2' =>
    let l1 := φv v1 v1' in
    let l2 := φv v2 v2' in
      if val_eq_dec va vb then l1 ++ l2 else l :: l1 ++ l2
| V_fun (va, l) v, V_fun (vb, l') v' =>
    let lf := φv v v' in if val_eq_dec va vb then lf else l :: lf
| V_app v1 v2, V_app v1' v2' => φv v1 v1' ++ φv v2 v2'
| V_open _ v1, V_open _ v2 => φv v1 v2
| _, _ => nil
end.

Fixpoint φt (p p' : m_type) : list ℒ :=
match p, p' with
| MT_functor ml t1 t2, MT_functor ml' t1' t2' =>
    φt t1 t1' ++ φt t2 t2'
| MT_constr t ml p, MT_constr t' ml' p' => φt t t'
| MT_dsubst t ml p, MT_dsubst t' ml' p' => φt t t'
| MT_sig l, MT_sig l' =>
  (fix φlist_S l l' := match l, l' with
    | h :: t, h'::t' => φS h h' ++ φlist_S t t'
    | _, _ => nil
    end) l l'
| MT_extr m, MT_extr m' => φm m m'
| _, _ => nil
end
with φS (p p' : sig_comp) : list ℒ :=
match p, p' with
| Sig_val (va, l), Sig_val (vb, l') => if val_eq_dec va vb then nil else l :: nil
| Sig_m ml t, Sig_m ml' t' => φt t t'
| Sig_concr tl m, Sig_concr tl' m' => φt m m'
| Sig_incl t, Sig_incl t' => φt t t'
| _, _ => nil
end
with φm (p p' : m_exp) : list ℒ :=
match p, p' with
| M_functor ml t m, M_functor ml' t' m' => φt t t' ++ φm m m'
| M_app m1 m2, M_app m1' m2' => φm m1 m1' ++ φm m2 m2'
| M_tannot m t, M_tannot m' t' => φt t t' ++ φm m m'
| M_struct l, M_struct l' =>
  (fix φlist_s l l' := match l, l' with
    | h :: t, h'::t' => φs h h' ++ φlist_s t t'
    | _, _ => nil
    end) l l'
| _, _ => nil
end
with φs s s' : list ℒ :=
match s, s' with
| Str_let (va, l) v, Str_let (vb, l') v' =>
    let lf := φv v v' in
      if val_eq_dec va vb then lf else l :: lf
| Str_let_ v, Str_let_ v' => φv v v'
| Str_mdef ml m, Str_mdef ml' m' => φm m m'
| Str_mtdef tl t, Str_mtdef tl' t' => φt t t'
| Str_incl m, Str_incl m' => φm m m'
| _, _ => nil
end.

Definition φlist_S := (fix φlist_S l l' := match l, l' with
    | h :: t, h'::t' => φS h h' ++ φlist_S t t'
    | _, _ => nil
    end).

Definition φlist_s := (fix φlist_s l l' := match l, l' with
    | h :: t, h'::t' => φs h h' ++ φlist_s t t'
    | _, _ => nil
    end).

Fixpoint φ p p' : list ℒ :=
match p, p' with
| P_exp v, P_exp v' => φv v v'
| P_mod ml e p, P_mod ml' e' p' => φm e e' ++ φ p p'
| _, _ => nil
end.

(* begin hide *)
Lemma incl_nil {A} (l : list A) : incl nil l.
Proof. intros x Hx; inversion Hx. Qed.
Global Hint Resolve incl_nil : core.

Lemma φv_locs v v': incl (φv v v') (locs_v v).
Proof.
revert v'.
induction v; intro v'; simpl; destruct v'; destr_locs; simpl; auto;
try case val_eq_dec; auto with *.
Qed.
Global Hint Resolve φv_locs : core.

Lemma φm_locs:
  (forall m m', incl (φm m m') (locs_m m)) *
  (forall s s', incl (φlist_s s s') (flat_map locs_s s)) *
  (forall t t', incl (φt t t') (locs_t t)) *
  (forall S S', incl (φlist_S S S') (flat_map locs_S S)).
Proof.
apply module_ind; intros; simpl; try destruct t'; try destruct S';
destr_locs; auto with *;
repeat match goal with  |- context foo [match ?x with _ => _ end] => destruct x end; destr_locs; auto with *.
Qed.

Lemma φ_locs: forall P P', incl (φ P P') (locs P).
Proof.
induction P as [v | [m l] e P]; intros P'; destruct P'; simpl; auto.
intro x; simpl; rewrite in_app_iff in *. intros [H'|H'].
- apply φm_locs in H'. auto with *.
- apply IHP in H'. auto with *.
Qed.

Lemma footprint_sym_v v v': is_renaming_v v v' -> φv v v' = φv v' v.
Proof.
revert v'.
induction v; intros v' Hv'; simpl; destruct v'; destr_locs; auto;
simpl in *; subst; repeat case val_eq_dec; intuition; subst; auto with *; try tauto;
try (rewrite IHv1, IHv2; auto). rewrite IHv; auto.
Qed.
Global Hint Resolve footprint_sym_v : core.

Lemma footprint_sym_m:
  (forall m m', is_renaming_m m m' -> φm m m' = φm m' m) *
  (forall s s', Forall2 is_renaming_s s s' -> φlist_s s s' = φlist_s s' s) *
  (forall t t', is_renaming_t t t' -> φt t t' = φt t' t) *
  (forall S S', is_renaming_list_S S S' -> φlist_S S S' = φlist_S S' S).
Proof.
apply module_ind; simpl; intros; simpl in *;
destr_locs; intuition; destr_pairs; subst; simpl in *;
try (inversion H0; subst); auto;
try (inversion H1; subst); auto;
repeat match goal with
| |- context foo [match ?x with _ => _ end] => destruct x; simpl in *
| [H: match ?x with _ => _ end |- _ ] => destruct x; simpl in * end;
destr_locs; intuition; destr_pairs; subst; simpl in *; try tauto;
try solve [repeat (f_equal; auto with *)]; auto;
try (case val_eq_dec; intro; subst; simpl; try tauto; auto);
try solve [repeat (f_equal; auto with *)]; auto;
repeat match goal with
| |- context foo [match ?x with _ => _ end] => destruct x; simpl in *
| [H: match ?x with _ => _ end |- _ ] => destruct x; simpl in * end;
intuition; try tauto.
- fold φlist_s. apply H. clear H. revert l H0.
  induction ls; intros [|al l] H0; intuition.
- inversion H. subst. auto.
Qed.
(* end hide *)

Lemma footprint_sym P P': is_renaming P P' -> φ P P' = φ P' P.
Proof.
revert P'; induction P; destruct P'; intro Hr; simpl; trivial.
- now apply footprint_sym_v.
- inversion Hr; subst. rewrite (fst (fst (fst footprint_sym_m))) by tauto.
  rewrite IHP; tauto.
Qed.

(** Dependencies (_Definition 4_) *)
(* begin hide *)
Definition δv p p' l:= In l (φv p p') /\ In l (decl_v p).

Definition δm p p' l:= In l (φm p p').
(* end hide *)

Definition δ p p' l:= In l (φ p p') /\ In l (decl p).

(** * Renaming composition *)

(* begin hide *)
Lemma renaming_φv_trans v v0 v1:
  is_renaming_v v v0 -> is_renaming_v v0 v1 ->
  incl (φv v v1) (φv v v0 ++ φv v0 v1).
Proof.
revert v0 v1;
induction v; intros v' v'' H' H'' x Hin; destruct v'; destruct v''; simpl in *; trivial;
unfold incl in *; try (apply in_app_iff);
repeat (match goal with
        | [a : vloc |- _] => destruct a
        | [ |- context [if ?a then ?b else ?c] ] => destruct a; subst
        | [Hin : In x (if ?a then ?b else ?c) |- _] => destruct a; subst
        end); subst;
try destruct Hin as [Hin | Hin]; try tauto; simpl;
repeat (rewrite in_app_iff in Hin); repeat(rewrite in_app_iff); intuition;
subst; auto with *; try inversion Hin;
match goal with
| [ H : In x (_ ?a ?b) |- _] =>
    (eapply IHv1 in H || eapply IHv2 in H || eapply IHv in H);
    eauto; rewrite in_app_iff in H; destruct H
end; try tauto.
Qed.
Global Hint Resolve renaming_φv_trans : core.

Lemma renaming_φm_trans :
  (forall m m0 m1,
    is_renaming_m m m0 -> is_renaming_m m0 m1 ->
      incl (φm m m1) (φm m m0 ++ φm m0 m1)) *
  (forall ls, Forall (fun s => forall s0 s1, is_renaming_s s s0 -> is_renaming_s s0 s1 ->
      incl (φs s s1) (φs s s0 ++ φs s0 s1)) ls) *
  (forall m m0 m1,
    is_renaming_t m m0 -> is_renaming_t m0 m1 ->
      incl (φt m m1) (φt m m0 ++ φt m0 m1)) *
  (forall ls, Forall (fun s =>
    forall s0 s1, is_renaming_S s s0 -> is_renaming_S s0 s1 ->
      incl (φS s s1) (φS s s0 ++ φS s0 s1)) ls).
Proof.
apply module_ind;
try (constructor; trivial); auto with *;
(intros ml m IHm1 m1 IHm2 v' v'' H' H'' x Hin ||
intros m IHm m1 q v' v'' H' H'' x Hin ||
intros l IHm v' v'' H' H'' x Hin ||
intros v' v'' H' H'' x Hin);
destruct v'; destruct v''; simpl in *; trivial;
unfold incl in *; try (apply in_app_iff); try tauto;
repeat (match goal with
        | [a : vloc |- _] => destruct a
        | [ |- context [if ?a then ?b else ?c] ] => destruct a; subst
        | [Hin : In ?x (if ?a then ?b else ?c) |- _] => destruct a; subst
        end); subst;
try destruct Hin as [Hin | Hin]; try tauto; simpl;
repeat (rewrite in_app_iff in Hin); repeat(rewrite in_app_iff); intuition; subst;
try match goal with [H1 : In ?x (_ ?y ?z), H2 : _, H3 : _ ?y ?t, H4: _ ?t ?z |- _ ] =>
  ((eapply H2 with t z x in H1 || eapply renaming_φv_trans with y t z in H1);
   trivial; eauto; rewrite in_app_iff in H1; destruct H1; try tauto) end;
try tauto.
- fold is_renaming_list_S φlist_S in *.
  revert l0 H' l1 H'' Hin; induction l; intros l0 H' l1 H'' Hin; destruct l0; destruct l1;
  repeat (rewrite in_app_iff in Hin; rewrite in_app_iff); subst; try tauto. simpl in *.
  repeat rewrite in_app_iff in *.
  destruct H' as [Has H'].
  destruct H'' as [Has0 H''].
  simpl in IHm.
  inversion IHm as [ | aa ll Ha Hall]; trivial; subst.
  destruct Hin as [Ha' | Hin].
  + eapply Ha in Ha'; eauto.
    rewrite in_app_iff in Ha'; destruct Ha'; tauto.
  + eapply IHl in Hall; eauto; tauto.
- revert l0 H' l1 H'' Hin; induction l; intros l0 H' l1 H'' Hin; destruct l0; destruct l1;
  repeat (rewrite in_app_iff in Hin; rewrite in_app_iff); subst; try tauto. simpl in *.
  repeat rewrite in_app_iff in *.
  destruct H' as [Has H'].
  destruct H'' as [Has0 H''].
  simpl in IHm.
  inversion IHm as [ | aa ll Ha Hall]; trivial; subst.
  destruct Hin as [Ha' | Hin].
  + eapply Ha in Ha'; eauto.
    rewrite in_app_iff in Ha'; destruct Ha'; tauto.
  + eapply IHl in Hall; eauto; tauto.
Qed.

(* end hide *)

(** (Proposition 1) *)
Proposition renaming_φtrans p p' p'':
  is_renaming p p' -> is_renaming p' p'' ->
  incl (φ p p'') (φ p p' ++ φ p' p'') /\
  (forall l, δ p p'' l -> δ p p' l \/ δ p' p'' l).
Proof.
intros Hp Hp'.
assert(Hi : incl (φ p p'') (φ p p' ++ φ p' p'')). {
revert p' p'' Hp Hp'.
induction p; intros p' p''; destruct p'; destruct p''; intros H' H''; simpl in *;
try tauto; auto with *.
intuition; subst.
  apply incl_app.
  + eapply incl_tran.
    * apply renaming_φm_trans; eauto.
    * auto with *.
  + eapply incl_tran.
    * apply IHp; eauto.
    * auto with *.
}
split; trivial. unfold δ. intros l [Hl1 Hl2].
apply Hi, in_app_iff in Hl1. destruct Hl1 as [Hl1 | Hl1];
[| erewrite renaming_decl in Hl2 by apply Hp]; tauto.
Qed.

End Renaming.
