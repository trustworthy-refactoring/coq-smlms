# Coq Semantics for an ML Module System

This work implements coq definitions and soundness results about a formal
semantics capturing value binding in a language with a powerful module system
such as OCaml.

[Original article.](https://dl.acm.org/doi/10.1145/3314221.3314600)

## Modules.v
Definition of the module system and basic syntactic properties (Section 2).

## Semantics.v
Semantic rules and their properties (Section 3), including
* determinism as well as a derivation function
* monotonicity properties
* weakening lemmas

## Default_renaming.v
Defines and prove the correction of the renaming from Proposition 5

## Renaming_operations.v
Definitions and basic properties of renamings (Section 2.1)

## Characterising_renaming.v
Proves properties from Section 4:
* Lower bound for footprints
* Factorisation

## Examples.v
Tools to write examples
