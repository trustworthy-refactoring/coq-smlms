Require Import AbstractRenaming.Modules List Relations Relation_Operators.
Import ListNotations String.
Import Program.Basics.
Require FMapAVL FunctionalExtensionality FMapFacts.

Set Implicit Arguments.

Open Scope program_scope.

(** A Static Semantics for Renaming *)
Module Semantics (Export L : LanguageSig).
Module Export ML := Modules L.
Module Import MN := ML.Notations.

Open Scope module_scope.

(* begin hide *)
(* Misc *)

Ltac nsplit n :=
  match n with
  | S ?X => split; [| nsplit X]
  | O  => idtac
  end.

(* "*" is left associative, contrary to /\ *)
Notation "a ∧ b" := (prod a b) (at level 40, b at level 40, right associativity).
(* end hide *)

(** * Semantic Elements *)

(** ** Binding Resolution *)

Definition binding_resolution := ℒ -> option(option ℒ).

(** ** Syntactic Characteristics *)
Definition ℛ := ℒ -> option var.

(** Binding resolution and syntactic reification will be implemented as
  finite maps on locations *)
Module DecidableLoc <: OrderedType.OrderedType.
  Definition t := ℒ.
  Definition eq (x y : ℒ) := x = y.
  Definition eq_refl := @eq_refl t.
  Definition eq_sym := @eq_sym t.
  Definition eq_trans := @eq_trans t.
  Definition eq_dec : forall x y : t, {eq x y} + {~ eq x y} := loc_eq_dec.
  Definition lt := lt.
  Definition lt_trans := lt_trans.
  Definition lt_not_eq := lt_not_eq.
  Definition compare := compare.
End DecidableLoc.

Module LocMaps : (FMapInterface.WSfun DecidableLoc) := FMapAVL.Make DecidableLoc.
Module Import LocMapsFacts := FMapFacts.WFacts_fun DecidableLoc LocMaps.

Record sem :=
{ br : LocMaps.t (option ℒ); (* Binding resolution *)
  ek : list (ℒ * ℒ); (* Value extension kernel *)
  sr : LocMaps.t ℐ   (* Syntactic reification *)
}.

Notation "'E'" := ek.

Definition br' Σ : binding_resolution := fun l => LocMaps.find l (br Σ).
Notation "↣" := br'.

Definition sr' Σ : ℛ := fun l => LocMaps.find l (sr Σ).
Notation "'ρ'" := sr'.

(** Symmetric-Reflexive-transitive closure of the extension kernel (_Definition 8_) *)
Definition Ê s : relation ℒ:= clos_refl_sym_trans_1n _ (fun l l' => In (l, l') (E s)).
Global Hint Constructors clos_refl_sym_trans_1n : core.

(* begin hide *)
Lemma clos_refl_sym_trans_1n_incl {A} (R1 R2: relation A) :
  (forall l1 l2, R1 l1 l2 -> R2 l1 l2) ->
    forall l1 l2, clos_refl_sym_trans_1n A R1 l1 l2 ->
                  clos_refl_sym_trans_1n A R2 l1 l2.
Proof.
intro Hincl.
apply clos_refl_sym_trans_1n_ind; trivial.
intros x y z [H|H] H1 H2; eauto.
Qed.

Lemma clos_refl_sym_trans_1n_eq_split A (R: relation A) l1 l2 l l':
  clos_refl_sym_trans_1n _ (fun l l' => (l1, l2) = (l, l') \/ R l l') l l'->
  clos_refl_sym_trans_1n _ R l l' \/
  (clos_refl_sym_trans_1n _ R l l1 /\ clos_refl_sym_trans_1n _ R l' l2) \/
  (clos_refl_sym_trans_1n _ R l l2 /\ clos_refl_sym_trans_1n _ R l' l1).
Proof.
intro H; induction H as [| l0 l l' H HR]; auto.
destruct H as [ [Heq|Hin] | [Heq|Hin] ]; try (inversion Heq; subst; clear Heq);
destruct IHHR as [ H | [(H&H') | (H&H')] ];
eauto using clos_rst1n_sym;right; eauto.
Qed.
(* end hide *)

(** Decidability for Ê *)
Definition ek_dec Σ l l': {Ê Σ l l'} + {~ Ê Σ l l'}.
Proof.
revert l l'.
unfold Ê.
 induction (E Σ) as [| [l1 l2] ll]; simpl; intros l l'; clear Σ.
- case (loc_eq_dec l l'); intro Heq;[subst; left; constructor|].
  right. intro H; inversion H; tauto.
- destruct (IHll l l'); [left; eapply clos_refl_sym_trans_1n_incl; eauto; tauto|].
  destruct (IHll l l1).
{
  + destruct (IHll l2 l').
    * left. apply clos_rst1n_trans with l1.
      -- eapply clos_refl_sym_trans_1n_incl; eauto; tauto.
      -- apply clos_rst1n_trans with l2;[auto with *|].
         ++ eapply rst1n_trans; [tauto|constructor].
         ++ eapply clos_refl_sym_trans_1n_incl; eauto; tauto.
    * right. intro H. apply clos_refl_sym_trans_1n_eq_split in H.
      destruct H as [H|[(H1&H2)|(H1&H2)]]; [tauto| |];
      eauto using clos_rst1n_sym.
      contradict n0. apply clos_rst1n_trans with l; eauto using clos_rst1n_sym.
      apply clos_rst1n_trans with l1; eauto using clos_rst1n_sym.
}
  destruct (IHll l' l1).
{
  + destruct (IHll l2 l).
    * left. apply clos_rst1n_trans with l2.
      -- eapply clos_rst1n_sym,clos_refl_sym_trans_1n_incl; eauto; tauto.
      -- apply clos_rst1n_sym, clos_rst1n_trans with l1;[|eauto].
         eapply clos_refl_sym_trans_1n_incl; eauto; tauto.
    * right. intro H. apply clos_refl_sym_trans_1n_eq_split in H.
      destruct H as [H|[(H1&H2)|(H1&H2)]]; [tauto| |];
      eauto using clos_rst1n_sym.
}
right. intro H. apply clos_refl_sym_trans_1n_eq_split in H.
destruct H as [H|[(H1&H2)|(H1&H2)]]; [tauto| |]; eauto using clos_rst1n_sym.
Defined.

(** The empty semantics *)
Definition empty_sem : sem :=
  {| br := LocMaps.empty _;
     ek := nil;
     sr := LocMaps.empty _ |}.
Notation Σ₀ := empty_sem.

(** Semantic equality based on finite maps equality *)
Definition sem_equal Σ1 Σ2 :=
  LocMaps.Equal (sr Σ1) (sr Σ2) /\
  (E Σ1 = E Σ2) /\
  LocMaps.Equal (br Σ1) (br Σ2).

(** Decidability for semantic equality *)
Definition sem_equal_dec Σ1 Σ2 : {sem_equal Σ1 Σ2} + {~ sem_equal Σ1 Σ2}.
Proof.
unfold sem_equal.
case_eq (LocMaps.equal (fun e e' => if var_eq_dec e e' then true else false) (sr Σ1) (sr Σ2)).
- intro H1. apply LocMaps.equal_2, Equal_Equivb_eqdec in H1.
  case_eq (LocMaps.equal (fun e e' => if (option_eq_dec loc_eq_dec) e e' then true else false) (br Σ1) (br Σ2)).
  + intro H2. apply LocMaps.equal_2, Equal_Equivb_eqdec in H2.
    assert(Hdec: forall x y : ℒ ∧ ℒ, {x = y} + {x <> y}).
    {
      intros (x1&x2) (y1&y2). destruct (loc_eq_dec x1 y1); destruct (loc_eq_dec x2 y2);
      subst; eauto; right; intro Heq; inversion Heq; subst; tauto.
    }
    edestruct (list_eq_dec Hdec (E Σ1) (E Σ2)); tauto.
  + intro H2. right. intros (_&_&H2'). rewrite Equal_Equivb_eqdec in H2'.
    apply LocMaps.equal_1 in H2'. rewrite H2' in H2. discriminate H2.
- intro H1. right. intros (H1'&_&_). rewrite Equal_Equivb_eqdec in H1'.
  apply LocMaps.equal_1 in H1'. rewrite H1' in H1. discriminate H1.
Defined.

Add Parametric Relation elt : (LocMaps.t elt) LocMaps.Equal
  reflexivity proved by (@LocMapsFacts.Equal_refl elt)
  symmetry proved by (@LocMapsFacts.Equal_sym elt)
  transitivity proved by (@LocMapsFacts.Equal_trans elt) as LocMaps_Equal.

Global Hint Immediate LocMapsFacts.Equal_refl : core.

Lemma sem_equal_refl Σ: sem_equal Σ Σ.
Proof. repeat split. Qed.
Global Hint Immediate sem_equal_refl : core.

Lemma sem_equal_sym Σ Σ': sem_equal Σ Σ' -> sem_equal Σ' Σ.
Proof. intros (H1&H2&H3). repeat split; now symmetry. Qed.

Global Hint Resolve sem_equal_sym : core.

Lemma sem_equal_trans Σ Σ' Σ'': sem_equal Σ Σ' -> sem_equal Σ' Σ'' -> sem_equal Σ Σ''.
Proof. intros (H1&H2&H3) (H1'&H2'&H3'). repeat split; etransitivity; eauto. Qed.

Add Parametric Relation: sem sem_equal
  reflexivity proved by sem_equal_refl
  symmetry proved by sem_equal_sym
  transitivity proved by sem_equal_trans as sem_equal_rel.

(** * Semantic Descriptions *)

(** Semantic descriptions (_Definition 9_) *)
Inductive desc :=
| Structural : (list ℒ) -> (list (ℒ * desc)) -> desc
| Functorial : ℒ -> desc -> desc -> desc.

(** locations of a description *)
Fixpoint locs_d Δ := match Δ with
| Structural l1 l2 => l1 ++ flat_map (fun ld => match ld with (l, d) => locs_d d end) l2
| Functorial l Δ1 Δ2 => locs_d Δ1 ++ locs_d Δ2
end.

(* begin hide *)

(* Better induction principle for desc *)
Definition desc_ind2 P:
  (forall lv, P (Structural lv [])) ->
  (forall lv ls l d, P d -> P (Structural lv ls) -> P (Structural lv ((l, d) :: ls))) ->
  (forall l d, P d -> forall d0, P d0 -> P (Functorial l d d0)) ->
  forall d : desc, P d.
Proof.
fix H 4.
intros Hnil HS HF [lv ls | l Δ Δ'].
- induction ls as [| [l d] ls]; auto.
- auto.
Defined.

Definition in_inv_dec A (x : A) (Hdec : forall a b : A, {a = b} + {a <> b}) h t :
  In x (h :: t) -> {x = h} + {In x t}.
Proof.
intro Hin. destruct (Hdec x h).
- subst; tauto.
- right. inversion Hin; subst; tauto.
Defined.

Definition desc_eq_dec : forall a b : desc, {a = b} + {a <> b}.
Proof.
induction a as [lv | lv ls l d | l d1 Hd1 d2 Hd2] using desc_ind2;
intros [lv' ls' | l' Δ1' Δ2'];
try (solve[right; intro Heq; inversion Heq]).
- destruct ls'; [|right; intro Heq'; inversion Heq'; trivial].
  destruct (list_eq_dec loc_eq_dec lv lv') as [Heq | Hneq]; subst;
  [tauto |].
  right; intro Heq'; apply Hneq; inversion Heq'; trivial.
- destruct ls' as [| [l' d'] ls'];[right; intro Heq'; inversion Heq'; trivial|].
  destruct (loc_eq_dec l l') as [Heq | Hneq];
  [subst l'|right; intro Heq'; apply Hneq; inversion Heq'; trivial].
  destruct (IHd d') as [Heq | Hneq];
  [subst d'|right; intro Heq'; apply Hneq; inversion Heq'; trivial].
  destruct (IHa (Structural lv ls')) as [Heq | Hneq];
  [|right; intro Heq'; apply Hneq; inversion Heq'; trivial].
  inversion Heq; subst ls'; clear Heq.
  destruct (list_eq_dec loc_eq_dec lv lv') as [Heq | Hneq]; subst;
  [|right; intro Heq'; apply Hneq; inversion Heq'; trivial].
  tauto.
- destruct (loc_eq_dec l l') as [Heq | Hneq];
  [subst l'|right; intro Heq'; apply Hneq; inversion Heq'; trivial].
  destruct (Hd1 Δ1') as [Heq | Hneq];
  [subst Δ1'|right; intro Heq'; apply Hneq; inversion Heq'; trivial].
  destruct (Hd2 Δ2') as [Heq | Hneq];
  [subst Δ2'|right; intro Heq'; apply Hneq; inversion Heq'; trivial].
  tauto.
Defined.

Definition ℒdesc_eq_dec : forall a b : ℒ * desc, {a = b} + {a <> b}.
Proof.
intros [l d] [l' d'].
destruct (desc_eq_dec d d') as [Heq | Hneq];
[subst d'| right; intro H; apply Hneq; inversion H; trivial].
destruct (loc_eq_dec l l') as [Heq | Hneq];
[subst l'| right; intro H; apply Hneq; inversion H; trivial].
tauto.
Defined.
(* end hide *)

(* induction principle for descriptions *)
Definition  desc_ind3 P:
  (forall lv ls,(forall l d, In (l, d) ls -> P d) -> P (Structural lv ls)) ->
  (forall l d, P d -> forall d0, P d0 -> P (Functorial l d d0)) ->
  forall d : desc, P d.
Proof.
fix H 3.
intros Hls HF [lv ls | l Δ Δ'].
- apply Hls.
  induction ls as [| [l d] ls].
  + intros l d Hin; inversion Hin.
  + intros l' d' Hin. apply (in_inv_dec ℒdesc_eq_dec) in Hin.
    destruct Hin as [Heq | Hin'].
    * inversion Heq; subst. auto.
    * eapply IHls; exact Hin'.
- apply HF; auto.
Defined.

(** Empty description *)
Definition empty_d := Structural nil nil.
Notation "∅" := empty_d.

(** Codomain of a semantics: dom(ρ) \ dom(↣) *)
Definition cod Σ l : Prop := exists v, ρ Σ l = Some (IV v) /\ ↣ Σ l = None.

(** Well-behaved Descriptions (_Definition 18_) *)
Inductive wb_d (Σ : sem) : desc -> Prop :=
(* (i) + (ii) + (iii) *)
| WB_Structural : forall lv ls,
  (* (i) *)
  (forall l, In l lv -> cod Σ l) ->
  (* (ii) *)
  (forall l d, In (l, d) ls ->
                wb_d Σ d /\
                ((exists m, ρ Σ l = Some (IM m)) \/ exists t, ρ Σ l = Some (IT t))) ->
  (* (iii) *)
  (forall v v', In v lv -> In v' lv ->  ρ Σ v = ρ Σ v' -> v = v') ->
  (* change: d = d', not only fst d = fst d' *)
  (forall d d', In d ls -> In d' ls -> ρ Σ (fst d) = ρ Σ (fst d') -> d = d') ->
  wb_d Σ (Structural lv ls)

(* (iv) *)
| WB_functorial : forall l Δ Δ', wb_d Σ Δ -> wb_d Σ Δ' ->
    wb_d Σ (Functorial l Δ Δ').

(** Description Superposition (_Definition 10_) *)
Program Definition desc_sup (r : ℛ) (d0 d0' : desc) : desc :=
match d0, d0' with
| Structural lv ls, Structural lv' ls' =>
    let lv'' := filter (fun l =>
        if Exists_dec (fun l' => r l' = r l) lv' _
        then false else true) lv in
    let ls'' := filter (fun ld =>
        if Exists_dec (fun ld' => r (fst ld') = r (fst ld)) ls' _
        then false else true) ls in
    Structural (lv' ++ lv'') (ls' ++ ls'')
| _, _ => ∅
end.
Next Obligation.
apply option_eq_dec, var_eq_dec.
Defined.
Next Obligation.
apply option_eq_dec, var_eq_dec.
Defined.

Notation "D1 '⊕' r '#' D2" := (desc_sup r D1 D2)
  (at level 40, r at level 40, no associativity).

Arguments desc_sup: simpl never.

(** Description Join (_Definition 11_) *)
Fixpoint join (r : ℛ) d1 d2: list (ℒ * ℒ) :=
match d1, d2 with
| Structural lv1 ls1, Structural lv2 ls2 =>
    (flat_map (fun l => flat_map (fun l' =>
        if option_eq_dec var_eq_dec (r l) (r l') then [(l, l')]
        else nil) lv2) lv1) ++
    (flat_map (fun ld => flat_map (fun ld' =>
        match (ld, ld') with
        | ((l, Δ), (l', Δ')) =>
            if option_eq_dec var_eq_dec (r l) (r l') then (join r Δ Δ')
            else nil
        end) ls2) ls1)
| Functorial l1 Δ1 Δ1',  Functorial l2 Δ2 Δ2' => join r Δ1 Δ2 ++ join r Δ1' Δ2'
| _, _ => nil
end.
Notation "D1 '⊗' r '#' D2" := (join r D1 D2) (at level 40, r at level 40, no associativity).

(** Description Modulation (_Definition 12_) *)
Fixpoint desc_mod (r : ℛ) (d0 d0' : desc) {struct d0} : desc :=
match (d0, d0') with
| (Structural lv ls, Structural lv' ls') => Structural
    ((filter (fun l' => if Exists_dec _ lv' (fun l => option_eq_dec var_eq_dec (r l') (r l)) then true else false) lv) ++
     (filter (fun l' => if Exists_dec _ lv (fun l => option_eq_dec var_eq_dec (r l') (r l)) then false else true) lv'))
    ((flat_map (fun ld : ℒ * desc => let (l, Δ) := ld in
      flat_map (fun ld' : ℒ * desc => let (l', Δ') := ld' in
        if option_eq_dec var_eq_dec (r l) (r l') then [(l, desc_mod r Δ Δ')] else nil) ls') ls) ++
    (filter (fun ld' : ℒ * desc => let (l', Δ') := ld' in
        if Exists_dec _ ls (fun ld => option_eq_dec var_eq_dec (r l') (r (fst ld)))
        then false else true) ls'))
| (Functorial l Δ1 Δ2, Functorial l' Δ1' Δ2') =>
     Functorial l (desc_mod r Δ1 Δ1') (desc_mod r Δ2 Δ2')
| _ => ∅
end.
Notation "D1 '⊳' r # D2" := (desc_mod r D1 D2) (at level 40).

(** Selective Modulation (_Definition 13_) *)
Definition s_mod (r : ℛ) (d0: desc) (x : M) (Δ' : desc) : desc :=
match d0 with
| Structural lv ls =>
    Structural
      lv
      (map (fun ld : ℒ * desc => let (l, Δ) := ld in
         if option_eq_dec var_eq_dec (r l) (Some (IM x))
         then (l, Δ ⊳r# Δ')
         else (l, Δ)) ls)
| Functorial l Δ1 Δ2 => ∅
end.
Notation "s '⊲' r # '(' x ',' Δ ')'" := (s_mod r s x Δ) (at level 40).

(** Description Filtering (_Definition 14_) *)
Definition desc_filtering (r : ℛ) (d0: desc) (x : M) : desc :=
match d0 with
| Structural lv ls =>
    Structural
      lv
      (filter (fun ld : ℒ * desc => let (l, Δ) := ld in
      if option_eq_dec var_eq_dec (r l) (Some (IM x))
                         then false else true) ls)
| Functorial l Δ Δ' => ∅
end.
Infix "\" := desc_filtering (at level 40).

(* begin hide *)
(* Various lemmas and semantic equality *)
Lemma desc_mod_sem_equal s s' Δ Δ':
  sem_equal s s' ->
  Δ ⊳ ρ s # Δ' = Δ ⊳ ρ s' # Δ'.
Proof.
intros (Heq1&_&Heq2).
revert Δ'. induction Δ using desc_ind3.
- intros [lv' ls'| l' d1' d2']; simpl; trivial.
  f_equal; f_equal; simpl.
  + induction lv; trivial. simpl. rewrite IHlv.
    case Exists_dec; intro Heq; case Exists_dec; intro Heq'; trivial.
    * contradict Heq'. apply Exists_exists; apply Exists_exists in Heq.
      destruct Heq as (x&Hx1&Hx2). exists x; split; trivial.
      unfold sr' in *. now rewrite <- Heq1.
    * contradict Heq. apply Exists_exists; apply Exists_exists in Heq'.
      destruct Heq' as (x&Hx1&Hx2). exists x; split; trivial.
      unfold sr' in *. now rewrite Heq1.
  + induction lv'; trivial. simpl. rewrite IHlv'.
    case Exists_dec; intro Heq; case Exists_dec; intro Heq'; trivial.
    * contradict Heq'. apply Exists_exists; apply Exists_exists in Heq.
      destruct Heq as (x&Hx1&Hx2). exists x; split; trivial.
      unfold sr' in *. now rewrite <- Heq1.
    * contradict Heq. apply Exists_exists; apply Exists_exists in Heq'.
      destruct Heq' as (x&Hx1&Hx2). exists x; split; trivial.
      unfold sr' in *. now rewrite Heq1.
  + induction ls as [|(l&d) ls]; trivial. simpl.
    repeat rewrite IHls; [|intros; eapply H; eauto with *].
    f_equal. clear IHls. induction ls' as [|(l'&d') ls']; simpl; trivial.
    repeat (erewrite H; auto with *). unfold sr'.
    f_equal; [|apply IHls'].
    now repeat rewrite Heq1.
  + induction ls' as [|(l'&d') ls']; simpl; trivial.
    rewrite IHls'.
    case Exists_dec; intro Heq; case Exists_dec; intro Heq'; trivial.
    * contradict Heq'. apply Exists_exists; apply Exists_exists in Heq.
      destruct Heq as (x&Hx1&Hx2). exists x; split; trivial.
      unfold sr' in *. now rewrite <- Heq1.
    * contradict Heq. apply Exists_exists; apply Exists_exists in Heq'.
      destruct Heq' as (x&Hx1&Hx2). exists x; split; trivial.
      unfold sr' in *. now rewrite Heq1.
- destruct Δ'; simpl; trivial. now rewrite IHΔ1, IHΔ2.
Qed.

Lemma s_mod_sem_equal s s' x Δ Δ':
  sem_equal s s' ->
  Δ ⊲ ρ s # (x, Δ') = Δ ⊲ ρ s' # (x, Δ').
Proof.
intros (Heq1&Heq0&Heq2).
revert Δ. apply desc_ind3; trivial.
intros lv ls Hind. simpl.
f_equal. induction ls as [| (l&d) ls]; simpl; trivial.
rewrite IHls; auto; [|intros; eapply Hind; eauto with *].
f_equal. unfold sr' in *. rewrite Heq1. case option_eq_dec; trivial.
intro. f_equal. apply desc_mod_sem_equal. split; tauto.
Qed.

Lemma desc_filtering_sem_equal s s' d x:
  sem_equal s s' -> (ρ s \ d) x = (ρ s' \ d) x.
Proof.
intros (Heq1&_&Heq2). destruct d as [lv ls|]; simpl; trivial.
f_equal. induction ls as [|(l'&d') ls]; simpl; trivial.
rewrite IHls. unfold sr'. now repeat rewrite Heq1.
Qed.
(* end hide *)

(** * Semantic Environments *)

(* module and type identifiers have been split for simplicity *)
Definition env : Type := (V -> option ℒ) * (M -> desc) * (T -> desc).

Definition envV (Γ : env) := match Γ with (Γ, _, _) => Γ end.
Definition envM (Γ : env) := match Γ with (_, Γ, _) => Γ end.
Definition envT (Γ : env) := match Γ with (_, _, Γ) => Γ end.

(** Empty environment *)
Definition empty_env : env := (fun _ => None, fun _ => ∅, fun _ => ∅).
Notation Γ₀ := empty_env.

(** Well-behaved environment (_Definition 18_) *)
Definition wb_env (Γ : env) Σ :=
  (forall v v', envV Γ v = envV Γ v' -> envV Γ v <> None -> v = v') /\
  (forall (v : V) l, envV Γ v = Some l -> ρ Σ l = Some (IV v) /\ ↣ Σ l = None) /\
  (forall m : M, wb_d Σ (envM Γ m)) /\
  (forall t : T, wb_d Σ (envT Γ t)).


(** Partial Semantic Environment (Γ +ρ D) *)
Definition p_sem_env (r : ℛ) (d0 : desc) (Γ : env) : env :=
match d0 with
| Structural lv ls =>
    (fun ι => match find ((var_eq_bool (Some (IV ι))) ∘ r) lv with
              | Some l => Some l
              | None => envV Γ ι
              end,
     fun ι => match find ((var_eq_bool (Some (IM ι))) ∘ r ∘ fst) ls with
              | Some (l, Δ) => Δ
              | None => envM Γ ι
              end,
     fun ι => match find ((var_eq_bool (Some (IT ι))) ∘ r ∘ fst) ls with
              | Some (l, Δ) => Δ
              | None => envT Γ ι
              end)
| Functorial _ _ _ => Γ
end.
Arguments p_sem_env: simpl never.

Lemma wb_p_sem_env Γ Σ d: wb_d Σ d -> wb_env Γ Σ -> wb_env (@p_sem_env (ρ Σ) d Γ) Σ.
Proof.
intro H.
destruct d as [ lv ls|]; trivial.
intros (H1&H2&H3&H4).
repeat apply conj; unfold p_sem_env; simpl.
- intros v v' Ha Hb.
  case_eq (find (var_eq_bool (Some (IV v)) ∘ (ρ Σ)) lv).
  + intros o Ho. rewrite Ho in *.
    apply find_some in Ho.
    destruct Ho as [Hin Heq].
    apply var_eq_bool_true in Heq.
    case_eq (find (var_eq_bool (Some (IV v')) ∘ (ρ Σ)) lv).
    * intros o' Ho'. rewrite Ho' in *. inversion Ha; subst o'.
      apply find_some in Ho'. destruct Ho' as [_ Heq'].
      apply var_eq_bool_true in Heq'. rewrite <- Heq' in Heq. now inversion Heq.
    * intros Ho'. rewrite Ho' in *. symmetry in Ha. apply H2 in Ha.
      rewrite <- Heq in Ha. destruct Ha as (Ha&_). now inversion Ha.
  + intros Ho. rewrite Ho in *. case_eq (envV Γ v); [|tauto].
    intros o Heq. case_eq (find (var_eq_bool (Some (IV v')) ∘ (ρ Σ)) lv).
    * intros o' Ho'. rewrite Ho' in *. rewrite Ha in Heq; inversion Heq; subst o'.
      apply find_some in Ho'. destruct Ho' as [Hin Heq'].
      apply var_eq_bool_true in Heq'.
      apply H2 in Ha. destruct Ha as (Ha&_); rewrite Ha in Heq'. now inversion Heq'.
    * intros Ho'. rewrite Ho' in *. now apply H1.
- intros v l. simpl in *.
  case_eq (find (var_eq_bool (Some (IV v)) ∘ (ρ Σ)) lv); auto.
  intros o Ho Heq; inversion Heq; subst o. apply find_some in Ho; clear Heq.
  destruct Ho as (Hin&Heq). apply var_eq_bool_true in Heq. split; auto.
  inversion H as [lv' ls' Hwb _ _ _|]; subst ls' lv'. apply Hwb in Hin.
  now destruct Hin as (_&_&Hn).
- intros m.
  case_eq (find (var_eq_bool (Some (IM m)) ∘ (ρ Σ) ∘ fst) ls); auto.
  intros [l d] Hld.
  apply find_some in Hld.
  destruct Hld as (Hin&Heq).
  apply var_eq_bool_true in Heq.
  simpl in Heq.
  inversion H as [lv' ls' _ Hwb _ _|]; subst ls' lv'.
  apply Hwb in Hin. apply Hin.
- intros t.
  case_eq (find (var_eq_bool (Some (IT t)) ∘ (ρ Σ) ∘ fst) ls); auto.
  intros [l d] Hld.
  apply find_some in Hld.
  destruct Hld as (Hin&Heq).
  apply var_eq_bool_true in Heq.
  simpl in Heq.
  inversion H as [lv' ls' _ Hwb _ _|]; subst ls' lv'.
  apply Hwb in Hin. apply Hin.
Qed.
Global Hint Resolve wb_p_sem_env : core.

(* begin hide *)
Definition sem_up_sr Σ l ι : sem :=
  {| br := br Σ;
     ek := E Σ;
     sr := LocMaps.add l ι (sr Σ) |}.
Notation "Σ '[[' l ↦ ι ']]'" := (sem_up_sr Σ l ι) (at level 40).

Lemma sem_up_l Σ l ι : ρ (Σ [[l ↦ ι]]) l = Some ι.
Proof. unfold sr'. now apply find_mapsto_iff, LocMaps.add_1. Qed.
Global Hint Resolve sem_up_l : core.
(* end hide *)

Definition sem_up_br Σ (l : ℒ) v (l' : option ℒ) :=
  {| br := LocMaps.add l l' (br Σ);
     ek := E Σ;
     sr := LocMaps.add l (IV v) (sr Σ) |}.
Notation "Σ '[[' ( l , v ) ↦l l0 ']]'" := (sem_up_br Σ l v l0) (at level 40).

(** Semantic join operation *)
Definition sem_join Σ Δ1 Δ2 :=
  {| br := br Σ;
     ek := join (ρ Σ) Δ1 Δ2 ++ E Σ;
     sr := sr Σ |}.
Notation "Σ '[[' Δ1 '⊛' Δ2 ']]'" := (sem_join Σ Δ1 Δ2) (at level 40).

(** Environment update operations *)
Definition env_up_v (Γ : env) v (l : option ℒ) : env :=
 match Γ with | (Γv, Γm, Γt) =>
 (fun v' => if var_eq_dec (IV v) (IV v') then l
            else Γv v', Γm, Γt) end.
Notation "Γ '[[' l ↦v l0 ']]'" := (env_up_v Γ l l0) (at level 40).

Definition env_up_m (Γ : env) m Δ : env :=
 match Γ with | (Γv, Γm, Γt) =>
 (Γv, fun m' => if var_eq_dec (IM m) (IM m') then Δ
                else Γm m', Γt) end.
Notation "Γ '[[' l ↦m Δ ']]'" := (env_up_m Γ l Δ) (at level 40).

Definition env_up_t (Γ : env) t Δ : env :=
 match Γ with | (Γv, Γm, Γt) =>
 (Γv, Γm, fun t' => if var_eq_dec (IT t) (IT t') then Δ
                    else Γt t') end.
Notation "Γ '[[' l ↦t l0 ']]'" := (env_up_t Γ l l0) (at level 40).

(* begin hide *)
Lemma envV_up_v_m Γ y ly: envM (Γ [[y ↦v ly]]) = envM Γ.
Proof. now destruct Γ as [ [ Γv Γm] Γt ]. Qed.
Global Hint Resolve envV_up_v_m : core.

Lemma envV_up_v_t Γ y ly: envT (Γ [[y ↦v ly]]) = envT Γ.
Proof. now destruct Γ as [ [ Γv Γm] Γt ]. Qed.
Global Hint Resolve envV_up_v_t : core.

Lemma env_up_v_m Γ y ly m d: Γ [[y ↦v ly]] [[m ↦m d]] = Γ [[m ↦m d]] [[y ↦v ly]].
Proof. now destruct Γ as [ [ Γv Γm] Γt ]. Qed.
Global Hint Resolve env_up_v_m : core.

Lemma env_up_v_t Γ y ly m d: Γ [[y ↦v ly]] [[m ↦t d]] = Γ [[m ↦t d]] [[y ↦v ly]].
Proof. now destruct Γ as [ [ Γv Γm] Γt ]. Qed.
Global Hint Resolve env_up_v_t : core.

Lemma sem_equal_join_compat Σ Σ' d d':
  sem_equal Σ Σ' ->
  sem_equal (Σ [[d ⊛ d']]) (Σ' [[d ⊛ d']]).
Proof.
  intros (H1&H2&H3). repeat split; simpl; trivial.
  do 2 (f_equal; trivial).
  apply FunctionalExtensionality.functional_extensionality. intro. apply H1.
Qed.
Global Hint Immediate sem_equal_join_compat : core.
(* end hide *)

(** * Semantic of Programs *)

(**  Semantic of programs (_Definition 15_) *)
(** ** Module paths *)
Reserved Notation "Σ '/' Γ '⊢mp' q '⋮' ( Δ ',' S )" (at level 40).
Inductive judgment_m_path : sem -> env -> m_path -> desc -> sem -> Type :=
| J_M_id : forall (Σ : sem) (Γ : env) x l,
    Σ / Γ ⊢mp M_id (x, l) ⋮ (envM Γ x, Σ)
(** Although in the paper the pre and post semantics are different, they can be proved
    to be equal in the case of simple module paths, so we make them equal by construction here. **)
| J_M_comp : forall Σ Γ x l (q : m_path) lv ls Δ,
    In (l, Δ) ls ->
    ρ Σ l = Some (IM (fst x)) ->
    Σ / Γ ⊢mp q ⋮ (Structural lv ls, Σ) ->
    Σ / Γ ⊢mp M_comp q x ⋮ (Δ, Σ)
where "Σ '/' Γ '⊢mp' q '⋮' ( Δ ',' S )" := (judgment_m_path Σ Γ q Δ S).

(** ** Extended module paths *)
Inductive judgment_em_path : sem -> env -> em_path -> desc -> sem -> Type :=
| J_EM_id : forall (Σ : sem) (Γ : env) (x : mloc),
    judgment_em_path Σ Γ x (envM Γ (fst x)) Σ
| J_EM_comp : forall Σ Σ' Γ x (q : em_path) lv ls Δ l,
    In (l, Δ) ls ->
    ρ Σ' l = Some (IM (fst x)) ->
    judgment_em_path Σ Γ q (Structural lv ls) Σ' ->
    judgment_em_path Σ Γ (EM_comp q x) Δ Σ'
| J_EM_app : forall Σ Σ' Σ'' Γ (q1 q2 : em_path) Δ1 Δ2 Δ1' (l : ℒ),
    judgment_em_path Σ Γ q1 (Functorial l Δ1 Δ2) Σ'' ->
    judgment_em_path Σ'' Γ q2 Δ1' Σ' ->
    judgment_em_path Σ Γ (EM_app q1 q2) Δ2 (sem_join Σ' Δ1 Δ1').
Notation "Σ '/' Γ '⊢emp' σ '⋮' ( Δ ',' S )" := (judgment_em_path Σ Γ σ Δ S) (at level 40).

(** ** Value Expressions *)
Reserved Notation "Σ '/' Γ '⊢v' e '⇝' S" (at level 40, S at level 40).
Inductive judgment_v : sem -> env -> v_exp -> sem -> Type :=
| J_V_id : forall Σ Γ v l,
    let l' := envV Γ v in
    Σ / Γ ⊢v V_id (v, l) ⇝ Σ[[ (l, v) ↦l l']]
| J_V_field : forall Σ Γ v l p lv ls l',
      In l' lv ->
      Σ / Γ ⊢mp p ⋮ (Structural lv ls, Σ) ->
      ρ Σ l' = Some (IV v) ->
    Σ / Γ ⊢v p ⋅ (v, l) ⇝ Σ[[(l, v) ↦l Some l' ]]
| J_V_field_bot : forall Σ Γ v p l lv ls,
     Σ / Γ ⊢mp p ⋮ (Structural lv ls, Σ) ->
    (forall l', In l' lv -> ρ Σ l' <> Some (IV v)) ->
    Σ / Γ ⊢v p ⋅ (v, l) ⇝ Σ[[(l, v) ↦l None ]]
| J_V_const : forall Σ Γ c, Σ / Γ ⊢v V_const c ⇝ Σ
| J_V_let : forall Σ Σ' Σ'' Γ e1 e2 v l,
   Σ / Γ ⊢v e1 ⇝ Σ'' ->
   Σ''[[l ↦ IV v]] / Γ[[v ↦v Some l]] ⊢v e2 ⇝ Σ' ->
   Σ / Γ ⊢v V_let (v, l) e1 e2 ⇝ Σ'
| J_V_fun : forall Σ Σ' Γ v l e,
   Σ[[l ↦ IV v]] / Γ[[v ↦v Some l]] ⊢v e ⇝ Σ' ->
   Σ / Γ ⊢v V_fun (v, l) e ⇝ Σ'
| J_V_app : forall Σ Σ' Σ'' Γ e1 e2,
   Σ / Γ ⊢v e1 ⇝ Σ'' ->
   Σ'' / Γ ⊢v e2 ⇝ Σ' ->
   Σ / Γ ⊢v V_app e1 e2 ⇝ Σ'
| J_V_open : forall Σ Σ' Γ p e lv ls,
  let d := Structural lv ls in
   Σ / Γ ⊢mp p ⋮ (d, Σ) ->
   Σ / (p_sem_env (ρ Σ) d Γ) ⊢v e ⇝ Σ' ->
   Σ / Γ ⊢v (V_open p e) ⇝ Σ'
where "Σ / Γ ⊢v e ⇝ S" := (judgment_v Σ Γ e S).


Reserved Notation "Σ '/' Γ '⊢S' σ '⋮' ( Δ ',' S )" (at level 40, S at level 40).
Reserved Notation "Σ '/' Γ '⊢t' σ '⋮' ( Δ ',' S )" (at level 40, S at level 40).
Reserved Notation "Σ '/' Γ '⊢' σ '⋮' ( Δ ',' S )" (at level 40, S at level 40).
Reserved Notation "Σ '/' Γ '⊢s' σ '⋮' ( Δ ',' S )" (at level 40, S at level 40).
(** ** Module Types *)
Inductive judgment_t : sem -> env -> m_type -> desc -> sem -> Type :=
| J_MT_id : forall Σ Γ t l,
    Σ / Γ ⊢t MT_id (t, l) ⋮ (envT Γ t, Σ)
| J_MT_field : forall Σ Σ' Γ t lt p Δ lv ls l,
    In (l, Δ) ls ->
    ρ Σ' l = Some (IT t) ->
    Σ / Γ ⊢emp p ⋮ (Structural lv ls, Σ') ->
    Σ / Γ ⊢t MT_field p (t,lt) ⋮ (Δ, Σ')
| J_MT_sig : forall Σ Σ' Γ S Δ,
    Σ / Γ ⊢S S ⋮ (Δ, Σ') ->
    Σ / Γ ⊢t MT_sig S ⋮ (Δ, Σ')
| J_MT_extr : forall Σ Σ' Γ m Δ,
    Σ / Γ ⊢ m ⋮ (Δ, Σ') ->
    Σ / Γ ⊢t MT_extr m ⋮ (Δ, Σ')
| J_MT_functor : forall Σ Σ' Σ'' Γ Δ Δ' M1 M2 x l,
    Σ / Γ ⊢t M1 ⋮ (Δ, Σ'') ->
    Σ''[[ l ↦ IM x]] / Γ[[x ↦m Δ]] ⊢t M2 ⋮ (Δ', Σ')->
    Σ / Γ ⊢t MT_functor (x, l) M1 M2 ⋮ (Functorial l Δ Δ', Σ')
| J_MT_constr : forall Σ Σ' Σ'' Γ Δ Δ' M x l q,
    Σ / Γ ⊢t M ⋮ (Δ, Σ'') ->
    Σ'' [[l ↦ IM x]]/ Γ ⊢emp q ⋮ (Δ', Σ')->
    Σ / Γ ⊢t MT_constr M (x, l) q ⋮ (s_mod (ρ Σ') Δ x Δ',
                                     Σ' [[ Δ ⊛ Structural nil [(l, Δ')] ]])
| J_MT_dsubst : forall Σ Σ' Σ'' Γ Δ Δ' M x l q,
    Σ / Γ ⊢t M ⋮ (Δ, Σ'') ->
    Σ'' [[ l ↦ IM x]] / Γ ⊢emp q ⋮ (Δ', Σ')->
    Σ / Γ ⊢t MT_dsubst M (x, l) q ⋮ (desc_filtering (ρ Σ') Δ x,
                                     Σ' [[ Δ ⊛ Structural nil [(l, Δ')] ]])
where "Σ / Γ ⊢t t '⋮' ( Δ ',' S )" := (judgment_t Σ Γ t Δ S)
(** ** Signature Bodies *)
with judgment_S : sem -> env -> (list sig_comp) -> desc -> sem -> Type :=
| J_Snil : forall Σ Γ, Σ / Γ ⊢S nil ⋮ (empty_d, Σ)
| J_val : forall Σ Σ' Γ l v S lv ls,
    Σ[[ l ↦ IV v]] / Γ[[v ↦v Some l]] ⊢S S ⋮ (Structural lv ls, Σ')->
    Σ / Γ ⊢S (val (v, l) :_ ) :: S ⋮ (Structural [l] [] ⊕ρ Σ'# Structural lv ls,
                                      Σ' [[Structural [l] [] ⊛ Structural lv ls]])
| J_m : forall Σ Σ' Σ'' Γ l x Δ M S lv ls,
    Σ / Γ ⊢t M ⋮ (Δ, Σ'') ->
    Σ''[[ l ↦ IM x]] / Γ[[x ↦m Δ]] ⊢S S ⋮ (Structural lv ls, Σ')->
    Σ / Γ ⊢S (module (x, l) : M) :: S ⋮ (Structural [] [(l, Δ)] ⊕ρ Σ'#
                                        Structural lv ls, Σ')
| J_alias : forall Σ Σ' Γ l x Δ p S lv ls,
    Σ / Γ ⊢mp p ⋮ (Δ, Σ) ->
    Σ[[ l ↦ IM x]] / Γ[[x ↦m Δ]] ⊢S S ⋮ (Structural lv ls, Σ')->
    Σ / Γ ⊢S (Sig_alias (x, l) p) :: S ⋮ (Structural [] [(l, Δ)] ⊕ρ Σ'#
                                         Structural lv ls, Σ')
| J_abs : forall Σ Σ' Γ l t S lv ls,
    Σ[[l ↦ IT t]] / Γ[[t ↦t ∅]] ⊢S S ⋮ (Structural lv ls, Σ') ->
    Σ / Γ ⊢S (Sig_abs (t, l)) :: S ⋮ (Structural [] [(l, ∅)] ⊕ρ Σ'#
                                     Structural lv ls, Σ')
| J_concr : forall Σ Σ' Σ'' Γ l t Δ M S lv ls,
    Σ / Γ ⊢t M ⋮ (Δ, Σ'') ->
    Σ''[[ l ↦ IT t]] / Γ[[t ↦t Δ]] ⊢S S ⋮ (Structural lv ls, Σ')->
    Σ / Γ ⊢S (Sig_concr (t,l) M) :: S ⋮ (Structural [] [(l, Δ)] ⊕ρ Σ'#
                                     Structural lv ls, Σ')
| J_Sincl : forall Σ Σ' Σ'' Γ S lv ls lv' ls' M,
    let d := Structural lv ls in
    let d' := Structural lv' ls' in
    let Γ' := p_sem_env (ρ Σ'') d Γ in
    let DUd' := Structural lv ls ⊕ρ Σ'# Structural lv' ls' in
    Σ / Γ ⊢t M ⋮ (d, Σ'') ->
    Σ'' / Γ' ⊢S S ⋮ (d', Σ') ->
    Σ / Γ ⊢S (Sig_incl M) :: S ⋮ (DUd', Σ' [[Structural lv [] ⊛ Structural lv' [] ]])
| J_S_open : forall Σ Σ' Γ p S lv ls lv' ls',
    let d := Structural lv ls in
    let d' := Structural lv' ls' in
    let Γ' := p_sem_env (ρ Σ) d Γ in
    let DUd' := Structural lv ls ⊕ρ Σ'# Structural lv' ls' in
    Σ / Γ ⊢mp p ⋮ (d, Σ) ->
    Σ / Γ' ⊢S S ⋮ (d', Σ') ->
    Σ / Γ ⊢S (Sig_open p) :: S ⋮ (DUd', Σ' [[Structural lv [] ⊛ Structural lv' [] ]])
where "Σ '/' Γ '⊢S' s '⋮' ( Δ ',' S )" := (judgment_S Σ Γ s Δ S)

(** ** Module Expressions *)
with judgment_m_exp : sem -> env -> m_exp -> desc -> sem -> Type :=
| J_path : forall (Σ : sem) (Γ : env) (q : m_path) Δ,
    Σ / Γ ⊢mp q ⋮ (Δ, Σ) -> Σ / Γ ⊢ M_path q ⋮ (Δ, Σ)
| J_struct : forall Σ Σ' Γ (s : list str_comp) Δ,
    Σ / Γ ⊢s s ⋮ (Δ, Σ') ->
    Σ / Γ ⊢ M_struct s ⋮ (Δ, Σ')
| J_functor : forall Σ Σ' Σ'' Γ Δ Δ' M (m : m_exp) x l,
    Σ / Γ ⊢t M ⋮ (Δ, Σ'') ->
    Σ''[[ l ↦ IM x]] / Γ[[x ↦m Δ]] ⊢ m ⋮ (Δ', Σ')->
    Σ / Γ ⊢ functor ((x, l) : M) → m ⋮ (Functorial l Δ Δ', Σ')
| J_app : forall Σ Σ' Σ'' Γ m1 m2 Δ1 Δ2 Δ1' l,
    Σ / Γ ⊢ m1 ⋮ (Functorial l Δ1 Δ2, Σ'') ->
    Σ'' / Γ ⊢ m2 ⋮ (Δ1', Σ') ->
    Σ / Γ ⊢ M_app m1 m2 ⋮ (Δ2, Σ' [[ Δ1 ⊛ Δ1' ]])
| J_tannot : forall Σ Σ' Σ'' Γ m M Δ1 Δ2,
    Σ / Γ ⊢ m ⋮ (Δ1, Σ'') ->
    Σ'' / Γ ⊢t M ⋮ (Δ2, Σ') ->
    Σ / Γ ⊢ M_tannot m M ⋮ (Δ1  ⊳ ρ Σ'# Δ2, Σ' [[ Δ1 ⊛ Δ2 ]])
where "Σ '/' Γ '⊢' σ '⋮' ( Δ ',' S )" := (judgment_m_exp Σ Γ σ Δ S)

(** ** Structure Bodies *)
with judgment_s : sem -> env -> list str_comp -> desc -> sem -> Type :=
| J_snil : forall Σ Γ, Σ / Γ ⊢s nil ⋮ (empty_d, Σ)
| J_let : forall Σ Σ' Σ'' Γ l v s lv ls e,
    Σ / Γ ⊢v e ⇝ Σ'' ->
    Σ''[[ l ↦ IV v]] / Γ[[v ↦v Some l]] ⊢s s ⋮ (Structural lv ls, Σ') ->
    Σ / Γ ⊢s (vlet (v,l) = e) :: s ⋮ (Structural [l] nil ⊕ρ Σ'#
                                      Structural lv ls , Σ' [[ Structural [l] nil ⊛ Structural lv ls ]])
| J_let_ : forall Σ Σ' Σ'' Γ s lv ls e,
    Σ / Γ ⊢v e ⇝ Σ'' ->
    Σ'' / Γ ⊢s s ⋮ (Structural lv ls, Σ') ->
    Σ / Γ ⊢s (Str_let_ e) :: s ⋮ (Structural lv ls , Σ')
| J_mdef : forall Σ Σ' Σ'' Γ l x Δ m s lv ls,
    Σ / Γ ⊢ m ⋮ (Δ, Σ'') ->
    Σ''[[ l ↦ IM x]] / Γ[[x ↦m Δ]] ⊢s s ⋮ (Structural lv ls, Σ')->
    Σ / Γ ⊢s (Str_mdef (x, l) m) :: s ⋮ (Structural [] [(l, Δ)] ⊕ρ Σ'#
                                         Structural lv ls, Σ')
| J_mtdef : forall Σ Σ' Σ'' Γ l Δ t M s lv ls,
    Σ / Γ ⊢t M ⋮ (Δ, Σ'') ->
    Σ''[[ l ↦ IT t]] / Γ[[t ↦t Δ]] ⊢s s ⋮ (Structural lv ls, Σ')->
    Σ / Γ ⊢s (moduletype (t, l) = M) :: s ⋮ (Structural [] [(l, Δ)] ⊕ρ Σ'#
                                             Structural lv ls, Σ')
| J_incl : forall Σ Σ' Σ'' Γ s lv ls lv' ls' m,
    let d := Structural lv ls in
    let d' := Structural lv' ls' in
    let Γ' := p_sem_env (ρ Σ'') d Γ in
    let DUd' := Structural lv ls ⊕ρ Σ'# Structural lv' ls'  in
    Σ / Γ ⊢ m ⋮ (d, Σ'') ->
    Σ'' / Γ' ⊢s s ⋮ (d', Σ') ->
    Σ / Γ ⊢s (include m) :: s ⋮ (DUd', Σ'[[Structural lv [] ⊛ Structural lv' [] ]])
| J_open : forall Σ Σ' Γ p s lv ls lv' ls',
    let d := Structural lv ls in
    let d' := Structural lv' ls' in
    let Γ' := p_sem_env (ρ Σ) d Γ in
    let DUd' := Structural lv ls ⊕ρ Σ'# Structural lv' ls' in
    Σ / Γ ⊢mp p ⋮ (d, Σ) ->
    Σ / Γ' ⊢s s ⋮ (d', Σ') ->
    Σ / Γ ⊢s (Str_open p) :: s ⋮ (DUd', Σ' [[Structural lv [] ⊛ Structural lv' [] ]])
where "Σ '/' Γ '⊢s' s '⋮' ( Δ ',' S )" := (judgment_s Σ Γ s Δ S).

Global Hint Constructors judgment_t : core.
Global Hint Constructors judgment_S : core.
Global Hint Constructors judgment_m_exp : core.
Global Hint Constructors judgment_s : core.

(** ** Programs *)
Reserved Notation "Σ '/' Γ '⊢' e '⇝' S" (at level 40, S at level 40).
Inductive judgment_p : sem -> env -> prog -> sem -> Type :=
| J_exp : forall e Σ Σ' Γ,
    Σ / Γ ⊢v e ⇝ Σ' ->
    Σ / Γ ⊢ e ⇝ Σ'
| J_mod : forall Σ Σ' Σ'' Γ x Δ m l P,
    Σ / Γ ⊢ m ⋮ (Δ, Σ'') ->
    Σ''[[ l ↦ IM x]] / Γ[[x ↦m Δ]] ⊢ P ⇝ Σ'->
    Σ / Γ ⊢ module (x, l) = m ;; P ⇝ Σ'
where "Σ / Γ ⊢ e ⇝ S" := (judgment_p Σ Γ e S).
Global Hint Constructors judgment_p : core.

(** _Semantic inclusion *)
Definition sem_incl (Σ1 Σ2 : sem):=
  (forall l v, ρ Σ1 l = Some v -> ρ Σ2 l = Some v /\ (↣ Σ1 l = None -> ↣ Σ2 l = None)) /\
  (forall l l', ↣ Σ1 l = Some l' -> ↣ Σ2 l = Some l') /\
  (forall l l', In (l, l') (E Σ1) -> In (l, l') (E Σ2)).
Infix "⊆" := sem_incl (at level 40).

(* begin hide *)
Lemma sem_incl_cod Σ1 Σ2: Σ1 ⊆ Σ2 -> forall l, cod Σ1 l -> cod Σ2 l.
Proof. intros Hi l (v&Hv&Hb). exists v; split; eapply Hi; eauto. Qed.
Global Hint Resolve sem_incl_cod : core.

Lemma sem_incl_refl Σ: Σ ⊆ Σ.
Proof. repeat apply conj; intros l v H; tauto. Qed.
Global Hint Resolve sem_incl_refl : core.

Lemma sem_incl_trans Σ Σ' Σ'': Σ ⊆ Σ' -> Σ' ⊆ Σ'' -> Σ ⊆ Σ'' .
Proof.
intros H1 H2; repeat apply conj; intros l v Hl.
- apply H1 in Hl. destruct Hl as (Hl'&Hb').
  apply H2 in Hl'. destruct Hl'; tauto.
- now apply H2, H1.
- now apply H2, H1.
Qed.
Global Hint Resolve sem_incl_trans : core.

Add Parametric Relation: sem sem_incl
  reflexivity proved by sem_incl_refl
  transitivity proved by sem_incl_trans as sem_incl_rel.

Lemma sem_incl_up_ρ Σ l i: ρ Σ l = None -> Σ ⊆ (Σ[[ l ↦ i]]).
Proof.
intros Hn; repeat apply conj; intros l' v' H; simpl; trivial.
case (loc_eq_dec l l'); intro; subst; try tauto. rewrite Hn in H; discriminate H.
split; trivial.
apply find_mapsto_iff. apply find_mapsto_iff in H.
now apply LocMaps.add_2; trivial.
Qed.
Global Hint Resolve sem_incl_up_ρ : core.

Lemma sem_incl_up_br Σ l v l0: ρ Σ l = None -> ↣ Σ l = None -> Σ ⊆ (Σ[[( l , v ) ↦l l0]]).
Proof.
intros Hn Hn'; repeat apply conj; intros l1 l2 H12; simpl; trivial.
- unfold sr', br', br in *. simpl. rewrite add_o.
  case loc_eq_dec; intro; subst; [rewrite Hn in H12|]; try discriminate H12.
  split; trivial. intro H. now rewrite add_neq_o.
- unfold sr', br', br in *. simpl. rewrite add_o.
  case loc_eq_dec; intro; subst; [rewrite Hn' in H12|tauto]; discriminate H12.
Qed.
Global Hint Resolve sem_incl_up_br : core.

Lemma sem_incl_join Σ Δ1 Δ2: Σ ⊆ (Σ[[Δ1 ⊛ Δ2]]).
Proof. repeat apply conj; simpl; auto with *. Qed.
Global Hint Resolve sem_incl_join : core.

Lemma join_locs r Δ1 Δ2 l l': In (l, l') (Δ1 ⊗r# Δ2) ->
  In l (locs_d Δ1) /\ In l' (locs_d Δ2).
Proof.
revert Δ2; induction Δ1 using desc_ind3; unfold locs_d; simpl;
intros [lv' ls'| l'' Δ1' Δ2']; fold locs_d;
repeat rewrite app_nil_r; intro Hin; try inversion Hin;
repeat rewrite in_app_iff in *.
- destruct Hin as [Hin|Hin].
  + rewrite in_flat_map in Hin. destruct Hin as (l1&Hin1&Hin).
    rewrite in_flat_map in Hin. destruct Hin as (l2&Hin2&Hin).
    case option_eq_dec in Hin; inversion Hin; inversion H0. subst. tauto.
  + rewrite in_flat_map in Hin. destruct Hin as ((l1&d1)&Hin1&Hin).
    rewrite in_flat_map in Hin. destruct Hin as ((l2&d2)&Hin2&Hin).
    case option_eq_dec in Hin; [|inversion Hin]. eapply H in Hin; eauto.
    split; right; apply in_flat_map; eexists; split; eauto; tauto.
- destruct Hin as [Hin|Hin].
  + apply IHΔ1_1 in Hin. tauto.
  + apply IHΔ1_2 in Hin. tauto.
Qed.

Lemma desc_mod_locs D1 D2 r l: In l (locs_d (D1 ⊳r# D2)) -> In l (locs_d D1) \/ In l (locs_d D2).
Proof.
revert D2; induction D1 using desc_ind3; destruct D2; simpl; try tauto.
- repeat rewrite in_app_iff. intros [ [Hin|Hin]|Hin].
  + apply filter_In in Hin. tauto.
  + apply filter_In in Hin. tauto.
  + apply in_flat_map in Hin. destruct Hin as ((l'&d')&Hin&Hl').
    rewrite in_app_iff in Hin. destruct Hin as [Hin|Hin].
    * apply in_flat_map in Hin. destruct Hin as ((l''&d'')&Hin''&Hin).
      apply in_flat_map in Hin. destruct Hin as ((l'''&d''')&Hl'''&Hin).
      case option_eq_dec in Hin;inversion Hin;inversion H0; subst. clear H0.
      eapply H in Hl'; eauto. destruct Hl' as [Hl'|Hl'].
      -- left. right. apply in_flat_map. eauto.
      -- right. right. apply in_flat_map. eauto.
    * apply filter_In in Hin. destruct Hin as (Hin''&He).
      case Exists_dec in He;[discriminate He|clear He].
      right; right. apply in_flat_map. eauto.
- repeat rewrite in_app_iff. intros [H|H].
  + apply IHD1_1 in H. tauto.
  + apply IHD1_2 in H. tauto.
Qed.

Lemma s_mod_locs r x Δ l Δ' : In l (locs_d (Δ ⊲r# (x, Δ'))) ->
  In l (locs_d Δ) \/ In l (locs_d Δ').
Proof.
destruct Δ as [lv ls |]; simpl; try tauto.
repeat rewrite in_app_iff. intros [Hin|Hin]; try tauto.
apply in_flat_map in Hin. destruct Hin as ((l'&d')&H').
rewrite in_map_iff in H'. destruct H' as (((l''&d'')&H''&Hin'')&Hin').
case option_eq_dec in H''.
- inversion H''; subst. apply desc_mod_locs in Hin'. destruct Hin'; try tauto.
  left. right. apply in_flat_map. eexists; split;eauto.
- inversion H''; subst. clear H''. left; right; apply in_flat_map; eauto.
Qed.

Lemma desc_sup_sem_equal s s' d1 d2:
  sem_equal s s' -> d1 ⊕ ρ s # d2 = d1 ⊕ ρ s' # d2.
Proof.
intros (Heq1&_); destruct d1 as [lv ls|]; destruct d2 as [lv' ls'|]; try tauto.
unfold desc_sup. f_equal; f_equal.
- induction lv; simpl; trivial. rewrite IHlv.
  case Exists_dec; intro Heq; case Exists_dec; intro Heq'; simpl; trivial.
  + contradict Heq'. apply Exists_exists; apply Exists_exists in Heq.
    destruct Heq as (x&Hx1&Hx2). exists x; split; trivial.
    unfold sr' in *. now rewrite <- Heq1.
  + contradict Heq. apply Exists_exists; apply Exists_exists in Heq'.
    destruct Heq' as (x&Hx1&Hx2). exists x; split; trivial.
    unfold sr' in *. now rewrite Heq1.
- induction ls as [|(l&d) ls]; trivial. simpl. rewrite IHls.
  case Exists_dec; intro Heq; case Exists_dec; intro Heq'; simpl; trivial.
  + contradict Heq'. apply Exists_exists; apply Exists_exists in Heq.
    destruct Heq as (x&Hx1&Hx2). exists x; split; trivial.
    unfold sr' in *. now rewrite <- Heq1.
  + contradict Heq. apply Exists_exists; apply Exists_exists in Heq'.
    destruct Heq' as (x&Hx1&Hx2). exists x; split; trivial.
    unfold sr' in *. now rewrite Heq1.
Qed.

Lemma join_sem_equal s s' d1 d2:
  sem_equal s s' -> d1 ⊗ ρ s # d2 = d1 ⊗ ρ s' # d2.
Proof.
intros (Heq1&_); revert d2; induction d1 as [lv ls| l' d1' d2'] using desc_ind3;
intros [lv' ls'|]; try tauto; unfold join; f_equal; auto.
- induction lv; simpl; trivial. rewrite IHlv. f_equal. clear IHlv H.
  induction lv'; simpl; trivial. rewrite IHlv'. f_equal.
  unfold sr'; now repeat rewrite Heq1.
- induction ls as [|(l&d) ls]; simpl; trivial.
  rewrite IHls by (intros; eapply H; eauto with *).
  f_equal. clear IHls.
  induction ls' as [|(l'&d') ls']; simpl; trivial. rewrite IHls'.
  unfold sr'; repeat rewrite Heq1. fold join. erewrite H; eauto with *.
Qed.

Lemma desc_sup_locs r D1 D2 l: In l (locs_d (D1 ⊕r# D2)) -> In l (locs_d D1) \/ In l (locs_d D2).
Proof.
intro Hin. destruct D1 as [lv ls|]; destruct D2 as [lv' ls'|]; simpl in Hin; try tauto.
repeat rewrite in_app_iff in Hin. simpl. destruct Hin as [ [Hin|Hin]|Hin]; auto with *.
- rewrite filter_In in Hin. destruct Hin as (Hin&_). auto with *.
- apply in_flat_map in Hin. destruct Hin as ((l'&d')& Hin&Hin').
  apply in_app_iff in Hin. destruct Hin as [Hin|Hin].
  + right. apply in_app_iff. right. apply in_flat_map; eauto.
  + rewrite filter_In in Hin. destruct Hin as (Hin&_).
    left; apply in_app_iff; right; apply in_flat_map; eexists; split; eauto.
Qed.

Lemma desc_filtering_locs r Δ m l: In l (locs_d ((r \ Δ) m)) -> In l (locs_d Δ).
Proof.
destruct Δ as [lv ls |]; simpl; try tauto.
intro Hin. rewrite in_app_iff in *. destruct Hin as [Hin|Hin]; auto with *.
apply in_flat_map in Hin. destruct Hin as ((l'&d')&H'&Hin'). right.
rewrite filter_In in H'. destruct H' as (H'&_). apply in_flat_map.
eexists; split;eauto.
Qed.

Lemma wb_desc_incl d Σ Σ': Σ ⊆ Σ' -> wb_d Σ d -> wb_d Σ' d.
Proof.
intro Hi; subst.
induction d using desc_ind3; intro Hwb.
- inversion Hwb as [lv' ls' H1 H2 H3 H4 | ]; subst lv' ls'.
  constructor.
  + intros l Hl. eapply sem_incl_cod; eauto.
  + intros l d Hin. destruct (H2 l d Hin) as [Hwbd Hor]. split.
    * apply H with l; auto.
    * destruct Hor as [ [m HH ] | [t HH] ]; apply Hi in HH; intuition; eauto.
  + intros l l' Hl Hl' Heq'. destruct (H1 _ Hl) as (v&Hv&_).
    destruct (H1 _ Hl') as (v'&Hv'&_). apply H3; trivial.
    rewrite Hv, Hv'.
    apply Hi in Hv. destruct Hv as (Hv&_).
    apply Hi in Hv'. destruct Hv' as (Hv'&_).
    rewrite Hv, Hv' in Heq'. inversion Heq'. now subst.
  + intros [l d] [l' d'] Hl Hl' Heq. simpl in Heq. apply H4; trivial.
    simpl.
    destruct (H2 _ _ Hl) as [ _ [ [m HH] | [t HH] ] ];
    destruct (H2 _ _ Hl') as [ _ [ [m' HH'] | [m' HH'] ] ];
    rewrite HH, HH';
    apply Hi in HH; destruct HH as (HH&_);
    apply Hi in HH'; destruct HH' as (HH'&_);
    rewrite HH, HH' in Heq; inversion Heq; now subst.
- inversion Hwb; subst. constructor; tauto.
Qed.
Global Hint Resolve wb_desc_incl : core.

Lemma wb_desc_eq Σ1 Σ2 Δ: ρ Σ1 = ρ Σ2 -> ↣ Σ1 = ↣ Σ2 -> wb_d Σ2 Δ -> wb_d Σ1 Δ.
Proof.
intros He1 He2 Hwb. induction Δ using desc_ind3; inversion Hwb; subst.
- constructor; unfold cod in *; simpl; try rewrite He1; try rewrite He2; trivial.
  intros l d Hld; split; [|eapply H3; eauto].
  eapply H; eauto. eapply H3; eauto.
- constructor; tauto.
Qed.
Global Hint Resolve wb_desc_eq : core.

Lemma wb_desc_lv Σ lv ls: wb_d Σ (Structural lv ls) -> wb_d Σ (Structural lv nil).
Proof.
intro H; inversion H; subst. constructor; trivial;
intros a b Hab; inversion Hab.
Qed.
Global Hint Resolve wb_desc_lv : core.

Lemma wb_env_incl Γ Σ Σ': wb_env Γ Σ -> Σ ⊆ Σ' -> wb_env Γ Σ'.
Proof.
destruct Γ as [ [ Γv Γm] Γt ].
intros (H1&H2&H3&H4) Hincl.
repeat apply conj; simpl in *; auto.
- intros v l Hv. apply H2 in Hv. destruct Hv as (Hv&Hn).
  apply Hincl  in Hv. tauto.
- intro m; eapply wb_desc_incl; eauto.
- intro m; eapply wb_desc_incl; eauto.
Qed.
Global Hint Resolve wb_env_incl : core.

Lemma envV_env_up_v_eq v Γ l: envV(Γ [[ v ↦v l]]) v = l.
Proof.
destruct Γ as [ [ Γv Γm] Γt ]. simpl.
case val_eq_dec.
- intro Heq; inversion Heq; subst. tauto.
- tauto.
Qed.
Global Hint Resolve envV_env_up_v_eq : core.

Lemma envV_env_up_v_neq v v' Γ l: v <> v' -> envV(Γ [[ v ↦v l]]) v' = envV Γ v'.
Proof.
destruct Γ as [ [ Γv Γm] Γt ]. simpl.
case val_eq_dec.
- intro Heq; inversion Heq; subst. tauto.
- intro Hneq. intro; subst. tauto.
Qed.
Global Hint Resolve envV_env_up_v_neq : core.

Lemma wb_env_up_v Γ Σ v l:
    ρ Σ l = None -> ↣ Σ l = None -> wb_env Γ Σ -> wb_env (Γ [[v ↦v Some l]]) (Σ[[l ↦ IV v]]).
Proof.
destruct Γ as [ [ Γv Γm] Γt ].
intros Hnone Hn (H1&H2&H3&H4).
repeat apply conj.
- intros v' v''. simpl.
  case(val_eq_dec v v').
  + intro; subst v'. case(val_eq_dec v v''); trivial.
    intros Hneq Hsome _. symmetry in Hsome. apply H2 in Hsome.
    rewrite Hnone in Hsome; destruct Hsome; discriminate.
  + intros Hneq Heq Hnone0.
    case val_eq_dec as [Heq' | Hneq'] in Heq; auto.
    apply H2 in Heq. rewrite Hnone in Heq; destruct Heq; discriminate.
- intros v' v''. simpl.
  case(val_eq_dec v v').
  + intros Heq Heql; injection Heql; intros; subst v' v''.
    unfold sr', br', br in *. simpl. rewrite add_o.
    case loc_eq_dec; intro.
    * split; trivial.
    * tauto.
  + intros Hneq Heq. unfold sr', br', br in *. simpl. rewrite add_o.
    case loc_eq_dec; auto.
    intro; subst v''; apply H2 in Heq.
    rewrite Hnone in Heq; destruct Heq; discriminate.
- intros m. eapply wb_desc_incl with Σ; eauto.
- intros m. eapply wb_desc_incl; [apply sem_incl_up_ρ |]; eauto.
Qed.
(* end hide *)

(** D isjointness of term locations with semantics' domain and environment's range *)
Notation "Σ '⊔' L" := ((forall l, In l L -> ρ Σ l = None) /\
                       (forall l, In l L -> ↣ Σ l = None)) (at level 40).
Notation "Γ '⊓' L" := (forall l, In l L -> forall v', envV Γ v' <> Some l) (at level 40).

(* begin hide *)
Ltac disjtac :=
match goal with | H : (forall (_ : ℒ), _ -> ρ _ _ = None) /\
                      (forall (_ : ℒ), _ -> ↣ _ _ = None) |- _ =>
  split; intros; try apply H; try tauto; auto with *
end.

Lemma env_up_m_disj Γ l m Δ: Γ ⊓ l -> (Γ [[m ↦m Δ]]) ⊓ l.
Proof. destruct Γ as [ [ Γv Γm] Γt ];trivial. Qed.
Global Hint Resolve env_up_m_disj : core.

Lemma env_up_t_disj Γ l t Δ: Γ ⊓ l -> (Γ [[t ↦t Δ]]) ⊓ l.
Proof. destruct Γ as [ [ Γv Γm] Γt ];trivial. Qed.
Global Hint Resolve env_up_t_disj : core.

Lemma p_sem_env_disj Σ d Γ L: Σ ⊔ L -> Γ ⊓ L -> p_sem_env (ρ Σ) d Γ ⊓ L.
Proof. destruct Γ as [ [ Γv Γm] Γt ].
unfold p_sem_env. destruct d as [lv ls|]; trivial. simpl.
intros HS HG l Hin v'.
case_eq (find (var_eq_bool (Some (IV v')) ∘ (ρ Σ)) lv); auto.
intros l0 Hl Hf. inversion Hf; subst l0.
apply find_some in Hl. destruct Hl as (Hin'&Hl). apply var_eq_bool_true in Hl.
simpl in Hl. symmetry in Hl. rewrite (proj1 HS) in Hl; trivial. inversion Hl.
Qed.
Global Hint Resolve p_sem_env_disj : core.

Lemma env_up_v_disj: forall (Γ : env) (l : list ℒ) (v : V) l',
  ~ In l' l -> Γ ⊓ l -> (Γ [[v ↦v Some l']]) ⊓ l.
Proof.
intros Γ l v l' Hl' Hd l0 Hin0 v' Hv'.
case(val_eq_dec v v').
- intro; subst. rewrite envV_env_up_v_eq in Hv'. inversion Hv'; subst. now apply Hl'.
- intro. rewrite envV_env_up_v_neq in Hv'; try tauto. eapply Hd; eauto.
Qed.
Global Hint Resolve env_up_v_disj : core.

Lemma sem_up_v_disj: forall Σ l v l',
  ~ In l' l -> Σ ⊔ l -> (Σ [[l' ↦ v]]) ⊔ l.
Proof.
intros Σ l v l' Hl' Hd. disjtac.
simpl. unfold sr', br', br in *. simpl. rewrite add_o. case loc_eq_dec.
- intro; subst. tauto.
- intro. now apply Hd.
Qed.
Global Hint Resolve sem_up_v_disj : core.

Lemma sem_up_sr_cod Σ l v: ↣ Σ l = None -> cod (Σ [[l ↦ IV v]]) l.
Proof. exists v. simpl. unfold sr', br', br in *. simpl. rewrite add_o. case loc_eq_dec; tauto. Qed.
Global Hint Resolve sem_up_sr_cod : core.

Lemma sr_up_br Σ l l' l0 v:
  ρ (Σ [[ (l, v)↦l l']]) l0 = if loc_eq_dec l l0 then Some (IV v) else ρ Σ l0.
Proof. unfold sr', sem_up_br. simpl. apply add_o. Qed.

Lemma sr_up Σ l l0 v:
  ρ (Σ [[ l ↦ v]]) l0 = if loc_eq_dec l l0 then Some v else ρ Σ l0.
Proof. unfold sr', sem_up_br. simpl. apply add_o. Qed.

Lemma br_up_br Σ l l' l0 v:
  ↣ (Σ [[ (l, v)↦l l']]) l0 = if loc_eq_dec l l0 then Some l' else ↣ Σ l0.
Proof. unfold sr', sem_up_br. simpl. apply add_o. Qed.

Lemma br_up Σ l v:
  ↣ (Σ [[ l ↦ v]]) = ↣ Σ.
Proof. unfold sem_up_sr, br'. now simpl. Qed.

Ltac vidtac:=
  repeat rewrite br_up_br; repeat rewrite sr_up_br;
  try (simpl; intros l0 v0 H' H''; repeat rewrite sr_up_br in H'';
  try (case loc_eq_dec in H''; subst; trivial; [now inversion H''|]));
match goal with
| H1: ?x = None, H2: ?x = Some _ |- _ => rewrite H1 in H2; discriminate H2 end.

Lemma cod_sem_up_sr_v Σ l l' v: cod (Σ [[l' ↦ IV v]]) l <-> (cod Σ l \/ (l = l' /\ ↣ Σ l = None)).
Proof. unfold cod. rewrite br_up, sr_up. case loc_eq_dec; intuition; subst.
- destruct H as (v'&Hv'); intuition.
- destruct H0 as (v'&Hv'); intuition. eexists; eauto.
- eexists; eauto.
- tauto.
Qed.
Global Hint Resolve cod_sem_up_sr_v : core.

Lemma cod_sem_up_sr_m Σ l l' m: cod (Σ [[l' ↦ IM m]]) l <-> (cod Σ l /\ l <> l').
Proof. unfold cod. rewrite br_up, sr_up. case loc_eq_dec; intuition; subst.
- destruct H as (v'&Hv'); intuition. discriminate.
- destruct H as (v'&Hv'); intuition. discriminate.
- tauto.
Qed.
Global Hint Resolve cod_sem_up_sr_m : core.

Lemma cod_sem_up_sr_t Σ l l' t: cod (Σ [[l' ↦ IT t]]) l <-> (cod Σ l /\ l <> l').
Proof. unfold cod. rewrite br_up, sr_up. case loc_eq_dec; intuition; subst.
- destruct H as (v'&Hv'); intuition. discriminate.
- destruct H as (v'&Hv'); intuition. discriminate.
- tauto.
Qed.
Global Hint Resolve cod_sem_up_sr_t : core.

Lemma cod_sem_up_br Σ l l' l0 v: cod (Σ [[(l', v ) ↦l l]]) l0 <-> (cod Σ l0 /\ l0 <> l').
Proof. unfold cod. rewrite br_up_br, sr_up_br. case loc_eq_dec; intuition; subst.
- destruct H as (v'&Hv'); intuition. discriminate.
- destruct H as (v'&Hv'); intuition. discriminate.
- tauto.
Qed.
Global Hint Resolve cod_sem_up_br : core.

Lemma cod_eqt_eq Σ Σ': ρ Σ = ρ Σ' -> ↣ Σ = ↣ Σ' ->
  (forall l, cod Σ l <-> cod Σ' l).
Proof. intros H1 H2 l. unfold cod; now rewrite H1, H2. Qed.

Lemma sem_up_disj Σ l L m: ~ In l L -> Σ ⊔ L -> (Σ [[l ↦ m]]) ⊔ L.
Proof.
intros Hnin H; split; intros x Hx; simpl; unfold sr', br', br in *; simpl; try rewrite add_o; try case loc_eq_dec; trivial.
- intro; subst. tauto.
- intros _. now apply H.
- now apply H in Hx.
Qed.
Global Hint Resolve sem_up_disj : core.

Lemma wb_empty r : wb_d r ∅.
Proof.
constructor; intros;
  match goal with |[ H: In ?a ?b |- _] => inversion H end.
Qed.
Global Hint Resolve wb_empty : core.

Lemma wb_desc_mod Σ Δ Δ': wb_d Σ Δ -> wb_d Σ Δ' -> wb_d Σ (Δ ⊳ρ Σ# Δ').
Proof.
intros H H'.
revert Δ H.
induction Δ' as [lv' ls' Hind'| l' Δ1' HΔ1' Δ2'] using desc_ind3;
destruct Δ as [lv ls | l Δ1 Δ2]; intro H; simpl; auto.
- inversion H as [lv0 ls0 H1 H2 H3 H4 |]; subst lv0 ls0.
  inversion H' as [lv0 ls0 H1' H2' H3' H4' |]; subst lv0 ls0.
  constructor.
  + intros l Hin; apply in_app_or in Hin; destruct Hin as [Hlv | Hlv]; auto;
    apply filter_In in Hlv; destruct Hlv as (Hin&Hfilter); auto.
  + intros l Δl Hin.
    apply in_app_or in Hin; destruct Hin as [Hin | Hin].
    * apply in_flat_map in Hin; destruct Hin as [ [l' Δl'] [Hin' Hin] ].
      apply in_flat_map in Hin; destruct Hin as [ [l'' Δl''] [Hin'' Hin] ].
      case option_eq_dec in Hin; inversion Hin; inversion H0; subst.
      split.
      -- eapply Hind'; [ | eapply H2' | eapply H2 ]; eauto.
      -- eapply H2; eauto.
    * apply filter_In in Hin; destruct Hin as (Hin&Hfilter); auto.
  + intros v v' Hin Hin' Heq.
    apply in_app_or in Hin; apply in_app_or in Hin'.
    destruct Hin as [Hfilter | Hfilter];
    (apply filter_In in Hfilter; destruct Hfilter as (Hin&Hfilter));
    (destruct Hin' as [Hfilter' | Hfilter'];
     apply filter_In in Hfilter'; destruct Hfilter' as (Hin'&Hfilter')).
    * case Exists_dec in Hfilter'; [clear Hfilter'|inversion Hfilter'].
      case Exists_dec in Hfilter; [clear Hfilter|inversion Hfilter]. auto.
    * case Exists_dec in Hfilter'; [inversion Hfilter'|clear Hfilter'].
      case Exists_dec in Hfilter; [clear Hfilter|inversion Hfilter].
      apply Exists_exists in e. destruct e as (x&Hin1&Hin2).
      apply Forall_Exists_neg in n. rewrite Forall_forall in n.
      symmetry in Heq; contradict (n v Hin Heq).
    * case Exists_dec in Hfilter'; [clear Hfilter'|inversion Hfilter'].
      case Exists_dec in Hfilter; [inversion Hfilter|clear Hfilter].
      apply Exists_exists in e. destruct e as (x&Hin1&Hin2).
      apply Forall_Exists_neg in n. rewrite Forall_forall in n.
      contradict (n _ Hin' Heq).
    * apply H3'; trivial.
  + intros [l d] [l' d'] Hin Hin' Heq.
    apply in_app_or in Hin; apply in_app_or in Hin'.
    destruct Hin as [Hls | Hls].
    * apply in_flat_map in Hls; destruct Hls as [ [l'' Δl''] [Hls' Hin] ].
      apply in_flat_map in Hin; destruct Hin as [ [l''' Δl'''] [Hin'' Hin] ].
      case option_eq_dec in Hin; inversion Hin; inversion H0; subst; clear H0 Hin.
      destruct Hin' as [Hls | Hls].
      -- apply in_flat_map in Hls; destruct Hls as [ [l1 Δl1] [Hin Hls] ].
         apply in_flat_map in Hls; destruct Hls as [ [l2 Δl2] [Hin' Hls] ].
         case option_eq_dec in Hls; inversion Hls; inversion H0; subst.
         clear H0 Hls.
         assert (HH : (ρ Σ) (fst (l, Δl'')) = (ρ Σ) (fst (l', Δl1))) by trivial.
         apply H4 in HH; trivial; inversion HH; subst l' Δl''; clear HH Heq.
         f_equal.
         assert (HH : (ρ Σ) (fst (l''', Δl''')) = (ρ Σ) (fst (l2, Δl2)))
           by (simpl; rewrite <- e0; auto).
         apply H4' in HH; trivial; inversion HH; trivial; subst l2 Δl2; clear HH e0.
      -- apply filter_In in Hls; destruct Hls as (Hin&Hfilter);
         (case Exists_dec in Hfilter; [ inversion Hfilter | clear Hfilter]);
         apply Forall_Exists_neg in n; rewrite Forall_forall in n;
         simpl in Heq; rewrite e in Heq; apply False_ind; eapply n; eauto.
         simpl; rewrite <- Heq; auto.
    * apply filter_In in Hls; destruct Hls as (Hin&Hfilter);
      (case Exists_dec in Hfilter; [ inversion Hfilter | clear Hfilter]);
      apply Forall_Exists_neg in n; rewrite Forall_forall in n.
      destruct Hin' as [Hin' | Hin'].
      --  apply in_flat_map in Hin'; destruct Hin' as [ [l'' Δl''] [Hls' Hin'] ].
          apply in_flat_map in Hin'; destruct Hin' as [ [l''' Δl'''] [Hin'' Hin'''] ].
          case option_eq_dec in Hin'''; inversion Hin'''; inversion H0; subst.
          clear H0 Hin'''. simpl in *.
          now edestruct (n _ Hls' ).
      --  apply filter_In in Hin'; destruct Hin' as (Hin'&Hfilter).
          case Exists_dec in Hfilter; [inversion Hfilter |clear Hfilter].
          apply Forall_Exists_neg in n0; rewrite Forall_forall in n0.
          eapply H4'; eauto.
- inversion H; inversion H'; subst. constructor; (apply HΔ1' || apply IHΔ2'); trivial.
Qed.
Global Hint Resolve wb_desc_mod : core.

Lemma wb_s_mod Σ Δ Δ' x: wb_d Σ Δ -> wb_d Σ Δ' ->
  wb_d Σ (Δ ⊲ (ρ Σ) # (x, Δ')).
Proof.
intros [ lv ls H1 H2 H3 H4 | l Δ1 Δ2 H1 H2] H; simpl.
- constructor; trivial.
  + intros l d Hin; apply in_map_iff in Hin.
    destruct Hin as [ [l' d'] [Hl' Hin] ].
    case option_eq_dec in Hl'; inversion Hl'; subst;
    (eapply H2 in Hin; split); [apply wb_desc_mod; tauto | | |]; tauto.
  + intros [l1 d1] [l2 d2] Hin1 Hin2 Heq;
    apply in_map_iff in Hin1; apply in_map_iff in Hin2.
    destruct Hin1 as [ [l1' d1'] H1'].
    destruct Hin2 as [ [l2' d2'] H2'].
    case option_eq_dec in H1'; inversion H1'; inversion H0; subst; clear H0;
    case option_eq_dec in H2'; inversion H2'; inversion H0; subst; clear H0.
    * assert (Heq0 : (ρ Σ) (fst (l1, d1')) = (ρ Σ) (fst (l2, d2')))
      by (simpl; rewrite e0; auto).
      eapply H4 in Heq0; trivial.
      inversion Heq0; now subst.
    * simpl in Heq; apply False_ind, n; rewrite <- e; auto.
    * simpl in Heq; apply False_ind, n; rewrite <- e; auto.
    * apply H4; trivial.
- apply wb_empty.
Qed.
Global Hint Resolve wb_s_mod : core.

Lemma wb_desc_filtering Σ Δ m: wb_d Σ Δ -> wb_d Σ (((ρ Σ) \ Δ) m).
Proof.
intros H.
destruct Δ as [lv ls|]; trivial.
simpl.
inversion H as [lv' ls' H1 H2 H3 H4|]; subst.
constructor; trivial.
- intros l d Hin.
  apply filter_In in Hin.
  destruct Hin as (Hin&Hc).
  case option_eq_dec in Hc; inversion Hc.
  auto.
- intros [l d] [l' d'] Hin Hin'.
  apply filter_In in Hin; destruct Hin as (Hin&Hc).
  case option_eq_dec in Hc; inversion Hc.
  apply filter_In in Hin'; destruct Hin' as (Hin'&Hc').
  case option_eq_dec in Hc'; inversion Hc'.
  auto.
Qed.
Global Hint Resolve wb_desc_filtering : core.

Lemma wb_desc_sup Σ lv ls lv' ls':
  wb_d Σ (Structural lv ls) ->
  wb_d Σ (Structural lv' ls') ->
    wb_d Σ (Structural lv ls ⊕ (ρ Σ) # Structural lv' ls').
Proof.
intros H H'.
inversion H as [lv0 ls0 H1 H2 H3 H4 |]; subst lv0 ls0.
inversion H' as [lv0 ls0 H1' H2' H3' H4' |]; subst lv0 ls0.
simpl.
constructor.
- intros l Hin.
  apply in_app_or in Hin; destruct Hin as [Hin | Hin]; auto.
  apply filter_In in Hin; destruct Hin as [Hin Hc]; auto.
- intros l d Hin.
  apply in_app_or in Hin; destruct Hin as [Hin | Hin]; auto.
  apply filter_In in Hin; destruct Hin as [Hin Hc]; auto.
- intros v v' Hin Hin'.
  apply in_app_or in Hin; destruct Hin as [Hin | Hin];
  apply in_app_or in Hin'; destruct Hin' as [Hin' | Hin']; auto;
  try (apply filter_In in Hin; destruct Hin as [Hin Hc]); auto;
  try (apply filter_In in Hin'; destruct Hin' as [Hin' Hc']); auto.
  + revert Hc'; case Exists_dec; rewrite Exists_exists; intros Hc Hf.
    * inversion Hf.
    * intuition; destruct Hc; eauto.
  + revert Hc; case Exists_dec; rewrite Exists_exists; intros Hc' Hf.
    * inversion Hf.
    * intuition; destruct Hc'; eauto.
- intros [l d] [l' d'] Hin Hin'.
  apply in_app_or in Hin; destruct Hin as [Hin | Hin];
  apply in_app_or in Hin'; destruct Hin' as [Hin' | Hin']; auto;
  try (apply filter_In in Hin; destruct Hin as [Hin Hc]); auto;
  try (apply filter_In in Hin'; destruct Hin' as [Hin' Hc']); auto.
  + revert Hc'; case Exists_dec; rewrite Exists_exists; intros Hc Hf.
    * inversion Hf.
    * intro Heq; destruct Hc; eauto.
  + revert Hc; case Exists_dec; rewrite Exists_exists; intros Hc' Hf.
    * inversion Hf.
    * intuition; destruct Hc'; eauto.
Qed.
Global Hint Resolve wb_desc_sup : core.

Lemma wb_env_up_m Σ Γ x Δ:
  wb_d Σ Δ -> wb_env Γ Σ -> wb_env (Γ [[x ↦m Δ]]) Σ.
Proof.
destruct Γ as [ [ Γv Γm] Γt ].
intros Hwb (H1&H2&H3&H4).
repeat apply conj; trivial.
clear H1 H2 H4. simpl in *.
intro m. case mod_eq_dec; intro; subst; trivial.
Qed.
Global Hint Resolve wb_env_up_m : core.

Lemma wb_env_up_t Γ Σ t Δ:
  wb_d Σ Δ -> wb_env Γ Σ -> wb_env (Γ [[t ↦t Δ]]) Σ.
Proof.
destruct Γ as [ [ Γv Γm] Γt ].
intros Hwb (H1&H2&H3&H4).
repeat apply conj; trivial.
clear H1 H2 H3. simpl in *.
intro m. case typ_eq_dec; intro; subst; trivial.
Qed.
Global Hint Resolve wb_env_up_t : core.

Lemma cod_join Σ Δ1 Δ2 l: cod Σ l <-> cod (Σ [[Δ1 ⊛ Δ2]]) l.
Proof. tauto. Qed.
Global Hint Rewrite cod_join : core.

Lemma cod_not_None Σ l : cod Σ l -> ρ Σ l <> None.
Proof. intros(v&Hv&_). now rewrite Hv. Qed.
Global Hint Resolve cod_not_None : core.

Lemma wb_desc_join Σ Δ Δ1 Δ2: wb_d Σ Δ -> wb_d (Σ [[Δ1 ⊛ Δ2]]) Δ.
Proof.
intro H. induction Δ using desc_ind3.
- inversion H as [lv' ls' H1 H2 H3 H4|]; subst.
  constructor; simpl; trivial.
  intros l d Hld. destruct (H2 _ _ Hld) as (Hwd&Hr). eauto.
- inversion H; subst. constructor; tauto.
Qed.
Global Hint Resolve wb_desc_join : core.

Lemma wb_env_join Γ Σ Δ1 Δ2: wb_env Γ Σ -> wb_env Γ (Σ [[Δ1 ⊛ Δ2]]).
Proof.
destruct Γ as [ [ Γv Γm] Γt ].
intros (H1&H2&H3&H4).
repeat apply conj; trivial; clear H1 H2; simpl in *; intro x;
now apply wb_desc_join.
Qed.
Global Hint Resolve wb_env_join : core.

Lemma wb_singleton_lv Σ l: cod Σ l -> wb_d Σ (Structural [l] []).
Proof.
constructor.
- intros l0 Hin; destruct Hin as [Hin | Hin]; [| inversion Hin]; subst; eauto.
- intros l0 d Hin. inversion Hin.
- intros v1 v2 H1 H2.
  destruct H1 as [H1 | H1]; [| inversion H1];
  destruct H2 as [H2 | H2]; [| inversion H2].
  now subst.
- intros d d' Hin; inversion Hin.
Qed.
Global Hint Resolve wb_singleton_lv : core.

Lemma wb_singleton_ls Σ l Δ: wb_d Σ Δ ->
  (exists m : M, ρ Σ l = Some (IM m)) \/ (exists t : T, (ρ Σ) l = Some (IT t)) ->
    wb_d Σ (Structural [] [(l, Δ)]).
Proof.
intros Hwb Hsome.
constructor.
- intros l0 Hin. inversion Hin.
- intros l0 d Hin; destruct Hin as [Hin | Hin]; inversion Hin; subst; eauto.
- intros v v' Hin; inversion Hin.
- intros d1 d2 H1 H2.
  destruct H1 as [H1 | H1]; inversion H1; destruct H2 as [H2 | H2]; inversion H2.
  now subst.
Qed.
Global Hint Resolve wb_singleton_ls : core.
(* end hide *)

(** Properness (_Definition 19_) *)
Definition proper (Σ : sem) : Prop :=
(** _(i) **)   (forall l l', ↣ Σ l' = Some (Some l) -> ↣ Σ l = None) /\
(** _(ii) **)  (forall l, (↣ Σ l <> None \/ exists l', ↣ Σ l' = Some (Some l)) ->
                   exists v, ρ Σ l = Some (IV v)) /\
(** _(iv) **)  (forall l l', In (l, l') (E Σ) -> (↣ Σ l = None /\ ↣ Σ l' = None /\
                  exists v, ρ Σ l = Some (IV v) /\ ρ Σ l' = Some (IV v))) /\
(** _(iii) **) (forall l l', ↣ Σ l = Some (Some l') -> ρ Σ l = ρ Σ l').

Lemma proper_sem_up_l Σ l l' v:
  ρ Σ l = Some (IV v) -> ρ Σ l' = Some (IV v) -> ρ Σ l = None -> proper Σ ->
    proper (Σ [[(l, v) ↦l Some l']]).
Proof.
intros Hl Hl' Hnone (H1&H2&H3&H4); repeat apply conj; simpl in *.
- rewrite Hnone in Hl; discriminate.
- intros l0. unfold sr', br', br in *. simpl. rewrite add_o. case loc_eq_dec.
  + intros; subst; clear Hl. unfold sr', br', br in *. simpl.
    rewrite add_eq_o; eauto.
  + intros Hneq [H|(l''&Hl'')].
    * rewrite add_neq_o; eauto.
    * rewrite add_o in Hl''. rewrite add_neq_o; trivial.
      case loc_eq_dec in Hl''; eauto.
      inversion Hl''; subst. eauto.
- intros l1 l2 Hl0.
  destruct (H3 _ _ Hl0) as (v'&Hv'1&(v''&Hv''1&Hv''2)).
  unfold br', br, sr', sr in *. simpl. repeat rewrite add_o.
  case loc_eq_dec; intro; subst;
  case loc_eq_dec; intro; subst; eauto; simpl in *;
  rewrite Hnone in *; discriminate.
- intros l1 l2. unfold br', br, sr', sr. simpl. repeat rewrite add_o.
  case loc_eq_dec; intro; subst.
  + intro Heq; inversion Heq; subst; clear Heq.
    case loc_eq_dec; intro; subst; auto.
  + intro Hl0. unfold sr' in H4. rewrite (H4 _ _ Hl0).
    case loc_eq_dec; trivial. intro; subst l2; trivial.
Qed.
Global Hint Resolve proper_sem_up_l : core.

Lemma join_values Σ Δ1 Δ2 l l':
  wb_d Σ Δ1 -> wb_d Σ Δ2 -> In (l, l') (Δ1 ⊗ ρ Σ # Δ2) ->
    (↣ Σ l = None /\ ↣ Σ l' = None /\
    exists v : V, ρ Σ l = Some (IV v) /\ ρ Σ l' = Some (IV v)).
Proof.
revert Δ2. induction Δ1 using desc_ind3; intros Δ2 H1 H2 Hj.
- inversion H1 as [lv' ls' Hw1 Hw2 _ _ |]; subst lv' ls'.
  destruct Δ2 as [lv1 ls2| l2 D1 D2]; simpl in Hj; [|tauto].
  inversion H2 as [lv' ls' Hw1' Hw2' _ _ |]; subst lv' ls'.
  rewrite in_app_iff in Hj; destruct Hj as [Hjv| Hjs].
  + rewrite in_flat_map in Hjv; destruct Hjv as (l0&Hl0&Hjv).
    rewrite in_flat_map in Hjv; destruct Hjv as (l0'&Hl0'&Hjv).
    case option_eq_dec in Hjv; [|inversion Hjv].
    destruct Hjv as [Hjv|Hjv]; inversion Hjv; subst. clear Hjv.
    apply Hw1 in Hl0. apply Hw1' in Hl0'. destruct Hl0' as (v&Hv&Hb).
    rewrite e, Hv; eauto. repeat split; eauto.
    destruct Hl0; tauto.
  + rewrite in_flat_map in Hjs; destruct Hjs as (l0&Hl0&Hjv).
    rewrite in_flat_map in Hjv; destruct Hjv as (l0'&Hl0'&Hjv).
    destruct l0 as [l0 d0]; destruct l0' as [l0' d0'].
    case option_eq_dec in Hjv; [|inversion Hjv].
    eapply H in Hjv;[trivial|eauto|eapply Hw2 | eapply Hw2']; eauto.
- inversion H1 as [| l0' D1 D2 HD1 HD2]; subst l0' D1 D2.
  destruct Δ2 as [| l0' D1 D2]; [inversion Hj|]. simpl in Hj.
  inversion H2.
  rewrite in_app_iff in Hj; destruct Hj as [Hl|Hr].
  + now apply IHΔ1_1 in Hl.
  + now apply IHΔ1_2 in Hr.
Qed.

Lemma proper_sem_join Σ Δ1 Δ2:
  wb_d Σ Δ1 -> wb_d Σ Δ2 -> proper Σ -> proper (Σ [[Δ1 ⊛ Δ2]]).
Proof.
intros H1 H2 (Hp1&Hp2&Hp3). repeat apply conj; simpl; trivial.
- intros l l'. rewrite in_app_iff. intros [HE|Hj].
  + apply join_values in HE; trivial.
  + now apply Hp3.
- intros l l' H. now apply Hp3.
Qed.
Global Hint Resolve proper_sem_join : core.

Lemma proper_up_br Σ Γ l v:
  ρ Σ l = None -> ↣ Σ l = None -> wb_env Γ Σ -> proper Σ ->
    proper (Σ [[ (l, v)↦l envV Γ v]]).
Proof.
intros Hl1 Hl2 Henv (H1&H2&H3&H4).
repeat apply conj; simpl in *.
- intros l1 l2. unfold br', br, sr', sr. simpl. repeat rewrite add_o.
  case loc_eq_dec; intro; subst; case loc_eq_dec; intro; subst; eauto.
  + intro H; inversion H as [H']; apply Henv in H'.
    destruct H' as (H'&_); rewrite H' in Hl1; discriminate.
  + intro H; inversion H as [H']; apply Henv in H'. tauto.
  + intro H. erewrite <- H4 in Hl1; eauto.
    destruct (H2 l2) as (v'&H');[unfold br'; rewrite H; left; discriminate|].
    rewrite H' in Hl1; discriminate.
- intro l'. unfold br', br, sr', sr. simpl. repeat rewrite add_o.
  case loc_eq_dec; eauto. unfold sr', br' in *.
  intros Hneq [Hb | (l''&Hl'')]; auto.
  rewrite add_o in Hl''. case loc_eq_dec in Hl''; eauto.
  subst. inversion Hl''. apply Henv in H0. intuition; eauto.
- intros l1 l2 HE. clear H1 H2 H4.
  unfold br', br, sr', sr. simpl. repeat rewrite add_o.
  case loc_eq_dec; intro; subst; case loc_eq_dec; intro; subst; eauto;
  apply H3 in HE; rewrite Hl1 in HE; destruct HE as (Hb1&Hb2&v'&Hv1&Hv2); discriminate.
- intros l1 l2. unfold br', br, sr', sr. simpl. repeat rewrite add_o.
  case loc_eq_dec; intro; subst; case loc_eq_dec; intro; subst; eauto.
  + intro Heq; inversion Heq. apply Henv in H0. intuition; auto.
  + intro Hb. destruct (H2 l1); [unfold br'; rewrite Hb; left; discriminate|].
    apply H4 in Hb. rewrite Hb, Hl1 in H; discriminate.
Qed.
Global Hint Resolve proper_up_br : core.

Lemma proper_up_br_some Σ l v l':
  ρ Σ l = None -> ↣ Σ l = None -> ↣ Σ l' = None -> ρ Σ l' = Some (IV v) -> proper Σ ->
    proper (Σ [[ (l, v)↦l Some l']]).
Proof.
intros Hl1 Hl2 Hn' Hl' (H1&H2&H3&H4).
repeat apply conj; simpl in *.
- intros l1 l2. unfold br', br, sr', sr. simpl. repeat rewrite add_o.
  case loc_eq_dec; intro; subst; case loc_eq_dec; intro; subst; eauto.
  + intro H; inversion H as [H']. subst. rewrite Hl' in Hl1; discriminate.
  + intro H; inversion H as [H']. now subst.
  + intro H. erewrite <- H4 in Hl1; eauto.
    destruct (H2 l2) as (v'&H'); [unfold br'; rewrite H; left; discriminate|].
    rewrite H' in Hl1; discriminate.
- intro l0. unfold br', br, sr', sr. simpl. repeat rewrite add_o.
  case loc_eq_dec; eauto.
  intros Hneq [Hb | (l''&Hl'')]; auto.
  + eapply H2; eauto.
  + rewrite add_o in Hl''. case loc_eq_dec in Hl''; eauto.
    subst. inversion Hl''. subst. eauto.
- intros l1 l2 HE. clear H1 H2 H4. unfold br', br, sr', sr. simpl. repeat rewrite add_o.
  case loc_eq_dec; intro; subst; case loc_eq_dec; intro; subst; eauto;
  apply H3 in HE; rewrite Hl1 in HE; destruct HE as (_&_&v'&Hv1&Hv2); discriminate.
- intros l1 l2. unfold br', br, sr', sr. simpl. repeat rewrite add_o.
  case loc_eq_dec; intro; subst; case loc_eq_dec; intro; subst; eauto.
  + intro Heq; inversion Heq. subst. auto.
  + intro Hb. destruct (H2 l1); [unfold br'; rewrite Hb; left; discriminate|].
    apply H4 in Hb. rewrite Hb, Hl1 in H; discriminate.
Qed.
Global Hint Resolve proper_up_br_some : core.

Lemma proper_up_br_none Σ l v:
  ρ Σ l = None -> ↣ Σ l = None -> proper Σ ->
    proper (Σ [[ (l, v)↦l None]]).
Proof.
intros Hl1 Hl2 (H1&H2&H3&H4).
repeat apply conj; simpl in *.
- intro l0. unfold br', br, sr', sr. simpl. repeat rewrite add_o.
  case loc_eq_dec; intro; subst; intros l';
  rewrite add_o; case loc_eq_dec; eauto.
  + intros; discriminate.
  + intros Hd H. erewrite <- H4 in Hl1; eauto.
    destruct (H2 l') as (v'&H'); [unfold br'; rewrite H; left; discriminate|].
    rewrite H' in Hl1; discriminate.
  + intros; subst; discriminate.
- intro l0. unfold br', br, sr', sr. simpl. repeat rewrite add_o.
  case loc_eq_dec; eauto.
  intros Hneq [Hb | (l''&Hl'')]; auto.
  + apply H2. eauto.
  + rewrite add_o in Hl''. case loc_eq_dec in Hl''; eauto.
    subst. inversion Hl''.
- intros l1 l2 HE. clear H1 H2 H4.
  unfold br', br, sr', sr. simpl. repeat rewrite add_o.
  case loc_eq_dec; intro; subst; case loc_eq_dec; intro; subst; eauto;
  apply H3 in HE; rewrite Hl1 in HE; destruct HE as (_&_&v'&Hv1&Hv2); discriminate.
- intros l1 l2. unfold br', br, sr', sr. simpl. repeat rewrite add_o.
  case loc_eq_dec; intro; subst; case loc_eq_dec; intro; subst; eauto.
  + intro Heq; inversion Heq.
  + intro Hb. destruct (H2 l1); [unfold br'; rewrite Hb; left; discriminate|].
    apply H4 in Hb. rewrite Hb, Hl1 in H; discriminate.
Qed.
Global Hint Resolve proper_up_br_none : core.

(* begin hide *)
Ltac case_loc_eq_dec :=
  unfold br', br, sr', sr; simpl; repeat rewrite add_o; case loc_eq_dec.
(* end hide *)

Lemma proper_up Σ l i:
  ρ Σ l = None -> ↣ Σ l = None -> proper Σ ->
    proper (Σ [[ l ↦ i]]).
Proof.
intros Hl1 Hl2 (H1&H2&H3&H4).
repeat apply conj; simpl in *.
- trivial.
- intro l0. case_loc_eq_dec; eauto.
  intros Hneq [Hb | (l''&Hl'')]; subst; [tauto|].
  destruct(H2 l''); [unfold br'; left; rewrite Hl''; discriminate|].
  apply H4 in Hl''. rewrite Hl'', Hl1 in H. discriminate.
- intros l1 l2 HE. clear H1 H2 H4.
  case_loc_eq_dec; intro; subst; case loc_eq_dec; intro; subst; eauto;
  apply H3 in HE; rewrite Hl1 in HE; destruct HE as (_&_&v'&Hv1&Hv2); discriminate.
- intros l1 l2. case_loc_eq_dec; intro; subst; case loc_eq_dec; intro; subst; eauto.
  + intro Hbr. unfold br' in Hl2. rewrite Hl2 in Hbr. discriminate.
  + intro Hb. destruct (H2 l1); [unfold br'; rewrite Hb; left; discriminate|].
    apply H4 in Hb. rewrite Hb, Hl1 in H; discriminate.
Qed.
Global Hint Resolve proper_up : core.

(** Domain of the extension kernel  *)
Definition dom_rel (L : list (ℒ * ℒ)) l := exists l', In (l, l') L \/ In (l', l) L.

(* begin hide *)
Lemma dom_rel_dec L l : dom_rel L l \/ ~ dom_rel L l.
Proof.
induction L.
- right; intro H; inversion H. tauto.
- destruct IHL as [(l'&Hl')|Hf].
  + left. exists l'. destruct Hl'; auto with *.
  + destruct a as (l1&l2). case (loc_eq_dec l l1).
    * intros; subst; left; exists l2. auto with *.
    * case (loc_eq_dec l l2).
      -- intros; subst; left; exists l1. auto with *.
      -- right. intros (l'& [ [Hl'|Hl']|[Hl'|Hl'] ]).
         ++ inversion Hl'; subst. tauto.
         ++ apply Hf; exists l'. tauto.
         ++ inversion Hl'; subst. tauto.
         ++ apply Hf; exists l'. tauto.
Qed.

Lemma dom_rel_app L1 L2 l : dom_rel (L1 ++ L2) l <-> dom_rel L1 l \/ dom_rel L2 l.
Proof.
unfold dom_rel; split.
- intros (l'&Hl'). do 2 rewrite in_app_iff in Hl'.
  decompose sum Hl'; eauto.
- intros [(l'&Hl')|(l'&Hl')]; exists l'; do 2 rewrite in_app_iff; tauto.
Qed.

Lemma dom_rel_join r Δ1 Δ2 l: dom_rel (Δ1 ⊗r# Δ2) l -> In l (locs_d Δ1) \/ In l (locs_d Δ2).
Proof. intros (l'&[Hl'|Hl']); apply join_locs in Hl'; tauto. Qed.

Lemma dom_rel_sem_join Σ Δ1 Δ2 l: dom_rel (E (Σ[[Δ1 ⊛ Δ2]])) l ->
  In l (locs_d Δ1) \/ In l (locs_d Δ2) \/ dom_rel (E Σ) l.
Proof.
simpl. rewrite dom_rel_app. intros [Hl|Hl]; try tauto.
apply dom_rel_join in Hl. tauto.
Qed.

Definition env_locs Γ l := (exists x, In l (locs_d (envM Γ x))) \/
                           (exists x, In l (locs_d (envT Γ x))).

Lemma in_locs_d l0 l' Δ lv ls:
  In (l', Δ) ls ->
  In l0 (locs_d Δ) ->
  In l0 (locs_d (Structural lv ls)).
Proof.
intros Hin Hlocs. simpl. apply in_app_iff. right. apply in_flat_map.
eexists; split; eauto.
Qed.
Global Hint Resolve in_locs_d : core.

Lemma env_up_v_neq Γ y v ly: y <> v -> envV (Γ [[y ↦v ly]]) v = envV Γ v.
Proof.
intro Hneq. destruct Γ as [ [ Γv Γm] Γt ]. simpl.
case val_eq_dec; trivial. intro H; inversion H; tauto.
Qed.

Lemma env_up_v_comm Γ y ly v l1: y <> v ->
  (Γ [[y ↦v ly]]) [[v ↦v l1]] = (Γ [[v ↦v l1]]) [[y ↦v ly]].
Proof.
intros Hneq. destruct Γ as [ [ Γv Γm] Γt ]. simpl.
f_equal; f_equal; apply FunctionalExtensionality.functional_extensionality.
intro v'. case val_eq_dec; trivial. case val_eq_dec; trivial.
intros H1 H2; inversion H1; inversion H2; subst. tauto.
Qed.

Lemma env_up_v_idem Γ v l l': (Γ [[v ↦v l]]) [[v ↦v l']] = Γ [[v ↦v l']].
Proof.
destruct Γ as [ [ Γv Γm] Γt ]. simpl. f_equal. f_equal.
apply FunctionalExtensionality.functional_extensionality.
intros. case val_eq_dec; trivial.
Qed.
Global Hint Resolve env_up_v_idem : core.

Lemma env_locs_up_m Γ m Δ l: env_locs (Γ [[m ↦m Δ]]) l ->  env_locs Γ l \/ In l (locs_d Δ).
Proof.
destruct Γ as [ [ Γv Γm] Γt ]. unfold env_up_m. intros [(x&Hx)|(t&Ht)]; simpl in *.
- revert Hx. case mod_eq_dec.
  + intro Heq; inversion Heq; subst. tauto.
  + intros _ Hm. left. left. eauto.
- left. right. eauto.
Qed.

Lemma locs_desc_lv lv ls l: In l (locs_d (Structural lv nil)) -> In l (locs_d (Structural lv ls)).
Proof. simpl. rewrite app_nil_r. auto with *. Qed.

Lemma env_locs_up_t Γ t Δ l: env_locs (Γ [[t ↦t Δ]]) l ->  env_locs Γ l \/ In l (locs_d Δ).
Proof.
destruct Γ as [ [ Γv Γm] Γt ]. unfold env_up_t. intros [(m&Hm)|(x&Hx)]; simpl in *.
- left. left. eauto.
- revert Hx. case typ_eq_dec.
  + intro Heq; inversion Heq; subst. tauto.
  + intros _ Hm. left. right. eauto.
Qed.

Lemma env_locs_up_v Γ v l l': env_locs (Γ [[v ↦v l']]) l = env_locs Γ l.
Proof. destruct Γ as [ [ Γv Γm] Γt ]. trivial. Qed.
Global Hint Rewrite env_locs_up_v : core.

(* end hide *)

Lemma env_locs_p r Δ Γ l: env_locs (p_sem_env r Δ Γ) l -> env_locs Γ l \/ In l (locs_d Δ).
Proof.
intros[(x&Hx)|(x&Hx)].
- destruct Δ as [lv ls | l' D1 D2]; unfold p_sem_env; simpl in *;
  rewrite in_app_iff.
  + case_eq (find (var_eq_bool (Some (IM x)) ∘ r ∘ fst) ls).
    * intros (l'&d') H'. rewrite H' in Hx.
      apply find_some in H'. destruct H' as (H'&_).
      right. right. apply in_flat_map. eauto.
    * intro Hn; rewrite Hn in Hx. left. left. eauto.
  + left; left. eauto.
- destruct Δ as [lv ls | l' D1 D2]; unfold p_sem_env; simpl in *;
  rewrite in_app_iff.
  + case_eq(find (var_eq_bool (Some (IT x)) ∘ r ∘ fst) ls).
    * intros (l'&d') H'. rewrite H' in Hx.
      apply find_some in H'. destruct H' as (H'&_).
      right. right. apply in_flat_map. eauto.
    * intro Hn; rewrite Hn in Hx. left. right. eauto.
  + left; right. eauto.
Qed.

(* begin hide *)
Lemma disj_incl {A} (l1 l2 l1' l2': list A):
  (forall l, In l l1 -> In l l1') ->
  (forall l, In l l2 -> In l l2') ->
  disj l1' l2' -> disj l1 l2.
Proof. intros Hi1 Hi2 Hd x Hin1 Hin2. eapply Hd; eauto. Qed.
Global Hint Resolve disj_incl : core.

Lemma disj_sym {A} (l1 l2: list A): disj l1 l2 -> disj l2 l1.
Proof. intros H l. eauto. Qed.

(* end hide *)

(* equality of environments up to values *)
Definition eq_mt Γ1 Γ2:= envM Γ1 = envM Γ2 /\ envT Γ1 = envT Γ2.
Infix "=mt" := eq_mt (at level 50).

(* begin hide *)
Lemma eq_mt_M Γ1 Γ2: Γ1 =mt Γ2 -> envM Γ1 = envM Γ2.
Proof. now intros [H1 H2]. Qed.

Lemma eq_mt_T Γ1 Γ2: Γ1 =mt Γ2 -> envT Γ1 = envT Γ2.
Proof. now intros [H1 H2]. Qed.

Lemma eq_mt_up_v Γ1 Γ2 v l: Γ1 =mt Γ2 -> Γ1[[v ↦v l]] =mt Γ2[[v ↦v l]].
Proof. destruct Γ1 as [ [ Γv Γm] Γt ]; now destruct Γ2 as [ [ Γv' Γm'] Γt' ]. Qed.
Global Hint Resolve eq_mt_up_v : core.

Lemma eq_mt_up_v_weak Γ v l: Γ[[v ↦v l]] =mt Γ.
Proof. now destruct Γ as [ [ Γv Γm] Γt ]. Qed.
Global Hint Resolve eq_mt_up_v_weak : core.

Lemma eq_mt_up_m Γ1 Γ2 v l: Γ1 =mt Γ2 -> (Γ1[[v ↦m l]]) =mt (Γ2[[v ↦m l]]).
Proof.
destruct Γ1 as [ [ Γv Γm] Γt ]; destruct Γ2 as [ [ Γv' Γm'] Γt' ].
intro. destruct H as [H1 H2]; simpl in *. subst. now split.
Qed.
Global Hint Resolve eq_mt_up_m : core.

Lemma eq_mt_up_t Γ1 Γ2 v l: Γ1 =mt Γ2 -> Γ1[[v ↦t l]] =mt Γ2[[v ↦t l]].
Proof.
destruct Γ1 as [ [ Γv Γm] Γt ]; destruct Γ2 as [ [ Γv' Γm'] Γt' ].
intro. destruct H as [H1 H2]; simpl in *. subst. now split.
Qed.
Global Hint Resolve eq_mt_up_t : core.

Lemma eq_mt_p Γ1 Γ2 d r: Γ1 =mt Γ2 -> p_sem_env r d Γ1 =mt p_sem_env r d Γ2.
Proof.
destruct Γ1 as [ [ Γv Γm] Γt ]; destruct Γ2 as [ [ Γv' Γm'] Γt' ].
intro. destruct H as [H1 H2]; simpl in *. subst.
destruct d; now split.
Qed.
Global Hint Resolve eq_mt_p : core.

Lemma eq_mt_refl Γ1: Γ1 =mt Γ1.
Proof. now destruct Γ1 as [ [ Γv Γm] Γt ]. Qed.

Lemma eq_mt_sym Γ1 Γ2 : Γ1 =mt Γ2 -> Γ2 =mt Γ1.
Proof.
destruct Γ1 as [ [ Γv Γm] Γt ]; destruct Γ2 as [ [ Γv' Γm'] Γt' ].
intros [H1 H2]. simpl in *; subst. now split.
Qed.

Lemma eq_mt_trans Γ1 Γ2 Γ3: Γ1 =mt Γ2 -> Γ2 =mt Γ3 -> Γ1 =mt Γ3.
Proof.
destruct Γ1 as [ [ Γv Γm] Γt ]; destruct Γ2 as [ [ Γv' Γm'] Γt' ].
destruct Γ3 as [ [ Γv'' Γm''] Γt'' ].
intros [H1 H2] [H3 H4]. simpl in *; subst. now split.
Qed.

Add Parametric Relation: env eq_mt
  reflexivity proved by eq_mt_refl
  symmetry proved by eq_mt_sym
  transitivity proved by eq_mt_trans as eq_mt_rel.

Lemma p_sem_env_sem_equal s s' d Γ:
  sem_equal s s' ->
  p_sem_env (ρ s) d Γ = p_sem_env (ρ s') d Γ.
Proof.
intros (Heq1&_). destruct d as [lv ls | l d1 d2]; unfold p_sem_env; trivial.
repeat f_equal; apply FunctionalExtensionality.functional_extensionality; intro.
- induction lv; simpl; trivial. unfold compose, sr'. repeat rewrite Heq1;
  case var_eq_bool; trivial.
- induction ls; simpl; trivial. unfold compose, sr'. repeat rewrite Heq1;
  case var_eq_bool; trivial.
- induction ls; simpl; trivial. unfold compose, sr'. repeat rewrite Heq1;
  case var_eq_bool; trivial.
Qed.

(* end hide *)

(** Semantic freeness *)
Definition not_sem_free Σ Γ σ v:=
  forall l, In l σ -> ρ Σ l = Some (IV v) ->
    exists l', ↣ Σ l = Some (Some l') /\ envV Γ v <> Some l'.

(* begin hide *)
Lemma not_sem_free_l Σ Γ σ1 σ2 v: not_sem_free Σ Γ (σ1 ++ σ2) v ->
  not_sem_free Σ Γ σ1 v.
Proof. intro H; intros l Hl Hl'; apply H; auto with *. Qed.
Global Hint Resolve not_sem_free_l : core.

Lemma not_sem_free_r Σ Γ σ1 σ2 v: not_sem_free Σ Γ (σ1 ++ σ2) v ->
  not_sem_free Σ Γ σ2 v.
Proof. intro H; intros l Hl Hl'; apply H; auto with *. Qed.
Global Hint Resolve not_sem_free_r : core.

Lemma not_sem_free_app Σ Γ σ1 σ2 v: not_sem_free Σ Γ σ1 v ->
  not_sem_free Σ Γ σ2 v -> not_sem_free Σ Γ (σ1 ++ σ2) v.
Proof. intros H H'; intros l. rewrite in_app_iff. intros [Hl|Hl] Hl'; auto with *. Qed.
Global Hint Resolve not_sem_free_app : core.

Lemma not_sem_free_incl Σ1 Σ2 Γ σ v:
  Σ1 ⊆ Σ2 -> not_sem_free Σ2 Γ σ v -> not_sem_free Σ1 Γ σ v.
Proof.
intros Hi H l Hl Hl'. apply Hi in Hl'.
destruct Hl' as (Hl'&Hbr').
destruct (H l Hl Hl') as (l''&H1&H2).
exists l''. split; auto. case_eq (↣ Σ1 l).
- intros s Hs. apply Hi in Hs. now rewrite <- Hs.
- intro Hn. apply Hbr' in Hn. rewrite Hn in H1; discriminate H1.
Qed.
Global Hint Resolve not_sem_free_incl : core.

Lemma not_sem_free_env_up_v Σ Γ σ v v1 l1:
  v <> v1 -> not_sem_free Σ Γ σ v ->
    not_sem_free Σ (Γ [[v1 ↦v Some l1]]) σ v.
Proof. intros Hneq H l Hl Hl'. rewrite env_up_v_neq by auto. now apply H. Qed.
Global Hint Resolve not_sem_free_env_up_v : core.

Lemma env_up_m_v Γ m d:  envV (Γ [[m ↦m d]]) = envV Γ.
Proof. now destruct Γ as [ [ Γv Γm] Γt ]. Qed.
Global Hint Resolve env_up_m_v : core.

Lemma env_up_t_v Γ m d:  envV (Γ [[m ↦t d]]) = envV Γ.
Proof. now destruct Γ as [ [ Γv Γm] Γt ]. Qed.
Global Hint Resolve env_up_t_v : core.

Lemma not_sem_free_env_up_m Σ Γ σ v m d:
  not_sem_free Σ Γ σ v ->
    not_sem_free Σ (Γ [[m ↦m d]]) σ v.
Proof. intros H l Hl Hl'. simpl. rewrite env_up_m_v. now apply H. Qed.
Global Hint Resolve not_sem_free_env_up_m : core.

Lemma not_sem_free_env_up_t Σ Γ σ v m d:
  not_sem_free Σ Γ σ v ->
    not_sem_free Σ (Γ [[m ↦t d]]) σ v.
Proof. intros H l Hl Hl'. simpl. rewrite env_up_t_v. now apply H. Qed.
Global Hint Resolve not_sem_free_env_up_t : core.

Lemma p_sem_env_up_v_out Γ1 Σ1 lv ls v l:
  ~ In (Some (IV v)) (map (ρ Σ1) lv) ->
  let d := Structural lv ls in
  p_sem_env (ρ Σ1) d (Γ1[[v ↦v l]]) = (p_sem_env (ρ Σ1) d Γ1) [[v ↦v l]].
Proof.
destruct Γ1 as [ [ Γv Γm] Γt ]. intro Hn. unfold p_sem_env. simpl.
f_equal. f_equal. apply FunctionalExtensionality.functional_extensionality. intro v'.
case_eq (find (var_eq_bool (Some (IV v')) ∘ ρ Σ1) lv); trivial.
intros l' Hl'. apply find_some in Hl'. destruct Hl' as (Hin&Hl').
apply var_eq_bool_true in Hl'. case val_eq_dec; trivial. intro Heq; inversion Heq; subst.
contradict Hn. apply in_map_iff. eauto.
Qed.

Lemma p_sem_env_up_v_in Γ1 Σ1 lv ls v l:
  In (Some (IV v)) (map (ρ Σ1) lv) ->
  let d := Structural lv ls in
  p_sem_env (ρ Σ1) d (Γ1[[v ↦v l]]) = p_sem_env (ρ Σ1) d Γ1.
Proof.
destruct Γ1 as [ [ Γv Γm] Γt ]. intro Hin. unfold p_sem_env. simpl.
f_equal. f_equal. apply FunctionalExtensionality.functional_extensionality. intro v'.
case_eq (find (var_eq_bool (Some (IV v')) ∘ ρ Σ1) lv); trivial.
intro Hn. apply in_map_iff in Hin. destruct Hin as (l'& Hl'&Hin').
case val_eq_dec; trivial; intro Heq; inversion Heq; subst.
eapply find_none in Hn; eauto.
rewrite <- Hl' in Hn. apply var_eq_bool_false in Hn; tauto.
Qed.

Lemma envV_p_sem_env_out Γ1 Σ1 lv ls v:
  ~ In (Some (IV v)) (map (ρ Σ1) lv) ->
  let d := Structural lv ls in
  envV (p_sem_env (ρ Σ1) d Γ1) v = envV Γ1 v.
Proof.
destruct Γ1 as [ [ Γv Γm] Γt ]. intro Hn. unfold p_sem_env. simpl.
case_eq (find (var_eq_bool (Some (IV v)) ∘ ρ Σ1) lv); trivial.
intros l' Hl'. apply find_some in Hl'. destruct Hl' as (Hin&Hl').
apply var_eq_bool_true in Hl'.
contradict Hn. apply in_map_iff. eauto.
Qed.
Global Hint Resolve envV_p_sem_env_out : core.

Lemma not_sem_free_p_out Σ Σ' Γ lv ls v σ:
  ~ In (Some (IV v)) (map (ρ Σ') lv) ->
    not_sem_free Σ Γ σ v ->
  not_sem_free Σ (p_sem_env (ρ Σ') (Structural lv ls) Γ) σ v.
Proof.
intros Hn H y Hin Hy.
destruct (H y Hin Hy) as (l'&Hbr&He).
exists l'. split; trivial. erewrite envV_p_sem_env_out; auto.
Qed.
(* end hide *)

(** being bound in the semantics *)
Definition bound Σ σ v:=
  forall l, In l σ -> ρ Σ l = Some (IV v) ->
    exists l', ↣ Σ l = Some (Some l').

Lemma bound_not_sem_free Σ Γ σ v:
  not_sem_free Σ Γ σ v ->
    bound Σ σ v.
Proof. intros H l Hin Hl. destruct (H l Hin Hl) as (l'&Hl'&_). now exists l'. Qed.

Lemma not_sem_free_sem_bound Σ Γ σ v:
  envV Γ v = None ->
  bound Σ σ v ->
  not_sem_free Σ Γ σ v.
Proof.
intros Hn H l0 Hin Hl0.
destruct (H l0 Hin Hl0) as (l'&Hl').
eexists; split; eauto. rewrite Hn. discriminate.
Qed.

(* begin hide *)
Lemma bound_l Σ σ1 σ2 v: bound Σ (σ1 ++ σ2) v ->
  bound Σ σ1 v.
Proof. intro H; intros l Hl Hl'; apply H; auto with *. Qed.
Global Hint Resolve bound_l : core.

Lemma bound_r Σ σ1 σ2 v: bound Σ (σ1 ++ σ2) v ->
  bound Σ σ2 v.
Proof. intro H; intros l Hl Hl'; apply H; auto with *. Qed.
Global Hint Resolve bound_r : core.

Lemma bound_app Σ σ1 σ2 v: bound Σ σ1 v ->
  bound Σ σ2 v -> bound Σ (σ1 ++ σ2) v.
Proof. intros H H'; intros l. rewrite in_app_iff. intros [Hl|Hl] Hl'; auto with *. Qed.
Global Hint Resolve bound_app : core.

Lemma bound_incl Σ1 Σ2 σ v:
  Σ1 ⊆ Σ2 -> bound Σ2 σ v -> bound Σ1 σ v.
Proof.
intros Hi H l Hl Hl'. apply Hi in Hl'.
destruct Hl' as (Hl'&Hbr').
destruct (H l Hl Hl') as (l''&H1).
exists l''. case_eq (↣ Σ1 l).
- intros s Hs. apply Hi in Hs. now rewrite <- Hs.
- intro Hn. apply Hbr' in Hn. rewrite Hn in H1; discriminate H1.
Qed.

Lemma not_sem_free_nil Σ Γ v: not_sem_free Σ Γ nil v.
Proof. intros l Hl. inversion Hl. Qed.
Global Hint Resolve not_sem_free_nil : core.

Lemma bound_nil Σ v: bound Σ nil v.
Proof. intros l Hl. inversion Hl. Qed.
Global Hint Resolve bound_nil : core.

Lemma not_sem_free_eq (Σ1 Σ2 : sem) (Γ : env) (σ : list ℒ) (v : V):
  (forall l, In l σ -> ρ Σ2 l = ρ Σ1 l) ->
  (forall l, In l σ -> ↣ Σ2 l = ↣ Σ1 l) ->
       not_sem_free Σ2 Γ σ v -> not_sem_free Σ1 Γ σ v.
Proof.
intros Hr Hb H l0 Hin Hl0.
destruct (H l0 Hin) as (l'&H'&He').
- now rewrite Hr.
- exists l'. split; trivial. now rewrite <- Hb.
Qed.

Lemma bound_eq (Σ1 Σ2 : sem) (σ : list ℒ) (v : V):
  (forall l, In l σ -> ρ Σ2 l = ρ Σ1 l) ->
  (forall l, In l σ -> ↣ Σ2 l = ↣ Σ1 l) ->
       bound Σ2 σ v -> bound Σ1 σ v.
Proof.
intros Hr Hb H l0 Hin Hl0.
destruct (H l0 Hin) as (l'&H').
- now rewrite Hr.
- exists l'. now rewrite <- Hb.
Qed.

Lemma env_up_v_mon Γ y l v ly:
  envV Γ y = Some ly ->
    envV (Γ[[v ↦v Some l]]) y = Some ly \/
    envV (Γ[[v ↦v Some l]]) y = Some l.
Proof.
intro H. destruct Γ as [ [ Γv Γm] Γt ].
simpl. case val_eq_dec; tauto.
Qed.

Lemma p_sem_up_mon Γ r lv ls y ly:
  envV Γ y = Some ly ->
    exists l', envV (p_sem_env r (Structural lv ls) Γ) y = Some l'.
Proof.
intro H. destruct Γ as [ [ Γv Γm] Γt ]. simpl in *.
simpl. case (find (var_eq_bool (Some (IV y)) ∘ r) lv);eexists; eauto.
Qed.

Lemma sr_sem_join Σ Δ1 Δ2: ρ (Σ [[Δ1 ⊛ Δ2]]) = ρ Σ.
Proof. trivial. Qed.
Global Hint Resolve sr_sem_join : core.

Lemma br_sem_join Σ Δ1 Δ2: ↣ (Σ [[Δ1 ⊛ Δ2]]) = ↣ Σ.
Proof. trivial. Qed.
Global Hint Resolve br_sem_join : core.

Lemma sem_equal_compat_m_path: forall m Σ Γ Δ',
 Σ / Γ ⊢mp m ⋮ (Δ', Σ) ->
 forall Σ0, sem_equal Σ Σ0 -> Σ0 / Γ ⊢mp m ⋮ (Δ', Σ0).
Proof.
induction m; intros Σ Γ Δ' H' Σ0; inversion H'; subst; intro H0.
- econstructor; auto.
- apply IHm with (Σ0 := Σ0) in X; auto.
  econstructor; eauto. destruct H0 as (H0&_).
   unfold sr'. rewrite <- H0. trivial.
Defined.
Global Hint Resolve sem_equal_compat_m_path : core.

Lemma sem_equal_compat_em_path: forall m Σ Σ' Γ Δ',
 Σ / Γ ⊢emp m ⋮ (Δ', Σ') ->
 forall Σ0, sem_equal Σ Σ0 -> {Σ'' & (Σ0 / Γ ⊢emp m ⋮ (Δ', Σ'')) ∧ sem_equal Σ' Σ''}.
Proof.
induction m; intros Σ Σ' Γ Δ' H' Σ0; inversion H'; subst; intro H0.
- eexists; simpl; split; eauto. econstructor; eauto.
- apply IHm with (Σ0 := Σ0) in X; auto.
  destruct X as (Σ''&HΣ''& H''). eexists; split; eauto.
  econstructor; try apply H''; eauto. destruct H'' as (H''&_).
  unfold sr'. now rewrite <- H''.
- apply IHm1 with (Σ0 := Σ0) in X; auto. destruct X as (Σ1&HΣ1& H1).
  apply IHm2 with (Σ0 := Σ1) in X0; auto.
  destruct X0 as (Σ2&HΣ2&H2). eexists; split; eauto.
   econstructor; try apply H''; eauto.
Defined.
Global Hint Resolve sem_equal_compat_em_path : core.

Global Hint Resolve add_m : core.

Lemma sem_equal_sem_up_br_compat Σ Σ' l v l':
  sem_equal Σ Σ' ->
  sem_equal (Σ [[(l, v) ↦l l']]) (Σ' [[(l, v) ↦l l']]).
Proof. intros (H1&H2&H3). repeat split; simpl; auto. Qed.
Global Hint Resolve sem_equal_sem_up_br_compat : core.

Lemma sem_equal_sem_up_compat Σ Σ' l l':
  sem_equal Σ Σ' ->
  sem_equal (Σ [[l↦ l']]) (Σ' [[l ↦ l']]).
Proof. intros (H1&H2&H3). repeat split; simpl; auto. Qed.
Global Hint Resolve sem_equal_sem_up_compat : core.

Lemma sem_equal_sr Σ Σ': sem_equal Σ Σ' -> ρ Σ = ρ Σ'.
Proof.
  intros (H&_). unfold sr'.
  apply FunctionalExtensionality.functional_extensionality; trivial.
Qed.
Global Hint Resolve sem_equal_sr : core.

Lemma sem_equal_sr' Σ Σ' l v: sem_equal Σ Σ' -> ρ Σ l = v -> ρ Σ' l = v.
Proof. intros (H&_) H'. unfold sr'. now rewrite <- H. Qed.
Global Hint Resolve sem_equal_sr' : core.

Lemma sem_equal_br' Σ Σ' l: sem_equal Σ Σ' -> ↣ Σ l = ↣ Σ' l.
Proof. intros (_&_&H). unfold br'. now rewrite <- H. Qed.
Global Hint Resolve sem_equal_br' : core.

Lemma sem_equal_proper s1 s2: sem_equal s1 s2 -> proper s1 -> proper s2.
Proof.
intros Heq (Hp1&Hp2&Hp3&Hp4). nsplit 3.
- intros l l' Hl. erewrite <- sem_equal_br'; [|exact Heq].
  erewrite <- sem_equal_br' in Hl; [|exact Heq].
  eapply Hp1; eauto.
- intros l Hl. destruct Hl as [Hl|(l'&Hl)];
  (erewrite <- sem_equal_br' in Hl; [|exact Heq];
   erewrite sem_equal_sr'; eauto).
- intros l l' HE. rewrite <- (proj1 (proj2 Heq)) in HE.
  apply Hp3 in HE. destruct HE as (H1&H2&v&H3&H4).
  do 2 (erewrite <- sem_equal_br' with (Σ' := s2); [|exact Heq]).
  erewrite <- sem_equal_sr with (Σ' := s2); [|exact Heq].
  eauto.
- intros l l' Hl. erewrite <- sem_equal_br' in Hl; [|exact Heq].
  erewrite <- sem_equal_sr with (Σ' := s2); [|exact Heq]. eauto.
Qed.

Lemma sem_equal_compat_v: forall v Σ Σ' Γ,
 Σ / Γ ⊢v v ⇝ Σ' ->
 forall Σ0, sem_equal Σ Σ0 -> {Σ'' & (Σ0 / Γ ⊢v v ⇝ Σ'') ∧ sem_equal Σ' Σ''}.
Proof.
induction v; intros Σ Σ' Γ H' Σ0; inversion H'; subst; intro H0;
try solve[eexists; split; [econstructor; eauto|]; auto].
- eexists; split; [econstructor 3; eauto|]; auto.
  intros. intro HF. eapply H4; eauto.
- apply IHv1 with (Σ0 := Σ0) in X; trivial. destruct X as (Σ1&HΣ1&HΣ1').
  eapply IHv2 with (Σ0 := Σ1 [[l ↦ IV v]]) in X0; eauto. destruct X0 as (Σ2&HΣ2&HΣ2').
  exists Σ2. split; auto. econstructor; eauto.
- apply IHv with (Σ0 := (Σ0 [[l ↦ IV v1]])) in X; eauto. destruct X as (Σ1&HΣ1&HΣ1').
  eexists Σ1; split; [econstructor; eauto|]; auto.
- apply IHv1 with (Σ0 := Σ0) in X; trivial. destruct X as (Σ1&HΣ1&HΣ1').
  eapply IHv2 with (Σ0 := Σ1) in X0; eauto. destruct X0 as (Σ2&HΣ2&HΣ2').
  exists Σ2. split; auto. econstructor; eauto.
- subst d.
  apply sem_equal_compat_m_path with (Σ0 := Σ0) in X; eauto.
  apply IHv with (Σ0 := Σ0) in X0; trivial. destruct X0 as (Σ1&HΣ1&HΣ1').
  eexists Σ1; split; [econstructor; eauto|]; auto.
  erewrite sem_equal_sr; eauto.
Defined.

Lemma sem_equal_compat_m:
 (forall m Δ Σ Σ' Γ, Σ / Γ  ⊢ m ⋮ (Δ, Σ') ->
   forall Σ0, sem_equal Σ Σ0 -> {Σ'' & (Σ0 / Γ ⊢ m ⋮ (Δ, Σ'')) ∧ sem_equal Σ' Σ''}) *
 (forall s Δ Σ Σ' Γ, Σ / Γ  ⊢s s ⋮ (Δ, Σ') ->
   forall Σ0, sem_equal Σ Σ0 -> {Σ'' & (Σ0 / Γ ⊢s s ⋮ (Δ, Σ'')) ∧ sem_equal Σ' Σ''}) *
 (forall t Δ Σ Σ' Γ, Σ / Γ  ⊢t t ⋮ (Δ, Σ') ->
   forall Σ0, sem_equal Σ Σ0 -> {Σ'' & (Σ0 / Γ ⊢t t ⋮ (Δ, Σ'')) ∧ sem_equal Σ' Σ''}) *
 (forall S Δ Σ Σ' Γ, Σ / Γ  ⊢S S ⋮ (Δ, Σ') ->
   forall Σ0, sem_equal Σ Σ0 -> {Σ'' & (Σ0 / Γ ⊢S S ⋮ (Δ, Σ'')) ∧ sem_equal Σ' Σ''}).
Proof.
apply module_ind.
- intros t Δ Σ Σ' Γ H' Σ0; inversion H'; subst; intro H0;
  eexists; split; [econstructor; eauto|]; auto.
- intros m t Δ Σ Σ' Γ H' Σ0. inversion H'; subst; intro H0.
  apply sem_equal_compat_em_path with (Σ0 := Σ0) in X; eauto.
  destruct X as (Σ2&HΣ2&HΣ2').
  eexists Σ2; split; [econstructor; eauto|]; eauto.
- intros (m&l) t1 Hind1 t2 Hind2 Δ Σ Σ' Γ H' Σ0; inversion H'; subst; intro H0.
  apply Hind1 with (Σ0 := Σ0) in X; trivial. destruct X as (Σ1&HΣ1&HΣ1').
  eapply Hind2 with (Σ0 := Σ1 [[l ↦ IM m]]) in X0; eauto. destruct X0 as (Σ2&HΣ2&HΣ2').
  eauto.
- intros t Hind (m&l) p Δ Σ Σ' Γ H' Σ0; inversion H'; subst; intro H0.
  apply Hind with (Σ0 := Σ0) in X; trivial. destruct X as (Σ1&HΣ1&HΣ1').
  eapply sem_equal_compat_em_path with (Σ0 := Σ1 [[l ↦ IM m]]) in X0; eauto.
  destruct X0 as (Σ2&HΣ2&HΣ2'). erewrite sem_equal_sr; eauto.
- intros t Hind (m&l) p Δ Σ Σ' Γ H' Σ0; inversion H'; subst; intro H0.
  apply Hind with (Σ0 := Σ0) in X; trivial. destruct X as (Σ1&HΣ1&HΣ1').
  eapply sem_equal_compat_em_path with (Σ0 := Σ1 [[l ↦ IM m]]) in X0; eauto.
  destruct X0 as (Σ2&HΣ2&HΣ2'). erewrite sem_equal_sr; eauto.
- intros l Hind Δ Σ Σ' Γ H' Σ0; inversion H'; subst; intro H0.
  apply Hind with (Σ0 := Σ0) in X; trivial. destruct X as (Σ1&HΣ1&HΣ1').
  eauto.
- intros m Hind Δ Σ Σ' Γ H' Σ0; inversion H'. subst. intro H0.
  apply Hind with (Σ0 := Σ0) in X; trivial. destruct X as (Σ1&HΣ1&HΣ1').
  eauto.
- intros ls (v&l) Hind Δ Σ Σ' Γ H' Σ0; inversion H'; subst; intro H0.
  apply Hind with (Σ0 := Σ0 [[l ↦ IV v]]) in X; auto. destruct X as (Σ1&HΣ1&HΣ1').
  eexists; econstructor; eauto. erewrite sem_equal_sr; eauto.
- intros ls ml t Hind1 Hind2 Δ Σ Σ' Γ H' Σ0 H0; inversion H'; subst.
  apply Hind2 with (Σ0 := Σ0) in X; trivial. destruct X as (Σ1&HΣ1&HΣ1').
  eapply Hind1 with (Σ0 := Σ1 [[l ↦ IM x]]) in X0; eauto. destruct X0 as (Σ2&HΣ2&HΣ2').
  eexists; econstructor; eauto. erewrite sem_equal_sr; eauto.
- intros ls (x, l) p Hind Δ Σ Σ' Γ H' Σ0 H0; inversion H'; subst.
  apply sem_equal_compat_m_path with (Σ0 := Σ0) in X; trivial.
  eapply Hind with (Σ0 := Σ0 [[l ↦ IM x]]) in X0; eauto. destruct X0 as (Σ2&HΣ2&HΣ2').
  eexists; econstructor; eauto. erewrite sem_equal_sr; eauto.
- intros ls t Hind Δ Σ Σ' Γ H' Σ0 H0; inversion H'; subst.
  eapply Hind with (Σ0 := Σ0 [[l ↦ IT t0]]) in X; eauto. destruct X as (Σ2&HΣ2&HΣ2').
  eexists; econstructor; eauto. erewrite sem_equal_sr; eauto.
- intros ls (t0, l) t Hind1 Hind2 Δ Σ Σ' Γ H' Σ0 H0; inversion H'; subst.
  apply Hind2 with (Σ0 := Σ0) in X; trivial. destruct X as (Σ1&HΣ1&HΣ1').
  eapply Hind1 with (Σ0 := Σ1 [[l ↦ IT t0]]) in X0; eauto. destruct X0 as (Σ2&HΣ2&HΣ2').
  eexists; econstructor; eauto. erewrite sem_equal_sr; eauto.
- intros ls t Hind1 Hind2 Δ Σ Σ' Γ H' Σ0 H0; inversion H'; subst.
  apply Hind2 with (Σ0 := Σ0) in X; trivial. destruct X as (Σ1&HΣ1&HΣ1').
  eapply Hind1 with (Σ0 := Σ1) in X0; eauto. destruct X0 as (Σ2&HΣ2&HΣ2').
  subst DUd' Γ' d d'.
  exists (Σ2 [[Structural lv [] ⊛ Structural lv' [] ]]) ; split; auto.
  erewrite sem_equal_sr; eauto. econstructor; eauto.
  erewrite sem_equal_sr; eauto.
- intros Δ Σ Σ' Γ H' Σ0 H0; inversion H'; subst. eauto.
- intros ls p Hind Δ Σ Σ' Γ H' Σ0 H0; inversion H'; subst.
  apply sem_equal_compat_m_path with (Σ0 := Σ0) in X; trivial.
  eapply Hind with (Σ0 := Σ0) in X0; eauto. destruct X0 as (Σ2&HΣ2&HΣ2').
  subst DUd' Γ' d d'.
  eexists (Σ2[[Structural lv [] ⊛ Structural lv' [] ]]); split; auto.
  erewrite sem_equal_sr; eauto. econstructor; eauto. erewrite sem_equal_sr; eauto.
- intros (v&l) e ls Hind Δ Σ Σ' Γ H' Σ0 H0; inversion H'; subst.
  apply sem_equal_compat_v with (Σ0 := Σ0) in X; trivial. destruct X as (Σ1&HΣ1&HΣ1').
  eapply Hind with (Σ0 := Σ1 [[l ↦ IV v]]) in X0; eauto.
  destruct X0 as (Σ2&HΣ2&HΣ2'). erewrite sem_equal_sr; eauto.
- intros e ls Hind Δ Σ Σ' Γ H' Σ0; inversion H'; subst; intro H0.
  apply sem_equal_compat_v with (Σ0 := Σ0) in X; trivial. destruct X as (Σ1&HΣ1&HΣ1').
  apply Hind with (Σ0 := Σ1) in X0; eauto.
  destruct X0 as (Σ2&HΣ2&HΣ2'). eauto.
- intros (x&l) m ls Hind1 Hind2 Δ Σ Σ' Γ H' Σ0; inversion H'; subst; intro H0.
  apply Hind2 with (Σ0 := Σ0) in X; trivial. destruct X as (Σ1&HΣ1&HΣ1').
  eapply Hind1 with (Σ0 := Σ1 [[l ↦ IM x]]) in X0; eauto. destruct X0 as (Σ2&HΣ2&HΣ2').
  erewrite sem_equal_sr; eauto.
- intros (t0&l) t ls Hind1 Hind2 Δ Σ Σ' Γ H' Σ0; inversion H'; subst; intro H0.
  apply Hind2 with (Σ0 := Σ0) in X; trivial. destruct X as (Σ1&HΣ1&HΣ1').
  apply Hind1 with (Σ0 := Σ1 [[l ↦ IT t0]]) in X0; eauto.
  destruct X0 as (Σ2&HΣ2&HΣ2'). erewrite sem_equal_sr; eauto.
- intros m ls Hind1 Hind2 Δ Σ Σ' Γ H' Σ0; inversion H'; subst; intro H0.
  apply Hind1 with (Σ0 := Σ0) in X; trivial. destruct X as (Σ1&HΣ1&HΣ1').
  eapply Hind2 with (Σ0 := Σ1) in X0; eauto. destruct X0 as (Σ2&HΣ2&HΣ2').
  subst DUd' Γ' d d'.
  eexists (Σ2[[Structural lv [] ⊛ Structural lv' [] ]]); split; auto.
  erewrite sem_equal_sr; eauto. econstructor; eauto. erewrite sem_equal_sr; eauto.
- intros p Δ Σ Σ' Γ H' Σ0; inversion H'; subst; intro H0. eauto.
- intros (x&l) t m Hind1 Hind2 Δ Σ Σ' Γ H' Σ0; inversion H'; subst; intro H0.
  apply Hind1 with (Σ0 := Σ0) in X; auto. destruct X as (Σ1&HΣ1&HΣ1').
  eapply Hind2 with (Σ0 := Σ1 [[l ↦ IM x]]) in X0; eauto.
  destruct X0 as (Σ2&HΣ2&HΣ2'). eauto.
- intros m1 Hind1 m2 Hind2 Δ Σ Σ' Γ H' Σ0 H0; inversion H'; subst.
  apply Hind1 with (Σ0 := Σ0) in X; trivial. destruct X as (Σ1&HΣ1&HΣ1').
  apply Hind2 with (Σ0 := Σ1) in X0; eauto. destruct X0 as (Σ2&HΣ2&HΣ2'). eauto.
- intros m t Hind1 Hind2 Δ Σ Σ' Γ H' Σ0 H0; inversion H'; subst.
  apply Hind1 with (Σ0 := Σ0) in X; trivial. destruct X as (Σ1&HΣ1&HΣ1').
  eapply Hind2 with (Σ0 := Σ1) in X0; eauto. destruct X0 as (Σ2&HΣ2&HΣ2').
  eexists; econstructor; eauto. erewrite sem_equal_sr; eauto.
- intros ls Hind Δ Σ Σ' Γ H' Σ0 H0; inversion H'; subst.
  eapply Hind with (Σ0 := Σ0) in X; eauto. destruct X as (Σ2&HΣ2&HΣ2'). eauto.
- intros Δ Σ Σ' Γ H' Σ0 H0; inversion H'; subst. eauto.
- intros ls p Hind Δ Σ Σ' Γ H' Σ0 H0; inversion H'; subst.
  apply sem_equal_compat_m_path with (Σ0 := Σ0) in X; trivial.
  eapply Hind with (Σ0 := Σ0) in X0; eauto. destruct X0 as (Σ2&HΣ2&HΣ2').
  subst DUd' Γ' d d'.
  exists (Σ2 [[Structural lv [] ⊛ Structural lv' [] ]]) ; split; auto.
  erewrite sem_equal_sr; eauto. econstructor; eauto.
  erewrite sem_equal_sr; eauto.
Defined.

Lemma sem_equal_compat: forall P Σ Σ' Γ,
 Σ / Γ ⊢ P ⇝ Σ' ->
 forall Σ0, sem_equal Σ Σ0 -> {Σ'' & (Σ0 / Γ ⊢ P ⇝ Σ'') ∧ sem_equal Σ' Σ''}.
Proof.
induction P; intros Σ Σ' Γ H' Σ0; inversion H'; subst; intro H0;
try solve[eexists; split; [econstructor; eauto|]; auto].
- eapply sem_equal_compat_v with (Σ0 := Σ0) in X; auto. destruct X as (Σ1&HΣ1&HΣ1').
  eauto.
- apply sem_equal_compat_m with (Σ0 := Σ0) in X; trivial. destruct X as (Σ1&HΣ1&HΣ1').
  eapply IHP with (Σ0 := Σ1 [[l ↦ IM x]]) in X0; eauto. destruct X0 as (Σ2&HΣ2&HΣ2').
  eauto.
Defined.

Lemma deterministic_semantics_m_path:
  forall m Σ Σ' Γ Δ',
    wb_env Γ Σ ->
    Σ / Γ ⊢mp m ⋮ (Δ', Σ') ->
    (forall Σ'' Δ'', Σ / Γ ⊢mp m ⋮ (Δ'', Σ'') -> Σ = Σ'' /\ Δ' = Δ'') ∧
    (Σ = Σ') ∧
    wb_d Σ Δ' ∧
    (forall Γ', Γ' =mt Γ -> Σ / Γ' ⊢mp m ⋮ (Δ', Σ'))(* weakening *) ∧
    (forall l, In l (locs_d Δ') -> env_locs Γ l).
Proof.
induction m; intros Σ Σ' Γ Δ' HΓ H';
inversion H'; subst; clear H'.
- split;[intros Σ'' Δ'' H''; inversion H''; subst; clear H''|]; intuition.
  + apply HΓ.
  + erewrite <- eq_mt_M ; eauto. constructor.
  + left. eauto.
- eapply IHm in X; eauto.
  destruct X as (Hdet&Heq0&Hwb&Hw&HΔ).
  nsplit 4; auto.
  + intros Σ'' Δ'' H''; inversion H''; subst.
    assert (Heq : (l, Δ')  = (l0, Δ'')).
    { eapply Hdet in X; eauto.
      destruct X as (_&X); inversion X; subst.
      inversion Hwb as [lv' ls' _ Hwbls Hlv Hls |]; subst.
      eapply Hls; eauto; simpl.
      now rewrite H8.
    }
    inversion Heq; now subst.
  + inversion Hwb as [lv' ls' _ Hwbls Hlv Hls |]; subst. eapply Hwbls; eauto.
  + intuition; econstructor; eauto.
  + intros l0 Hl0. eapply HΔ. eauto.
Defined.

Lemma deterministic_semantics_em_path:
  forall m Σ Σ' Γ Δ',
    wb_env Γ Σ ->
    Σ / Γ ⊢emp m ⋮ (Δ', Σ') ->
    (forall Σ'' Δ'', Σ / Γ ⊢emp m ⋮ (Δ'', Σ'') -> Σ' = Σ'' /\ Δ' = Δ'') ∧
    wb_env Γ Σ' ∧ wb_d Σ' Δ' ∧
    (ρ Σ' = ρ Σ) ∧
    (↣ Σ' = ↣ Σ) ∧
    (Σ ⊆ Σ') ∧
    (proper Σ -> proper Σ') ∧
    (forall Γ', Γ' =mt Γ ->  Σ / Γ' ⊢emp m ⋮ (Δ', Σ'))(* weakening *) ∧
    ((forall l, dom_rel (E Σ') l -> ~ dom_rel (E Σ) l -> env_locs Γ l)  /\
    (forall l, In l (locs_d Δ') -> env_locs Γ l)).
Proof.
induction m; intros Σ Σ' Γ Δ' HΓ H'.
- split; [intros Σ'' Δ'' H''; inversion H''; subst; clear H'' |];
  inversion H'; subst; intuition.
  + apply HΓ.
  + erewrite <- eq_mt_M ; eauto. constructor.
  + left; eexists; eauto.
- destruct m0 as (m0&lm0). simpl in *.
  inversion H'; subst.
  eapply IHm in X; eauto; clear IHm.
  destruct X as (Hdet&Henv&Hwb&Hlocs&Hbr&HE&Hc).
  nsplit 9; try solve[intuition];
  try(inversion Hwb as [ lv' ls' _ Hwbls Hlv Hls | ] ; subst).
  + intros Σ'' Δ'' H''; inversion H''; subst; clear H''.
    split. eapply Hdet; eauto.
    assert (Heq : (l, Δ')  = (l0, Δ'')).
    { eapply Hdet in X; eauto.
      destruct X as (Heq&X); inversion X; subst.
      eapply (Hls (l, _) (l0, _)); eauto.
      simpl; rewrite H8; auto.
    }
    now inversion Heq.
  + eapply Hwbls; eauto.
  + intuition. econstructor; eauto.
  + intros l0 Hl0. apply Hc. simpl. apply in_app_iff. right. apply in_flat_map.
    eexists; split; eauto.
- inversion H'; simpl in *; subst.
  eapply IHm1 in X; auto with *.
  destruct X as (Hdet&Henv&Hwb&Hlocs&Hbr&HE&Hpr); subst.
  eapply IHm2 in X0; auto with *.
  destruct X0 as (Hdet'&Henv''&Hwb'&Hlocs'&Hbr'&Hpr'); subst.
  clear IHm1 IHm2.
  nsplit 9; auto; try (inversion Hwb; subst; clear Hwb).
  + intros Σ''' Δ''' H'''; inversion H'''; subst; clear H'''.
    apply Hdet in X; eauto. destruct X as (Heq1&Heq2).
    inversion Heq2; subst; clear Heq2.
    apply Hdet' in X0; eauto. destruct X0 as (Heq1&Heq2).
    inversion Heq2; now subst.
  + eauto.
  + rewrite sr_sem_join, Hlocs'. trivial.
  + now rewrite br_sem_join, Hbr'.
  + transitivity Σ'0; trivial. transitivity Σ''; tauto.
  + intro Hpr0. apply Hpr,Hpr' in Hpr0.
    rewrite <- Hlocs' in *. apply proper_sem_join; eauto.
  + intros. econstructor; eauto;[apply Hpr|apply Hpr']; trivial.
  + intros l0 Hl0 HnE. rewrite dom_rel_app in Hl0. destruct Hl0 as [Hl0|Hl0].
    * apply dom_rel_join in Hl0. destruct Hl0 as [Hl0|Hl0].
      -- apply Hpr. simpl. rewrite in_app_iff. tauto.
      -- now apply Hpr'.
    * destruct (dom_rel_dec (E Σ'') l0) as [HE''|HnE''].
      -- now apply Hpr in HE''.
      -- now apply Hpr' in Hl0.
  + intros l0 Hl0. apply Hpr. simpl. rewrite in_app_iff. tauto.
Defined.

(* for module paths, the semantics is unchanged *)
Lemma judgment_p_sem Σ Σ' Γ p Δ : Σ / Γ ⊢mp p ⋮ (Δ, Σ') -> Σ = Σ'.
Proof. intro H; now inversion H. Qed.

Lemma destr_v v Σ Σ' Γ :
wf_v v ->
wb_env Γ Σ ->
(Γ ⊓ locs_v v) ->
(Σ ⊔ locs_v v) ->
Σ / Γ ⊢v v ⇝ Σ' ->
match v with
| V_id (v, l) => (wb_env Γ (Σ [[ (l, v)↦l envV Γ v]])) /\
                 (Σ ⊆ (Σ [[ (l, v)↦l envV Γ v]])) /\
                 (Σ' = Σ[[ (l, v) ↦l envV Γ v]]) : Type
| p ⋅ (v, l) =>
    {lv & {ls & Σ / Γ ⊢mp p ⋮ (Structural lv ls, Σ) ∧
    ({l' | In l' lv /\
                ρ Σ l' = Some (IV v) /\ Σ' =  Σ[[(l, v) ↦l Some l' ]] /\
                wb_env Γ (Σ [[ (l, v)↦l Some l']]) /\
                Σ ⊆ (Σ [[ (l, v)↦l Some l']])} +
    {(forall l', In l' lv -> ρ Σ l' <> Some (IV v)) /\ Σ' = Σ[[(l, v) ↦l None ]]}) } }
| V_const c => Σ' = Σ
| V_let (v, l) e1 e2 => {Σ'' & (Σ / Γ ⊢v e1 ⇝ Σ'') ∧
                         (Σ''[[l ↦ IV v]] / Γ[[v ↦v Some l]] ⊢v e2 ⇝ Σ') ∧
                         ((wf_v e1) /\ (Γ ⊓ locs_v e1) /\ (Σ ⊔ locs_v e1) /\
                         (wf_v e2) /\ ((Γ[[v ↦v Some l]]) ⊓ locs_v e2))}
| V_fun (v, l) e => (Σ[[l ↦ IV v]] / Γ[[v ↦v Some l]] ⊢v e ⇝ Σ') ∧
    (wf_v e /\ (Γ[[v ↦v Some l]] ⊓ locs_v e)/\ (Σ[[l ↦ IV v]] ⊔ locs_v e))
| V_app e1 e2 => {Σ'' & (Σ / Γ ⊢v e1 ⇝ Σ'') ∧
                        (Σ'' / Γ ⊢v e2 ⇝ Σ') ∧
                        ((wf_v e1) /\ (Γ ⊓ locs_v e1) /\ (Σ ⊔ locs_v e1) /\
                        (wf_v e2) /\ (Γ ⊓ locs_v e2) /\ (Σ ⊔ locs_v e2))}
| V_open p e => {lv & {ls &
  let d := Structural lv ls in
  let Γ' := (p_sem_env (ρ Σ) d Γ) in
    (Σ / Γ ⊢mp p ⋮ (d, Σ)) ∧ (Σ / Γ' ⊢v e ⇝ Σ') ∧ ((wf_v e) /\ (Γ' ⊓ locs_v e) /\
    (Σ ⊔ locs_v e) /\ (wb_env Γ' Σ))}}
end.
Proof.
revert Σ Σ' Γ.
intros Σ Σ' Γ Hwf Henv Hlocs1 Hlocs2 H; inversion H; subst;
unfold disj in *; auto with *.
- nsplit 2; auto with *;[eapply wb_env_incl; [ exact Henv|]|];
  apply sem_incl_up_br; simpl; auto; apply Hlocs2; simpl; tauto.
- exists lv; exists ls. split; auto. left; exists l'.
  do 4 (split; trivial);
  [eapply wb_env_incl; [ exact Henv|]|]; eapply sem_incl_up_br;
  simpl; auto; apply Hlocs2; simpl; tauto.
- exists lv; exists ls. split; auto.
- simpl in *. exists Σ''. nsplit 6; auto with *; try tauto;
  [split; intros; apply Hlocs2; auto with *|].
  intros l0 Hl0 v' Heqv'. destruct Γ as [ [ Γv Γm] Γt ]. simpl in *.
  case val_eq_dec as [Heqv | Hneqv] in Heqv'.
  * injection Heqv'; intros; subst l0 v'; clear Heqv'. tauto.
  * eapply Hlocs1 in Heqv'; auto with *.
- simpl in *. nsplit 3; auto with *; try tauto.
  + apply env_up_v_disj; auto with *. tauto.
  + split; intros l0 Hl0.
    * case_loc_eq_dec; intro; subst;[tauto|]. apply Hlocs2. tauto.
    * apply Hlocs2. tauto.
- simpl in *. exists Σ''. nsplit 7; auto with *; try tauto; split;
  intros; apply Hlocs2; auto with *.
- exists lv. exists ls. nsplit 5; auto with *; try tauto.
  + apply p_sem_env_disj; trivial.
  + apply wb_p_sem_env; auto. apply deterministic_semantics_m_path in X; tauto.
Defined.

Lemma deterministic_semantics_v: forall v Σ Σ' Γ,
  wf_v v ->
  wb_env Γ Σ ->
  (Γ ⊓ locs_v v) ->
  (Σ ⊔ locs_v v) ->
  Σ / Γ ⊢v v ⇝ Σ' ->
    ((forall Σ'0, Σ / Γ ⊢v v ⇝ Σ'0 -> Σ' = Σ'0) /\
    wb_env Γ Σ' /\
    (forall l, ~ In l (locs_v v) -> ρ Σ' l = ρ Σ l) /\
    (forall l, ~ In l (locs_v v) -> ↣ Σ' l = ↣ Σ l) /\
    (forall l, (In l (decl_v v) \/ cod Σ l) <-> cod Σ' l) /\
    (Σ ⊆ Σ') /\
    (forall l v', ρ Σ l = None -> ρ Σ' l = Some (IV v') <-> value_identifier_v (v', l) v) /\
    (proper Σ -> proper Σ') /\
    (E Σ' = E Σ) /\
    (forall y l, envV Γ y = Some l -> bound Σ' (ref_v v) y)).
Proof.
induction v; intros Σ Σ' Γ Hv HΓ HΓlocs HΣlocs Hj; destr_locs;
apply destr_v in Hj; trivial; simpl in Hj; subst; simpl in *.
- destruct Hj as (Henv'&Hj&Heq); subst.
  simpl; nsplit 9; try tauto.
  + intros Σ'0 H'; inversion H'; subst; clear H'; trivial.
  + intros l0 H0; case_loc_eq_dec; try tauto.
  + intros l0 H0; case_loc_eq_dec; try tauto.
  + intro l. rewrite cod_sem_up_br. split; try tauto. intros [HF|Hcod]; try tauto.
    split; trivial. intro; subst. destruct Hcod as (v'&Hcod&_).
    rewrite (proj1 HΣlocs) in Hcod; [discriminate|tauto].
  + intros l' v' H'. rewrite sr_up_br.
    case loc_eq_dec; intro; split; intro H''; inversion H''; subst; trivial;
    [vidtac|tauto].
  + apply proper_up_br; try tauto; apply HΣlocs; tauto.
  + intros y l0 Hl0 l1 Hin. inversion Hin; subst;[|inversion H].
    simpl. case_loc_eq_dec; try tauto. intros _ Heq; inversion Heq.
    eexists; f_equal. eauto.
- destruct Hj as (lv&ls&Hj&[(l'&Hin&Hsome&Heq&Henv')| (Hnone&Heq)]); subst.
  + nsplit 9; try tauto; trivial.
    * intros Σ'0 H'. apply destr_v in H'; trivial.
      apply deterministic_semantics_m_path in Hj; trivial.
      destruct H' as (lv'&ls'&Hj'&Hcase').
      apply Hj in Hj'. destruct Hj' as (_&Hj'); inversion Hj'; subst; clear Hj'.
      destruct Hcase' as [(l''&Hin'&Hsome'&Heq'&Henv'')| (Hnone'&Heq')];
      subst; eauto.
      -- destruct Hj as (_&_&Hwb&_).
        inversion Hwb as [lv0 ls0 Hwb' _ Hinj _ | ] ; subst.
        replace l'' with l'; trivial; apply Hinj; trivial. now rewrite Hsome.
      -- contradict Hnone'. eauto.
    * intros l0 H0. simpl; case_loc_eq_dec; try tauto.
    * intros l0 H0. simpl; case_loc_eq_dec; try tauto.
    * intro l. rewrite cod_sem_up_br. split; try tauto. intros [HF|Hcod]; try tauto.
      split; trivial. intro; subst. destruct Hcod as (v'&Hcod&_).
      rewrite (proj1 HΣlocs) in Hcod; [discriminate|tauto].
    * intros; rewrite sr_up_br.
      case loc_eq_dec; intro; split; intro H''; inversion H''; subst; trivial;
      [vidtac|tauto].
    * intro; apply proper_up_br_some; trivial.
      -- apply HΣlocs. tauto.
      -- apply HΣlocs. tauto.
      -- apply deterministic_semantics_m_path in Hj; trivial.
         destruct Hj as (_&_&Hwb&Hw&_).
         inversion Hwb as [lv' ls' Hwb' _ _ _|]; subst.
         apply Hwb' in Hin. destruct Hin. tauto.
  + nsplit 9; trivial.
    * intros Σ'0 H'. apply destr_v in H'; trivial.
      apply deterministic_semantics_m_path in Hj; trivial.
      destruct H' as (lv'&ls'&Hj'&Hcase').
      apply Hj in Hj'. destruct Hj' as (_&Hj'); inversion Hj'; subst; clear Hj'.
      destruct Hcase' as [(l''&Hin'&Hsome'&Heq'&Henv'')| (Hnone'&Heq')];
      subst; trivial. contradict Hnone; eauto.
    * eapply wb_env_incl; [exact HΓ |].
      apply sem_incl_up_br; apply HΣlocs; tauto.
    * intros l0 H0; simpl; case_loc_eq_dec; try tauto.
    * intros l0 H0; simpl; case_loc_eq_dec; try tauto.
    * intro l. rewrite cod_sem_up_br. split; try tauto. intros [HF|Hcod]; try tauto.
      split; trivial. intro; subst. destruct Hcod as (v'&Hcod&_).
      rewrite (proj1 HΣlocs) in Hcod; [discriminate|tauto].
    * apply sem_incl_up_br; apply HΣlocs; tauto.
    * intros; rewrite sr_up_br.
      case loc_eq_dec; intro; split; intro H''; inversion H''; subst; trivial;
      [vidtac|tauto].
    * intro; apply proper_up_br_none; trivial; apply HΣlocs; tauto.
- nsplit 9; eauto; try tauto.
  + intros Σ'0 H'; inversion H'; subst; clear H'; split.
  + intros l v' H'. rewrite H'. split; discriminate || tauto.
(* let case *)
- destruct Hj as (Σ''&Hj1&Hj2&HH). decompose record HH; clear HH.
  apply IHv1 in Hj1; trivial; auto.
  destruct Hj1 as (Hdet''&Henv''&HΣlocs''&Hbr''&Hcod''&Hincl''&Hvid''&Hpr'').
  assert(Hj2'' := Hj2). apply IHv2 in Hj2; simpl in *; trivial; clear IHv1 IHv2.
  + destruct Hj2 as (Hdet'&Henv'&HΣlocs'&Hbr'&Hcod'&Hincl'&Hvid'&Hpr'&Hc').
    assert(Hincl0: Σ'' ⊆ Σ').
    {
      etransitivity; [|apply Hincl'].
      apply sem_incl_up_ρ. rewrite HΣlocs'';[apply HΣlocs|]; tauto.
    }
    assert(Hincl: Σ ⊆ Σ') by(now transitivity Σ'').
    nsplit 9; trivial.
    * intros Σ'0 H'; apply destr_v in H'; trivial.
      destruct H' as (Σ'''&Hj1'&Hj2'&HH'). decompose record HH'; clear HH'.
      apply Hdet'' in Hj1'; subst.
      apply Hdet' in Hj2'; subst. trivial.
    * eauto.
    * intro l. rewrite in_app_iff. intros HF.
      rewrite HΣlocs'; [|tauto]. rewrite sr_up, HΣlocs''; [|tauto].
      case loc_eq_dec; tauto.
      * intro l. rewrite in_app_iff. intros HF.
        rewrite Hbr'; [|tauto]. rewrite br_up, Hbr''; [trivial|tauto].
      * intro l0. rewrite <- Hcod', cod_sem_up_sr_v, <- Hcod'', in_app_iff.
        intuition; subst. right; right; split; trivial.
        rewrite Hbr'' by tauto. auto.
    * intros l' v' H'. assert(Hl':= Hvid' l' v').
      rewrite sr_up in Hl'. case loc_eq_dec in Hl'.
      ++ subst. rewrite HΣlocs'; [|tauto].
         rewrite sr_up; case loc_eq_dec; try tauto.
         intros _; split; [intro H''; inversion H''; subst; tauto|].
         intros [H''| [H''|H''] ].
        ** inversion H''; now subst.
        ** apply value_identifier_locs_v in H''. tauto.
        ** apply value_identifier_locs_v in H''. tauto.
      ++ assert(Hl'':= Hvid'' l' v' H').
         case_eq (ρ Σ'' l'); [intros j Hj; split|split; [tauto|] ].
        ** intro H''. right; left; eapply Hvid''; eauto. rewrite Hj.
           apply Hincl0 in Hj. destruct Hj as (Hj&_).
           rewrite Hj in H''; inversion H''; now subst.
        ** intros [Heq | [Heq|Heq] ].
          --- inversion Heq; subst; tauto.
          --- now apply Hincl0, Hl''.
          --- apply value_identifier_locs_v in Heq.
              rewrite HΣlocs'' in Hj; [rewrite H' in Hj; discriminate|].
              intro HF; apply Hv in HF. tauto.
        ** intros [Heq | [Heq|Heq] ].
          --- inversion Heq; subst; tauto.
          --- now apply Hincl0, Hl''.
          --- intuition.
    * intro Hpr. apply Hpr'.
      apply proper_up; trivial; try (apply HΣlocs; tauto);
      try (rewrite Hbr'' || rewrite HΣlocs''); try apply HΣlocs; try tauto.
    * transitivity (E Σ''); tauto.
    * intros y ly Hy. apply bound_app.
      -- eapply bound_eq with (Σ2 := Σ''); auto.
        ++ intros la Ha. apply ref_locs_v in Ha.
           rewrite HΣlocs'; [|now apply Hv in Ha].
           case_loc_eq_dec;[intro; subst; tauto|trivial].
        ++ intros la Ha. apply ref_locs_v in Ha.
           rewrite Hbr'; [|now apply Hv in Ha]. trivial.
        ++ eapply Hpr''; eauto.
      -- eapply env_up_v_mon in Hy; destruct Hy; eapply Hc'; eauto.
  + apply wb_env_up_v; trivial.
    * rewrite HΣlocs''; auto;[apply HΣlocs|]; tauto.
    * rewrite Hbr'';[apply HΣlocs|];tauto.
  + apply sem_up_disj; try tauto.
    split; intros l0 Hl0; try case_loc_eq_dec.
    * rewrite HΣlocs''; [apply HΣlocs; auto with *|].
      intro HF; apply Hv in HF; tauto.
    * rewrite Hbr''; [apply HΣlocs; auto with *|].
      intro HF; apply Hv in HF; tauto.
(* fun case *)
- destruct Hj as (Hj&HH). decompose record HH; clear HH.
  assert(Hj' := Hj). apply IHv in Hj; simpl in*; auto; clear IHv.
  + destruct Hj as (Hdet'&Henv''&Hlocs'&Hbr'&Hcod'&Hincl'&Hpr').
    assert(Hincl: Σ ⊆ Σ').
    {
      etransitivity; [|apply Hincl'].
      apply sem_incl_up_ρ. apply HΣlocs; tauto.
    }
    nsplit 9; trivial.
    * intros Σ'0 H'; inversion H'; subst; clear H'; trivial. now apply Hdet'.
    * eapply wb_env_incl; [exact HΓ |]; trivial.
    * intros l0 HF. rewrite Hlocs'; [|tauto]. case_loc_eq_dec; tauto.
    * intros; intuition; subst; eauto.
    * intro l. rewrite <- Hcod', cod_sem_up_sr_v. intuition.
    * intros l0 v' Hl0. case (loc_eq_dec l0 vl).
     -- intro; subst. rewrite Hlocs'; [|tauto].
        rewrite sr_up; case loc_eq_dec;[|tauto].
        intros _; split; [intro Hv'| intros [Hv' | Hv'] ]; try inversion Hv'; try tauto.
        apply value_identifier_locs_v in Hv'. tauto.
     -- intro Hneq. split; intro H''.
      ++ right. eapply Hpr'; eauto. case_loc_eq_dec ; intro; subst; tauto.
      ++ destruct H'' as [ H'' |H''].
        ** inversion H''; now subst.
        ** apply Hpr'; trivial. rewrite sr_up. case loc_eq_dec; intro; subst; tauto.
    * intuition.
    * tauto.
    * intros y ly Hy. eapply env_up_v_mon in Hy; destruct Hy; eapply Hpr'; eauto.
  + apply wb_env_up_v; trivial; apply HΣlocs; tauto.
(* app case *)
- destruct Hj as (Σ''&Hj1&Hj2&HH). decompose record HH; clear HH.
  apply IHv1 in Hj1; trivial.
  destruct Hj1 as (Hdet''&Henv''''&Hlocs''&Hbr''&Hcod''&Hincl''&Hpr'').
  apply IHv2 in Hj2; auto; simpl in *; clear IHv1 IHv2.
  + destruct Hj2 as (Hdet'&Henv''&Hlocs'&Hbr'&Hcod'&Hincl'&Hpr').
    assert(Hincl: Σ ⊆ Σ') by eauto.
    nsplit 9; trivial.
    * intros Σ'0 H'; apply destr_v in H'; trivial.
      destruct H' as (Σ'''&Hj1'&Hj2'&HH'). decompose record HH'; clear HH'.
      apply Hdet'' in Hj1'; subst. apply Hdet' in Hj2'; now subst.
    * intros l0 HF. rewrite Hlocs'; [|auto with *].
      rewrite Hlocs'';auto with *.
    * intuition;rewrite Hbr'; auto with *.
    * intro l. rewrite <- Hcod', <- Hcod'', in_app_iff. tauto.
    * intros l v' Hl. case_eq (ρ Σ'' l).
      -- intros j Hj. split.
        ++ left. eapply Hpr''; eauto. rewrite Hj. apply Hincl' in Hj.
           destruct Hj as (Hj&_); now rewrite <- Hj.
        ++ intros [Heq|Heq].
          ** apply Hpr'' in Heq; trivial. now apply Hincl'.
          ** apply value_identifier_locs_v in Heq.
             rewrite Hlocs'' in Hj; [rewrite Hl in Hj; discriminate|].
             intro HF; apply Hv in HF. tauto.
      -- intro. split.
        ++ right. eapply Hpr'; eauto.
        ++ intros [Heq|Heq].
          ** apply Hpr'' in Heq; trivial. now apply Hincl'.
          ** now apply Hpr'.
    * tauto.
    * transitivity (E Σ''); tauto.
    * intros y ly Hy. apply bound_app.
      ** eapply bound_eq with (Σ2 := Σ''); auto.
        --- intros l Hl. apply ref_locs_v in Hl.
            rewrite Hlocs';trivial.
            now apply Hv in Hl.
        --- intros l Hl. apply ref_locs_v in Hl.
            rewrite Hbr';trivial.
            now apply Hv in Hl.
        --- eapply Hpr''; eauto.
      ** eapply Hpr'; eauto.
  + split; intros l0 Hl0.
    * rewrite Hlocs''; auto; intro HF; apply Hv in HF; tauto.
    * rewrite Hbr''; auto; intro HF; apply Hv in HF; tauto.
  + tauto.
- destruct Hj as (lv&ls&Hj1&Hj2&Hwf&Hlocs1&Hlocs2&Henv). assert(Hj1' := Hj1).
  apply deterministic_semantics_m_path in Hj1; trivial.
  destruct Hj1 as (Hdet&_&Hd&Hw&_). assert(Hj2' := Hj2).
  apply IHv in Hj2; auto. clear IHv.
  destruct Hj2 as (Hdet'&Henv''&Hlocs'&Hbr'&Hcod'&Hincl'&Hvid'&Hpr'&HE'&Hb').
  nsplit 9; auto; simpl; try tauto.
  + intros Σ'0 HΣ'0. inversion HΣ'0; subst. subst d.
    assert(Heq: Structural lv ls = Structural lv0 ls0) by (eapply Hdet; eauto).
    inversion Heq; subst. apply Hdet'; eauto.
  + eapply wb_env_incl; eauto.
  + intros y ly Hy. eapply p_sem_up_mon in Hy. destruct Hy. eapply Hb'; eassumption.
Qed.

Lemma sem_up_sr_id Σ l l' v: l <> l' -> ρ (Σ[[l ↦ v]]) l' = ρ Σ l'.
Proof. intro Hneq. simpl. rewrite sr_up. case loc_eq_dec; tauto. Qed.

Lemma weakening_v: forall v Σ Σ' Γ,
  wf_v v ->
  wb_env Γ Σ ->
  (Γ ⊓ locs_v v) ->
  (Σ ⊔ locs_v v) ->
  Σ / Γ ⊢v v ⇝ Σ' ->
    (forall y ly, not_sem_free Σ' Γ (ref_v v) y -> Σ / Γ[[y ↦v ly]] ⊢v v ⇝ Σ') (* weakening *).
Proof.
induction v; intros Σ Σ' Γ Hwf HΓ HΓlocs Hlocs2 Hj; destr_locs; inversion Hj; subst; simpl in *.
- assert (Henv': wb_env Γ (Σ [[ (vl, v)↦l envV Γ v]]))
  by (eapply wb_env_incl; [ exact HΓ|apply sem_incl_up_br; simpl; auto; apply Hlocs2; simpl; tauto]).
  intros y ly Hneq. case(val_eq_dec y v).
  + intro; subst y.
    assert(Hl:= Hneq vl). simpl in Hl.
    rewrite br_up_br, sr_up_br in Hl; case loc_eq_dec in Hl; try tauto. apply False_rect.
    destruct Hl as (l''&H1&H2); try tauto. inversion H1. tauto.
  + intro. subst l'. replace (envV Γ v) with (envV (Γ [[y ↦v ly]]) v) by now apply env_up_v_neq.
    econstructor.
- intros y ly Hneq; econstructor; eauto; eapply deterministic_semantics_m_path; eauto.
- intros y ly Hneq; econstructor; eauto.
  eapply deterministic_semantics_m_path; eauto.
- intros y ly Hneq; econstructor; eauto.
(* vlet *)
- assert(Hj1 := X).
  apply deterministic_semantics_v in X; auto with *;
  [|tauto|split; intros; apply Hlocs2; auto with *].
  destruct X as (_&Henv''&Hlocs1''&Hbr''&Hdecl1&Hincl1&_&_).
  assert(Hl1 : ρ Σ'' v1l = None)
  by (rewrite Hlocs1''; [apply Hlocs2; auto with *|tauto]).
  assert(Hl2 : ↣ Σ'' v1l = None)
   by (rewrite Hbr'';[apply Hlocs2|]; auto with *; tauto).
  assert(Henv' : wb_env (Γ [[v1 ↦v Some v1l]]) (Σ'' [[v1l ↦ IV v1]]))
  by (apply wb_env_up_v; trivial).
  assert(Hlocs0: (Σ'' [[v1l ↦ IV v1]]) ⊔ locs_v v3).
  {
    disjtac.
    - rewrite sem_up_sr_id, Hlocs1''; eauto.
      + apply Hlocs2; auto with *.
      + intro HF; apply Hwf in HF. tauto.
      + intro HF; subst. tauto.
    - rewrite br_up, Hbr''; [apply Hlocs2; auto with *|].
      intro HF; apply Hwf in HF; tauto.
  }
  assert(Hj2:= X0).
  apply deterministic_semantics_v in X0; eauto; try tauto;
  [| apply env_up_v_disj; auto with *; tauto].
  destruct X0 as (_&Henvf1'&Hlocs'&Hlocs''&_&Hinclf'&_).
  assert(Hincl0: Σ'' ⊆ Σ').
  {
    etransitivity; [|apply Hinclf'].
    apply sem_incl_up_ρ. rewrite Hlocs1'';[apply Hlocs2|]; auto with *; tauto.
  }
  intros y ly Hy. econstructor; eauto.
  + apply IHv1; eauto with *; try tauto. disjtac; apply Hlocs2; auto with *.
  +  case (val_eq_dec y v1); intro Heq; subst.
    * now rewrite env_up_v_idem.
    * rewrite env_up_v_comm; trivial. apply IHv2; eauto; try tauto.
      apply env_up_v_disj; auto with *; tauto.
(* fun case *)
- assert(Hj1 := X).
  assert(wb_env (Γ [[v ↦v Some vl]]) (Σ [[vl ↦ IV v]]))
  by (apply wb_env_up_v; trivial; apply Hlocs2; tauto).
  assert((Γ [[v ↦v Some vl]]) ⊓ locs_v v0) by (apply env_up_v_disj; auto with *; tauto).
  assert((Σ [[vl ↦ IV v]]) ⊔ locs_v v0) by
  (disjtac; case_loc_eq_dec; intro; subst; [tauto|]; apply Hlocs2; tauto).
  apply deterministic_semantics_v in X; auto with *; try tauto.
  destruct X as (Hdet'&Henv''&Hlocs'&Hbr'&Hcod'&Hincl'&Hpr').
  assert(Hincl: Σ ⊆ Σ')
  by (etransitivity; [|apply Hincl']; apply sem_incl_up_ρ; apply Hlocs2; tauto).
  intros y ly Hy. constructor. case (val_eq_dec y v); intro; subst.
  * now rewrite env_up_v_idem.
  * rewrite env_up_v_comm; trivial. apply IHv; auto; try tauto.
- assert(Hj1 := X).
  apply deterministic_semantics_v in X; auto with *; try tauto; [|disjtac].
  destruct X as (Hdet'&Henv''&Hlocs'&Hbr'&Hcod'&Hincl'&Hpr').
  assert(Σ'' ⊔ locs_v v2).
  {
    split; intros l0 Hl0.
    - rewrite Hlocs'; [| intro; apply Hwf in Hl0; tauto]. apply Hlocs2; auto with *.
    - rewrite Hbr'; [| intro; apply Hwf in Hl0; tauto]. apply Hlocs2; auto with *.
  }
  assert(Hj2 := X0).
  apply deterministic_semantics_v in X0; auto with *; try tauto.
  intros y ly Hy. econstructor.
  + eapply IHv1; eauto; auto with *; try tauto; [disjtac; apply Hlocs2; auto with *|].
    eapply not_sem_free_l in Hy.
    eapply not_sem_free_incl; [|exact Hy]. tauto.
  + apply IHv2; try tauto; auto with *. eauto.
- assert(Hj1 := X). apply deterministic_semantics_m_path in Hj1; auto.
  assert (Hj2 := X0). apply deterministic_semantics_v in Hj2; auto;
  [|apply wb_p_sem_env; tauto| now apply p_sem_env_disj].
  intros y ly Hy. econstructor.
  + apply Hj1; auto.
  + case(List.In_dec (option_eq_dec var_eq_dec) (Some (IV y)) (map (ρ Σ) lv)); intro Hdec.
    * rewrite p_sem_env_up_v_in; auto.
    * rewrite p_sem_env_up_v_out; trivial. apply IHv; eauto.
     -- apply wb_p_sem_env; tauto.
     -- apply not_sem_free_p_out; auto.
Defined.

Lemma deterministic_semantics_m:
(forall m Σ Γ Δ' Σ',
Σ / Γ ⊢ m ⋮ (Δ', Σ') ->
wb_env Γ Σ ->
wf_m m ->
(Γ ⊓ locs_m m) ->
(Σ ⊔ locs_m m) ->
  (forall Δ'' Σ'', Σ / Γ ⊢ m ⋮ (Δ'', Σ'') -> Σ' = Σ'' /\ Δ' = Δ'') ∧
  wb_env Γ Σ' ∧ wb_d Σ' Δ' ∧
  (forall l, ~ In l (locs_m m) -> ρ Σ' l = ρ Σ l) ∧
  (forall l, ~ In l (locs_m m) -> ↣ Σ' l = ↣ Σ l) ∧
  (forall l, (In l (decl_m m) \/ cod Σ l) <-> cod Σ' l) ∧
  (Σ ⊆ Σ') ∧
  (forall l v', ρ Σ l = None -> ρ Σ' l = Some (IV v') <-> value_identifier_m (v', l) m) ∧
  (proper Σ -> proper Σ') ∧
  (forall l, dom_rel (E Σ') l -> ~ dom_rel (E Σ) l -> In l (val_locs_m m) \/ env_locs Γ l) ∧
  (forall l, In l (locs_d Δ') -> In l (val_locs_m m) \/ env_locs Γ l) ∧
  (forall v, not_sem_free Σ' Γ (ref_m m) v -> forall l, Σ / Γ[[v ↦v l]] ⊢ m ⋮ (Δ', Σ')) ∧
  (forall y l, envV Γ y = Some l -> bound Σ' (ref_m m) y))
*
 (forall s Σ Γ Δ' Σ', Σ / Γ ⊢s s ⋮ (Δ', Σ') ->
  wb_env Γ Σ ->
  wf_list_s s ->
  (Γ ⊓ flat_map locs_s s) ->
  (Σ ⊔ flat_map locs_s s) ->
  (forall Δ'' Σ'', Σ / Γ ⊢s s ⋮ (Δ'', Σ'') -> Σ' = Σ'' /\ Δ' = Δ'') ∧
  wb_env Γ Σ' ∧ wb_d Σ' Δ' ∧
  (forall l, ~ In l (flat_map locs_s s) -> ρ Σ' l = ρ Σ l) ∧
  (forall l, ~ In l (flat_map locs_s s) -> ↣ Σ' l = ↣ Σ l) ∧
  (forall l, (In l (flat_map decl_s s) \/ cod Σ l) <-> cod Σ' l) ∧
  (Σ ⊆ Σ') ∧
  (forall l v', ρ Σ l = None -> ρ Σ' l = Some (IV v') <-> value_identifier_s (v', l) s) ∧
  (proper Σ -> proper Σ') ∧
  (forall l, dom_rel (E Σ') l -> ~ dom_rel (E Σ) l -> In l (flat_map val_locs_s s) \/ env_locs Γ l) ∧
  (forall l, In l (locs_d Δ') -> In l (flat_map val_locs_s s) \/ env_locs Γ l) ∧
  (forall v, not_sem_free Σ' Γ (flat_map ref_s s) v -> forall l, Σ / Γ[[v ↦v l]] ⊢s s ⋮ (Δ', Σ')) ∧
  (forall y l, envV Γ y = Some l -> bound Σ' (flat_map ref_s s) y))
*
 (forall t Σ Γ Δ' Σ', Σ / Γ ⊢t t ⋮ (Δ', Σ') ->
  wb_env Γ Σ ->
  wf_t t ->
  (Γ ⊓ locs_t t) ->
  (Σ ⊔ locs_t t) ->
    (forall Δ'' Σ'', Σ / Γ ⊢t t ⋮ (Δ'', Σ'') -> Σ' = Σ'' /\ Δ' = Δ'') ∧
     wb_env Γ Σ' ∧ wb_d Σ' Δ' ∧
    (forall l, ~ In l (locs_t t) -> ρ Σ' l = ρ Σ l) ∧
    (forall l, ~ In l (locs_t t) -> ↣ Σ' l = ↣ Σ l) ∧
    (forall l, (In l (decl_t t) \/ cod Σ l) <-> cod Σ' l) ∧
    (Σ ⊆ Σ') ∧
    (forall l v', ρ Σ l = None -> ρ Σ' l = Some (IV v') <-> value_identifier_t (v', l) t) ∧
    (proper Σ -> proper Σ') ∧
    (forall l, dom_rel (E Σ') l -> ~ dom_rel (E Σ) l -> In l (val_locs_t t) \/ env_locs Γ l) ∧
    (forall l, In l (locs_d Δ') -> In l (val_locs_t t) \/ env_locs Γ l) ∧
    (forall v, not_sem_free Σ' Γ (ref_t t) v -> forall l, Σ / Γ[[v ↦v l]] ⊢t t ⋮ (Δ', Σ')) ∧
    (forall y l, envV Γ y = Some l -> bound Σ' (ref_t t) y))
*
(forall S Σ Γ Δ' Σ', Σ / Γ ⊢S S ⋮ (Δ', Σ') ->
  wb_env Γ Σ ->
  wf_list_S S ->
  (Γ ⊓ flat_map locs_S S) ->
  (Σ ⊔ flat_map locs_S S) ->
    (forall Δ'' Σ'', Σ / Γ ⊢S S ⋮ (Δ'', Σ'') -> Σ' = Σ'' /\ Δ' = Δ'') ∧
    wb_env Γ Σ' ∧ wb_d Σ' Δ' ∧
    (forall l, ~ In l (flat_map locs_S S) -> ρ Σ' l = ρ Σ l) ∧
    (forall l, ~ In l (flat_map locs_S S) -> ↣ Σ' l = ↣ Σ l) ∧
    (forall l, (In l (flat_map decl_S S) \/ cod Σ l) <-> cod Σ' l) ∧
    (Σ ⊆ Σ') ∧
    (forall l v', ρ Σ l = None -> ρ Σ' l = Some (IV v') <-> value_identifier_S (v', l) S) ∧
    (proper Σ -> proper Σ') ∧
    (forall l, dom_rel (E Σ') l -> ~ dom_rel (E Σ) l -> In l (flat_map val_locs_S S) \/ env_locs Γ l) ∧
    (forall l, In l (locs_d Δ') -> In l (flat_map val_locs_S S) \/ env_locs Γ l) ∧
    (forall v, not_sem_free Σ' Γ (flat_map ref_S S) v -> forall l, Σ / Γ[[v ↦v l]] ⊢S S ⋮ (Δ', Σ')) ∧
    (forall y l, envV Γ y = Some l -> bound Σ' (flat_map ref_S S) y)).
Proof.
apply module_ind.
- intros; split; inversion X; subst; simpl in *.
  + intros Δ'' Σ'' H'. inversion H'; subst; tauto.
  + nsplit 11; auto; try tauto.
    * apply H.
    * intros l0 v' Hf1. split; intro; [vidtac|tauto].
    * intros l0 Hl0. right. right. eauto.
    * intros v Hv l0.
      replace (envT Γ t0) with (envT (Γ [[v ↦v l0]]) t0); [constructor|].
      now rewrite envV_up_v_t.
- intros; inversion X; subst; simpl in *.
  eapply deterministic_semantics_em_path in X0; eauto.
  destruct X0 as (Hdet&Henv&Hwb&Hr&Hbr&HE&Hp&Hmt&Hdom&Hc). split.
  + intros Δ'' Σ'' H'; inversion H'; subst.
    apply Hdet in X0. destruct X0 as [Hseq H8]; inversion H8; subst; clear H8.
    split; trivial. inversion Hwb; subst. assert (Heq := H9 _ _ H7 H12).
    simpl in Heq. rewrite H13 in Heq. apply Heq in H10; now inversion H10.
  + rewrite Hr, Hbr.
    nsplit 11; auto; try tauto.
    * inversion Hwb as [lv' ls' Hwb1 Hwb2 Hwb3 Hwb4|]; subst.
      eapply Hwb2; eauto.
    * intros l0. rewrite cod_eqt_eq with (Σ' := Σ'); eauto. tauto.
    * intros; split; intro; [vidtac|tauto].
    * intros. right; eauto.
    * intros. econstructor; eauto.
- intros [m l] t1 Hind1 t2 Hind2 Σ Γ Δ' Σ' H1 Henv Hwf Hlocs1 Hlocs2.
  inversion H1; subst; simpl in *.
  apply Hind1 in X; try tauto; auto with *; [|disjtac].
  destruct X as (Hdet'&Henv'&Hwb'&Hlocs'&Hbr'&Hcod'&Hincl'&Hvi'&Hpr'&HE'&Hd'&Hw'&Hmon').
  apply Hind2 in X0; try tauto; auto with *; clear Hind1 Hind2.
  + destruct X0 as (Hdet''&Henv''&Hwb''&Hlocs''&Hbr''&Hcod''&Hincl''&Hvi''&Hpr''&HE''&Hd''&Hw''&Hmon'').
    assert (Hincl''' : Σ'' ⊆ Σ').
    {
     etransitivity;[|apply Hincl''].
     apply sem_incl_up_ρ.  rewrite Hlocs';[apply Hlocs2|]; tauto.
    }
    nsplit 12.
    * intros Δ'' Σ''0 H''.
      inversion H''; subst.
      apply Hdet' in X; destruct X; subst.
      apply Hdet'' in X0; destruct X0; subst.
      tauto.
    * eapply wb_env_incl; eauto.
    * constructor; trivial. eapply wb_desc_incl; [| exact Hwb']; trivial.
    * intros l0 Hl0; rewrite in_app_iff in Hl0; rewrite Hlocs''; [|tauto].
      simpl; rewrite sr_up, Hlocs'; [|tauto]. case_loc_eq_dec; trivial. tauto.
    * intros l0 Hl0; rewrite in_app_iff in Hl0; rewrite Hbr''; [|tauto].
      simpl; rewrite br_up, Hbr'; [trivial|tauto].
    * intro l0. rewrite <- Hcod'', cod_sem_up_sr_m, <- Hcod', in_app_iff.
      split; [intros [ [HF|HF]|HF]|]; try tauto.
      -- right. intuition. subst. apply decl_locs_m in HF; tauto.
      -- right. split; try tauto. intro; subst. destruct HF as (v0&HF&_).
         rewrite (proj1 Hlocs2) in HF; [discriminate|tauto].
    * eauto.
    * intros l' v' H'.
      assert(Hl':= Hvi'' l' v'). simpl in Hl'.
      rewrite sr_up in Hl'; case loc_eq_dec in Hl'.
      ++ subst. rewrite Hlocs''; [|tauto].
         rewrite sr_up; case loc_eq_dec; try tauto.
         intro; split; intro H''; [inversion H'' | destruct H'' as [H''|H''] ].
        ** apply value_identifier_locs_m in H''. tauto.
        ** apply value_identifier_locs_m in H''. tauto.
      ++ case_eq (ρ Σ'' l'); [intros j Hj; split|split; [tauto|] ].
        ** intro H''. left. eapply Hvi'; eauto. rewrite Hj.
           apply Hincl''' in Hj. destruct Hj as (Hj&_).
           rewrite Hj in H''; inversion H''; now subst.
        ** intros [Heq|Heq].
          --- now apply Hincl''', Hvi'.
          --- apply value_identifier_locs_m in Heq.
              rewrite Hlocs' in Hj; [rewrite H' in Hj; discriminate|].
              intro HF; apply Hwf in HF. tauto.
        ** intros [Heq|Heq].
          --- now apply Hincl''', Hvi'.
          --- now apply Hl'.
    * intro Hbr. apply Hpr'', proper_up; try rewrite Hlocs'; try rewrite Hbr';
      try (apply Hlocs2); tauto.
    * clear Hvi'' Hvi' Hdet' Hdet''.
      intros l0 Hl0 Hl0'. rewrite in_app_iff.
      destruct (dom_rel_dec (E (Σ'' [[l ↦ IM m]])) l0) as [Hd1|Hd2].
      -- apply HE' in Hd1; trivial. destruct Hd1; auto with *.
      -- apply HE'' in Hl0; trivial. destruct Hl0 as [Hl0|Hl0]; auto with *.
         apply env_locs_up_m in Hl0. destruct Hl0 as [Hl0|Hl0]; auto with *.
         apply Hd' in Hl0. destruct Hl0; auto with *.
    * intros l0 Hl0. rewrite in_app_iff in Hl0; destruct Hl0 as [Hl0|Hl0].
      -- apply Hd' in Hl0. destruct Hl0; auto with *.
      -- apply Hd'' in Hl0. destruct Hl0 as [Hl0|Hl0]; auto with *.
         apply env_locs_up_m in Hl0. destruct Hl0 as [Hl0|Hl0]; auto with *.
         apply Hd' in Hl0. destruct Hl0; auto with *.
    * intros v Hv l0; econstructor.
      -- eapply Hw', not_sem_free_incl; [exact Hincl'''|]. eapply not_sem_free_l; exact Hv.
      -- rewrite env_up_v_m. eapply Hw''. eapply not_sem_free_r, not_sem_free_env_up_m. exact Hv.
    * intros y ly Hy. simpl. apply bound_app.
      -- eapply bound_eq with (Σ2 := Σ''); eauto; simpl.
        ++ intros la Ha. apply ref_locs_m in Ha.
           rewrite Hlocs''; [|intro HF; apply Hwf in HF; auto with *].
           simpl. case_loc_eq_dec;[intro; subst; tauto|trivial].
        ++ intros la Ha. apply ref_locs_m in Ha.
           rewrite Hbr''; [|intro HF; apply Hwf in HF; auto with *]. trivial.
      -- eapply Hmon''; eauto. now rewrite env_up_m_v, Hy.
  + eapply wb_env_incl; [| apply sem_incl_up_ρ]; [apply wb_env_up_m; auto|].
    rewrite Hlocs'; [|tauto]. auto. rewrite (proj1 Hlocs2); tauto.
  + apply env_up_m_disj; auto with *.
  + apply sem_up_disj; [ tauto |].
    split; intros l0 Hl0; try rewrite Hlocs'.
    * rewrite (proj1 Hlocs2); auto with *.
    * intro Hf; apply Hwf in Hf. tauto.
    * rewrite Hbr'. apply Hlocs2; auto with *.
      intro HF; apply Hwf in HF; tauto.
- intros t1 Hind [m l] q Σ Γ Δ' Σ' H1 Henv Hwf Hlocs1 Hlocs2.
  inversion H1; subst; simpl in *.
  apply Hind in X; try tauto; auto with *; [|disjtac]. clear Hind.
  destruct X as (Hdet'&Henv'&Hwb'&Hlocs'&Hbr'&Hcod'&Hincl'&Hvi'&Hpr'&HE'&Hd'&Hw'&Hmon').
  assert(Hincl0: Σ'' ⊆ (Σ'' [[l ↦ IM m]]))
  by (apply sem_incl_up_ρ; rewrite Hlocs'; [|tauto]; auto; rewrite (proj1 Hlocs2); tauto).
  apply deterministic_semantics_em_path in X0; try tauto; auto with *;
  [|eapply wb_env_incl; eauto].
  destruct X0 as (Hdet''&Henv''&Hwb''&Hlocs''&Hbr''&Hincl''&Hpr'').
  nsplit 12.
  + intros Δ'' Σ''0 H''; inversion H''; subst.
    apply Hdet' in X. destruct X; subst.
    apply Hdet'' in X0. destruct X0; subst. tauto.
  + intuition; eauto.
  + apply wb_desc_join. apply wb_s_mod; trivial. eapply wb_desc_incl;[|exact Hwb']. eauto.
  + intros l0 Hl0. rewrite in_app_iff in Hl0. rewrite sr_sem_join, Hlocs''.
    rewrite sr_up. case loc_eq_dec; try tauto. rewrite Hlocs'; [|tauto]. trivial.
  + intros l0 Hl0. rewrite in_app_iff in Hl0. rewrite br_sem_join, Hbr''.
    simpl; rewrite br_up, Hbr'; [trivial|tauto].
  + intros l0. rewrite <- cod_join. erewrite (@cod_eqt_eq Σ'0); try eassumption.
    rewrite cod_sem_up_sr_m, <- Hcod'. split; [intros [HF|HF]|]; try tauto.
    * intuition. subst. apply decl_locs_m in HF; tauto.
    * intuition; subst. eapply cod_not_None; [apply HF|intuition].
  + eauto.
  + intros l'' l' H'. erewrite <- Hvi', sr_sem_join, Hlocs'', sr_up; eauto.
    case loc_eq_dec; intro; subst; [|tauto].
    split; intro HF; try discriminate.
    apply Hvi', value_identifier_locs_m in HF; tauto.
  + intro; apply proper_sem_join.
    * eauto.
    * apply wb_singleton_ls; trivial. rewrite Hlocs''. eauto.
    * apply Hpr'', proper_up; try tauto.
      -- rewrite Hlocs' by tauto. apply Hlocs2; tauto.
      -- rewrite Hbr' by tauto. apply Hlocs2; tauto.
  + intro l0. rewrite dom_rel_app. intros [Hl0|Hl0].
    * apply dom_rel_join in Hl0; destruct Hl0 as [Hl0|Hl0].
      -- now apply Hd' in Hl0.
      -- simpl in Hl0. rewrite app_nil_r in Hl0. apply Hpr'' in Hl0; tauto.
    * intro Hl0'. destruct (dom_rel_dec (E Σ'') l0) as [Hd1|Hd2]; auto.
      apply Hpr'' in Hl0; tauto.
  + intros l0 Hl0; apply  s_mod_locs in Hl0. destruct Hl0 as [Hl0|Hl0]; auto.
    apply Hpr'' in Hl0; tauto.
  + intros v Hv l0; econstructor; eauto.
    eapply Hpr''; eauto.
  + intros y ly Hy. simpl.
    eapply bound_eq with (Σ2 := Σ''); eauto; simpl.
    * intros la Ha. apply ref_locs_m in Ha.
      rewrite sr_sem_join, Hlocs'', sem_up_sr_id; trivial.
      intro; subst; tauto.
    * intros la Ha. apply ref_locs_m in Ha.
      now rewrite br_sem_join, Hbr'', br_up.
- intros t1 Hind [m l] q Σ Γ Δ' Σ' H1 Henv Hwf Hlocs1 Hlocs2.  inversion H1; subst; simpl in *.
  apply Hind in X; try tauto; auto with *;[|disjtac]; clear Hind.
  destruct X as (Hdet'&Henv'&Hwb'&Hlocs'&Hbr'&Hcod'&Hincl'&Hvi'&Hpr'&HE' &Hd'&Hw'&Hmon').
  assert(Hincl'': Σ'' ⊆ (Σ'' [[l ↦ IM m]]))
  by (apply sem_incl_up_ρ; rewrite Hlocs'; [|tauto]; auto; rewrite (proj1 Hlocs2); tauto).
  apply deterministic_semantics_em_path in X0; try tauto; auto with *;
  [|eapply wb_env_incl; eauto].
  destruct X0 as (Hdet''&Henv''&Hwb''&Hlocs''&Hbr''&Hincl0&Hpr'').
  assert(Hincl''' : Σ ⊆ Σ'0) by (etransitivity;[apply Hincl'|]; eauto).
  assert(HwD: wb_d Σ'0 Δ) by (eapply wb_desc_incl; [| exact Hwb']; eauto).
  nsplit 12; auto.
  + intros Δ'' Σ''0 H''; inversion H''; subst.
    apply Hdet' in X. destruct X; subst.
    apply Hdet'' in X0. destruct X0; subst. tauto.
  + intros l0 Hl0. rewrite in_app_iff in Hl0. rewrite sr_sem_join, Hlocs''.
    simpl; case_loc_eq_dec; try tauto. intro. apply Hlocs'. tauto.
  + intros l0 Hl0. rewrite in_app_iff in Hl0. rewrite br_sem_join, Hbr''. simpl. rewrite br_up, Hbr'; tauto.
  + intros l0. rewrite <- cod_join. erewrite (@cod_eqt_eq Σ'0); try eassumption.
    rewrite cod_sem_up_sr_m, <- Hcod'. split; [intros [HF|HF]|]; try tauto.
    * intuition. subst. apply decl_locs_m in HF; tauto.
    * intuition; subst. eapply cod_not_None; [apply HF|intuition].
  + transitivity Σ'0; trivial.
  + intros l'' l' H'. erewrite <- Hvi', sr_sem_join, Hlocs'', sr_up; eauto.
    case loc_eq_dec; intro; subst; [|tauto].
    split; intro HF; try discriminate.
    apply Hvi', value_identifier_locs_m in HF; tauto.
  + intro; apply proper_sem_join.
    * eauto.
    * apply wb_singleton_ls; trivial. rewrite Hlocs''. eauto.
    * apply Hpr'', proper_up; try tauto.
      -- rewrite Hlocs' by tauto. apply Hlocs2; tauto.
      -- rewrite Hbr' by tauto. apply Hlocs2; tauto.
  + intro l0. rewrite dom_rel_app. intros [Hl0|Hl0].
    * apply dom_rel_join in Hl0; destruct Hl0 as [Hl0|Hl0].
      -- now apply Hd' in Hl0.
      -- simpl in Hl0. rewrite app_nil_r in Hl0. apply Hpr'' in Hl0; tauto.
    * intro Hl0'. destruct (dom_rel_dec (E Σ'') l0) as [Hd1|Hd2].
      -- apply HE' in Hd1; trivial.
      -- apply Hpr'' in Hl0; tauto.
  + intros l0 Hl0; apply desc_filtering_locs in Hl0. now apply Hd' in Hl0.
  + intros v Hv l0; econstructor; eauto.
    eapply Hpr''; eauto.
  + intros y ly Hy. simpl.
    eapply bound_eq with (Σ2 := Σ''); eauto; simpl.
    * intros la Ha. apply ref_locs_m in Ha.
      rewrite sr_sem_join, Hlocs'', sem_up_sr_id; trivial.
      intro; subst; tauto.
    * intros la Ha. apply ref_locs_m in Ha.
      now rewrite br_sem_join, Hbr'', br_up.
- intros l Hind Σ Γ Δ' Σ' H1 Henv Hwf Hlocs1 Hlocs2.
  inversion H1; subst.
  apply Hind in X; auto with *.
  destruct X as (Hdet'&Henv'&Hwb'&Hlocs'&Hbr'&Hincl'&Hpr').
  nsplit 12; try tauto.
  + intros Δ'' Σ''0 H''; inversion H''; subst. now apply Hdet' in X.
  + intros; constructor. now apply Hpr'.
- intros m Hind Σ Γ Δ' Σ' H1 Henv Hwf Hlocs1 Hlocs2.
  inversion H1; subst.
  assert(X' := X).
  apply Hind in X; auto with *.
  destruct X as (Hdet'&Henv'&Hwb'&Hlocs'&Hbr'&Hcod'&Hincl'&Hvi'&Hpr'&HE' &Hd').
  nsplit 12; try tauto.
  + intros Δ'' Σ''0 H''; inversion H''; subst. now apply Hdet' in X.
  + intros v Hv l0; econstructor; eauto. eapply Hd'; eauto.
- intros ls [v l] Hind Σ Γ Δ' Σ' H1 Henv Hwf Hlocs1 Hlocs2.
  inversion H1; subst. simpl in Hwf.
  assert(X' := X).
  apply Hind in X; auto with *; clear Hind.
  + destruct X as (Hdet'&Henv'&Hwb'&Hlocs'&Hbr'&Hcod'&Hincl'&Hvi'&Hpr'&HE' &Hd'&Hw'&Hmon').
    assert(Hincl : Σ ⊆ Σ'0)
    by (etransitivity; try apply Hincl'; apply sem_incl_up_ρ, Hlocs2; simpl; tauto).
    assert(Hwb0: wb_d Σ'0 (Structural [l] [])) by
    ( eapply wb_singleton_lv; eapply sem_incl_cod; try apply Hincl';
      apply sem_up_sr_cod; apply Hlocs2; simpl; tauto).
    nsplit 12; auto.
    * intros Δ'' Σ''0 H''; inversion H''; subst.
      apply Hdet' in X. destruct X as (Heq&Hs).
      inversion Hs; now subst.
    * eauto.
    * simpl. intros l0 Hl0. rewrite sr_sem_join, Hlocs'; try tauto.
      simpl. case_loc_eq_dec; intro; intros; subst; tauto.
    * simpl. intros l0 Hl0. rewrite br_sem_join, Hbr'; try tauto.
    * intros l0. rewrite <- cod_join, <- Hcod', cod_sem_up_sr_v. simpl.
      simpl in Hlocs2. intuition.
    * eauto.
    * unfold disj in *. simpl in *. intros l' v' H'.
      assert (HH:= Hvi' l' v'). simpl in HH. rewrite sr_up in HH; case loc_eq_dec in HH.
      -- subst. rewrite sr_sem_join, Hlocs';[|intro HF; apply Hwf in HF; tauto].
         rewrite sem_up_l. split; intro H''; [inversion H''; tauto|].
         destruct H'' as [H''|H'']; [inversion H''; tauto|].
         apply value_identifier_locs_m in H''. apply Hwf in H''; tauto.
      -- rewrite sr_sem_join, HH; trivial. split; try tauto.
         intros [H''|H'']; [inversion H''; subst|]; tauto.
    * intro. eapply proper_sem_join; trivial.
      apply Hpr', proper_up; trivial; apply Hlocs2; simpl; tauto.
    * intros l0 Hl0 Hl0'. simpl. apply dom_rel_sem_join in Hl0.
      destruct Hl0 as [Hl0|[Hl0|Hl0] ].
      -- simpl in Hl0; inversion Hl0; subst; try tauto.
      -- apply Hd' in Hl0. rewrite env_locs_up_v in Hl0. tauto.
      -- apply HE' in Hl0; trivial. rewrite env_locs_up_v in Hl0. tauto.
    * intros l0 Hl0. apply desc_sup_locs in Hl0.
      destruct Hl0 as [Hl0|Hl0].
      -- simpl in Hl0; inversion Hl0; subst; try tauto. simpl. tauto.
      -- apply Hd' in Hl0. rewrite env_locs_up_v in Hl0. simpl. tauto.
    * intros v0 Hv0 l0. econstructor.
      simpl in Hv0. case(val_eq_dec v0 v).
      -- intro; subst v. rewrite env_up_v_idem. exact X'.
      -- intro. rewrite env_up_v_comm by auto. eauto.
    * intros y ly Hy. simpl.
      eapply bound_eq with (Σ2 := Σ'0); eauto.
      eapply env_up_v_mon in Hy; destruct Hy; eapply Hmon'; eauto.
  + apply wb_env_up_v; trivial; simpl in Hlocs2; apply Hlocs2; tauto.
  + tauto.
  + simpl flat_map in Hlocs1. apply env_up_v_disj. unfold disj in Hwf. intuition.
    * eapply H6; auto with *.
    * auto with *.
  + simpl flat_map in Hlocs2. apply sem_up_disj. unfold disj in Hwf. intuition.
    * eapply H6; auto with *.
    * disjtac.
- intros ls [m l] t Hind1 Hind2 Σ Γ Δ' Σ' H1 Henv Hwf Hlocs1 Hlocs2.
  inversion H1; subst. simpl in Hwf, Hlocs1, Hlocs2.
  assert(X' := X).
  apply Hind2 in X; try solve[simpl in *; try tauto; auto with *]; [|simpl in *; disjtac].
  destruct X as (Hdet'&Henv'&Hwb'&Hlocs'&Hbr'&Hcod'&Hincl'&Hvi'&Hpr'&HE'&Hd'&Hw'&Hmon').
  assert(X0' := X0).
  apply Hind1 in X0; try tauto; auto with *; clear Hind1 Hind2.
  + destruct X0 as (Hdet''&Henv''&Hwb''&Hlocs''&Hbr''&Hcod''&Hincl''&Hvi''&Hpr''&HE''&Hd''&Hw''&Hmon'').
  assert(Hincl''' : Σ'' ⊆ Σ')
  by (etransitivity; try apply Hincl''; apply sem_incl_up_ρ; rewrite Hlocs';[apply Hlocs2|]; simpl in *; tauto).
    nsplit 12; trivial. 1-8: simpl in *.
    * intros Δ'' Σ''0 H''.
      inversion H''; subst.
      apply Hdet' in X; destruct X; subst.
      apply Hdet'' in X0; destruct X0; subst.
      inversion H0; subst. tauto.
    * eauto.
    * apply wb_desc_sup; auto with *.
      apply wb_singleton_ls; [ eapply wb_desc_incl; eauto|].
      left. exists m. apply Hincl''. trivial.
    * intros l0 Hl0. rewrite in_app_iff in Hl0.
      rewrite Hlocs''; [|tauto]. simpl; rewrite sr_up, Hlocs';[|tauto].
      case_loc_eq_dec; tauto.
    * intros l0 Hl0. rewrite in_app_iff in Hl0.
      rewrite Hbr''; [|tauto]. simpl; rewrite br_up, Hbr';[trivial|tauto].
    * intros l0. rewrite in_app_iff, <- Hcod'', cod_sem_up_sr_m, <- Hcod'.
      split; [intros [ [HF|HF]|HF]|]; try tauto.
      -- right. intuition. subst. apply decl_locs_m in HF; tauto.
      -- right. split; try tauto. intro; subst. destruct HF as (v0&HF&_).
         rewrite (proj1 Hlocs2) in HF; [discriminate|tauto].
    * eauto.
    * intros l' v' H'.
      assert(Hl':= Hvi'' l' v'). simpl in Hl'.
      rewrite sr_up in Hl'; case loc_eq_dec in Hl'.
      ++ subst. rewrite Hlocs''; [|intro HF; apply Hwf in HF; auto with *].
         rewrite sr_up; case loc_eq_dec; try tauto.
         intro; split; intro H''; [inversion H'' | destruct H'' as [H''|H''] ].
        ** apply value_identifier_locs_m in H''. tauto.
        ** apply value_identifier_locs_m in H''. apply Hwf in H''; auto with *; tauto.
      ++ case_eq (ρ Σ'' l'); [intros j Hj; split|split; [tauto|] ].
        ** intro H''. left. eapply Hvi'; eauto. rewrite Hj.
           apply Hincl''' in Hj. destruct Hj as (Hj&_).
           rewrite Hj in H''; inversion H''; now subst.
        ** intros [Heq|Heq].
          --- now apply Hincl''', Hvi'.
          --- apply value_identifier_locs_m in Heq.
              rewrite Hlocs' in Hj; [rewrite H' in Hj; discriminate|].
              intro HF; apply Hwf in Heq; auto with *; tauto.
        ** intros [Heq|Heq].
          --- now apply Hincl''', Hvi'.
          --- now apply Hl'.
    * intro. apply Hpr'', proper_up; trivial; eauto;
      try rewrite Hlocs'; try rewrite Hbr'; try (apply Hlocs2); try tauto.
    * clear Hvi' Hvi'' Hdet' Hdet''. intros l0 Hl0 Hl0'.
      destruct (dom_rel_dec (E (Σ''[[l ↦ IM m]])) l0) as [HE'''|HnE'''].
      -- apply HE' in HE'''; trivial. destruct HE'''; simpl; auto with *.
      -- simpl in HnE'''. apply HE'' in Hl0; trivial. simpl.
         destruct Hl0 as [Hl0|Hl0]; auto with *.
         apply env_locs_up_m in Hl0. destruct Hl0 as [Hl0|Hl0]; auto with *.
         apply Hd' in Hl0. destruct Hl0; auto with *.
    * clear Hvi' Hvi'' Hdet' Hdet''. intros l0 Hl0. apply desc_sup_locs in Hl0.
      destruct Hl0 as [Hl0|Hl0].
      -- simpl in Hl0. rewrite app_nil_r in Hl0. apply Hd' in Hl0.
         destruct Hl0; simpl; auto with *.
      -- apply Hd'' in Hl0. destruct Hl0 as [Hl0|Hl0]; simpl; auto with *.
         apply env_locs_up_m in Hl0. destruct Hl0 as [Hl0|Hl0]; auto with *.
         apply Hd' in Hl0. destruct Hl0; auto with *.
    * intros v Hv l0; simpl in Hv; econstructor; eauto.
      rewrite env_up_v_m. eauto.
    * simpl; intros y ly Hy. eapply bound_app; eauto.
      -- eapply bound_eq with (Σ2 := Σ''); eauto; simpl.
        ++ intros la Ha. apply ref_locs_m in Ha.
           rewrite Hlocs''; [|intro HF; apply Hwf in HF; auto with *].
           simpl. case_loc_eq_dec;[intro; subst; tauto|trivial].
        ++ intros la Ha. apply ref_locs_m in Ha.
           rewrite Hbr''; [|intro HF; apply Hwf in HF; auto with *]. trivial.
      -- eapply Hmon''. rewrite env_up_m_v; eassumption.
  + eapply wb_env_incl; [| apply sem_incl_up_ρ]; [apply wb_env_up_m; auto|].
    rewrite Hlocs';[|tauto]. apply Hlocs2; tauto.
  + apply env_up_m_disj; auto with *.
  + unfold disj in Hwf.
    apply sem_up_disj.
    * intro H; apply Hwf in H; auto with *.
    * split; intros l0 Hl0; try rewrite Hlocs'.
      -- rewrite (proj1 Hlocs2); auto with *.
      -- simpl in Hwf. intro Hf; apply Hwf in Hl0; tauto.
      -- rewrite Hbr'. apply Hlocs2; auto with *.
        intro HF; simpl in Hwf; apply Hwf in Hl0; tauto.
- intros ls [m l] q Hind Σ Γ Δ' Σ' H1 Henv Hwf Hlocs1 Hlocs2.
  inversion H1; subst; simpl in Hwf, Hlocs2, Hlocs1.
  assert(Hj1 := X). apply deterministic_semantics_m_path in X; auto.
  destruct X as (Hdet&Heq0&Hwb&_&HD).
  apply Hind in X0; try tauto; auto with *. clear Hind.
  + destruct X0 as (Hdet''&Henv''&Hwb''&Hlocs''&Hbr''&Hcod'&Hincl'&Hvi'&Hpr'&HE'&Hd'&Hw'&Hmon').
    assert(Hincl : Σ ⊆ Σ')
    by (etransitivity; try apply Hincl'; apply sem_incl_up_ρ; apply Hlocs2; tauto).
    nsplit 12; trivial. 1-8: simpl.
    * intros Δ'' Σ''0 H''. inversion H''; subst.
      apply Hdet in X; destruct X as (Heq1&Heq2); subst.
      apply Hdet'' in X0; destruct X0; inversion H0; subst; clear H0. tauto.
    * eauto.
    * apply wb_desc_sup; auto with *.
      apply wb_singleton_ls; [ eapply wb_desc_incl; eauto|].
      left. exists m. apply Hincl'. trivial.
    * intros l0 Hl0. rewrite in_app_iff in Hl0.
      rewrite Hlocs''; [|tauto]. simpl. case_loc_eq_dec; tauto.
    * intros l0 Hl0. rewrite in_app_iff in Hl0.
      rewrite Hbr''; [|tauto]. now simpl.
    * intros l0. rewrite <- Hcod', cod_sem_up_sr_m.
      split; [intros [HF|HF]|]; try tauto. right; split; trivial; intro; subst.
      eapply cod_not_None; eauto. intuition.
    * intros l' v' H'. case (loc_eq_dec l l').
     -- intro; subst.
        rewrite Hlocs'';[|intro H''; apply Hwf in H''; [tauto| auto with *] ].
        rewrite sem_up_l. split; [intro; discriminate|]. intro H''.
        apply value_identifier_locs_m in H''. apply Hwf in H''; [tauto| auto with *].
     -- intro. eapply Hvi'; eauto. rewrite sem_up_sr_id; trivial.
    * intro. apply Hpr', proper_up; trivial; eauto;
      try rewrite Hlocs'; try rewrite br_up, Hbr'; try (apply Hlocs2); tauto.
    * clear Hdet Hvi' Hdet''. intros l0 Hl0 Hl0'. simpl in HE'.
      apply HE' in Hl0; trivial. destruct Hl0 as [Hl0|Hl0]; auto with *.
      apply env_locs_up_m in Hl0. destruct Hl0 as [Hl0|Hl0]; auto with *.
    * clear Hvi' Hdet Hdet''. intros l0 Hl0. apply desc_sup_locs in Hl0. simpl.
      destruct Hl0 as [Hl0|Hl0].
      -- simpl in Hl0. rewrite app_nil_r in Hl0. apply HD in Hl0. auto with *.
      -- apply Hd' in Hl0. destruct Hl0 as [Hl0|Hl0]; simpl; auto with *.
         apply env_locs_up_m in Hl0. destruct Hl0 as [Hl0|Hl0]; auto with *.
    * intros v Hv l0; econstructor; eauto.
      -- eapply deterministic_semantics_m_path; eauto.
      -- rewrite env_up_v_m. apply Hw'. auto.
    * simpl; intros y ly Hy. eapply Hmon'. rewrite env_up_m_v; eassumption.
  + eapply wb_env_incl; [| apply sem_incl_up_ρ]; [apply wb_env_up_m; auto|].
    apply Hlocs2; tauto.
  + apply env_up_m_disj; auto with *.
  + unfold disj in Hwf.
    apply sem_up_disj.
    * intro H; apply Hwf in H; auto with *.
    * disjtac.
- intros ls [t l] Hind Σ Γ Δ' Σ' H1 Henv Hwf Hlocs1 Hlocs2.
  inversion H1; subst; simpl in Hwf, Hlocs1, Hlocs2.
  apply Hind in X; try tauto; auto with *; clear Hind.
  + destruct X as (Hdet''&Henv''&Hwb''&Hlocs''&Hbr''&Hcod''&Hincl''&Hvi''&Hpr''&HE''&Hd'').
    assert(Hincl : Σ ⊆ Σ')
    by (etransitivity; try apply Hincl''; apply sem_incl_up_ρ; apply Hlocs2; tauto).
    nsplit 12; trivial. 1-8: simpl.
    * intros Δ'' Σ''0 H''.
      inversion H''; subst.
      apply Hdet'' in X; destruct X; inversion H0; subst; tauto.
    * eauto.
    * apply wb_desc_sup; auto with *.
      apply wb_singleton_ls; [ eapply wb_desc_incl; eauto|].
      right. exists t. apply Hincl''. trivial.
    * intros l0 Hl0. rewrite Hlocs''; [|tauto]. simpl. case_loc_eq_dec; tauto.
    * intros l0 Hl0. rewrite Hbr''; [|tauto]. now simpl.
    * intros l0. rewrite <- Hcod'', cod_sem_up_sr_t.
      split; [intros [HF|HF]|]; try tauto. right; split; trivial; intro; subst.
      eapply cod_not_None; eauto. intuition.
    * intros l' v' H'. case (loc_eq_dec l l').
     -- intro; subst.
        rewrite Hlocs'';[|intro H''; apply Hwf in H''; [tauto| auto with *] ].
        rewrite sem_up_l. split; [intro; discriminate|]. intro H''.
        apply value_identifier_locs_m in H''. apply Hwf in H''; [tauto| auto with *].
     -- intro. eapply Hvi''; eauto. rewrite sem_up_sr_id; trivial.
    * intro. apply Hpr'', proper_up; trivial; eauto;
      try rewrite Hlocs'; try rewrite br_up, Hbr'; try (apply Hlocs2); tauto.
    * intros l0 Hl0 Hl0'. simpl in HE''. apply HE'' in Hl0; trivial.
      destruct Hl0 as [Hl0|Hl0]; auto with *. apply env_locs_up_t in Hl0.
      simpl in Hl0. destruct Hl0; auto with *. tauto.
    * intros l0 Hl0. apply desc_sup_locs in Hl0. simpl.
      destruct Hl0 as [Hl0|Hl0].
      -- simpl in Hl0. tauto.
      -- apply Hd'' in Hl0. destruct Hl0 as [Hl0|Hl0]; simpl; auto with *.
         apply env_locs_up_t in Hl0. simpl in Hl0.
         destruct Hl0 as [Hl0|Hl0]; auto with *. tauto.
    * intros; econstructor; eauto. rewrite env_up_v_t.
       apply Hd''. auto.
    * simpl; intros y ly Hy. eapply Hd''. rewrite env_up_t_v; eassumption.
  + eapply wb_env_incl; [| apply sem_incl_up_ρ]; [apply wb_env_up_t; auto|].
    apply Hlocs2; tauto.
  + apply env_up_t_disj; auto with *.
  + unfold disj in Hwf.
    apply sem_up_disj.
    * intro H; apply Hwf in H; auto with *.
    * disjtac.
- intros ls [t l] m Hind1 Hind2 Σ Γ Δ' Σ' H1 Henv Hwf Hlocs1 Hlocs2.
  inversion H1; subst. simpl in Hwf, Hlocs1, Hlocs2.
  apply Hind2 in X; try tauto; auto with *; [|disjtac].
  destruct X as (Hdet'&Henv'&Hwb'&Hlocs'&Hbr'&Hcod'&Hincl'&Hvi'&Hpr'&HE'&Hd'&Hw'&Hmon').
  apply Hind1 in X0; try tauto; auto with *; clear Hind1 Hind2.
  + destruct X0 as (Hdet''&Henv''&Hwb''&Hlocs''&Hbr''&Hcod''&Hincl''&Hvi''&Hpr''&HE''&Hd''&Hw''&Hmon'').
    assert(Hincl''' : Σ'' ⊆ Σ')
    by (etransitivity; try apply Hincl''; apply sem_incl_up_ρ; rewrite Hlocs';[apply Hlocs2|]; tauto).
     nsplit 12; trivial. 1-9: simpl.
    * intros Δ'' Σ''0 H''. inversion H''; subst.
      apply Hdet' in X; destruct X; subst.
      apply Hdet'' in X0; destruct X0; subst.
      inversion H0; subst. tauto.
    * eauto.
    * apply wb_desc_sup; auto with *.
      apply wb_singleton_ls; [ eapply wb_desc_incl; eauto|].
      right; exists t; apply Hincl''; trivial.
    * intros l0 Hl0. rewrite in_app_iff in Hl0.
      rewrite Hlocs''; [|tauto]. simpl; rewrite sr_up, Hlocs';[|tauto].
      case_loc_eq_dec; tauto.
    * intros l0 Hl0. rewrite in_app_iff in Hl0.
      rewrite Hbr''; [|tauto]. simpl; rewrite br_up, Hbr';[trivial|tauto].
    * intros l0. rewrite in_app_iff, <- Hcod'', cod_sem_up_sr_t, <- Hcod'.
      split; [intros [ [HF|HF]|HF]|]; try tauto.
      -- right. intuition. subst. apply decl_locs_m in HF; tauto.
      -- right. split; try tauto. intro; subst. destruct HF as (v0&HF&_).
         rewrite (proj1 Hlocs2) in HF; [discriminate|tauto].
    * eauto.
    * intros l' v' H'.
      assert(Hl':= Hvi'' l' v'). simpl in Hl'.
      rewrite sr_up in Hl'; case loc_eq_dec in Hl'.
      -- subst. rewrite Hlocs''; [|intro Hf; apply Hwf in Hf; auto with *].
         rewrite sr_up; case loc_eq_dec; try tauto.
         intro; split; [intro H''; inversion H''|].
         intros [H''|H'']; apply value_identifier_locs_m in H''; try tauto.
         apply Hwf in H''; auto with *; tauto.
      -- case_eq (ρ Σ'' l'); [intros j Hj; split|split; [tauto|] ].
        ** intro H''. left. eapply Hvi'; eauto. rewrite Hj.
           apply Hincl''' in Hj. destruct Hj as (Hj&_).
           rewrite Hj in H''; inversion H''; now subst.
        ** intros [Heq|Heq].
          --- now apply Hincl''', Hvi'.
          --- apply value_identifier_locs_m in Heq.
              rewrite Hlocs' in Hj; [rewrite H' in Hj; discriminate|].
              intro HF; apply Hwf in Heq; auto with *; tauto.
        ** intros [Heq|Heq].
          --- now apply Hincl''', Hvi'.
          --- now apply Hl'.
    * intro. apply Hpr'', proper_up; trivial; eauto;
      try rewrite Hlocs'; try rewrite Hbr'; try (apply Hlocs2); try tauto.
    * clear Hvi' Hvi'' Hdet' Hdet''. intros l0 Hl0 Hl0'.
      destruct (dom_rel_dec (E (Σ''[[l ↦ IT t]])) l0) as [HE'''|HnE'''].
      -- apply HE' in HE'''; trivial. destruct HE'''; simpl; auto with *.
      -- simpl in HnE'''. apply HE'' in Hl0; trivial. simpl.
         destruct Hl0 as [Hl0|Hl0]; auto with *.
         apply env_locs_up_t in Hl0. destruct Hl0 as [Hl0|Hl0]; auto with *.
         apply Hd' in Hl0. destruct Hl0; auto with *.
    * clear Hvi' Hvi'' Hdet' Hdet''. intros l0 Hl0. apply desc_sup_locs in Hl0.
      destruct Hl0 as [Hl0|Hl0].
      -- simpl in Hl0. rewrite app_nil_r in Hl0. apply Hd' in Hl0.
         destruct Hl0; simpl; auto with *.
      -- apply Hd'' in Hl0. destruct Hl0 as [Hl0|Hl0]; simpl; auto with *.
         apply env_locs_up_t in Hl0. destruct Hl0 as [Hl0|Hl0]; auto with *.
         apply Hd' in Hl0. destruct Hl0; auto with *.
    * intros v Hv l0; simpl in Hv; econstructor; eauto.
      rewrite env_up_v_t. apply Hw''. eauto.
    * intros y ly Hy. simpl. apply bound_app.
      -- eapply bound_eq with (Σ2 := Σ''); eauto; simpl.
        ++ intros la Ha. apply ref_locs_m in Ha.
           rewrite Hlocs''; [|intro HF; apply Hwf in HF; auto with *].
           simpl. case_loc_eq_dec;[intro; subst; tauto|trivial].
        ++ intros la Ha. apply ref_locs_m in Ha.
           rewrite Hbr''; [|intro HF; apply Hwf in HF; auto with *]. trivial.
      -- eapply Hmon''; eauto. now rewrite env_up_t_v, Hy.
  + eapply wb_env_incl; [| apply sem_incl_up_ρ]; [apply wb_env_up_t; auto|].
    rewrite Hlocs';[|tauto]. apply Hlocs2; tauto.
  + apply env_up_t_disj; auto with *.
  + unfold disj in Hwf. apply sem_up_disj.
    * intro H; apply Hwf in H; auto with *.
    * simpl in Hwf. disjtac.
      -- rewrite Hlocs'; auto with *;[|intro; apply Hwf in H; try tauto].
         apply Hlocs2. auto with *.
      -- rewrite Hbr'. apply Hlocs2; auto with *.
         intro HF; apply Hwf in H; tauto.
- intros ls t Hind1 Hind2 Σ Γ Δ' Σ' H1 Henv Hwf Hlocs1 Hlocs2.
  inversion H1; subst. simpl in Hwf, Hlocs1, Hlocs2.
  apply Hind2 in X; try tauto; auto with *; [|disjtac].
  destruct X as (Hdet'&Henv'&Hwb'&Hlocs'&Hbr'&Hcod'&Hincl'&Hvi'&Hpr'&HE'&Hd'&Hw'&Hmon').
  subst d' d Γ' DUd'.
  assert(Hdisj'': Σ'' ⊔ flat_map locs_S ls).
  {
    disjtac; try rewrite Hlocs'.
    - rewrite (proj1 Hlocs2); auto with *.
    - intro Hf; apply Hwf in Hf. tauto.
    - rewrite Hbr'. apply Hlocs2; auto with *.
      intro HF; apply Hwf in HF; tauto.
  }
  assert(X0' := X0).
  apply Hind1 in X0; try tauto; auto with *; clear Hind1 Hind2.
  + destruct X0 as (Hdet''&Henv''&Hwb''&Hlocs''&Hbr''&Hcod''&Hincl''&Hvi''&Hpr''&HE''&Hd''&Hw''&Hmon'').
     nsplit 12; trivial. 1-8: simpl.
    * intros Δ'' Σ''0 H''.
      inversion H''; subst.
      apply Hdet' in X. destruct X as [Heq HS]; subst.
      inversion HS; subst; clear HS.
      apply Hdet'' in X0. destruct X0 as  [Heq HS]; subst.
      inversion HS; clear HS. subst d' d Γ' DUd' lv' ls'. tauto.
    * eauto.
    * eapply wb_desc_eq; simpl; trivial.
      eapply wb_desc_sup; auto with *. eapply wb_desc_incl; eauto.
    * intros l0 Hl0. rewrite in_app_iff in Hl0.
      rewrite sr_sem_join, Hlocs''; [|tauto]. simpl; rewrite Hlocs';[trivial|tauto].
    * intros l0 Hl0. rewrite in_app_iff in Hl0.
      rewrite br_sem_join, Hbr''; [|tauto]. simpl; rewrite Hbr';[trivial|tauto].
    * intros l0. rewrite in_app_iff, <- cod_join, <- Hcod'', <- Hcod'. intuition.
    * eauto.
    * unfold disj in *. simpl in *. intros l' v' H'.
      rewrite sr_sem_join, <- Hvi'; trivial. split; [intro H'' | intros [H''|H''] ].
     -- case_eq (ρ Σ'' l').
      ++ intros j Hj.
         left. apply Hincl'' in Hj. destruct Hj as (Hj&_).
         rewrite Hj in H''. inversion H''; now subst.
      ++ intro Hn. apply Hvi'' in H''; tauto.
     -- now apply Hincl''.
     -- apply Hvi''; trivial. rewrite Hlocs'; trivial.
        intro HF; apply Hwf in HF; [tauto|].
        apply value_identifier_locs_m in H''; trivial.
    * intro.  eapply proper_sem_join; eauto; now apply Hpr'',Hpr'.
    * clear Hpr' Hpr'' Hdet' Hdet'' Hvi' Hvi''.
      intros l0 Hl0 Hl0'. apply dom_rel_sem_join in Hl0.
      destruct Hl0 as [Hl0|[Hl0|Hl0] ]; auto with *.
      -- simpl. eapply locs_desc_lv, Hd' in Hl0. destruct Hl0; auto with *.
      -- simpl. eapply locs_desc_lv, Hd'' in Hl0. destruct Hl0 as [Hl0|Hl0]; auto with *.
         apply env_locs_p in Hl0. destruct Hl0 as [Hl0|Hl0]; auto with *.
         apply Hd' in Hl0. destruct Hl0; auto with *.
      -- destruct (dom_rel_dec (E Σ'') l0) as [HE'''|HnE'''].
         ++ apply HE' in HE'''; trivial. destruct HE'''; simpl; auto with *.
         ++ apply HE'' in Hl0; trivial. simpl.
            destruct Hl0 as [Hl0|Hl0]; auto with *.
            apply env_locs_p in Hl0. destruct Hl0 as [Hl0|Hl0]; auto with *.
         apply Hd' in Hl0. destruct Hl0; auto with *.
    * clear Hpr' Hpr'' Hdet' Hdet'' Hvi' Hvi'' Hcod' Hcod''.
      intros l0 Hl0. apply desc_sup_locs in Hl0. simpl.
      destruct Hl0 as [Hl0|Hl0].
      -- apply Hd' in Hl0. destruct Hl0 as [Hl0|Hl0]; simpl; auto with *.
      -- apply Hd'' in Hl0. destruct Hl0 as [Hl0|Hl0]; simpl; auto with *.
         apply env_locs_p in Hl0. destruct Hl0 as [Hl0|Hl0]; auto with *.
         apply Hd' in Hl0. destruct Hl0 as [Hl0|Hl0]; auto with *.
    * intros v Hv l0; simpl in Hv; econstructor; eauto.
      case(List.In_dec (option_eq_dec var_eq_dec) (Some (IV v)) (map (ρ Σ'') lv)); intro H.
      ++ rewrite p_sem_env_up_v_in; eauto.
      ++ rewrite p_sem_env_up_v_out; trivial. apply Hw''.
         eapply not_sem_free_p_out; eauto.
    * intros y ly Hy. simpl. apply bound_app.
      -- eapply bound_eq with (Σ2 := Σ''); eauto; simpl.
        ++ intros la Ha. apply ref_locs_m in Ha.
           rewrite sr_sem_join, Hlocs''; [|intro HF; apply Hwf in HF; auto with *]. trivial.
        ++ intros la Ha. apply ref_locs_m in Ha.
           rewrite br_sem_join, Hbr''; [|intro HF; apply Hwf in HF; auto with *]. trivial.
      -- eapply p_sem_up_mon in Hy. destruct Hy. eapply Hmon''. eauto.
  + apply p_sem_env_disj; auto with *.
- intros Σ Γ Δ' Σ' H1 Henv Hwf Hlocs1 Hlocs2.
  simpl. inversion H1; subst.
 nsplit 11; auto.
  + intros d'' S'' H''; inversion H''; subst. tauto.
  + tauto.
  + intros. split; [intro; vidtac|tauto].
- intros ls p Hind Σ Γ Δ' Σ' H1 Henv Hwf Hlocs1 Hlocs2.
  inversion H1; subst. simpl in Hwf, Hlocs1, Hlocs2.
  apply deterministic_semantics_m_path in X; trivial.
  destruct X as (Hdet'&_&Hwb'&Hw'&Hd').
  assert(X0' := X0).
  apply Hind in X0; try tauto; auto with *; clear Hind.
  + destruct X0 as (Hdet''&Henv''&Hwb''&Hlocs''&Hbr''&Hcod''&Hincl''&Hvi''&Hpr''&HE''&Hd''&Hw'').
     nsplit 12; trivial. 1-5: simpl.
    * intros Δ'' Σ''0 H''.
      inversion H''; subst.
      apply Hdet' in X. destruct X as [Heq HS]; subst.
      inversion HS; subst; clear HS Heq.
      apply Hdet'' in X0. destruct X0 as  [Heq HS]; subst.
      inversion HS; clear HS. subst d' d Γ' DUd' lv' ls'. tauto.
    * eauto.
    * eapply wb_desc_eq; simpl; trivial.
      eapply wb_desc_sup; auto with *. eapply wb_desc_incl; eauto.
    * eauto.
    * intro.  eapply proper_sem_join; eauto; now apply Hpr'',Hpr'.
    * clear Hpr'' Hdet' Hdet'' Hvi''.
      intros l0 Hl0 Hl0'. apply dom_rel_sem_join in Hl0.
      destruct Hl0 as [Hl0|[Hl0|Hl0] ]; auto with *.
      -- simpl. eapply locs_desc_lv, Hd' in Hl0; tauto.
      -- simpl. eapply locs_desc_lv, Hd'' in Hl0. destruct Hl0 as [Hl0|Hl0]; auto with *.
         apply env_locs_p in Hl0. destruct Hl0 as [Hl0|Hl0]; auto with *.
      -- apply HE'' in Hl0; trivial. simpl.
         destruct Hl0 as [Hl0|Hl0]; auto with *.
         apply env_locs_p in Hl0. destruct Hl0 as [Hl0|Hl0]; auto with *.
    * clear Hpr'' Hdet' Hdet'' Hvi''. subst DUd'.
      intros l0 Hl0. apply desc_sup_locs in Hl0. simpl.
      destruct Hl0 as [Hl0|Hl0].
      -- apply Hd' in Hl0. tauto.
      -- apply Hd'' in Hl0. destruct Hl0 as [Hl0|Hl0]; simpl; auto with *.
         apply env_locs_p in Hl0. destruct Hl0 as [Hl0|Hl0]; auto with *.
    * intros v Hv H0. constructor; auto.
      -- case(List.In_dec (option_eq_dec var_eq_dec) (Some (IV v)) (map (ρ Σ) lv)); intro H.
         ++ rewrite p_sem_env_up_v_in; eauto.
         ++ rewrite p_sem_env_up_v_out; trivial. apply Hw''.
            eapply not_sem_free_p_out; eauto.
    * intros y ly Hy. eapply p_sem_up_mon in Hy. destruct Hy. eapply Hw''; eassumption.
  + apply wb_p_sem_env; auto.
  + apply p_sem_env_disj; auto with *.
(* module expressions *)
- intros [v l] e ls Hind Σ Γ Δ' Σ' H1 Henv Hwf Hlocs1 Hlocs2.
  inversion H1; subst. simpl in Hwf, Hlocs1, Hlocs2.
  assert(Hw' := X).
  apply deterministic_semantics_v in X; auto with *; [|intuition |disjtac].
  destruct X as (Hdet'&Henv'&Hlocs'&Hbr'&Hcod'&Hincl'&Hvi'&Hpr'&HE'&Hmon').
  assert(Hj2:= X0). apply Hind in X0; try tauto.
  + destruct X0 as (Hdet''&Henv''&Hwb''&Hlocs''&Hbr''&Hcod''&Hincl''&Hvi''&Hpr''&HE''&Hd''&Hw''&Hmon''); clear Hind.
    assert(Hincl0 : Σ'' ⊆ Σ'0)
    by (etransitivity; try apply Hincl''; apply sem_incl_up_ρ; rewrite Hlocs';[apply Hlocs2|]; tauto).
    assert(Hincl''': Σ'' ⊆ (Σ'0 [[Structural [l] [] ⊛ Structural lv ls0]]))
    by eauto.
    assert(Hwb' : wb_d Σ'0 (Structural [l] [])).
    {
      eapply wb_singleton_lv.
      eapply sem_incl_cod; try apply Hincl''.
      apply sem_up_sr_cod. rewrite Hbr'; [|tauto]. apply Hlocs2; tauto.
    }
    assert(Hincl : Σ ⊆ (Σ'0 [[Structural [l] [] ⊛ Structural lv ls0]])) by (etransitivity; try apply Hincl'; trivial).
    nsplit 12; trivial. 1-8: simpl.
    * intros Δ'' Σ'1 H'; inversion H'; subst; clear H'; trivial.
      apply Hdet' in X; subst.
      apply Hdet'' in X0. destruct X0 as [Heq Hd].
      inversion Hd; subst. tauto.
    * eauto.
    * eapply wb_desc_eq; simpl; trivial.
      apply wb_desc_sup; trivial.
    * simpl. intros l0 Hl0. rewrite in_app_iff in Hl0.
      rewrite sr_sem_join, Hlocs'';[|tauto]. simpl.
      rewrite sr_up; case loc_eq_dec; intro; intros; subst; [tauto|].
      rewrite Hlocs';[trivial|tauto].
    * simpl. intros l0 Hl0. rewrite in_app_iff in Hl0.
      rewrite br_sem_join, Hbr'';[|tauto]. simpl. rewrite br_up, Hbr';[trivial|tauto].
    * intro l0. rewrite <- cod_join, <- Hcod'', cod_sem_up_sr_v, <- Hcod', in_app_iff.
      intuition; subst. right; right; split; trivial.
      rewrite Hbr' by tauto. auto.
    * intros l' v' H'. assert(Hl':= Hvi'' l' v').
      rewrite sr_up in Hl'. rewrite sr_sem_join. case loc_eq_dec in Hl'.
      ++ subst. rewrite Hlocs''; [|intro HF; apply Hwf in HF; auto with *; tauto].
         rewrite sr_up; case loc_eq_dec; try tauto.
         intros _; split; [intro H''; inversion H''; subst; tauto|].
         intros [H''| [H''|H''] ].
        ** inversion H''; now subst.
        ** apply value_identifier_locs_v in H''. tauto.
        ** apply value_identifier_locs_m in H''. apply Hwf in H''; auto with *; tauto.
      ++ assert(Hl'':= Hvi' l' v' H').
         case_eq (ρ Σ'' l'); [intros j Hj; split|split; [tauto|] ].
        ** intro H''. right; left; eapply Hvi'; eauto. rewrite Hj.
           apply Hincl0 in Hj. destruct Hj as (Hj&_).
           rewrite Hj in H''; inversion H''; now subst.
        ** intros [Heq | [Heq|Heq] ].
          --- inversion Heq; subst; tauto.
          --- now apply Hincl0, Hl''.
          --- apply value_identifier_locs_m in Heq.
              rewrite Hlocs' in Hj; [rewrite H' in Hj; discriminate|].
              intro HF; apply Hwf in Heq; auto with *; tauto.
        ** intros [Heq | [Heq|Heq] ].
          --- inversion Heq; subst; tauto.
          --- now apply Hincl0, Hl''.
          --- intuition.
    * intro. apply proper_sem_join; trivial.
      apply Hpr'', proper_up; trivial; eauto;
      try rewrite Hlocs'; try rewrite Hbr'; try (apply Hlocs2); tauto.
    * clear Hvi' Hdet' Hdet'' Hvi''.
      intros l0 Hl0 Hl0'. simpl. apply dom_rel_sem_join in Hl0.
      destruct Hl0 as [Hl0|[Hl0|Hl0] ].
      -- simpl in Hl0; inversion Hl0; subst; try tauto.
      -- apply Hd'' in Hl0. rewrite env_locs_up_v in Hl0. tauto.
      -- apply HE'' in Hl0; simpl; trivial.
         ++ rewrite env_locs_up_v in *; simpl; tauto.
         ++ now rewrite HE'.
    * clear Hvi' Hdet' Hdet'' Hvi''.
      intros l0 Hl0. apply desc_sup_locs in Hl0.
      destruct Hl0 as [Hl0|Hl0].
      -- simpl in Hl0; inversion Hl0; subst; try tauto. simpl. tauto.
      -- apply Hd'' in Hl0. rewrite env_locs_up_v in Hl0. simpl. tauto.
    * intros v0 Hv0 l0. econstructor.
      -- eapply weakening_v; eauto; auto with *; try tauto disjtac. intuition.
      -- simpl in Hv0. case(val_eq_dec v0 v).
         ++ intro; subst v. now rewrite env_up_v_idem.
         ++ intro. rewrite env_up_v_comm by auto. eauto.
    * intros y ly Hy. simpl. apply bound_app.
      -- eapply bound_eq with (Σ2 := Σ''); eauto; simpl.
        ++ intros la Ha. apply ref_locs_v in Ha.
           rewrite sr_sem_join, Hlocs''; [|intro HF; apply Hwf in HF; auto with *].
           simpl. case_loc_eq_dec;[intro; subst; tauto|trivial].
        ++ intros la Ha. apply ref_locs_v in Ha.
           rewrite br_sem_join, Hbr''; [|intro HF; apply Hwf in HF; auto with *]. trivial.
      -- eapply env_up_v_mon in Hy; destruct Hy; eapply Hmon''; eauto.
  + apply wb_env_up_v; trivial.
    * rewrite Hlocs'; try tauto; apply Hlocs2; tauto.
    * rewrite Hbr'; try tauto; apply Hlocs2; tauto.
  + intros l0 Hl0 v' Heqv'. destruct Γ as [ [ Γv Γm] Γt ]. simpl in *.
    case val_eq_dec as [Heqv | Hneqv] in Heqv'.
    * injection Heqv'; intros; subst l0 v'; clear Heqv'.
      unfold disj in *. apply Hwf in Hl0; auto with *.
    * eapply Hlocs1 in Heqv'; auto with *.
  + apply sem_up_disj;[intro HF; apply Hwf in HF; auto with *|].
    replace (E Σ'') with (E Σ) by intuition.
    disjtac.
    * rewrite Hlocs'. apply Hlocs2; auto with *.
      intro Hf; apply Hwf in H; auto with *.
    * rewrite Hbr'. apply Hlocs2; auto with *.
      intro Hf; apply Hwf in H; auto with *.
- intros e ls Hind Σ Γ Δ' Σ' H1 Henv Hwf Hlocs1 Hlocs2.
  inversion H1; subst. simpl in Hwf, Hlocs1, Hlocs2.
  assert(Hw' := X).
  apply deterministic_semantics_v in X; auto with *;[|intuition |disjtac].
  destruct X as (Hdet'&Henv'&Hlocs'&Hbr'&Hcod'&Hincl'&Hvi'&Hpr'&HE'&Hmon').
  assert(Hj2:= X0). apply Hind in X0; try tauto.
  + destruct X0 as (Hdet''&Henv''&Hwb''&Hlocs''&Hbr''&Hcod''&Hincl''&Hvi''&Hpr''&HE''&Hd''&Hw''&Hmon''); clear Hind.
    nsplit 12; trivial. 1-8: simpl.
    * intros Δ'' Σ'1 H'; inversion H'; subst; clear H'; trivial.
      apply Hdet' in X; subst.
      apply Hdet'' in X0. destruct X0 as [Heq Hd].
      inversion Hd; subst. tauto.
    * simpl. intros l0 Hl0. rewrite in_app_iff in Hl0.
      rewrite Hlocs'';[|tauto]. simpl.
      rewrite Hlocs';[trivial|tauto].
    * simpl. intros l0 Hl0. rewrite in_app_iff in Hl0.
      rewrite Hbr'';[|tauto]. simpl. rewrite Hbr';[trivial|tauto].
    * intros l0. rewrite in_app_iff, <- Hcod'', <- Hcod'. intuition.
    * etransitivity; eauto.
    * intros l v' Hl. case_eq (ρ Σ'' l).
      -- intros j Hj. split.
        ++ left. eapply Hvi'; eauto. rewrite Hj. apply Hincl'' in Hj.
           destruct Hj as (Hj&_); now rewrite <- Hj.
        ++ intros [Heq|Heq].
          ** apply Hvi' in Heq; trivial. now apply Hincl''.
          ** apply value_identifier_locs_m in Heq.
             rewrite Hlocs' in Hj; [rewrite Hl in Hj; discriminate|].
             intro HF; apply Hwf in HF. tauto.
      -- intro. split.
        ++ right. apply Hvi''; auto.
        ++ intros [Heq|Heq].
          ** apply Hvi' in Heq; trivial. now apply Hincl''.
          ** now apply Hvi''.
    * intro. apply Hpr''. tauto.
    * clear Hvi' Hdet' Hdet'' Hvi''.
      intros l0 Hl0 Hl0'.
      apply HE'' in Hl0; simpl; trivial. now rewrite HE'.
    * intros v0 Hv0 l0. econstructor.
      -- eapply weakening_v; eauto; auto with *; try tauto; disjtac.
      -- eauto.
    * intros y ly Hy. simpl. apply bound_app.
      -- eapply bound_eq with (Σ2 := Σ''); eauto; simpl.
        ++ intros la Ha. apply ref_locs_v in Ha.
           rewrite Hlocs''; [|intro HF; apply Hwf in HF; auto with *].
           simpl. trivial.
        ++ intros la Ha. apply ref_locs_v in Ha.
           rewrite Hbr''; [|intro HF; apply Hwf in HF; auto with *]. trivial.
      -- eapply Hmon''; eauto.
  + intros l0 Hl0 v' Heqv'. destruct Γ as [ [ Γv Γm] Γt ]. simpl in *.
    eapply Hlocs1 in Heqv'; auto with *.
  + replace (E Σ'') with (E Σ) by intuition.
    disjtac.
    * rewrite Hlocs'. apply Hlocs2; auto with *.
      intro Hf; apply Hwf in H; auto with *.
    * rewrite Hbr'. apply Hlocs2; auto with *.
      intro Hf; apply Hwf in H; auto with *.
- intros [m l] e ls Hind1 Hind2 Σ Γ Δ' Σ' H1 Henv Hwf Hlocs1 Hlocs2.
  inversion H1; subst. simpl in Hwf, Hlocs1, Hlocs2.
  apply Hind2 in X; try tauto; auto with *.
  destruct X as (Hdet'&Henv'&Hwb'&Hlocs'&Hbr'&Hcod'&Hincl'&Hvi'&Hpr'&HE'&Hd'&Hw'&Hmon').
  apply Hind1 in X0; try tauto; auto with *; clear Hind1 Hind2.
  + destruct X0 as (Hdet''&Henv''&Hwb''&Hlocs''&Hbr''&Hcod''&Hincl''&Hvi''&Hpr''&HE''&Hd''&Hw''&Hmon'').
    assert(Hincl''' : Σ'' ⊆ Σ') by
      (etransitivity; try apply Hincl'';
       apply sem_incl_up_ρ; rewrite Hlocs';[apply Hlocs2|]; tauto).
    nsplit 12; trivial. 1-10: simpl.
    * intros Δ'' Σ''0 H''. inversion H''; subst.
      apply Hdet' in X; destruct X; subst.
      apply Hdet'' in X0; destruct X0; subst.
      inversion H0; subst. tauto.
    * eauto.
    * apply wb_desc_sup; auto with *.
      apply wb_singleton_ls; [ eapply wb_desc_incl; eauto|].
      rewrite Hlocs'';[left; now exists m|].
      intro HF; apply Hwf in HF; auto with *.
    * intros x Hx. rewrite in_app_iff in Hx. rewrite Hlocs''; [|tauto].
      simpl. rewrite sr_up. case loc_eq_dec; intros; subst; [tauto|].
      rewrite Hlocs'; [trivial|tauto].
    * intros x Hx. rewrite in_app_iff in Hx. rewrite Hbr''; [|tauto].
      simpl. rewrite br_up, Hbr'; [trivial|tauto].
    * intros l0. rewrite in_app_iff, <- Hcod'', cod_sem_up_sr_m, <- Hcod'.
      split; [intros [ [HF|HF]|HF]|]; try tauto.
      -- right. intuition. subst. apply decl_locs_m in HF; tauto.
      -- right. split; try tauto. intro; subst. destruct HF as (v0&HF&_).
         rewrite (proj1 Hlocs2) in HF; [discriminate|tauto].
    * eauto.
    * intros l' v' H'.
      assert(Hl':= Hvi'' l' v'). simpl in Hl'.
      rewrite sr_up in Hl'; case loc_eq_dec in Hl'.
      -- subst. rewrite Hlocs''; [|intro HF; apply Hwf in HF; auto with *; tauto].
         rewrite sr_up; case loc_eq_dec; try tauto.
         intro; split; intro H''; [inversion H'' | destruct H'' as [H''|H''] ].
        ** apply value_identifier_locs_m in H''. tauto.
        ** apply value_identifier_locs_m in H''. apply Hwf in H''; auto with *; tauto.
      -- case_eq (ρ Σ'' l'); [intros j Hj; split|split; [tauto|] ].
        ++ intro H''. left. eapply Hvi'; eauto. rewrite Hj.
           apply Hincl''' in Hj. destruct Hj as (Hj&_).
           rewrite Hj in H''; inversion H''; now subst.
        ++ intros [Heq|Heq].
          ** now apply Hincl''', Hvi'.
          ** apply value_identifier_locs_m in Heq. rewrite Hlocs' in Hj; [vidtac|].
          intro. apply Hwf in Heq; simpl; tauto.
        ++ intros [Heq|Heq].
          ** now apply Hincl''', Hvi'.
          ** intuition.
    * intro. apply Hpr'', proper_up; trivial; eauto;
      try rewrite Hlocs'; try rewrite Hbr'; try (apply Hlocs2); tauto.
    * clear Hvi' Hvi'' Hdet' Hdet''. intros l0 Hl0 Hl0'.
      destruct (dom_rel_dec (E (Σ''[[l ↦ IM m]])) l0) as [HE'''|HnE'''].
      -- apply HE' in HE'''; trivial. destruct HE'''; simpl; auto with *.
      -- simpl in HnE'''. apply HE'' in Hl0; trivial. simpl.
         destruct Hl0 as [Hl0|Hl0]; auto with *.
         apply env_locs_up_m in Hl0. destruct Hl0 as [Hl0|Hl0]; auto with *.
         apply Hd' in Hl0. destruct Hl0; auto with *.
    * clear Hvi' Hvi'' Hdet' Hdet''. intros l0 Hl0. apply desc_sup_locs in Hl0.
      destruct Hl0 as [Hl0|Hl0].
      -- simpl in Hl0. rewrite app_nil_r in Hl0. apply Hd' in Hl0.
         destruct Hl0; simpl; auto with *.
      -- apply Hd'' in Hl0. destruct Hl0 as [Hl0|Hl0]; simpl; auto with *.
         apply env_locs_up_m in Hl0. destruct Hl0 as [Hl0|Hl0]; auto with *.
         apply Hd' in Hl0. destruct Hl0; auto with *.
    * simpl. intros y ly Hy; econstructor.
      -- apply Hw'. apply not_sem_free_l in ly.
         eapply not_sem_free_incl; [exact Hincl'''|exact ly].
      -- rewrite env_up_v_m. apply Hw''. eauto.
    * intros y ly Hy. simpl. apply bound_app.
      -- eapply bound_eq with (Σ2 := Σ''); eauto; simpl.
        ++ intros la Ha. apply ref_locs_m in Ha.
           rewrite Hlocs''; [|intro HF; apply Hwf in HF; auto with *].
           simpl. case_loc_eq_dec;[intro; subst; tauto|trivial].
        ++ intros la Ha. apply ref_locs_m in Ha.
           rewrite Hbr''; [|intro HF; apply Hwf in HF; auto with *]. trivial.
      -- eapply Hmon''. rewrite env_up_m_v; eauto.
  + eapply wb_env_incl; [| apply sem_incl_up_ρ]; [apply wb_env_up_m; auto|].
    rewrite Hlocs'; [|tauto]. apply Hlocs2; tauto.
  + apply env_up_m_disj; auto with *.
  + unfold disj in Hwf.
    apply sem_up_disj.
    * intro H; apply Hwf in H; auto with *.
    * split; intros l0 Hl0; rewrite Hlocs' || rewrite Hbr';
      try (apply Hlocs2; auto with *);
      intro HF; apply Hwf in Hl0; auto with *.
  + disjtac.
(* module type *)
- intros [t l] t0 ls Hind1 Hind2 Σ Γ Δ' Σ' H1 Henv Hwf Hlocs1 Hlocs2.
  inversion H1; subst.
  assert(X' := X).
  apply Hind2 in X; simpl in Hwf, Hlocs1, Hlocs2; try tauto; auto with *. clear Hind2.
  destruct X as (Hdet'&Henv'&Hwb'&Hlocs'&Hbr'&Hcod'&Hincl'&Hvi'&Hpr'&HE'&Hd'&Hw'&Hmon').
  assert(Hincl0: Σ'' ⊆ (Σ'' [[l ↦ IT t]]))
  by (apply sem_incl_up_ρ; rewrite Hlocs';[apply Hlocs2|]; simpl in *; tauto).
  assert(X0' := X0).
  apply Hind1 in X0; try tauto; auto with *; clear Hind1.
  + destruct X0 as (Hdet''&Henv''&Hwb''''&Hlocs''&Hbr''&Hcod''&Hincl''&Hvi''&Hpr''&HE''&Hd''&Hw''&Hmon'').
    assert(Hincl''' : Σ'' ⊆ Σ') by (etransitivity; try apply Hincl''; trivial).
    nsplit 12; trivial. 1-9: simpl.
    * intros Δ'' Σ''0 H''. inversion H''; subst.
      apply Hdet' in X; destruct X; subst.
      apply Hdet'' in X0; destruct X0; subst.
      inversion H0; subst. tauto.
    * eauto.
    * apply wb_desc_sup; auto with *.
      apply wb_singleton_ls; [ eapply wb_desc_incl; eauto|].
      rewrite Hlocs'';[|intro HF; apply Hwf in HF; auto with *]. eauto.
    * intros x Hx. rewrite in_app_iff in Hx. rewrite Hlocs''; [|tauto].
      simpl. rewrite sr_up. case loc_eq_dec; intros; subst; [tauto|].
      rewrite Hlocs'; [trivial|tauto].
    * intros x Hx. rewrite in_app_iff in Hx. rewrite Hbr''; [|tauto].
      simpl. rewrite br_up, Hbr'; [trivial|tauto].
    * intros l0. rewrite <- Hcod'', cod_sem_up_sr_t, <- Hcod', in_app_iff.
      split; [intros [ [HF|HF]|HF]|]; intuition.
      -- right. intuition. subst. apply decl_locs_m in HF; auto.
      -- right. split; intuition. subst. eapply cod_not_None; eauto.
    * eauto.
    * intros l' v' H'.
      assert(Hl':= Hvi'' l' v'). simpl in Hl'.
      rewrite sr_up in Hl'; case loc_eq_dec in Hl'.
      -- subst. rewrite Hlocs''; [|intro Hf; apply Hwf in Hf; auto with *].
         rewrite sr_up; case loc_eq_dec; try tauto.
         intro; split; [intro H''; inversion H''|].
         intros [H''|H''].
        ++ apply value_identifier_locs_m in H''; try tauto.
        ++ apply value_identifier_locs_m in H''; try tauto.
           apply Hwf in H''; auto with *; tauto.
      -- case_eq (ρ Σ'' l'); [intros j Hj; split|split; [tauto|] ].
        ** intro H''. left. eapply Hvi'; eauto. rewrite Hj.
           apply Hincl''' in Hj. destruct Hj as (Hj&_).
           rewrite Hj in H''; inversion H''; now subst.
        ** intros [Heq|Heq].
          --- now apply Hincl''', Hvi'.
          --- apply value_identifier_locs_m in Heq.
              rewrite Hlocs' in Hj; [rewrite H' in Hj; discriminate|].
              intro HF; apply Hwf in Heq; auto with *; tauto.
        ** intros [Heq|Heq].
          --- now apply Hincl''', Hvi'.
          --- now apply Hl'.
    * intro. apply Hpr'', proper_up; trivial; eauto;
      try rewrite Hlocs'; try rewrite Hbr'; try (apply Hlocs2); tauto.
    * clear Hvi' Hvi'' Hdet' Hdet''. intros l0 Hl0 Hl0'.
      destruct (dom_rel_dec (E (Σ''[[l ↦ IT t]])) l0) as [HE'''|HnE'''].
      -- apply HE' in HE'''; trivial. destruct HE'''; simpl; auto with *.
      -- simpl in HnE'''. apply HE'' in Hl0; trivial. simpl.
         destruct Hl0 as [Hl0|Hl0]; auto with *.
         apply env_locs_up_t in Hl0. destruct Hl0 as [Hl0|Hl0]; auto with *.
         apply Hd' in Hl0. destruct Hl0; auto with *.
    * clear Hvi' Hvi'' Hdet' Hdet''. intros l0 Hl0. apply desc_sup_locs in Hl0.
      destruct Hl0 as [Hl0|Hl0].
      -- simpl in Hl0. rewrite app_nil_r in Hl0. apply Hd' in Hl0.
         destruct Hl0; simpl; auto with *.
      -- apply Hd'' in Hl0. destruct Hl0 as [Hl0|Hl0]; simpl; auto with *.
         apply env_locs_up_t in Hl0. destruct Hl0 as [Hl0|Hl0]; auto with *.
         apply Hd' in Hl0. destruct Hl0; auto with *.
    * simpl. intros y ly Hy; econstructor; auto.
      -- apply Hw'. eapply not_sem_free_incl; eauto.
      -- rewrite env_up_v_t. apply Hw''. apply not_sem_free_env_up_t. eauto.
    * intros y ly Hy. simpl. apply bound_app.
      -- eapply bound_eq with (Σ2 := Σ''); eauto; simpl.
        ++ intros la Ha. apply ref_locs_m in Ha.
           rewrite Hlocs''; [|intro HF; apply Hwf in HF; auto with *].
           rewrite sem_up_sr_id; trivial. intro; subst; tauto.
        ++ intros la Ha. apply ref_locs_m in Ha.
           rewrite Hbr''; [|intro HF; apply Hwf in HF; auto with *]. trivial.
      -- eapply Hmon''. rewrite env_up_t_v; eassumption.
  + eapply wb_env_incl; [| apply sem_incl_up_ρ]; [apply wb_env_up_t; auto|].
    rewrite Hlocs'; [|tauto]. apply Hlocs2; tauto.
  + apply env_up_t_disj; auto with *.
  + unfold disj in Hwf.
    apply sem_up_disj.
    * intro H; apply Hwf in H; auto with *.
    * split; intros l0 Hl0; rewrite Hlocs' || rewrite Hbr'; try (apply Hlocs2; auto with *);
      intro HF; apply Hwf in Hl0; auto with *.
  + disjtac.
- intros m ls Hind1 Hind2 Σ Γ Δ' Σ' H1 Henv Hwf Hlocs1 Hlocs2.
  inversion H1; subst. simpl in Hwf, Hlocs1, Hlocs2.
  apply Hind1 in X; try tauto; auto with *; [|disjtac].
  destruct X as (Hdet'&Henv'&Hwb'&Hlocs'&Hbr'&Hcod'&Hincl'&Hvi'&Hpr'&HE'&Hd'&Hw'&Hmon').
  subst d' d Γ' DUd'. assert(Hj2 := X0).
  apply Hind2 in X0; try tauto; auto with *; clear Hind1 Hind2.
  + destruct X0 as (Hdet''&Henv''&Hwb''&Hlocs''&Hbr''&Hcod''&Hincl''&Hvi''&Hpr''&HE''&Hd''&Hw''&Hmon'').
    assert(Hincl0: Σ ⊆ (Σ'0 [[Structural lv [] ⊛ Structural lv' [] ]])) by eauto.
    nsplit 12; auto. 1-6: simpl.
    * intros Δ'' Σ''0 H''. inversion H''; subst.
      apply Hdet' in X. destruct X as [Heq HS]; subst.
      inversion HS; subst; clear HS.
      apply Hdet'' in X0. destruct X0 as  [Heq HS]; subst.
      inversion HS; clear HS. subst d' d Γ' DUd' lv' ls'. tauto.
    * eapply wb_env_incl;[|exact Hincl0]. trivial.
    * eauto.
    * intros l Hl. rewrite in_app_iff in Hl. simpl in Hl.
      rewrite sr_sem_join, Hlocs'', Hlocs'; tauto.
    * intros l Hl. rewrite in_app_iff in Hl. simpl in Hl.
      rewrite br_sem_join, Hbr'', Hbr'; tauto.
    * intros l0. rewrite in_app_iff, <- cod_join, <- Hcod'', <- Hcod'. intuition.
    * unfold disj in *. simpl in *. intros l' v' H'.
      rewrite sr_sem_join, <- Hvi'; trivial. split; [intro H'' | intros [H''|H''] ].
     -- case_eq (ρ Σ'' l').
      ++ intros j Hj.
         left. apply Hincl'' in Hj. destruct Hj as (Hj&_).
         rewrite Hj in H''. inversion H''; now subst.
      ++ intro Hn. apply Hvi'' in H''; tauto.
     -- now apply Hincl''.
     -- apply Hvi''; trivial. rewrite Hlocs'; trivial.
        intro HF; apply Hwf in HF; [tauto|].
        apply value_identifier_locs_m in H''; trivial.
    * intro. apply proper_sem_join; eauto.
    * clear Hpr' Hpr'' Hdet' Hdet'' Hvi' Hvi''.
      intros l0 Hl0 Hl0'. apply dom_rel_sem_join in Hl0.
      destruct Hl0 as [Hl0|[Hl0|Hl0] ]; auto with *.
      -- simpl. eapply locs_desc_lv, Hd' in Hl0. destruct Hl0; auto with *.
      -- simpl. eapply locs_desc_lv, Hd'' in Hl0. destruct Hl0 as [Hl0|Hl0]; auto with *.
         apply env_locs_p in Hl0. destruct Hl0 as [Hl0|Hl0]; auto with *.
         apply Hd' in Hl0. destruct Hl0; auto with *.
      -- destruct (dom_rel_dec (E Σ'') l0) as [HE'''|HnE'''].
         ++ apply HE' in HE'''; trivial. destruct HE'''; simpl; auto with *.
         ++ apply HE'' in Hl0; trivial. simpl.
            destruct Hl0 as [Hl0|Hl0]; auto with *.
            apply env_locs_p in Hl0. destruct Hl0 as [Hl0|Hl0]; auto with *.
         apply Hd' in Hl0. destruct Hl0; auto with *.
    * clear Hpr' Hpr'' Hdet' Hdet'' Hvi' Hvi''.
      intros l0 Hl0. apply desc_sup_locs in Hl0. simpl.
      destruct Hl0 as [Hl0|Hl0].
      -- apply Hd' in Hl0. destruct Hl0 as [Hl0|Hl0]; simpl; auto with *.
      -- apply Hd'' in Hl0. destruct Hl0 as [Hl0|Hl0]; simpl; auto with *.
         apply env_locs_p in Hl0. destruct Hl0 as [Hl0|Hl0]; auto with *.
         apply Hd' in Hl0. destruct Hl0 as [Hl0|Hl0]; auto with *.
    * simpl. intros y Hy ly; econstructor.
      -- apply Hw'. eapply not_sem_free_incl;[exact Hincl''|].
         now apply not_sem_free_l in Hy.
      -- case(List.In_dec (option_eq_dec var_eq_dec) (Some (IV y)) (map (ρ Σ'') lv)); intro H.
         ++ rewrite p_sem_env_up_v_in; eauto.
         ++ rewrite p_sem_env_up_v_out; trivial. apply Hw''.
            eapply not_sem_free_p_out; eauto.
    * intros y ly Hy. simpl. apply bound_app.
      -- eapply bound_eq with (Σ2 := Σ''); eauto; simpl.
        ++ intros la Ha. apply ref_locs_m in Ha.
           rewrite sr_sem_join, Hlocs''; [|intro HF; apply Hwf in HF; auto with *]. trivial.
        ++ intros la Ha. apply ref_locs_m in Ha.
           rewrite br_sem_join, Hbr''; [|intro HF; apply Hwf in HF; auto with *]. trivial.
      -- eapply p_sem_up_mon in Hy. destruct Hy. eapply Hmon''. eauto.
  + apply p_sem_env_disj; auto with *.
    split; intros; rewrite Hlocs' || rewrite Hbr'; try (apply Hlocs2; auto with *);
    intro HF; apply Hwf in HF; tauto.
  + split; intros; rewrite Hlocs' || rewrite Hbr'; try (apply Hlocs2; auto with *);
    intro HF; apply Hwf in HF; tauto.
- intros m Σ Γ Δ' Σ' H Henv Hwf Hlocs1 Hlocs2.
  inversion H; subst. apply deterministic_semantics_m_path in X; trivial.
  destruct X as (Hdet&_&Hwb&Hw&HD).
  nsplit 12; eauto 4.
  + intros Δ'' Σ'' H''; inversion H''. subst. auto.
  + simpl. intuition.
  + intros; split; intro; [vidtac|simpl in *; tauto].
  + intuition.
- intros [x l] t e Hind1 Hind2 Σ Γ Δ' Σ' H1 Henv Hwf Hlocs1 Hlocs2.
  inversion H1; subst; simpl in Hwf, Hlocs1, Hlocs2.
  assert(X' := X).
  apply Hind1 in X; try tauto; auto with *. clear Hind1.
  destruct X as (Hdet'&Henv'&Hwb'&Hlocs'&Hbr'&Hcod'&Hincl'&Hvi'&Hpr'&HE'&Hd'&Hw'&Hmon').
  apply Hind2 in X0; try tauto; auto with *; clear Hind2.
  + destruct X0 as (Hdet''&Henv''&Hwb''&Hlocs''&Hbr''&Hcod''&Hincl''&Hvi''&Hpr''&HE''&Hd''&Hw''&Hmon'').
    assert(Hincl''' :Σ'' ⊆ Σ') by
      (etransitivity; try apply Hincl'';
       apply sem_incl_up_ρ; rewrite Hlocs';[apply Hlocs2|]; tauto).
    simpl. nsplit 12; trivial.
    * intros Δ'' Σ''0 H''. inversion H''; subst.
      apply Hdet' in X; destruct X; subst.
      apply Hdet'' in X0; destruct X0; subst. tauto.
    * eauto.
    * constructor; trivial. eapply wb_desc_incl; [| exact Hwb']; trivial.
    * intros l0 Hl0; rewrite in_app_iff in Hl0. rewrite Hlocs'';[|tauto].
      simpl. rewrite sr_up. case loc_eq_dec;[tauto|].
      intros _. rewrite Hlocs'; [trivial|tauto].
    * intros l0 Hl0; rewrite in_app_iff in Hl0. rewrite Hbr'';[|tauto].
      simpl. rewrite br_up, Hbr'; [trivial|tauto].
    * intro l0. rewrite <- Hcod'', cod_sem_up_sr_m, <- Hcod', in_app_iff.
      split; [intros [ [HF|HF]|HF]|]; intuition.
      -- right. intuition. subst. apply decl_locs_m in HF; auto.
      -- right. split; intuition. subst. eapply cod_not_None; eauto.
    * eauto.
    * intros l' v' H'.
      assert(Hl':= Hvi'' l' v'). simpl in Hl'.
      rewrite sr_up in Hl'; case loc_eq_dec in Hl'.
      ++ subst. rewrite Hlocs''; [|tauto].
         rewrite sr_up; case loc_eq_dec; try tauto.
         intro; split; intro H''; [inversion H'' | destruct H'' as [H''|H''] ].
        ** apply value_identifier_locs_m in H''. tauto.
        ** apply value_identifier_locs_m in H''. tauto.
      ++ case_eq (ρ Σ'' l'); [intros j Hj; split|split; [tauto|] ].
        ** intro H''. left. eapply Hvi'; eauto. rewrite Hj.
           apply Hincl''' in Hj. destruct Hj as (Hj&_).
           rewrite Hj in H''; inversion H''; now subst.
        ** intros [Heq|Heq].
          --- now apply Hincl''', Hvi'.
          --- apply value_identifier_locs_m in Heq.
              rewrite Hlocs' in Hj; [rewrite H' in Hj; discriminate|].
              intro HF; apply Hwf in HF; tauto.
        ** intros [Heq|Heq].
          --- now apply Hincl''', Hvi'.
          --- now apply Hl'.
    * intro. apply Hpr'', proper_up; trivial; eauto;
      try rewrite Hlocs'; try rewrite Hbr'; try (apply Hlocs2); tauto.
    * clear Hvi'' Hvi' Hdet' Hdet''.
      intros l0 Hl0 Hl0'. rewrite in_app_iff.
      destruct (dom_rel_dec (E (Σ'' [[l ↦ IM x]])) l0) as [Hd1|Hd2].
      -- apply HE' in Hd1; trivial. destruct Hd1; auto with *.
      -- apply HE'' in Hl0; trivial. destruct Hl0 as [Hl0|Hl0]; auto with *.
         apply env_locs_up_m in Hl0. destruct Hl0 as [Hl0|Hl0]; auto with *.
         apply Hd' in Hl0. destruct Hl0; auto with *.
    * intros l0 Hl0. rewrite in_app_iff in Hl0; destruct Hl0 as [Hl0|Hl0].
      -- apply Hd' in Hl0. destruct Hl0; auto with *.
      -- apply Hd'' in Hl0. destruct Hl0 as [Hl0|Hl0]; auto with *.
         apply env_locs_up_m in Hl0. destruct Hl0 as [Hl0|Hl0]; auto with *.
         apply Hd' in Hl0. destruct Hl0; auto with *.
    * simpl. intros y ly Hy; econstructor.
      -- apply Hw'. eapply not_sem_free_incl; [exact Hincl'''|]; eauto.
      -- rewrite env_up_v_m. apply Hw''. apply not_sem_free_env_up_m. eauto.
    * intros y ly Hy. simpl. apply bound_app.
      -- eapply bound_eq with (Σ2 := Σ''); eauto; simpl.
        ++ intros la Ha. apply ref_locs_m in Ha.
           rewrite Hlocs''; [|intro HF; apply Hwf in HF; auto with *].
           rewrite sem_up_sr_id; trivial. intro; subst; tauto.
        ++ intros la Ha. apply ref_locs_m in Ha.
           rewrite Hbr''; [|intro HF; apply Hwf in HF; auto with *]. trivial.
      -- eapply Hmon''. rewrite env_up_m_v; eauto.
  + eapply wb_env_incl; [| apply sem_incl_up_ρ]; [apply wb_env_up_m; auto|].
    rewrite Hlocs';[|tauto]. apply Hlocs2; tauto.
  + apply env_up_m_disj; auto with *.
  + unfold disj in Hwf.
    eapply sem_up_disj.
    * intro H; apply Hwf in H; tauto.
    * split; intros l0 Hl0; rewrite Hlocs' || rewrite Hbr';
      try (apply Hlocs2; auto with *);
      intro HF; apply Hwf in Hl0; auto with *.
  + disjtac.
- intros m1 Hind1 m2 Hind2 Σ Γ Δ' Σ' H Henv Hwf Hlocs1 Hlocs2.
  inversion H; subst. simpl in *.
  apply Hind1 in X; auto with *;
  [| apply Hwf| disjtac].
  destruct X as (Hdet''&Henv''&Hwb''&Hlocs''&Hbr''&Hcod''&Hincl''&Hvi''&Hpr''&HE''&Hd''&Hw''&Hmon'').
  apply Hind2 in X0; auto with *; simpl in *; clear Hind1 Hind2.
  + destruct X0 as (Hdet'&Henv'&Hwb'&Hlocs'&Hbr'&Hcod'&Hincl'&Hvi'&Hpr'&HE'&Hd'&Hw'&Hmon').
    nsplit 12; auto.
    * intros Δ Σ' H'; inversion H'; subst; clear H'; trivial.
      apply Hdet'' in X; destruct X as [Heq Hd]; inversion Hd; subst.
      apply Hdet' in X0. intuition; now subst.
    * inversion Hwb''; subst; eauto.
    * intros x Hx. rewrite sr_sem_join, Hlocs';[|auto with *]. rewrite Hlocs''; auto with *.
    * intros x Hx. rewrite br_sem_join, Hbr';[|auto with *]. rewrite Hbr''; auto with *.
    * intro l0. rewrite in_app_iff, <- cod_join, <- Hcod', <- Hcod''. tauto.
    * eauto.
    * unfold disj in *. simpl in *. intros l' v' H'.
      rewrite sr_sem_join; trivial.
      split; [intro H'' | intros [H''|H''] ]; trivial.
     -- case_eq (ρ Σ'' l').
      ++ intros j Hj. left. apply Hvi''; trivial. rewrite Hj.
         apply Hincl' in Hj. destruct Hj as (Hj&_).
         now rewrite <- Hj, <- H''.
      ++ intro Hn. right. now apply Hvi'.
     -- now apply Hincl', Hvi''.
     -- rewrite <- Hlocs'' in H'; [apply Hvi'; trivial|].
        apply value_identifier_locs_m in H''.
        intro HF; apply Hwf in HF; auto with *; try tauto.
    * intro. apply proper_sem_join; try tauto. inversion Hwb''.
      eapply wb_desc_incl; trivial. eauto.
    * clear Hpr' Hpr'' Hdet' Hdet'' Hvi' Hvi''.
      intros l0 Hl0 Hl0'. apply dom_rel_sem_join in Hl0.
      destruct Hl0 as [Hl0|[Hl0|Hl0] ]; auto with *.
      -- destruct (Hd'' l0) as [Hl0''|Hl0'']; auto with *.
      -- destruct (Hd' l0) as [Hl0''|Hl0'']; auto with *.
      -- destruct (dom_rel_dec (E Σ'') l0) as [HE'''|HnE'''].
         ++ apply HE'' in HE'''; trivial. destruct HE'''; simpl; auto with *.
         ++ apply HE' in Hl0; trivial. destruct Hl0 as [Hl0|Hl0]; auto with *.
    * clear Hpr' Hpr'' Hdet' Hdet'' Hvi' Hvi''.
      intros l0 Hl0. destruct (Hd'' l0) as [Hl0''|Hl0'']; auto with *.
    * simpl. intros y ly Hy; econstructor.
      -- apply Hw''. eapply not_sem_free_incl; eauto.
      -- apply Hw'. eauto.
    * intros y ly Hy. simpl. apply bound_app.
      -- eapply bound_eq with (Σ2 := Σ''); eauto; simpl.
        ++ intros la Ha. apply ref_locs_m in Ha.
           rewrite sr_sem_join, Hlocs'; [|intro HF; apply Hwf in HF; auto with *]. trivial.
        ++ intros la Ha. apply ref_locs_m in Ha.
           rewrite br_sem_join, Hbr'; [|intro HF; apply Hwf in HF; auto with *]. trivial.
      -- eapply Hmon'. eauto.
  + tauto.
  + split; intros l0 Hl0; rewrite Hlocs'' || rewrite Hbr'';
    try (apply Hlocs2; auto with *);
    intro HF; apply Hwf in Hl0; auto with *.
- intros e t Hind1 Hind2 Σ Γ Δ' Σ' H Henv Hwf Hlocs1 Hlocs2.
  simpl in *. inversion H; subst.
  apply Hind1 in X; auto with *; [clear Hind1| apply Hwf| disjtac].
  destruct X as (Hdet''&Henv''&Hwb''&Hlocs''&Hbr''&Hcod''&Hincl''&Hvi''&Hpr''&HE''&Hd''&Hw''&Hmon'').
  apply Hind2 in X0; clear Hind2; auto with *;[| apply Hwf |].
  + destruct X0 as (Hdet'&Henv'&Hwb'&Hlocs'&Hbr'&Hcod'&Hincl'&Hvi'&Hpr'&HE'&Hd'&Hw'&Hmon').
    nsplit 12; auto.
    * intros Δ Σ' H'; inversion H'; subst; clear H'; trivial.
      apply Hdet'' in X. destruct X; subst.
      apply Hdet' in X0; destruct X0 as [Heq Hd]; inversion Hd; subst. tauto.
    * eapply wb_desc_eq; simpl; trivial. eapply wb_desc_mod; eauto.
    * intros x Hx. simpl. rewrite sr_sem_join, Hlocs';[|auto with *].
      rewrite Hlocs''; auto with *.
    * intros x Hx. simpl. rewrite br_sem_join, Hbr';[|auto with *]. rewrite Hbr''; auto with *.
    * intro l0. rewrite in_app_iff, <- cod_join, <- Hcod', <- Hcod''. tauto.
    * eauto.
    * unfold disj in *. simpl in *. intros l' v' H'.
      rewrite sr_sem_join; trivial.
      split; [intro H'' | intros [H''|H''] ]; trivial.
     -- case_eq (ρ Σ'' l').
      ++ intros j Hj. left. apply Hvi''; trivial. rewrite Hj.
         apply Hincl' in Hj. destruct Hj as (Hj&_).
         now rewrite Hj in H''.
      ++ intro Hn. rewrite <- Hvi'; tauto.
     -- rewrite <- Hvi'' in H''; trivial. now apply Hincl'.
     -- rewrite <- Hvi' in H''; trivial.
        rewrite <- Hlocs'' in H'; trivial.
        apply value_identifier_locs_m in H''.
        intro HF; apply Hwf in HF; auto with *; try tauto.
    * intuition. eauto.
    * intros l0 Hl0 Hl0'. apply dom_rel_sem_join in Hl0.
      destruct Hl0 as [Hl0|[Hl0|Hl0] ]; auto with *.
      -- destruct (Hd'' l0) as [Hl0''|Hl0'']; auto with *.
      -- destruct (Hd' l0) as [Hl0''|Hl0'']; auto with *.
      -- destruct (dom_rel_dec (E Σ'') l0) as [HE'''|HnE'''].
         ++ apply HE'' in HE'''; trivial. destruct HE'''; simpl; auto with *.
         ++ apply HE' in Hl0; trivial. destruct Hl0 as [Hl0|Hl0]; auto with *.
    * clear Hpr' Hpr'' Hdet' Hdet'' Hvi' Hvi''.
      intros l0 Hl0. apply desc_mod_locs in Hl0. destruct Hl0 as [Hl0|Hl0].
      -- destruct (Hd'' l0) as [Hl0''|Hl0'']; auto with *.
      -- destruct (Hd' l0) as [Hl0''|Hl0'']; auto with *.
    * simpl. intros y ly Hy; econstructor.
      -- apply Hw''. eapply not_sem_free_incl; eauto.
      -- apply Hw'. eauto.
    * intros y ly Hy. simpl. apply bound_app; auto.
      -- eapply bound_eq with (Σ2 := Σ''); eauto; simpl.
         ++ intros la Ha. apply ref_locs_m in Ha.
         rewrite sr_sem_join, Hlocs'; [|intro HF; apply Hwf in HF; auto with *]; trivial.
         ++ intros la Ha. apply ref_locs_m in Ha.
           rewrite br_sem_join, Hbr'; [|intro HF; apply Hwf in HF; auto with *]. trivial.
      --  eapply bound_eq with (Σ2 := Σ'0); eauto.
  + split; intros l0 Hl0; rewrite Hlocs'' || rewrite Hbr'';
    try (apply Hlocs2; auto with *);
    intro HF; apply Hwf in Hl0; auto with *.
- intros ls Hind Σ Γ Δ' Σ' H Henv Hwf Hlocs1 Hlocs2. inversion H; subst.
  apply Hind in X; auto with *. clear Hind.
  destruct X as (Hdet''&Henv''&Hwb''&Hlocs''&Hbr''&Hcod''&Hincl''&Hvi''&Hpr''&HE''&Hd''&Hw''&Hmon'').
  nsplit 12; auto with *.
  intros Δ'' Σ'' H''. inversion H''; subst. auto.
- intros. simpl in *; inversion X; subst;
  intuition; auto with *; inversion X; subst; trivial.
  + inversion X0; subst; trivial.
  + inversion X0; subst; trivial.
  + vidtac.
- intros ls p Hind Σ Γ Δ' Σ' H1 Henv Hwf Hlocs1 Hlocs2.
  inversion H1; subst. simpl in Hwf, Hlocs1, Hlocs2.
  apply deterministic_semantics_m_path in X; trivial.
  destruct X as (Hdet'&_&Hwb'&Hw'&Hd'). assert(Hj2 := X0).
  apply Hind in X0; try tauto; auto with *; clear Hind.
  + destruct X0 as (Hdet''&Henv''&Hwb''&Hlocs''&Hbr''&Hcod''&Hincl''&Hvi''&Hpr''&HE''&Hd''&Hw''&Hmon'').
     nsplit 12; trivial. 1-5: simpl.
    * intros Δ'' Σ''0 H''.
      inversion H''; subst.
      apply Hdet' in X. destruct X as [Heq HS]; subst.
      inversion HS; subst; clear HS Heq.
      apply Hdet'' in X0. destruct X0 as  [Heq HS]; subst.
      inversion HS; clear HS. subst d' d Γ' DUd' lv' ls'. tauto.
    * eauto.
    * eapply wb_desc_eq; simpl; trivial.
      eapply wb_desc_sup; auto with *. eapply wb_desc_incl; eauto.
    * eauto.
    * intro.  eapply proper_sem_join; eauto; now apply Hpr'',Hpr'.
    * clear Hpr'' Hdet' Hdet'' Hvi''.
      intros l0 Hl0 Hl0'. apply dom_rel_sem_join in Hl0.
      destruct Hl0 as [Hl0|[Hl0|Hl0] ]; auto with *.
      -- simpl. eapply locs_desc_lv, Hd' in Hl0; tauto.
      -- simpl. eapply locs_desc_lv, Hd'' in Hl0. destruct Hl0 as [Hl0|Hl0]; auto with *.
         apply env_locs_p in Hl0. destruct Hl0 as [Hl0|Hl0]; auto with *.
      -- apply HE'' in Hl0; trivial. simpl.
         destruct Hl0 as [Hl0|Hl0]; auto with *.
         apply env_locs_p in Hl0. destruct Hl0 as [Hl0|Hl0]; auto with *.
    * clear Hpr'' Hdet' Hdet'' Hvi''. subst DUd'.
      intros l0 Hl0. apply desc_sup_locs in Hl0. simpl.
      destruct Hl0 as [Hl0|Hl0].
      -- apply Hd' in Hl0. tauto.
      -- apply Hd'' in Hl0. destruct Hl0 as [Hl0|Hl0]; simpl; auto with *.
         apply env_locs_p in Hl0. destruct Hl0 as [Hl0|Hl0]; auto with *.
    * intros v Hv l. constructor; auto.
      case(List.In_dec (option_eq_dec var_eq_dec) (Some (IV v)) (map (ρ Σ) lv)); intro Hdec.
      -- rewrite p_sem_env_up_v_in; auto.
      -- rewrite p_sem_env_up_v_out; trivial. apply Hw''.
         apply not_sem_free_p_out; auto.
    * intros y ly Hy. eapply p_sem_up_mon in Hy. destruct Hy. eapply Hmon''; eassumption.
  + apply wb_p_sem_env; auto.
  + apply p_sem_env_disj; auto with *.
Defined.

Lemma prog_ind2 (P: sem -> sem -> env -> prog -> Type)
    (f : forall Σ Σ' Γ (v: v_exp), Σ / Γ ⊢v v ⇝ Σ' -> wb_env Γ Σ -> wf_v v -> (Γ ⊓ locs_v v) ->
  (Σ ⊔ locs_v v) -> P Σ Σ' Γ v)
    (f0 : forall Σ Σ' Σ'' Γ x l (m : m_exp) (p : prog) Δ,
      wb_env Γ Σ ->
      wf (module (x, l) = m;; p) -> Γ ⊓ locs (module (x, l) = m;; p) -> Σ ⊔ locs (module (x, l) = m;; p) ->
      (Γ [[x ↦m Δ]]) ⊓ locs p -> (Σ'' [[l ↦ IM x]]) ⊔ locs p ->
      wb_env (Γ [[x ↦m Δ]]) (Σ'' [[l ↦ IM x]]) ->
     Σ / Γ ⊢ m ⋮ (Δ, Σ'') -> (Σ'' [[l ↦ IM x]]) / Γ [[x ↦m Δ]] ⊢ p ⇝ Σ' ->
     P (Σ'' [[l ↦ IM x]]) Σ' (Γ [[x ↦m Δ]]) p -> P Σ Σ' Γ (module (x, l) = m;; p))
: forall p Σ Σ' Γ, wb_env Γ Σ -> wf p -> (Γ ⊓ locs p) -> (Σ ⊔ locs p) ->
    Σ / Γ ⊢ p ⇝ Σ' -> P Σ Σ' Γ p.
Proof.
fix prog_ind2 1. intros p Σ Σ' Γ Henv Hwf Hlocs1 Hlocs2 HP.
destruct p as [v | m p]; inversion HP; subst.
- clear prog_ind2. auto.
- simpl in *. destruct ((fst (fst (fst deterministic_semantics_m))) _ _ _ _ _ X) as
      (_&Hwb'&HΔ''&Hlocs'&Hbr'&Hcod'&Hincl'&Hpr').
  1-4: clear prog_ind2; simpl in *; try tauto; auto with *; disjtac.
  assert(Henv': wb_env (Γ [[x ↦m Δ]]) (Σ'' [[l ↦ IM x]])) by
    (eapply wb_env_incl; [| apply sem_incl_up_ρ]; [apply wb_env_up_m; auto|];
    rewrite Hlocs';[|tauto]; apply Hlocs2; auto with *).
  eapply f0; try apply H6. 10: apply prog_ind2.
  all: clear prog_ind2; simpl in *; try tauto; eauto.
  + destruct Γ as [ [ Γv Γm] Γt ]; eauto with *.
  + split; intros l0 Hl0; simpl; rewrite sr_up, Hlocs' || rewrite br_up, Hbr'.
    * case_loc_eq_dec; intro; subst;[tauto|]. apply Hlocs2; auto with *.
    * intro HF; apply Hwf in HF. tauto.
    * intuition; eauto.
    * intuition; eauto.
  + destruct Γ as [ [ Γv Γm] Γt ]; eauto with *.
  + split; intros l0 Hl0. simpl; rewrite sr_up, Hlocs' || rewrite br_up, Hbr'.
    * case_loc_eq_dec; intro; subst;[tauto|]. apply Hlocs2; auto with *.
    * intro HF; apply Hwf in HF. tauto.
    * simpl. rewrite br_up, Hbr'.
      -- apply Hlocs2; auto with *.
      -- intro HF; apply Hwf in HF; tauto.
Defined.

(* end hide *)

(** * Semantic properties *)
(** For each type of expressions, we prove that:
  - Judgments preserve well-behavedness
  - The semantics is deterministic on well-behaved inputs (_Lemma 5_)
  - The declarations of the program = the codomain of the semantics (_Proposition 2_)
  - Judgments are compatible with semantic inclusion
  - Judgments preserve properness (_Lemma 6_) *)

Proposition deterministic_semantics:
  forall P Σ Σ' Γ,
    wb_env Γ Σ ->
    wf P ->
    (Γ ⊓ locs P) ->
    (Σ ⊔ locs P) ->
    Σ / Γ ⊢ P ⇝ Σ' ->
    (forall Σ'', Σ / Γ ⊢ P ⇝ Σ'' -> Σ' = Σ'') /\
    wb_env Γ Σ' /\
    (forall l, ~ In l (locs P) -> ρ Σ' l = ρ Σ l) /\
    (forall l, ~ In l (locs P) -> ↣ Σ' l = ↣ Σ l) /\
    (forall l, (In l (decl P) \/ cod Σ l) <-> cod Σ' l) /\
    (Σ ⊆ Σ') /\
    (forall l v', ρ Σ l = None -> ρ Σ' l = Some (IV v') <-> value_identifier (v', l) P) /\
    (proper Σ -> proper Σ') /\
    (forall l, dom_rel (E Σ') l -> ~ dom_rel (E Σ) l -> In l (val_locs P) \/ env_locs Γ l).
Proof.
apply prog_ind2.
- intros Σ Σ' Γ v Hv Hwb Hwf HΓlocs HΣlocs; simpl in *.
  destruct (deterministic_semantics_v Hwf Hwb HΓlocs HΣlocs Hv) as
   (Hdet'&Hwb'&Hlocs'&Hbr'&Hcod'&Hincl'&Hvi'&Hpr'&HE'&_).
  repeat (split; try tauto).
  + intros Σ'' H''. inversion H''; subst. apply Hdet' in X; now subst.
  + intros l0 Hl0 Hl0'. rewrite HE' in Hl0. tauto.
- intros Σ Σ' Σ'' Γ x l m p Δ Hwb Hwf HΓlocs HΣlocs Hlocs1' Hlocs2' Henv' Hm Hp; simpl in *.
  intros (Hdet''&Hwb''''&Hlocs''&Hbr''&Hcod''&Hincl''&Hvi''&Hpr''&HE'').
  edestruct (fst (fst (fst(deterministic_semantics_m))) m)
    as (Hdet'&Hwb'&HΔ''&Hlocs'&Hbr'&Hcod'&Hincl'&Hvi'&Hpr'&HE'&Hd'); eauto;
  [tauto | eauto with *| intuition|].
  assert(Hincl''': Σ'' ⊆ Σ') by
    (etransitivity; try apply Hincl'';
     apply sem_incl_up_ρ; rewrite Hlocs';[apply HΣlocs|]; tauto).
  assert(Hincl: Σ ⊆ Σ') by eauto.
  nsplit 8; trivial.
  + intros Σ'0 H''; inversion H''; subst; trivial.
    apply Hdet' in X; destruct X; subst. now apply Hdet''.
  + eauto.
  + intros y Hy. rewrite Hlocs''.
    * simpl. rewrite sr_up; case loc_eq_dec; intro.
      -- contradict Hy. subst; auto with *.
      -- rewrite Hlocs'; trivial. intro HF. auto with *.
    * intro HF; apply Hy. auto with *.
  + intros y Hy. rewrite Hbr''.
    * simpl. rewrite br_up, Hbr'; trivial. intro HF. auto with *.
    * intro HF; apply Hy. auto with *.
  + intro l0. rewrite in_app_iff, <- Hcod'', cod_sem_up_sr_m, <- Hcod'.
    split; [intros [ [HF|HF]|HF]|]; intuition.
    * right. intuition. subst. apply decl_locs_m in HF; auto.
    * right. split; intuition. subst. eapply cod_not_None; eauto.
   + intros l' v' H'. assert(Hl':= Hvi'' l' v').
      rewrite sr_up in Hl'. case loc_eq_dec in Hl'.
      ++ subst. rewrite Hlocs''; [|intro HF; apply Hwf in HF; auto with *; tauto].
         rewrite sr_up; case loc_eq_dec; try tauto.
         intros _; split; [intro H''; inversion H''; subst; tauto|].
         intros [H''|H''].
        ** apply value_identifier_locs_m in H''. tauto.
        ** apply value_identifier_locs in H''. apply Hwf in H''; auto with *; tauto.
      ++ assert(Hl'':= Hvi' l' v' H').
         case_eq (ρ Σ'' l'); [intros j Hj; split|split; [tauto|] ].
        ** intro H''. left; eapply Hvi'; eauto. rewrite Hj.
           apply Hincl''' in Hj. destruct Hj as (Hj&_).
           rewrite Hj in H''; inversion H''; now subst.
        ** intros [Heq|Heq].
          --- now apply Hincl''', Hl''.
          --- apply value_identifier_locs in Heq.
              rewrite Hlocs' in Hj; [rewrite H' in Hj; discriminate|].
              intro HF; apply Hwf in Heq; auto with *; tauto.
        ** intros [Heq|Heq].
          --- now apply Hincl''', Hl''.
          --- intuition.
  + intro. apply Hpr'', proper_up; eauto;
      try rewrite Hlocs'; try rewrite Hbr'; try (apply HΣlocs); simpl; tauto.
  + clear Hdet' Hdet'' Hvi' Hvi''.
    intros l0 Hl0 Hl0'.
    destruct (dom_rel_dec (E Σ'') l0) as [HE'''|HnE'''].
    * apply HE' in HE'''; trivial. destruct HE'''; simpl; auto with *.
    * apply HE'' in Hl0; trivial. destruct Hl0 as [Hl0|Hl0]; auto with *.
      apply env_locs_up_m in Hl0. destruct Hl0 as [Hl0|Hl0]; auto with *.
      apply Hd' in Hl0. destruct Hl0; auto with *.
Defined.

(** * Deriving Semantics of Programs *)

(* begin hide *)

(** TODO: move, for debugging purposes *)
Definition pp_of_loc Σ l : string :=
match ρ Σ l with
| Some (IM m) => "(module " ++ m ++ " at location " ++ print_loc l ++ ")"
| Some (IT t) => "(module type " ++ t ++ " at location " ++ print_loc l ++ ")"
| Some (IV v) => "(identifier " ++ v ++ " at location " ++ print_loc l ++ ")"
| None => "unexpected"
end.

Definition pp_of_module_loc Σ l : string :=
match ρ Σ l with
| Some (IM m) => "(module " ++ m ++ " at location " ++ print_loc l ++ ")"
| _ => "unexpected"
end.

Definition print_D (Σ : sem) (d : desc) : string.
Proof.
assert(f : nat -> string);[|exact (f 0)].
induction d using desc_ind2; intro n.
- exact (("Values:" ++ LF ++ indent n) ++
          concat (LF ++ indent n) (map (pp_of_loc Σ) lv) ++
          "Modules:" ++ LF)%string.
- exact (IHd0 n ++ LF ++
         indent n ++ (pp_of_module_loc Σ l) ++ ":" ++ LF ++
         IHd (S n))%string.
- exact (indent n ++ "Functor " ++ (pp_of_loc Σ l))%string.
Defined.

Definition derive_m_path (Σ: sem) (Γ: env) (p : m_path):
  {d: desc & judgment_m_path Σ Γ p d Σ} +
  ({err : string | forall d Σ', wb_env Γ Σ -> judgment_m_path Σ Γ p d Σ' -> False}).
Proof.
induction p; destruct m as (m&l).
- left; exists (envM Γ m);  constructor.
- destruct IHp as [ [ d H'] | [err Hr] ].
  + destruct d as [lv ls | l' D1 D2].
    * case_eq (find ((var_eq_bool (Some (IM m))) ∘ ρ Σ ∘ fst) ls).
      -- intros [l'' Δ] Hsome. left.
         destruct (find_some _ _ Hsome) as (Hin&Heq).
         apply var_eq_bool_true in Heq.
         simpl in H'.
         eexists _; econstructor; eauto with *.
      -- right. exists ("Module " ++ m ++ " not found in " ++ pp_m_path p ++ ", " ++ l ++
           ". Parent path contains " ++ LF ++  print_D  Σ (Structural lv ls))%string.
         intros d Σ'' Henv HF.
         inversion HF; subst.
         assert (Heq : Structural lv ls = Structural lv0 ls0)
           by (eapply deterministic_semantics_m_path; eauto).
         inversion Heq; subst lv0 ls0.
         apply find_none with (x := (l0, d)) in H; [ | trivial].
         unfold compose in H; simpl in *.
         symmetry in H7; rewrite <- var_eq_bool_true in H7.
         rewrite H7 in H.
         auto with *.
    * simpl in H'. right.
      exists ("Bad functorial module path for " ++ pp_m_path p ++ " in " ++ l')%string.
      intros d0 Σ' Henv HF; inversion HF; subst.
      destruct (deterministic_semantics_m_path Henv H') as (Hdet&_ ).
      destruct (Hdet _ _ X) as (_&Hfalse).
      inversion Hfalse.
  + right. exists err.
    intros d Σ' He HF; inversion HF; subst. eapply Hr; eauto.
Defined.

Definition derive_em_path (Σ: sem) (Γ: env) (p : em_path):
  {d & {Σ' & Σ / Γ ⊢emp p ⋮ (d, Σ')}} +
  {err : string | forall d Σ', wb_env Γ Σ -> Σ / Γ ⊢emp p ⋮ (d, Σ') -> False }.
Proof.
revert Σ.
induction p; intros Σ.
- destruct m as (m&l);
  left; exists (envM Γ m); exists Σ; constructor.
- destruct m as (m&l). simpl in *.
  destruct (IHp Σ) as [ (d&Σ'&H') | [err Hr] ]; auto;
  [|right; exists err; intros d Σ'' Henv HF; inversion HF; subst;
    eapply Hr; eauto; try tauto; disjtac].
  destruct d as [lv ls | l' D1varD2].
  + case_eq (find ((var_eq_bool (Some (IM m))) ∘ ρ Σ' ∘ fst) ls).
    * intros [l'' Δ] Hsome. left. destruct (find_some _ _ Hsome) as (Hin&Heq).
       apply var_eq_bool_true in Heq. simpl in *.
       exists Δ; exists Σ'; simpl. econstructor; eauto.
    * right; simpl in H'. exists ("Module " ++ m ++ " not found in " ++ l
        ++ "Parent path contains " ++ LF ++  print_D  Σ' (Structural lv ls))%string.
      intros d Σ'' Henv HF.
      inversion HF; subst.
       assert (Heq : Structural lv ls = Structural lv0 ls0)
         by (eapply deterministic_semantics_em_path; eauto; try tauto;
             split; intros; apply HΣlocs; tauto).
       inversion Heq; subst lv0 ls0; clear Heq.
       replace Σ' with Σ'' in * by (eapply deterministic_semantics_em_path; eauto; try tauto;
             split; intros; apply HΣlocs; tauto).
       apply find_none with (x := (l0, d)) in H; [ | trivial].
       unfold compose in H; simpl in *.
       symmetry in H7; rewrite <- var_eq_bool_true in H7.
       rewrite H7 in H. auto with *.
  + simpl in H'. right.
    exists ("Bad functorial module path for " ++ pp_em_path p ++ " in " ++ l)%string.
    intros d Σ'' Henv HF. inversion HF; subst.
    apply deterministic_semantics_em_path in H'; auto; try tauto.
    destruct H' as (Hdet&_).
      destruct (Hdet _ _ X) as (_&Hfalse). inversion Hfalse.
- simpl. destruct (IHp1 Σ) as [ (D1&Σ1'&H1') | [err Hr] ]; auto with *;
  [|right; exists err; intros d Σ'' Henv HF; inversion HF; subst;
    eapply Hr; eauto; try tauto; auto with *; disjtac].
  simpl in *. assert(H1 := H1').
  destruct (IHp2 Σ1') as [ (D2&Σ2'&H2') | [err Hr] ]; simpl in *;
  auto with *; try tauto.
  + destruct D1 as [lv ls | l Δ1 Δ2].
    * right.
      exists ("Bad structural module path for " ++ pp_em_path p1 ++ " applied to " ++ pp_em_path p2)%string.
      intros d Σ' He HF; inversion HF; subst.
      apply deterministic_semantics_em_path in H1; auto.
      destruct H1 as (Hdet&_).
      destruct (Hdet _ _ X) as (_&Hfalse). inversion Hfalse.
    * left; eexists; eexists; econstructor; eauto.
  + right; exists err; intros d Σ'' Henv HF. inversion HF; subst.
    simpl in H1'.
    apply deterministic_semantics_em_path in H1; auto with *.
    assert (HΓ' : wb_env Γ Σ1') by tauto.
    destruct H1 as (Hdet&_&_&Hlocs'&Hbr'&Hpr').
    destruct (Hdet _ _ X) as (Hfalse&_).
    subst; eapply Hr; eauto; try tauto;auto with *.
Defined.

Definition derive_v (Σ: sem) (Γ: env) v:
  {Σ': sem & judgment_v Σ Γ v Σ'} +
  {err : string | forall Σ', wb_env Γ Σ -> wf_v v -> (Γ ⊓ locs_v v) -> (Σ ⊔ locs_v v) ->
    judgment_v Σ Γ v Σ' -> False}.
Proof.
revert Σ Γ.
induction v; try (destruct v as (v&l)); intros Σ Γ;
simpl in *;
try (solve[left; eexists; econstructor]).
- destruct (derive_m_path Σ Γ m) as [ [d Hd] | [err Hf] ].
  + destruct d as [lv ls | l' D1 D2].
    * simpl in *.
      case_eq (find ((var_eq_bool (Some (IV v))) ∘ ρ Σ) lv).
      -- intros l' Hl'.
         apply find_some in Hl'; destruct Hl' as (Hin&Heq).
         apply var_eq_bool_true in Heq.
         left. eexists; econstructor; eauto.
      -- intro Hnone; left. eexists; eapply J_V_field_bot; eauto.
         intros l' H' Hin.
         apply find_none with (x := l') in Hnone; trivial.
         apply var_eq_bool_false in Hnone; auto.
    * right. exists ("Bad functorial module path for " ++ pp_m_path m ++ " in " ++ l')%string.
      intros Σ' Henv Hwf Hlocs1 Hlocs2 H'; inversion H'; subst;
      destruct (deterministic_semantics_m_path Henv Hd) as (Hdet&_).
      -- destruct (Hdet _ _ X) as (_&Heq2); inversion Heq2.
      -- destruct (Hdet _ _ X) as (_&Heq2); inversion Heq2.
  + right. exists (err ++ " in module path " ++ pp_m_path m  ++ " at " ++ l)%string.
    intros Σ' Henv Hwf Hlocs1 Hlocs2 H'; inversion H'; subst; eapply Hf; eauto.
- destruct v1 as (v1&l1).
  destruct (IHv1 Σ Γ) as [ [Σ1 H1] | [err H1] ]; auto with *;
  [|right; exists ("In " ++ l1 ++ "\n" ++ err)%string;
    intros d He Hc Hl1 Hl2 HF; inversion HF; subst; eapply H1; eauto; try tauto; auto with *; disjtac].
  destruct (IHv2 (Σ1[[l1 ↦ IV v1]]) (Γ [[v1 ↦v Some l1]])) as [ [Σ2 H2] | [err H2] ]; try tauto.
  + left; eexists; econstructor; eauto.
  + right; exists ("In " ++ l1 ++ "\n" ++ err)%string; intros Σ' Henv Hwf Hlocs1 Hlocs2 HF; inversion HF; subst.
    assert(H1' := H1).
    apply deterministic_semantics_v in H1'; auto with *; [|tauto|disjtac].
    destruct H1' as (Hdet1&Henv1&Hr'&H1'&Hcod'&Hincl'&Hpr').
    assert (Hwb' : wb_env (Γ [[v1 ↦v Some l1]]) (Σ1[[l1 ↦ IV v1]]))
     by (apply wb_env_up_v; trivial;
         ((rewrite Hr'||rewrite H1');[apply Hlocs2;auto with *|tauto])).
    eapply H2; eauto; auto with *; try tauto.
    * intros l Hl v' Hv'. destruct Γ as [ [Γv Γm] Γt].
      simpl in Hv'; case val_eq_dec in Hv'.
     -- injection Hv'; intro; subst l1; tauto.
     -- apply Hlocs1 in Hv'; auto with *.
    * simpl. split;intros l Hl; rewrite br_up || rewrite sr_up; try case loc_eq_dec; intros; subst.
     -- tauto.
     -- rewrite Hr';[apply Hlocs2;auto with *|].
        intro H'; apply Hwf in H'. tauto.
     -- rewrite H1';[apply Hlocs2;auto with *|].
        intro H'; apply Hwf in H'. tauto.
    * erewrite Hdet1; eauto.
- destruct (IHv (Σ[[l ↦ IV v]]) (Γ [[v ↦v Some l]])) as [ [Σ' H'] | [err H'] ].
  + left; eexists; econstructor; eauto.
  + right; exists err; intros Σ' Henv Hwf HΓlocs HΣlocs HF; inversion HF; subst.
    assert (Hwb' : wb_env (Γ [[v ↦v Some l]]) (Σ[[l ↦ IV v]])) by
    (apply wb_env_up_v; auto; apply HΣlocs; tauto).
    eapply H'; eauto; try tauto.
    * intros l0 Hl0 v' Hv'.
      destruct Γ as [ [Γv Γm] Γt].
      simpl in Hv'; case val_eq_dec in Hv'.
      -- injection Hv'; intro; subst l0; tauto.
      -- apply HΓlocs in Hv'; auto with *.
    * simpl. split; intros l0 Hl0; try case_loc_eq_dec; intros; subst.
      -- tauto.
      -- apply HΣlocs; auto with *.
      -- apply HΣlocs; auto with *.
- destruct (IHv1 Σ Γ) as [ [Σ1 H1] | [err H1] ]; auto with *;
    [|right; exists err; intros Σ' Henv Hwf Hlocs1 Hlocs2 HF; inversion HF; subst; eapply H1;
      eauto; auto with *; try tauto; disjtac].
  destruct (IHv2 Σ1 Γ) as [ [Σ2 H2] | [err H2] ]; auto with *.
  + left; eexists; econstructor; eauto.
  + right; exists err; intros Σ' Henv Hwf Hlocs1 HΣlocs HF.
    apply deterministic_semantics_v in H1; auto with *; [|tauto|disjtac].
    destruct H1 as (Hdet1&Hwb'&Hlocs'&H1'&Hcod'&Hincl'&Hpr').
    inversion HF; subst; eapply H2; try tauto; auto with *.
    * disjtac; rewrite Hlocs' || rewrite H1'; try apply HΣlocs; auto with *;
      intro H'; apply Hwf in H'; tauto.
    * erewrite Hdet1; eauto.
- destruct (derive_m_path Σ Γ m) as [ [d H1] | [err H1] ]; auto with *;
    [|right; exists (err ++ " while opening module path "++ pp_m_path m)%string;
      intros Σ' Henv Hwf Hlocs1 Hlocs2 HF; inversion HF; subst; eapply H1;
      eauto; auto with *; try tauto; disjtac].
  destruct d as [lv ls|];
  [|right; exists ("Bad functorial module path at " ++ l)%string;
    intros Σ' Henv Hwf Hlocs1 Hlocs2 HF; inversion HF; subst; subst d;
    apply deterministic_semantics_m_path in H1; trivial; apply H1 in X;
    destruct X as [_ HF']; inversion HF'].
  destruct (IHv Σ (p_sem_env (ρ Σ) (Structural lv ls) Γ)) as [ [Σ2 H2] | [err H2] ]; auto with *.
  + left; eexists; econstructor; eauto.
  + right; exists ("In 'value open' statement, " ++ err)%string; intros Σ' Henv Hwf Hlocs1 HΣlocs HF.
    assert (Hj1 := H1).
    apply deterministic_semantics_m_path in H1; trivial.
    destruct H1 as (Hdet1&Hwb'&Hlocs'&H1'&_).
    inversion HF; subst; eapply H2; try tauto; auto with *.
    * now apply p_sem_env_disj.
    * apply deterministic_semantics_m_path in Hj1; trivial. apply Hj1 in X.
      destruct X as [_ Heq]; subst d. inversion Heq; subst. eassumption.
Defined.

Definition derive_m :
  (forall (m : m_exp) (Σ: sem) (Γ: env),
  {d & {Σ' & Σ / Γ ⊢ m ⋮ (d, Σ')}} +
  {err : string | forall d Σ', wb_env Γ Σ -> wf_m m -> (Γ ⊓ locs_m m) ->
     (Σ ⊔ locs_m m) -> Σ / Γ ⊢ m ⋮ (d, Σ') -> False}) *
  (forall s (Σ: sem) (Γ: env),
  {d & {Σ' & Σ / Γ ⊢s s ⋮ (d, Σ')}} +
  {err : string | forall (d0 : desc) (Σ' : sem),   wb_env Γ Σ -> wf_list_s s ->
    (Γ ⊓ flat_map locs_s s) -> (Σ ⊔ flat_map locs_s s) ->
      Σ / Γ ⊢s s ⋮ (d0,  Σ') -> False}) *
  (forall (t : m_type) (Σ: sem) (Γ: env),
  {d & {Σ' & Σ / Γ ⊢t t ⋮ (d, Σ')}} +
  {err : string | forall d Σ', wb_env Γ Σ -> wf_t t -> (Γ ⊓ locs_t t) -> (Σ ⊔ locs_t t) ->
     Σ / Γ ⊢t t ⋮ (d, Σ') -> False}) *
  (forall S (Σ: sem) (Γ: env),
  {d & {Σ' & Σ / Γ ⊢S S ⋮ (d, Σ')}} +
  {err : string | (forall d0 Σ', wb_env Γ Σ -> wf_list_S S -> (Γ ⊓ flat_map locs_S S) -> (Σ ⊔ flat_map locs_S S) ->
     Σ / Γ ⊢S S ⋮ (d0,  Σ') -> False)}).
Proof.
apply module_ind.
- intros [t l] Σ Γ. left; eexists. econstructor. eauto.
- intros p [t l] Σ Γ.
  destruct (derive_em_path Σ Γ p) as [ [ [lv ls | l' D1 D2] [Σ' H''] ] | [err HF] ].
  + case_eq (find ((var_eq_bool (Some (IT t))) ∘ ρ Σ' ∘ fst) ls).
    * intros [o D] Ho.
      apply find_some in Ho; destruct Ho as [Hin Ho].
      apply var_eq_bool_true in Ho.
      subst. left; eexists; econstructor; eauto.
    * intros Hnone. right. exists ("Module " ++ t ++ " not found at " ++ l)%string.
      intros Δ''' Σ''' Henv Hwf Hlocs1 Hlocs2 H'''.
      inversion H'''; subst.
      apply deterministic_semantics_em_path in H''; trivial.
      apply H'' in X. destruct X as (Heq&HF); inversion HF; subst.
      eapply find_none, var_eq_bool_false in Hnone; eauto; auto.
  + right. exists ("Bad functorial module path at " ++ l')%string.
    intros Δ''' Σ''' Henv Hwf Hlocs1 Hlocs2 H'''.
    inversion H'''; subst; simpl in *.
    apply deterministic_semantics_em_path in X; trivial.
    destruct X as (Hdet&_).
    apply Hdet in H''. destruct H'' as (_&HF).
    inversion HF.
  + right; exists (err ++ " in extended module path "++ pp_em_path p ++ " at " ++ l)%string;
    intros d e He Hwf Hl1 Hl2 HF'. inversion HF'. subst. eapply HF; eauto.
- intros [x l] t1 Hind1 t2 Hind2 Σ Γ. simpl in *.
  destruct (Hind1 Σ Γ) as [ (Δ''&Σ''&H'') | [err HF] ];
  [|right; exists err; intros Δ'' Σ''' Henv Hwf Hlocs1 Hlocs2 H''; inversion H'';
    subst; eapply HF; eauto; auto with *; try tauto; disjtac].
  destruct (Hind2 (Σ'' [[l ↦ IM x]]) (Γ [[x ↦m Δ'']])) as [ (Δ'&Σ'&H') | [err HF] ]; auto with *.
  + left; eexists; eexists; econstructor; eauto.
  + right; exists err; intros Δ' Σ' Henv Hwf Hlocs1 Hlocs2 H'; inversion H'; subst.
    assert(Hdet := H'').
    apply deterministic_semantics_m in Hdet; auto with *; [|tauto|disjtac].
    destruct Hdet as (Hdet1&Henv1&Hwb11&Hlocs'&H1'&Hincl'&Hpr').
    eapply HF; auto.
    * eapply wb_env_incl; [| apply sem_incl_up_ρ]; [apply wb_env_up_m; auto|];
      rewrite Hlocs';[apply Hlocs2;auto|tauto].
    * tauto.
    * apply env_up_m_disj. auto with *.
    * apply sem_up_disj; [tauto|].
      simpl in *. split; intros l0 Hl0;
      rewrite Hlocs'|| rewrite H1'; try(apply Hlocs2;auto with * );
      intro HF'; apply Hwf in HF'; tauto.
    * assert(Hdet := H'').
      apply deterministic_semantics_m in H''; simpl; auto with *; [|tauto|disjtac].
      apply H'' in X. destruct X; subst. eauto.
- intros t Hind [m l] q Σ Γ. simpl in *.
  destruct (Hind Σ Γ) as [ (Δ''&Σ''&H'') | [err HF] ];
  [|right; exists err; intros Δ'' Σ''' Henv Hwf Hlocs1 Hlocs2 H''; inversion H'';
    subst; eapply HF; eauto; auto with *; try tauto; disjtac].
  edestruct (derive_em_path (Σ'' [[l ↦ IM m]]) Γ q) as [ (Δ'&Σ'&H') | [err HF] ]; auto with *.
  + simpl in *. left; eexists; eexists; econstructor; eauto.
  + right; exists (err ++ " in extended module path "++ pp_em_path q ++ " at " ++ l)%string;
    intros Δ' Σ' Henv Hwf Hlocs1 Hlocs2 H'; inversion H'; subst.
    assert(Hdet := H'').
    apply deterministic_semantics_m in Hdet; auto with *; [|tauto|disjtac].
    destruct Hdet as (Hdet1&Henv1&Hwb11&Hlocs'&H1'&Hincl'&Hpr').
   eapply HF; auto.
   * eapply wb_env_incl; [| apply sem_incl_up_ρ]; trivial;
     rewrite Hlocs' by tauto; apply Hlocs2; tauto.
   * apply deterministic_semantics_m in H''; auto with *; [|tauto|disjtac].
     apply H'' in X. destruct X; subst. eauto.
- intros t Hind [m l] q Σ Γ. simpl in *.
  destruct (Hind Σ Γ)as [ (Δ''&Σ''&H'') | [err HF] ];
  [|right; exists err; intros Δ'' Σ''' Henv Hwf Hlocs1 Hlocs2 H''; inversion H'';
    subst; eapply HF; eauto; auto with *; try tauto; disjtac].
  destruct (derive_em_path (Σ'' [[l ↦ IM m]]) Γ q) as [ (Δ'&Σ'&H') | [err HF] ]; auto with *.
  + left; eexists; eexists; econstructor; eauto.
  + right; exists (err ++ " in extended module path "++ pp_em_path q  ++ " at " ++ l)%string;
    intros Δ' Σ' Henv Hwf Hlocs1' Hlocs2 H'; inversion H'; subst.
    assert(Hdet := H'').
    apply deterministic_semantics_m in H''; auto with *; [|tauto|disjtac].
    destruct H'' as (Hdet1&Henv1&Hwb11&Hlocs'&H1'&Hincl'&Hpr').
    apply Hdet1 in X. destruct X; subst. eapply HF; auto.
    * eapply wb_env_incl; [| apply sem_incl_up_ρ]; trivial.
      rewrite Hlocs' by tauto; apply Hlocs2; tauto.
    * eassumption.
- intros ls Hind Σ Γ.
  destruct (Hind Σ Γ)as [ (Δ''&Σ''&H'') | [err HF] ];
  [|right; exists err; intros Δ'' Σ''' Henv Hwf Hlocs1 Hlocs2 H''; inversion H''; subst; eapply HF; eauto].
  left. eexists; eexists. constructor; eassumption.
- intros m Hind Σ Γ.
  destruct (Hind Σ Γ)as [ (Δ''&Σ''&H'') | [err HF] ];
  [|right; exists err; intros Δ'' Σ''' Henv Hwf Hlocs1 Hlocs2 H''; inversion H''; subst; eapply HF; eauto].
  left. eexists; eexists. constructor; eassumption.
- intros lS [v l] Hind Σ Γ. simpl in *.
  destruct (Hind (Σ [[l ↦ IV v]]) (Γ [[v ↦v Some l]])) as [ [ [lv ls|l' D1 D2] [Σ'' H''] ] | [err HF] ]; simpl in *; auto with *.
  + left; eexists; eexists; simpl. econstructor; eauto.
  + right. exists ("Bad functorial module type at " ++ l')%string.
    intros Δ''' Σ''' Henv Hwf Hlocs1 Hlocs2 H''';  inversion H'''; subst; simpl in *.
    apply deterministic_semantics_m in X; trivial; try tauto.
    * eapply X in H''. destruct H'' as (_&H''). inversion H''.
    * eapply wb_env_up_v; simpl; try apply Hlocs2;auto.
    * apply env_up_v_disj; auto with *.
      unfold disj in *; simpl in *; intuition; eauto.
    * apply sem_up_disj; auto with *; [|disjtac].
      unfold disj in *; simpl in *; intuition; eauto.
  + right; exists err; intros Δ'' Σ''' Henv Hwf Hlocs1 Hlocs2 H''; inversion H''; subst.
    eapply HF; auto.
    * eapply wb_env_up_v; simpl; try apply Hlocs2;auto.
    * tauto.
    * apply env_up_v_disj; auto with *.
      unfold disj in *; simpl in *; intuition; eauto.
    * apply sem_up_disj; auto with *; [|disjtac].
      unfold disj in *; simpl in *; intuition; eauto.
    * eassumption.
- intros ls [x l] t Hind1 Hind2 Σ Γ. simpl.
  destruct (Hind2 Σ Γ) as [ (Δ''&Σ''&H'') | [err HF] ];
  [|right; exists err; intros Δ'' Σ''' Henv Hwf Hlocs1 Hlocs2 H''; inversion H'';
    subst; eapply HF; eauto; auto with *; try tauto; disjtac].
  destruct (Hind1 (Σ'' [[l ↦ IM x]]) (Γ [[x ↦m Δ'']])) as [ [ [lv' ls' | l' D1 D2] [Σ' H'] ] | [err HF] ];clear Hind1.
  + left; eexists; eexists; simpl in *; econstructor; eauto.
  + right. exists ("Bad functorial module type at " ++ l')%string.
    intros Δ''' Σ''' Henv Hwf Hlocs1 Hlocs2 H''';  inversion H'''; subst; simpl in *.
    assert(Hdet := H'').
    apply deterministic_semantics_m in Hdet; auto with *; [|tauto|disjtac].
    destruct Hdet as (Hdet1&Henv1&Hwb11&Hlocs'&H1'&Hincl'&Hpr').
    apply deterministic_semantics_m in X; trivial; auto with *; try tauto;[|disjtac].
    eapply X in H''. destruct H''; subst.
    apply deterministic_semantics_m in X0; trivial; try tauto.
    * eapply X0 in H'. destruct H' as [Heq H']; inversion H'; subst.
    * eapply wb_env_incl; [| apply sem_incl_up_ρ]; [apply wb_env_up_m; auto|]; try tauto;
      rewrite Hlocs';[apply Hlocs2;auto|tauto].
    * apply env_up_m_disj; auto with *.
    * apply sem_up_disj; auto with *.
      -- intuition; eauto with *.
      -- split;intros l0 Hl0; rewrite Hlocs'|| rewrite H1'; try apply Hlocs2; auto with *;
         intro HF; apply Hwf in Hl0; auto with *.
  + right; exists err; intros Δ' Σ' Henv Hwf Hlocs1' Hlocs2 H'; inversion H'; subst.
    assert(Hdet := H'').
    apply deterministic_semantics_m in Hdet; auto with *; [|tauto|disjtac].
    destruct Hdet as (Hdet1&Henv1&Hwb11&Hlocs'&H1'&Hincl'&Hpr').
    apply deterministic_semantics_m in H''; auto with *; [|tauto|disjtac].
    eapply HF; auto.
    * eapply wb_env_incl; [| apply sem_incl_up_ρ]; [apply wb_env_up_m; auto|]; try tauto;
      rewrite Hlocs';[apply Hlocs2;auto|tauto].
    * tauto.
    * apply env_up_m_disj; auto with *.
    * apply sem_up_disj; auto with *.
      -- intuition; eauto with *.
      -- split;intros l0 Hl0; rewrite Hlocs'|| rewrite H1'; try apply Hlocs2; auto with *;
         intro HF'; apply Hwf in Hl0; auto with *.
    * apply H'' in X. destruct X; subst. eauto.
- intros lS [x l] p Hind Σ Γ.
  destruct (derive_m_path Σ Γ p) as [ [Δ'' H''] | [err HF] ];
  [|right; exists (err ++ " in module path " ++ pp_m_path p ++ " at " ++ l)%string; intros Δ'' Σ''' Henv Hwf Hlocs1 Hlocs2 H''; inversion H'';
    subst; eapply HF; eauto; auto with *; try tauto; disjtac].
  destruct (Hind (Σ [[l ↦ IM x]]) (Γ [[x ↦m Δ'']])) as [ [ [lv' ls' | l' D1 D2] [Σ' H'] ] | [err HF] ]; auto with *.
  + left; eexists; eexists; simpl in *; econstructor; eauto.
  + right. exists ("Bad functorial module type at " ++ l')%string.
    intros Δ''' Σ''' Henv Hwf Hlocs1 Hlocs2 H''';  inversion H'''; subst; simpl in *.
    assert(Hdet := H'').
    apply deterministic_semantics_m_path in Hdet; auto.
    destruct Hdet as (Hdet&_&Hwb&_).
    apply Hdet in X. destruct X; subst.
    apply deterministic_semantics_m in X0; trivial; auto with *; try tauto.
    * eapply X0 in H'. destruct H' as [Heq H']; inversion H'; subst.
    * eapply wb_env_incl; [| apply sem_incl_up_ρ]; [apply wb_env_up_m|]; simpl; try apply Hlocs2;auto.
    * apply env_up_m_disj; auto with *.
    * apply sem_up_disj; auto with *; intuition; eauto with *.
  + right; exists err; intros Δ' Σ' Henv Hwf Hlocs1' Hlocs2 H'; inversion H'; subst.
    assert(Hdet := H'').
    apply deterministic_semantics_m_path in Hdet; auto.
    destruct Hdet as (Hdet&_&Hwb&_).
    apply Hdet in X; destruct X; subst. simpl in *.
    apply HF in X0; trivial.
    * eapply wb_env_incl; [| apply sem_incl_up_ρ]; [apply wb_env_up_m|]; simpl; try apply Hlocs2;auto.
    * tauto.
    * apply env_up_m_disj. auto with *.
    * apply sem_up_disj; auto with *; intuition; eauto with *.
- intros ls [t l] Hind Σ Γ. simpl in *.
  destruct (Hind (Σ [[l ↦ IT t]]) (Γ [[t ↦t ∅]])) as [ [ [lv' ls' | l' D1 D2] [Σ' H'] ] | [err HF] ]; auto with *.
  + left; eexists; eexists; simpl in *; econstructor; eauto.
  + right. exists ("Bad functorial module type at " ++ l')%string.
    intros Δ''' Σ''' Henv Hwf Hlocs1 Hlocs2 H''';  inversion H'''; subst; simpl in *.
    apply deterministic_semantics_m in H'; trivial.
    * apply H' in X. destruct X as [Heq HFF]; inversion HFF; subst.
    * eapply wb_env_incl; [| apply sem_incl_up_ρ]; [apply wb_env_up_t|]; simpl;try apply Hlocs2;auto.
    * tauto.
    * apply env_up_t_disj; auto with *.
    * apply sem_up_disj; auto with *; intuition; eauto with *.
  + right; exists err; intros Δ' Σ' Henv Hwf Hlocs1' Hlocs2 H'; inversion H'; subst; eapply HF; auto.
    * eapply wb_env_incl; [| apply sem_incl_up_ρ]; [apply wb_env_up_t|]; simpl;try apply Hlocs2;auto.
    * tauto.
    * eauto.
    * apply sem_up_disj; auto with *; intuition; eauto with *.
    * eassumption.
- intros ls [t l] t0 Hind1 Hind2 Σ Γ. simpl in *.
  destruct (Hind2 Σ Γ) as [ (Δ''&Σ''&H'') | [err HF] ]; clear Hind2;
  [|right; exists err; intros Δ'' Σ''' Henv Hwf Hlocs1 Hlocs2 H''; inversion H'';
    subst; eapply HF; eauto; auto with *; try tauto; disjtac].
  destruct (Hind1 (Σ'' [[l ↦ IT t]]) (Γ [[t ↦t Δ'']])) as [ [ [lv' ls' | l' D1 D2] [Σ' H'] ] | [err HF] ]; auto with *; clear Hind1.
  + left; eexists; eexists; simpl in *; econstructor; eauto.
  + right; exists ("Bad functorial module type at " ++ l')%string.
    intros Δ''' Σ''' Henv Hwf Hlocs1 Hlocs2 H''';  inversion H'''; subst; simpl in *.
    assert(Hdet := H'').
    apply deterministic_semantics_m in Hdet; auto with *; [|tauto|disjtac].
    destruct Hdet as (Hdet1&Henv1&Hwb11&Hlocs'&H1'&Hincl'&Hpr').
    apply deterministic_semantics_m in X; trivial; auto with *; try tauto; [|disjtac].
    eapply X in H''. destruct H''; subst.
    apply deterministic_semantics_m in X0; trivial; try tauto.
    * eapply X0 in H'. destruct H' as [Heq H']; inversion H'; subst.
    * eapply wb_env_incl; [| apply sem_incl_up_ρ]; [apply wb_env_up_t; auto|]; try tauto;
  rewrite Hlocs';[try apply Hlocs2;auto|tauto].
    * apply env_up_t_disj; auto with *.
    * apply sem_up_disj; auto with *.
    -- intuition; eauto with *.
    -- split;intros l0 Hl0; rewrite Hlocs'|| rewrite H1'; try apply Hlocs2; auto with *;
       intro HF; apply Hwf in Hl0; auto with *.
  + right; exists err; intros Δ' Σ' Henv Hwf Hlocs1' Hlocs2 H'; inversion H'; subst.
    assert(Hdet := H'').
    apply deterministic_semantics_m in Hdet; auto with *; [|tauto|disjtac].
    destruct Hdet as (Hdet1&Henv1&Hwb11&Hlocs'&H1'&Hincl'&Hpr').
    apply deterministic_semantics_m in H''; auto with *; [|tauto|disjtac].
    eapply HF; auto.
    * eapply wb_env_incl; [| apply sem_incl_up_ρ]; [apply wb_env_up_t; auto|]; try tauto;
  rewrite Hlocs';[try apply Hlocs2;auto|tauto].
    * tauto.
    * apply env_up_t_disj; auto with *.
    * apply sem_up_disj; auto with *.
      -- intuition; eauto with *.
      -- split;intros l0 Hl0; rewrite Hlocs'|| rewrite H1'; try apply Hlocs2; auto with *;
         intro HF'; apply Hwf in Hl0; auto with *.
    * apply H'' in X. destruct X; subst. eauto.
- intros ls t Hind1 Hind2 Σ Γ. simpl in *.
  destruct (Hind2 Σ Γ) as [ (Δ''&Σ''&H'') | [err HF] ];
  [|right; exists ("In Sig_incl, " ++ err)%string ; intros Δ'' Σ''' Henv Hwf Hlocs1 Hlocs2 H''; inversion H'';
    subst; eapply HF; eauto; auto with *; try tauto; disjtac]; clear Hind2.
  destruct (Hind1 Σ'' (p_sem_env (ρ Σ'') Δ'' Γ)) as [ [ [lv' ls' | l' D1 D2] [Σ' H'] ] | [err HF] ]; auto with *; clear Hind1.
  + destruct Δ'' as [lv'' ls'' | l' D1 D2].
    * left; eexists; eexists; simpl in *; econstructor; eauto.
    * right. exists ("Bad functorial module type at " ++ l')%string.
      intros Δ''' Σ''' Henv Hwf Hlocs1 Hlocs2 H''';  inversion H'''; subst; simpl in *.
      apply deterministic_semantics_m in X; trivial; auto with *; try tauto; [|disjtac].
      apply X in H''. destruct H'' as [Heq H'']; inversion H''; subst.
  + right; exists ("Bad functorial module type at " ++ l')%string.
    intros Δ''' Σ''' Henv Hwf Hlocs1 Hlocs2 H''';  inversion H'''; subst; simpl in *.
    apply deterministic_semantics_m in X; trivial; auto with *; try tauto;[|disjtac].
    apply X in H''. destruct H'' as [Heq H'']; inversion H''; subst. clear H.
    destruct X as (Hdet1&Henv1&Hwb11&Hlocs'&H1'&Hincl'&Hpr').
    apply deterministic_semantics_m in X0; trivial; auto with *; try tauto.
    * apply X0 in H'. destruct H' as [Heq H']; inversion H'; subst.
    * apply wb_p_sem_env; tauto.
    * apply p_sem_env_disj; auto with *.
      split;intros l0 Hl0; rewrite Hlocs'|| rewrite H1'; try apply Hlocs2; auto with *;
      intro HF; apply Hwf in Hl0; auto with *.
    * split;intros l0 Hl0; rewrite Hlocs'|| rewrite H1'; try apply Hlocs2; auto with *;
      intro HF; apply Hwf in Hl0; auto with *.
  + right; exists err; intros Δ' Σ' Henv Hwf Hlocs1' Hlocs2 H'; inversion H'; subst.
    assert(Hdet := H'').
    apply deterministic_semantics_m in Hdet; auto with *; [|tauto|disjtac].
    destruct Hdet as (Hdet1&Henv1&Hwb11&Hlocs'&H1'&Hincl'&Hpr').
    apply Hdet1 in X; destruct X; simpl in *; subst. apply HF in X0; trivial.
    * apply wb_p_sem_env; tauto.
    * tauto.
    * apply p_sem_env_disj; auto with *.
      split;intros l0 Hl0; rewrite Hlocs'|| rewrite H1'; try apply Hlocs2; auto with *;
      intro; apply Hwf in Hl0; auto with *.
    * split;intros l0 Hl0; rewrite Hlocs'|| rewrite H1'; try apply Hlocs2; auto with *;
      intro; apply Hwf in Hl0; auto with *.
- intros. left; eexists; eexists; simpl in *; econstructor; eauto.
- intros ls p Hind Σ Γ. simpl in *.
  destruct (derive_m_path Σ Γ p) as [ [ Δ'' H''] | [err HF] ];
  [|right; exists (err ++ " while opening module path " ++ pp_m_path p)%string;
    intros Δ'' Σ''' Henv Hwf Hlocs1 Hlocs2 H''; inversion H'';
    subst; eapply HF; eauto; auto with *; try tauto; disjtac].
  destruct (Hind Σ (p_sem_env (ρ Σ) Δ'' Γ)) as [ [ [lv' ls' | l' D1 D2] [Σ' H'] ] | [err HF] ]; auto with *; clear Hind.
  + destruct Δ'' as [lv'' ls'' | l' D1 D2].
    * left; eexists; eexists; simpl in *; econstructor; eauto.
    * right. exists ("Bad functorial module type at " ++ l')%string.
      intros Δ''' Σ''' Henv Hwf Hlocs1 Hlocs2 H''';  inversion H'''; subst; simpl in *.
      apply deterministic_semantics_m_path in X; trivial.
      apply X in H''. destruct H'' as [Heq H'']; inversion H''; subst.
  + right; exists ("Bad functorial module type at " ++ l')%string.
    intros Δ''' Σ''' Henv Hwf Hlocs1 Hlocs2 H''';  inversion H'''; subst; simpl in *.
    apply deterministic_semantics_m_path in X; trivial.
    apply X in H''. destruct H'' as [_ H'']; inversion H''; subst. clear H.
    destruct X as (Hdet1&_&Hwb11&Hlocs').
    apply deterministic_semantics_m in X0; trivial; auto with *; try tauto.
    * apply X0 in H'. destruct H' as [Heq H']; inversion H'; subst.
    * apply wb_p_sem_env; tauto.
    * apply p_sem_env_disj; auto with *.
  + right; exists err; intros Δ' Σ' Henv Hwf Hlocs1' Hlocs2 H'; inversion H'; subst.
    assert(Hdet := H'').
    apply deterministic_semantics_m_path in Hdet; trivial.
    destruct Hdet as (Hdet1&_&Hwb11&Hlocs').
    apply Hdet1 in X; destruct X; simpl in *; subst. apply HF in X0; trivial.
    * apply wb_p_sem_env; tauto.
    * tauto.
    * apply p_sem_env_disj; auto with *.
- intros [v l] e ls Hind Σ Γ. simpl in *.
  edestruct (derive_v Σ Γ e) as [ [Σ'' H''] | [err HF] ]; eauto; auto with *;
  [|right; exists err; intros Δ'' Σ''' Henv Hwf Hlocs1 Hlocs2 H'''; inversion H''';
    subst; eapply HF; eauto; auto with *; try tauto; disjtac].
  destruct (Hind (Σ'' [[l ↦ IV v]]) (Γ [[v ↦v Some l]])) as [ [ [lv' ls' | l' D1 D2] [Σ' H'] ] | [err HF] ].
  + left; eexists; eexists; simpl in *; econstructor; eauto.
  + right. exists ("Bad functorial module at " ++ l')%string.
    intros Δ''' Σ''' Henv Hwf Hlocs1 Hlocs2 H''';  inversion H'''; subst; simpl in *.
    assert(Hdet := H'').
    apply deterministic_semantics_v in Hdet; auto with *; [|tauto|disjtac].
    destruct Hdet as (Hdet&Henv1&Hlocs'&H1'&Hcod'&Hincl'&Hpr').
    apply Hdet in X. destruct X; subst.
    apply deterministic_semantics_m in X0; trivial; auto with *; try tauto.
    * eapply X0 in H'. destruct H' as [Heq H']; inversion H'; subst.
    * apply wb_env_up_v;trivial;(rewrite Hlocs'||rewrite H1';[apply Hlocs2;auto|tauto]).
    * apply env_up_v_disj; auto with *.
    intro HF; apply Hwf in HF; auto with *.
    * unfold disj in Hwf; simpl in Hwf; apply sem_up_disj;
      [intuition; eauto with *|]; split;intros l0 Hl0; rewrite Hlocs'|| rewrite H1';
      try apply Hlocs2; auto with *; intro; apply Hwf in Hl0; auto with *.
  + right; exists err; intros Δ' Σ' Henv Hwf Hlocs1' Hlocs2 H'; inversion H'; subst.
    assert(Hdet := H'').
    apply deterministic_semantics_v in Hdet; auto with *; [|tauto|disjtac].
    destruct Hdet as (Hdet&Henv1&Hlocs'&H1'&Hcod'&Hincl'&Hpr').
    apply Hdet in X; destruct X; subst. apply HF in X0.
    * tauto.
    * apply wb_env_up_v;trivial;(rewrite Hlocs'||rewrite H1';[apply Hlocs2;auto|tauto]).
    * tauto.
    * apply env_up_v_disj; auto with *. intro HF'; apply Hwf in HF'; auto with *.
    * unfold disj in Hwf; simpl in Hwf; apply sem_up_disj;
      [intuition; eauto with *|]; split;intros l0 Hl0; rewrite Hlocs'|| rewrite H1';
      try apply Hlocs2; auto with *; intro; apply Hwf in Hl0; auto with *.
- intros e ls Hind Σ Γ. simpl in *.
  edestruct (derive_v Σ Γ e) as [ [Σ'' H''] | [err HF] ]; eauto; auto with *;
  [|right; exists err; intros Δ'' Σ''' Henv Hwf Hlocs1 Hlocs2 H'''; inversion H''';
    subst; eapply HF; eauto; auto with *; try tauto; disjtac].
  destruct (Hind Σ'' Γ) as [ [ [lv' ls' | l' D1 D2] [Σ' H'] ] | [err HF] ].
  + left; eexists; eexists; simpl in *; econstructor; eauto.
  + right. exists ("Bad functorial module at " ++ l')%string.
    intros Δ''' Σ''' Henv Hwf Hlocs1 Hlocs2 H''';  inversion H'''; subst; simpl in *.
    assert(Hdet := H'').
    apply deterministic_semantics_v in Hdet; auto with *; [|tauto|disjtac].
    destruct Hdet as (Hdet&Henv1&Hlocs'&H1'&Hcod'&Hincl'&Hpr').
    apply Hdet in X. destruct X; subst.
    apply deterministic_semantics_m in X0; trivial; auto with *; try tauto.
    * eapply X0 in H'. destruct H' as [Heq H']; inversion H'; subst.
    * unfold disj in Hwf; simpl in Hwf; split;intros l0 Hl0; rewrite Hlocs'|| rewrite H1';
      try apply Hlocs2; auto with *; intro; apply Hwf in Hl0; auto with *.
  + right; exists err; intros Δ' Σ' Henv Hwf Hlocs1' Hlocs2 H'; inversion H'; subst.
    assert(Hdet := H'').
    apply deterministic_semantics_v in Hdet; auto with *; [|tauto|disjtac].
    destruct Hdet as (Hdet&Henv1&Hlocs'&H1'&Hcod'&Hincl'&Hpr').
    apply Hdet in X; destruct X; subst. apply HF in X0; try tauto.
    * intros; apply Hlocs1'. apply in_app_iff; now right.
    * unfold disj in Hwf; simpl in Hwf; split;intros l0 Hl0; rewrite Hlocs'|| rewrite H1';
      try apply Hlocs2; auto with *; intro; apply Hwf in Hl0; auto with *.
- intros [x l] e ls Hind1 Hind2 Σ Γ. simpl in *.
  destruct (Hind2 Σ Γ) as [ (Δ''&Σ''&H'') | [err HF] ];
  [|right; exists err; intros Δ'' Σ''' Henv Hwf Hlocs1 Hlocs2 H'''; inversion H''';
    subst; eapply HF; eauto; auto with *; try tauto; disjtac].
  destruct (Hind1 (Σ'' [[l ↦ IM x]]) (Γ [[x ↦m Δ'']])) as [ [ [lv' ls' | l' D1 D2] [Σ' H'] ] | [err HF] ].
  + left; eexists; eexists; simpl in *; econstructor; eauto.
  + right. exists ("Bad functorial module at " ++ l')%string.
    intros Δ''' Σ''' Henv Hwf Hlocs1 Hlocs2 H''';  inversion H'''; subst; simpl in *.
    assert(Hdet := H'').
    apply deterministic_semantics_m in Hdet; auto with *; [|tauto|disjtac].
    destruct Hdet as (Hdet&Henv1&Hwb1&Hlocs'&H1'&Hcod'&Hincl'&Hpr').
    apply Hdet in X; destruct X as [Heq H7]; inversion H7; subst.
    apply deterministic_semantics_m in X0; trivial; auto with *; try tauto.
    * apply X0 in H'. destruct H' as [Heq H']; inversion H'; subst.
    * eapply wb_env_incl; [| apply sem_incl_up_ρ]; [apply wb_env_up_m|];
      auto; try tauto; rewrite Hlocs';[apply Hlocs2;auto|tauto].
    * apply env_up_m_disj; auto with *.
    * apply sem_up_disj; auto with *;[intuition; eauto with *|];
      split;intros l0 Hl0; rewrite Hlocs'|| rewrite H1'; try apply Hlocs2; auto with *;
      intro; apply Hwf in Hl0; auto with *.
  + right; exists err; intros Δ' Σ' Henv Hwf Hlocs1' Hlocs2 H'; inversion H'; subst.
    assert(Hdet := H'').
    apply deterministic_semantics_m in Hdet; auto with *; [|tauto|disjtac].
    destruct Hdet as (Hdet&Henv1&Hwb1&Hlocs'&H1'&Hcod'&Hincl'&Hpr').
    apply Hdet in X; destruct X as [Heq H7]; inversion H7; subst.
    apply HF in X0; auto with *; try tauto.
    * eapply wb_env_incl; [| apply sem_incl_up_ρ]; [apply wb_env_up_m|];
      auto; try tauto; rewrite Hlocs';[apply Hlocs2;auto|tauto].
    * apply env_up_m_disj; auto with *.
    * apply sem_up_disj; auto with *;[intuition; eauto with *|];
      split;intros l0 Hl0; rewrite Hlocs'|| rewrite H1'; try apply Hlocs2; auto with *;
      intro; apply Hwf in Hl0; auto with *.
- intros [t l] t0 ls Hind1 Hind2 Σ Γ. simpl in *.
  destruct (Hind2 Σ Γ) as [ (Δ''&Σ''&H'') | [err HF] ];
  [|right; exists err; intros Δ'' Σ''' Henv Hwf Hlocs1 Hlocs2 H'''; inversion H''';
    subst; eapply HF; eauto; auto with *; try tauto; disjtac]. clear Hind2.
  destruct (Hind1 (Σ'' [[l ↦ IT t]]) (Γ [[t ↦t Δ'']])) as
    [ [ [lv' ls' | l' D1 D2] [Σ' H'] ] | [err HF] ].
  + left; eexists; eexists; simpl in *; econstructor; eauto.
  + right. exists ("Bad functorial module at " ++ l')%string.
    intros Δ''' Σ''' Henv Hwf Hlocs1 Hlocs2 H''';  inversion H'''; subst; simpl in *.
    assert(Hdet := H'').
    apply deterministic_semantics_m in Hdet; auto with *; [|tauto|disjtac].
    destruct Hdet as (Hdet&Henv1&Hwb11&Hlocs'&H1'&Hincl'&Hpr').
    apply Hdet in X; destruct X as [Heq H7]; inversion H7; subst.
    apply deterministic_semantics_m in X0; trivial; auto with *; try tauto.
    * apply X0 in H'. destruct H' as [Heq H']; inversion H'; subst.
    * eapply wb_env_incl; [| apply sem_incl_up_ρ]; [apply wb_env_up_t|];
      auto; try tauto; rewrite Hlocs';[apply Hlocs2;auto|tauto].
    * apply env_up_t_disj; auto with *.
    * apply sem_up_disj; auto with *;[intuition; eauto with *|];
      split;intros l0 Hl0; rewrite Hlocs'|| rewrite H1'; try apply Hlocs2; auto with *;
      intro; apply Hwf in Hl0; auto with *.
  + right; exists err; intros Δ' Σ' Henv Hwf Hlocs1' Hlocs2 H'; inversion H'; subst.
    assert(Hdet := H'').
    apply deterministic_semantics_m in Hdet; auto with *; [|tauto|disjtac].
    destruct Hdet as (Hdet&Henv1&Hwb11&Hlocs'&H1'&Hincl'&Hpr').
    apply deterministic_semantics_m in H''; auto with *; [|tauto|disjtac].
    eapply HF; auto.
    * eapply wb_env_incl; [| apply sem_incl_up_ρ]; [apply wb_env_up_t|];
      auto; try tauto; rewrite Hlocs';[apply Hlocs2;auto|tauto].
    * tauto.
    * apply env_up_t_disj; auto with *.
    * apply sem_up_disj; auto with *;[intuition; eauto with *|];
      split;intros l0 Hl0; rewrite Hlocs'|| rewrite H1'; try apply Hlocs2; auto with *;
      intro; apply Hwf in Hl0; auto with *.
    * apply H'' in X. destruct X; subst. eauto.
- intros e ls Hind1 Hind2 Σ Γ. simpl in *.
  edestruct Hind1 as [ (Δ''&Σ''&H'') | [err HF] ];
  [|right; exists err; intros Δ'' Σ''' Henv Hwf Hlocs1 Hlocs2 H'''; inversion H''';
    subst; eapply HF; eauto; auto with *; try tauto; disjtac].
  destruct (Hind2 Σ'' (p_sem_env (ρ Σ'') Δ'' Γ)) as [ [ [lv' ls' | l' D1 D2] [Σ' H'] ] | [err HF] ].
  + destruct Δ'' as [lv'' ls'' | l' D1 D2].
    * left; eexists; eexists; simpl in *; econstructor; eauto.
    * right. exists ("Bad functorial module at " ++ l')%string.
      intros Δ''' Σ''' Henv Hwf Hlocs1 Hlocs2 H''';  inversion H'''; subst; simpl in *.
       assert(Hdet := H'').
       apply deterministic_semantics_m in Hdet; auto with *; [|tauto|disjtac].
       destruct Hdet as (Hdet&Henv1&Hwb11&Hlocs'&H1'&Hcod'&Hincl'&Hpr').
       apply deterministic_semantics_m in X; trivial; auto with *; try tauto;[|disjtac].
       apply X in H''. destruct H'' as [Heq H'']; inversion H''; subst.
  + right. exists ("Bad functorial module at " ++ l')%string.
    intros Δ''' Σ''' Henv Hwf Hlocs1 Hlocs2 H''';  inversion H'''; subst; simpl in *.
    assert(Hdet := H'').
    apply deterministic_semantics_m in Hdet; auto with *; [|tauto|disjtac].
    destruct Hdet as (Hdet&Henv1&Hwb11&Hlocs'&H1'&Hcod'&Hincl'&Hpr').
    apply deterministic_semantics_m in X; trivial; auto with *; try tauto;[|disjtac].
    apply X in H''. destruct H'' as [Heq H'']; inversion H''; subst.
    apply deterministic_semantics_m in X0; trivial; auto with *; try tauto.
    * apply X0 in H'. destruct H' as [Heq H']; inversion H'; subst.
    * apply wb_p_sem_env; tauto.
    * subst Γ'. apply p_sem_env_disj; auto with *;
      split;intros l0 Hl0; rewrite Hlocs'|| rewrite H1'; try apply Hlocs2; auto with *;
      intro; apply Hwf in Hl0; auto with *.
    * split;intros l0 Hl0; rewrite Hlocs'|| rewrite H1'; try apply Hlocs2; auto with *;
      intro; apply Hwf in Hl0; auto with *.
  + right; exists err; intros Δ' Σ' Henv Hwf Hlocs1' Hlocs2 H'; inversion H'; subst.
    assert(Hdet := H'').
    apply deterministic_semantics_m in Hdet; auto with *; [|tauto|disjtac].
    destruct Hdet as (Hdet&Henv1&Hwb11&Hlocs'&H1'&Hcod'&Hincl'&Hpr').
    apply Hdet in X; destruct X; simpl in *; subst.
    apply HF in X0; auto with *; try tauto.
    * subst Γ'. apply p_sem_env_disj; auto with *;
      split;intros l0 Hl0; rewrite Hlocs'|| rewrite H1'; try apply Hlocs2; auto with *;
      intro; apply Hwf in Hl0; auto with *.
    * split;intros l0 Hl0; rewrite Hlocs'|| rewrite H1'; try apply Hlocs2; auto with *;
      intro; apply Hwf in Hl0; auto with *.
- intros p Σ Γ. simpl in *.
  destruct (derive_m_path Σ Γ p) as [ [D HD] | [err Hf] ].
  + left. eexists; eexists. econstructor; simpl. eauto.
  + right. exists (err ++ "in module path " ++ pp_m_path p)%string;
    intros Δ' Σ' Henv Hwf Hlocs1' Hlocs2 H'; inversion H'; subst.
    now apply Hf in X.
- intros [x l] t e Hind1 Hind2 Σ Γ. simpl in *.
  edestruct (Hind1 Σ Γ) as [ (Δ''&Σ''&H'') | [err HF] ];
  [|right; exists err; intros Δ'' Σ''' Henv Hwf Hlocs1 Hlocs2 H'''; inversion H''';
    subst; eapply HF; eauto; auto with *; try tauto; disjtac]. clear Hind1.
  destruct (Hind2 (Σ'' [[l ↦ IM x]]) (Γ [[x ↦m Δ'']])) as [ (Δ'&Σ'&H') | [err HF] ].
  + left; eexists; eexists; simpl in *; econstructor; eauto.
  + right; exists err; intros Δ''' Σ''' Henv Hwf Hlocs1 Hlocs2 H''';  inversion H'''; subst; simpl in *.
    assert(Hdet := H'').
    apply deterministic_semantics_m in Hdet; auto with *; [|tauto|disjtac].
    destruct Hdet as (Hdet&Henv1&Hwb11&Hlocs'&H1'&Hincl'&Hpr').
    apply Hdet in X; destruct X as [Heq H7]; subst.
    apply HF in X0; auto with *; try tauto.
    * eapply wb_env_incl; [| apply sem_incl_up_ρ]; [apply wb_env_up_m|]; auto;
      try tauto; rewrite Hlocs';[apply Hlocs2;auto|tauto].
    * apply env_up_m_disj; auto with *.
    * apply sem_up_disj; auto with *;[intuition; eauto with *|];
      split;intros l0 Hl0; rewrite Hlocs'|| rewrite H1'; try apply Hlocs2; auto with *;
      intro; apply Hwf in Hl0; auto with *.
- intros m1 Hind1 m2 Hind2 Σ Γ. simpl.
  destruct (Hind1 Σ Γ) as [ (Δ''&Σ''&H'') | [err HF] ];
  [|right; exists err; intros Δ'' Σ''' Henv Hwf Hlocs1 Hlocs2 H'''; inversion H''';
    subst; eapply HF; eauto; auto with *; try tauto; disjtac].
  destruct (Hind2 Σ'' Γ) as [ [ Δ' [ Σ' H'] ] | [err HF] ].
  + destruct Δ'' as [lv' ls' | l' D1 D2].
    * right. exists ("Bad structural module in unknown module application")%string.
      intros Δ''' Σ''' Henv Hwf Hlocs1 Hlocs2 H''';  inversion H'''; subst; simpl in *.
      assert(Hdet := H'').
      apply deterministic_semantics_m in Hdet; auto with *; [|tauto|disjtac].
      destruct Hdet as (Hdet&Henv'&Hwb'&Hlocs'&Hr'&Hcod'&Hincl'&Hpr').
      apply deterministic_semantics_m in X; trivial; auto with *; try tauto;[|disjtac].
      apply X in H''. destruct H'' as [Heq H'']; inversion H''; subst.
    * left; eexists; eexists; simpl in *; econstructor; eauto.
  + right; exists err; intros Δ''' Σ''' Henv Hwf Hlocs1 Hlocs2 H''';  inversion H'''; subst; simpl in *.
    assert(Hdet := H'').
    apply deterministic_semantics_m in Hdet; auto with *; [|tauto|disjtac].
    destruct Hdet as (Hdet&Henv'&Hwb'&Hlocs'&Hr'&Hcod'&Hincl'&Hpr').
    apply Hdet in X; destruct X as [Heq X]; subst.
    apply HF in X0; auto with *; try tauto.
    simpl. split;intros l0 Hl0; rewrite Hlocs'|| rewrite Hr'; try apply Hlocs2; auto with *;
    intro; apply Hwf in Hl0; auto with *.
- intros e t Hind1 Hind2 Σ Γ. simpl in *.
  destruct (Hind1 Σ Γ)as [ (Δ''&Σ''&H'') | [err HF] ];
  [|right; exists err; intros Δ'' Σ''' Henv Hwf Hlocs1 Hlocs2 H'''; inversion H''';
    subst; eapply HF; eauto; auto with *; try tauto; disjtac].
  destruct (Hind2 Σ'' Γ) as [ (Δ'&Σ'&H') | [err HF] ].
  + left; eexists; eexists; simpl in *; econstructor; eauto.
  + right; exists err; intros Δ''' Σ''' Henv Hwf Hlocs1 Hlocs2 H''';  inversion H'''; subst; simpl in *.
    assert(Hdet := H'').
    apply deterministic_semantics_m in Hdet; auto with *; [|tauto|disjtac].
    destruct Hdet as (Hdet&Henv'&Hwb'&Hlocs'&Hr'&Hcod'&Hincl'&Hpr').
    apply Hdet in X; destruct X as [Heq H3]; subst.
    apply HF in X0; auto with *; try tauto.
    split;intros l0 Hl0; rewrite Hlocs'|| rewrite Hr'; try apply Hlocs2; auto with *;
    intro; apply Hwf in Hl0; auto with *.
- intros ls Hind Σ Γ. simpl in *.
  destruct (Hind Σ Γ)as [ (Δ''&Σ''&H'') | [err HF] ].
  + eauto.
  + right; exists err; intros Δ''' Σ''' Henv Hwf Hlocs1 Hlocs2 H''';  inversion H'''; subst; simpl in *.
    apply HF in X; auto with *.
- intros Σ Γ. simpl in *. left; eexists; eexists; simpl in *; econstructor; eauto.
- intros ls p Hind Σ Γ. simpl in *.
  destruct (derive_m_path Σ Γ p) as [ [ Δ'' H''] | [err HF] ];
  [|right; exists (err ++ " in module path " ++ pp_m_path p)%string;
    intros Δ'' Σ''' Henv Hwf Hlocs1 Hlocs2 H''; inversion H'';
    subst; eapply HF; eauto; auto with *; try tauto; disjtac].
  destruct (Hind Σ (p_sem_env (ρ Σ) Δ'' Γ)) as [ [ [lv' ls' | l' D1 D2] [Σ' H'] ] | [err HF] ]; auto with *; clear Hind.
  + destruct Δ'' as [lv'' ls'' | l' D1 D2].
    * left; eexists; eexists; simpl in *; econstructor; eauto.
    * right.  exists ("Bad functorial module at " ++ l')%string.
      intros Δ''' Σ''' Henv Hwf Hlocs1 Hlocs2 H''';  inversion H'''; subst; simpl in *.
      apply deterministic_semantics_m_path in X; trivial.
      apply X in H''. destruct H'' as [Heq H'']; inversion H''; subst.
  + right. exists ("Bad functorial module at " ++ l')%string.
    intros Δ''' Σ''' Henv Hwf Hlocs1 Hlocs2 H''';  inversion H'''; subst; simpl in *.
      apply deterministic_semantics_m_path in X; trivial.
      apply X in H''. destruct H'' as [_ H'']; inversion H''; subst. clear H.
      destruct X as (Hdet1&_&Hwb11&Hlocs').
      apply deterministic_semantics_m in X0; trivial; auto with *; try tauto.
      -- apply X0 in H'. destruct H' as [Heq H']; inversion H'; subst.
      -- apply wb_p_sem_env; tauto.
      -- apply p_sem_env_disj; auto with *.
  + right; exists err; intros Δ' Σ' Henv Hwf Hlocs1' Hlocs2 H'; inversion H'; subst.
    assert(Hdet := H'').
    apply deterministic_semantics_m_path in Hdet; trivial.
    destruct Hdet as (Hdet1&_&Hwb11&Hlocs').
    apply Hdet1 in X; destruct X; simpl in *; subst. apply HF in X0; trivial.
    * apply wb_p_sem_env; tauto.
    * tauto.
    * apply p_sem_env_disj; auto with *.
Defined.

(* end hide *)

Definition derive (P : prog) : forall (Σ: sem) (Γ: env),
  {Σ': sem & Σ / Γ ⊢ P ⇝ Σ'} +
  {err : string | forall Σ', wb_env Γ Σ -> wf P -> (Γ ⊓ locs P) -> (Σ ⊔ locs P) ->
     Σ / Γ ⊢ P ⇝ Σ' -> False}.
Proof.
induction P as [v | [x l] e P].
- intros Σ Γ. simpl in *.
  edestruct derive_v as [ [ Σ'' H''] | [err HF] ].
  + eauto.
  + right; exists err; intros Σ' Henv Hwf Hlocs1 Hlocs2 H';  inversion H'; subst; simpl in *.
    apply HF in X; auto with *.
- intros Σ Γ. simpl in *.
  destruct (fst (fst (fst derive_m)) e Σ Γ) as [ (Δ''&Σ''&H'') | [err HF] ];
  [|right; exists ("In toplevel module, " ++ print_loc l ++ ", " ++ err)%string;
    intros Σ''' Henv Hwf Hlocs1 Hlocs2 H'''; inversion H''';
    subst; eapply HF; eauto; auto with *; try tauto; disjtac].
  destruct (IHP (Σ'' [[l ↦ IM x]]) (Γ [[x ↦m Δ'']])) as [ (Σ'&H') | [err HF] ].
  + left; eexists; simpl in *; econstructor; eauto.
  + right; exists err;
    intros Σ''' Henv Hwf Hlocs1 Hlocs2 H''';  inversion H'''; subst; simpl in *.
    assert(Hdet := H''). simpl in *.
    apply deterministic_semantics_m in Hdet; auto with *; [|tauto|disjtac].
    destruct Hdet as (Hdet&Henv'&Hwb'&Hlocs'&Hr'&Hcod'&Hincl'&Hpr').
    apply Hdet in X; destruct X as [Heq X]; subst.
    apply HF in X0; auto with *; try tauto.
    * eapply wb_env_incl; [| apply sem_incl_up_ρ]; [apply wb_env_up_m|]; auto;
      intros; rewrite Hlocs';[apply Hlocs2;auto with *|tauto].
    * apply env_up_m_disj; auto with *.
    * simpl; split;intros; rewrite sr_up, Hlocs'||rewrite br_up, Hr';
      try (intro HF'; apply Hwf in HF'; tauto);
      try (case_loc_eq_dec; intros; subst;[tauto|]); apply Hlocs2; auto with *.
Defined.

(** The locations which are related to lx in Σf *)
Definition L Σf lx l := Ê Σf lx l \/ (exists l', Ê Σf lx l' /\ ↣ Σf l = Some (Some l')).

(** This set is decidable *)
Definition L_dec Σf lx l: {L Σf lx l} + {~ L Σf lx l}.
Proof.
unfold L.
destruct(ek_dec Σf lx l).
- left; tauto.
- destruct (↣ Σf l) as [ [l'|]|].
  + destruct(ek_dec Σf lx l'); [left; eauto|].
    right. intro H; destruct H as [H|(l'' & Hl'' & Heq)]; [tauto|].
    inversion Heq; subst. tauto.
  + right. intro H; destruct H as [H|(l' & _ & Hl')]; [tauto|discriminate Hl'].
  + right. intro H; destruct H as [H|(l' & _ & Hl')]; [tauto|discriminate Hl'].
Defined.

End Semantics.

Module Type SemanticsType (L : LanguageSig).
  Include Semantics L.
End SemanticsType.

(** * Semantic Equivalence **)

Module Equivalence (Import L : LanguageSig) (Import ST : SemanticsType L).

(** Semantic equivalence (_Definition 16_) *)
Definition sem_eq Σ Σ' : Prop :=
  LocMaps.Equal (br Σ) (br Σ') /\ E Σ = E Σ' /\
  (forall l, match ρ Σ l, ρ Σ' l with
             | Some (IV v), Some (IV v') => True
             | Some (IM m), Some (IM m') => m = m'
             | Some (IT t), Some (IT t') => t = t'
             | None, None => True
             | _, _ => False end).
Infix "≡" := sem_eq (at level 40).

(* begin hide *)
Lemma sem_eq_sr' Σ Σ': Σ ≡ Σ' -> forall l,
  (ρ Σ l = None -> ρ Σ' l = None) /\
  (forall m, ρ Σ l = Some (IM m) -> ρ Σ' l = Some (IM m)) /\
  (forall t, ρ Σ l = Some (IT t) -> ρ Σ' l = Some (IT t)) /\
  (forall v, ρ Σ l = Some (IV v) -> exists v', ρ Σ' l = Some (IV v')).
Proof.
intros Heq l. destruct Heq as (_&_&Heq). assert (Hl:= Heq l).
revert Hl; clear Heq.
destruct (ρ Σ l) as [ [m|t|v]|]; intuition; try discriminate;
destruct (ρ Σ' l) as [ [m'|t'|v']|]; intuition; subst; try discriminate; trivial.
eauto.
Qed.

Lemma sem_eq1 v0 Σ Σ':
  let abstract_V v i := match i with | IV v' => IV v | _ => i end in
  sem_eq Σ Σ' <->
  LocMaps.Equal (br Σ) (br Σ') /\ E Σ = E Σ' /\
  LocMaps.Equal (LocMaps.map (abstract_V v0) (sr Σ)) (LocMaps.map (abstract_V v0) (sr Σ')).
Proof.
intro abstract_V. split.
- intros (H1&H2&H3). repeat split; trivial.
  apply LocMapsFacts.Equal_mapsto_iff. intros k e.
  do 2 rewrite LocMapsFacts.find_mapsto_iff, LocMapsFacts.map_o.
  assert (Hk := H3 k). revert Hk. unfold sr'.
  destruct (LocMaps.find k (sr Σ)) as [ [m | t | v] |];
  destruct (LocMaps.find k (sr Σ')) as [ [m' | t' | v'] |]; intro; subst; tauto.
- intros (H1&H2&H3). repeat split; trivial.
  intro l. rewrite LocMapsFacts.Equal_mapsto_iff in H3.
  unfold sr'.
  case_eq (LocMaps.find l (sr Σ)).
  + intros e He. assert(H3e:= H3 l (abstract_V v0 e)).
    do 2 rewrite LocMapsFacts.find_mapsto_iff, LocMapsFacts.map_o in H3e.
    rewrite He in H3e. destruct H3e as [H3e _]. simpl in H3e.
    assert(Heq: Some (abstract_V v0 e) = Some (abstract_V v0 e)) by trivial.
    apply H3e in Heq. revert Heq.
    destruct e as [ m | t | v]; destruct (LocMaps.find l (sr Σ')) as [ [m' | t' | v'] | ]; simpl;
    intro Heq; inversion Heq; subst; trivial.
  + intros Hnone. case_eq (LocMaps.find l (sr Σ')); [intros e' He'| trivial].
    assert(H3e:= H3 l (abstract_V v0 e')).
    do 2 rewrite LocMapsFacts.find_mapsto_iff, LocMapsFacts.map_o in H3e.
    rewrite He' in H3e. destruct H3e as [_ H3e]. simpl in H3e.
    rewrite Hnone in H3e. discriminate H3e; trivial.
Qed.
(* end hide *)

(** Semantic equivalence is decidable **)
Definition sem_eq_dec (v0 : V) Σ1 Σ2: {sem_eq Σ1 Σ2} + {~ sem_eq Σ1 Σ2}.
Proof.
case_eq (LocMaps.equal (fun e e' => if (option_eq_dec loc_eq_dec) e e' then true else false) (br Σ1) (br Σ2)).
- intro Heq1. apply LocMaps.equal_2, LocMapsFacts.Equal_Equivb in Heq1;
  [|intros; case option_eq_dec; intro; subst; split; try tauto; discriminate].
  assert(Hdec: forall x y : ℒ ∧ ℒ, {x = y} + {x <> y}).
  {
    intros (x1&x2) (y1&y2). destruct (loc_eq_dec x1 y1); destruct (loc_eq_dec x2 y2);
    subst; eauto; right; intro Heq; inversion Heq; subst; tauto.
  }
  edestruct (list_eq_dec Hdec (E Σ1) (E Σ2)) as [HeqE|]; [|right; unfold sem_eq; tauto].
  pose (f := fun i => match i with | IV v' => IV v0 | _ => i end).
  case_eq (LocMaps.equal (fun a b => if var_eq_dec a b then true else false)
          (LocMaps.map f (sr Σ1)) (LocMaps.map f (sr Σ2))).
  + intro Heq2. apply LocMaps.equal_2, LocMapsFacts.Equal_Equivb in Heq2;
    [|intros; case var_eq_dec; intro; subst; split; try tauto; discriminate].
    left. rewrite sem_eq1. subst f. repeat split; eauto.
  + intro H; right. rewrite sem_eq1. intros (_&_&Heq). contradict H. subst f.
    apply Bool.not_false_iff_true.
    apply LocMaps.equal_1, LocMapsFacts.Equal_Equivb; eauto.
    intros; case var_eq_dec; intro; subst; split; try tauto; discriminate.
- intro H1. right. intros (H1'&_&_). contradict H1. apply Bool.not_false_iff_true.
    apply LocMaps.equal_1, LocMapsFacts.Equal_Equivb; eauto.
    intros; case option_eq_dec; intro; subst; split; try tauto; discriminate.
Defined.

(* begin hide *)
Lemma sem_eq_refl Σ : Σ ≡ Σ.
Proof. repeat split. intro l; destruct (ρ Σ l) as [ [v | m | t] |]; trivial. Qed.
Global Hint Resolve sem_eq_refl : core.

Lemma sem_eq_sym Σ Σ': Σ ≡ Σ' -> Σ' ≡ Σ.
Proof.
intros (H1&H2&H3).
split; auto using LocMapsFacts.Equal_sym.
split; auto.
intro l. assert(Hl:= H3 l).
destruct (ρ Σ l) as [ [v | m | t] |];
destruct (ρ Σ' l) as [ [v' | m' | t'] |]; auto.
Qed.

Lemma sem_eq_trans Σ Σ' Σ'': Σ ≡ Σ' -> Σ' ≡ Σ'' -> Σ ≡ Σ''.
Proof.
intros (H1&H2&H3). intros (H1'&H2'&H3').
split;[etransitivity; eauto|]. split;[etransitivity; eauto|].
intro l. assert(Hl:= H3 l). assert(Hl':= H3' l).
destruct (ρ Σ l) as [ [v | m | t] |];
destruct (ρ Σ' l) as [ [v' | m' | t'] |];
destruct (ρ Σ'' l) as [ [v'' | m'' | t''] |];
subst; auto; tauto.
Qed.
Global Hint Resolve sem_eq_trans : core.
(* end hide *)

(** Semantic equivalence is an equivalence relation *)
Add Parametric Relation: sem sem_eq
  reflexivity proved by sem_eq_refl
  symmetry proved by sem_eq_sym
  transitivity proved by sem_eq_trans as sem_eq_rel.

(* begin hide *)
Lemma sem_equal_eq Σ Σ': sem_equal Σ Σ' -> Σ ≡ Σ'.
Proof.
intros (H1&H2&H3). split; [|split]; trivial.
intros l. replace (ρ Σ' l) with (ρ Σ l) by apply H1.
destruct (ρ Σ l) as [ [v | m | t] |]; trivial.
Qed.
Global Hint Immediate sem_equal_eq : core.
(* end hide *)

(** Only the empty semantics is equivalent to itself, up to equality *)
Lemma Σ₀_eq Σ : Σ ≡ Σ₀ -> sem_equal Σ Σ₀.
Proof.
intros (H1&H2&H3). unfold Σ₀, sem_equal. destruct Σ as [r b e]. f_equal; trivial.
unfold sr' in H3. simpl in *; intuition; subst.
apply LocMapsFacts.Equal_mapsto_iff.
intros l j. assert (Hl := H3 l); clear H3; split; intro H.
- apply LocMapsFacts.find_mapsto_iff in H.
  rewrite H in Hl.
  destruct j as [v | m | t]; rewrite LocMapsFacts.empty_o in Hl; tauto.
- apply LocMapsFacts.empty_mapsto_iff in H; tauto.
Qed.

(** Equivalence of environments (_Definition 16_)*)
Definition env_eq Γ Γ': Prop :=
  envM Γ = envM Γ' /\ envT Γ = envT Γ' /\
  (forall l, (exists v, envV Γ v = Some l) <-> (exists v, envV Γ' v = Some l)).
Infix "≖" := env_eq (at level 40).

(** Only the empty environment is equivalent to itself *)
Lemma Γ₀_eq Γ : Γ ≖ Γ₀ -> Γ = Γ₀.
Proof.
intros (H1&H2&H3). unfold Γ₀. destruct Γ as [ [ Γv Γm] Γt ]. repeat f_equal; trivial.
apply FunctionalExtensionality.functional_extensionality. intro.
simpl in *. case_eq (Γv x); trivial.
intros l Hl. destruct (H3 l) as [ [_ HH] _]; eauto.
Qed.
Global Hint Resolve Γ₀_eq : core.

(* begin hide *)
Lemma Σ₀_sr l : ρ Σ₀ l = None.
Proof. unfold Σ₀, sr'. apply LocMapsFacts.empty_o. Qed.
Global Hint Immediate Σ₀_sr : core.

Lemma Σ₀_br l : ↣ Σ₀ l = None.
Proof. unfold Σ₀, sr'. apply LocMapsFacts.empty_o. Qed.
Global Hint Immediate Σ₀_sr : core.

Lemma cod_Σ₀ l: ~ cod Σ₀ l.
Proof. unfold cod. rewrite Σ₀_sr. intros (v&Hv1&_). discriminate. Qed.

Lemma br_cod Σ l l': proper Σ -> ↣ Σ l = Some (Some l') -> cod Σ l'.
Proof.
intros (HP1&HP2&_) Hbr.
destruct (HP2 l') as (v&Hv);[eauto|].
exists v; split; trivial. eapply HP1; eauto.
Qed.
Global Hint Resolve br_cod : core.

Lemma Σ₀_sem_incl s : Σ₀ ⊆ s.
Proof.
nsplit 2; intuition.
- rewrite Σ₀_sr in *; discriminate.
- rewrite Σ₀_sr in *; discriminate.
- rewrite Σ₀_br in *; discriminate.
- simpl in H. tauto.
Qed.
Global Hint Immediate Σ₀_sem_incl : core.

Lemma sem_eq_br s1 s2 l: s1 ≡ s2 -> ↣ s1 l = ↣ s2 l.
Proof. intros (Heq&_). unfold br'. auto. Qed.

Lemma wb_env_Γ₀ r: wb_env Γ₀ r.
Proof. unfold Γ₀. repeat split; simpl in *; try tauto; try discriminate; trivial. Qed.
Global Hint Resolve wb_env_Γ₀ : core.

Lemma Γ₀_disj P: Γ₀ ⊓ locs P.
Proof. unfold Γ₀. intros l Hl v. discriminate. Qed.
Global Hint Resolve Γ₀_disj : core.

Lemma Σ₀_disj P: Σ₀ ⊔locs P.
Proof. unfold Σ₀. simpl. unfold br', sr'. split; intros; repeat rewrite LocMapsFacts.empty_o; tauto. Qed.
Global Hint Resolve Σ₀_disj : core.

Lemma proper_Σ₀: proper Σ₀.
Proof.
repeat split; unfold Σ₀, sr', br'; simpl;
try solve[intros; rewrite LocMapsFacts.empty_o in *; try discriminate; trivial].
- intros l [H| (l'&H)]; rewrite LocMapsFacts.empty_o in H.
  + tauto.
  + inversion H.
- inversion H.
Qed.
Global Hint Resolve proper_Σ₀ : core.

Lemma env_eq_refl Γ : Γ ≖ Γ.
Proof. repeat split; trivial. Qed.
Global Hint Resolve env_eq_refl : core.

Lemma env_eq_sym Γ Γ' : Γ ≖ Γ' -> Γ' ≖ Γ.
Proof.
intros (H1&H2&H3).
repeat split; auto; intros [v H]; destruct (H3 l); eauto.
Qed.

Lemma env_eq_trans Γ Γ' Γ'' : Γ ≖ Γ' -> Γ' ≖ Γ'' -> Γ ≖ Γ''.
Proof.
intros (H1&H2&H3) (H1'&H2'&H3').
nsplit 2.
- now rewrite H1, H1'.
- now rewrite H2, H2'.
- intro; split; intro.
  + now apply H3', H3.
  + now apply H3, H3'.
Qed.
(* end hide *)

(** Environment equivalence is an equivalence relation *)
Add Parametric Relation: env env_eq
  reflexivity proved by env_eq_refl
  symmetry proved by env_eq_sym
  transitivity proved by env_eq_trans as env_eq_rel.

(** Notation for "well-formed" program semantics *)
Definition has_sem P Σ Γ :=
 {Σ' & (Σ / Γ ⊢ P ⇝ Σ') ∧ (wb_env Γ Σ /\ wf P /\ (Γ ⊓ locs P) /\ (Σ ⊔ locs P))}.
Notation "'〚' P '〛' Σ ';' Γ" := (has_sem P Σ Γ) (at level 40).

(* begin hide *)
Definition get_sem {P Σ Γ}: (has_sem P Σ Γ) -> sem :=
fun H => let (s, _) := H in s.

Coercion get_sem: has_sem>->sem.

Lemma sem_unique {P Σ Γ} (s1: 〚P〛Σ;Γ) (s2: 〚P〛Σ;Γ): get_sem s1 = get_sem s2.
Proof.
destruct s1 as (s1&H1&Hc1). destruct s2 as (s2&H2&Hc2). simpl.
intuition. eapply deterministic_semantics; eauto.
Qed.

Lemma sem_proper {P} (s1: 〚P〛Σ₀;Γ₀) : proper s1.
Proof.
destruct s1 as (s1&H1&Hc1). simpl.
intuition. eapply deterministic_semantics; eauto.
Qed.
Global Hint Resolve sem_proper : core.
(* end hide *)

(** Valid Renamings (_Definition 17_) **)
Definition valid_renaming {Σ Γ} P1 P2 :=
  {Σ1: 〚P1〛Σ;Γ & {Σ': sem&{Γ': env&{Σ2: 〚P2〛Σ';Γ' &
    Σ' ≡ Σ /\ Γ' ≖ Γ /\ get_sem Σ1 ≡ get_sem Σ2} } } }.

Definition valid := @valid_renaming Σ₀ Γ₀.

(** Proposition 3 *)
Proposition valid_sem_eq P P':
  ({s1 : 〚P〛Σ₀;Γ₀ & {s2 : 〚P'〛Σ₀;Γ₀ & s1 ≡ s2}} -> valid P P') *
  (valid P P' -> {s1 : 〚P〛Σ₀;Γ₀ & {s2 : 〚P'〛Σ₀;Γ₀ & s1 ≡ s2}}).
Proof.
split.
- intros (s1&s2&Heq). exists s1. exists Σ₀. exists Γ₀. exists s2. auto.
- intros (s1&Σ&Γ&s2&Heq1&Heq2&Heq).
  apply Σ₀_eq in Heq1.
  apply Γ₀_eq in Heq2; subst.
  exists s1. destruct s2 as (s2 & H2&Hc2). simpl in *.
  eapply sem_equal_compat with (Σ0 := Σ₀) in H2; auto. destruct H2 as (s2' & Hs2' & Hseq2').
  eexists (existT _ s2' _). simpl. eauto.
  Unshelve. simpl. nsplit 4; trivial.
  + intuition.
  + discriminate.
Defined.

(** Validity of renamings is an equivalence relation (_Proposition 4_) *)
Proposition valid_refl P (Σ : 〚P〛Σ₀;Γ₀) : valid P P.
Proof. unfold valid, valid_renaming. exists Σ. eexists. eexists. eauto. Defined.

Proposition valid_sym P P': valid P P' -> valid P' P.
Proof.
intro H. apply valid_sem_eq in H. apply valid_sem_eq.
destruct H as (s1&s2&Heq).
exists s2; exists s1. auto with *.
Defined.

Proposition valid_trans P P' P'': valid P P' -> valid P' P'' -> valid P P''.
Proof.
intros H1 H2. apply valid_sem_eq in H1. apply valid_sem_eq in H2. apply valid_sem_eq.
destruct H1 as (s1&s2&Heq).
destruct H2 as (s1'&s2'&Heq').
exists s1. exists s2'.
rewrite (sem_unique s2 s1') in Heq. eauto.
Defined.

End Equivalence.
