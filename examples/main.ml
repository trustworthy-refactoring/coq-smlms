open List
open Examples
open Format
open Datatypes
open Specif

let pp p = Examples.SM.ML.pp p
(* Print the example program *)
let () = print_string "Original program:\n\n";
iter print_char (pp example2);
print_newline();

(* Print the renamed program *)
print_string "Renamed program:\n\n";
iter print_char (pp e2');
print_newline();

(* List the renamed locations *)
print_string "Renamed locations: ";
iter (fun x -> print_int x; print_string " ") (renamed_locs example2);
print_newline(); print_newline();

(* Display the extension kernel *)
print_string "Extension kernel:\n\n";
List.iter (fun x -> Printf.printf "  %d--%d\n" (fst x) (snd x)) e2;
print_newline();

(* Derive the semantics and check their equivalence *)
match (derive_0 example2) with
| Coq_inl (Coq_existT(s1, _)) ->
  (match derive_0 (TestR.rename s1 Examples.vx Examples.lx example2) with
   | Coq_inl (Coq_existT(s2, _)) ->
     if SE.sem_eq_dec [] s1 s2
     then print_string "The semantics are equivalent\n"
     else print_string "The semantics are not equivalent\n"
   | Coq_inr err -> Format.printf "Failed to derive renamed semantics:\n"; (iter print_char err))
| Coq_inr err -> Format.printf "Failed to derive original semantics:\n"; (iter print_char err);;
